﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConstantSkillRange : BaseSkillRange
{
	int horizontal;
	int vertical;
	int currentZ;

	public override List<BoardTile> GetTilesInRange(Board board, Unit unit, int horizontal, int vertical)
	{
		this.horizontal = horizontal;
		this.vertical = vertical;
		currentZ = unit.tile.z;

		return board.Search(unit.tile, ExpandSearch);
	}

	bool ExpandSearch(BoardTile from, BoardTile to)
	{
		return (from.distance + 1) <= horizontal && Mathf.Abs(to.z - currentZ) <= vertical;
	}
}