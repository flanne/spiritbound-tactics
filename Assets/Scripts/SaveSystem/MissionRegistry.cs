﻿using UnityEngine;

[CreateAssetMenu(menuName = "SpiritlinkTactics/Registry/Mission Registry")]
public class MissionRegistry : Registry<MissionData>
{
}