﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Feature : ScriptableObject
{
	#region Public
	public void Activate(Unit target)
	{
		OnApply(target);
	}

	public void Deactivate(Unit target)
	{
		OnRemove(target);
	}

	public void Apply(Unit target)
	{
		OnApply(target);
	}
	#endregion

	#region Private
	protected abstract void OnApply(Unit target);
	protected virtual void OnRemove(Unit target) { }
	#endregion
}
