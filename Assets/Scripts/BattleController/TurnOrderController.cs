﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Spiritlink.Battle
{
    public class TurnOrderController : MonoBehaviour
    {
        #region Notifications
        public const string RoundBeganNotification = "TurnOrderController.roundBegan";
        public const string TurnCheckNotification = "TurnOrderController.turnCheck";
        public const string TurnBeganNotification = "TurnOrderController.TurnBeganNotification";
        public const string TurnCompletedNotification = "TurnOrderController.turnCompleted";
        public const string RoundEndedNotification = "TurnOrderController.roundEnded";
        #endregion

        #region Public
        public IEnumerator Round()
        {
            BattleController bc = GetComponent<BattleController>();

            while (true)
            {
                this.PostNotification(RoundBeganNotification);
                List<Unit> units = new List<Unit>(bc.units);

                for (int i = 0; i < units.Count; ++i)
                    units[i].stats[StatTypes.CTR] -= 1;

                units.Sort((a, b) => GetCounter(a).CompareTo(GetCounter(b)));

                for (int i = 0; i < units.Count; i++)
                {
                    if (CanTakeTurn(units[i]))
                    {
                        units[i].PostNotification(TurnBeganNotification);

                        bc.turn.Change(units[i]);
                        yield return units[i];

                        units[i].PostNotification(TurnCompletedNotification);
                    }
                }

                this.PostNotification(RoundEndedNotification);
            }
        }
        #endregion

        #region Private
        bool CanTakeTurn(Unit target)
        {
            if (target.IsKnockedOut())
                return false;

            BaseException exc = new BaseException(GetCounter(target) <= 0);
            target.PostNotification(TurnCheckNotification, exc);
            return exc.toggle;
        }

        int GetCounter(Unit target)
        {
            return target.stats[StatTypes.CTR];
        }
        #endregion
    }
}