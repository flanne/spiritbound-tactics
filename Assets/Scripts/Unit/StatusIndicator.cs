﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusIndicator : MonoBehaviour
{
    [SerializeField]
    Unit owner;

    [SerializeField]
    SpriteRenderer buffIndicator;

    [SerializeField]
    SpriteRenderer debuffIndicator;

    Vector3 centerPos;

    List<Sprite> buffSprites = new List<Sprite>();
    List<Sprite> debuffSprites = new List<Sprite>();

    int currentDisplayBuff;
    int currentDisplayDebuff;

    float secondsPerDisplay = 1f;
    float timer = 0;

    void Awake()
    {
        centerPos = buffIndicator.transform.localPosition;
    }

    void OnEnable()
    {
        this.AddObserver(OnAddedStatus, Status.AddedNotification, owner);
        this.AddObserver(OnRemovedStatus, Status.RemovedNotification, owner);
    }

    void OnDisable()
    {
        this.RemoveObserver(OnAddedStatus, Status.AddedNotification, owner);
        this.RemoveObserver(OnRemovedStatus, Status.RemovedNotification, owner);
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (buffSprites.Count > 0)
        {
            int i = Mathf.FloorToInt((timer / secondsPerDisplay) % (buffSprites.Count));
            buffIndicator.sprite = buffSprites[i];
        }

        if (debuffSprites.Count > 0)
        {
            int i = Mathf.FloorToInt((timer / secondsPerDisplay) % (debuffSprites.Count));
            debuffIndicator.sprite = debuffSprites[i];
        }
    }

    public void Show()
    {
        LeanTween.alpha(buffIndicator.gameObject, 1, 0.1f);
        LeanTween.alpha(debuffIndicator.gameObject, 1, 0.1f);
    }

    public void Hide()
    {
        LeanTween.alpha(buffIndicator.gameObject, 0, 0.1f);
        LeanTween.alpha(debuffIndicator.gameObject, 0, 0.1f);
    }

    void OnAddedStatus(object sender, object args)
    {
        StatusEffect statusEffect = args as StatusEffect;

        if (statusEffect.spriteFileName == "") // No indicator if empty file name
            return;

        var sprite = Resources.Load<Sprite>(statusEffect.spriteFileName);

        if (statusEffect.isBuff)
        {
            if (!buffSprites.Contains(sprite))
                buffSprites.Add(sprite);
        }
        else
        {
            if (!debuffSprites.Contains(sprite))
                debuffSprites.Add(sprite);
        }

        if (buffSprites.Count > 0 && debuffSprites.Count > 0) // move icons side by side if both debuffed and buffed
        {
            buffIndicator.transform.localPosition = centerPos + new Vector3(-0.15f, 0, 0);
            debuffIndicator.transform.localPosition = centerPos + new Vector3(0.15f, 0, 0);
        }
    }

    void OnRemovedStatus(object sender, object args)
    {
        StatusEffect statusEffect = args as StatusEffect;

        if (statusEffect.spriteFileName == "") // No indicator if empty file name
            return;

        var sprite = Resources.Load<Sprite>(statusEffect.spriteFileName);

        if (statusEffect.isBuff)
            buffSprites.Remove(sprite);
        else
            debuffSprites.Remove(sprite);

        if (!(buffSprites.Count > 0 && debuffSprites.Count > 0)) // move icons to center if only buffed or only debuffed
        {
            buffIndicator.transform.localPosition = centerPos;
            debuffIndicator.transform.localPosition = centerPos;
        }

        if (buffSprites.Count == 0)
            buffIndicator.sprite = null;

        if (debuffSprites.Count == 0)
                debuffIndicator.sprite = null;
    }
}
