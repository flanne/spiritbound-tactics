﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

namespace Spiritlink.Pub
{
    public class MissionSelectState : PubState
    {
        MissionData[] availableMissions { get { return MasterSingleton.main.MissionLog.GetAvailable().ToArray(); } }
        #region Eventhandler
        void OnClick(object sender, InfoEventArgs<int> e)
        {
            missionSelectMenu.interactable = false;

            owner.selectedMission = availableMissions[e.info];

            owner.ChangeState<MissionConfirmState>();

            audioPlayer.Play(owner.confirmSFX);
        }

        void OnSelect(object sender, InfoEventArgs<int> e)
        {
            questDescription.SetProperties(new DescribableProperties(availableMissions[e.info]));

            audioPlayer.Play(owner.selectSFX);
        }

        void OnCancel(InputAction.CallbackContext obj)
        {
            owner.ChangeState<ExitToWorldMapState>();
            missionSelectMenu.interactable = false;

            audioPlayer.Play(owner.cancelSFX);
        }

        void OnNoMissionAnyKey(InputAction.CallbackContext obj)
        {
            playerInput.currentActionMap.FindAction("AnyButton").started -= OnNoMissionAnyKey;
            owner.ChangeState<ExitToWorldMapState>();
        }
        #endregion

        #region LifeCycle
        public override void Enter()
        {
            missionSelectMenu.Clear();
            if (availableMissions.Length > 0)
            {
                foreach (var mission in availableMissions)
                    missionSelectMenu.AddEntry(new DescribableProperties(mission));

                missionSelectMenu.Show();
                missionSelectMenu.Select(0);

                questDescription.SetProperties(new DescribableProperties(availableMissions[0]));
                questDescription.Show();
            }
            else
            {
                missionSelectMenu.Hide();
                questDescription.Hide();

                playerInput.currentActionMap.FindAction("AnyButton").started += OnNoMissionAnyKey;
            }

            missionSelectMenu.ClickEvent += OnClick;
            missionSelectMenu.SelectEvent += OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;
        }

        public override void Exit()
        {
            missionSelectMenu.ClickEvent -= OnClick;
            missionSelectMenu.SelectEvent -= OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;
        }
        #endregion
    }
}
