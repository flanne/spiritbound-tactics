﻿using UnityEngine;
using System.Collections;

public class AutoStatusController : MonoBehaviour
{
    void OnEnable()
    {
        this.AddObserver(OnHPDidChangeNotification, Stats.DidChangeNotification(StatTypes.HP));
    }

    void OnDisable()
    {
        this.RemoveObserver(OnHPDidChangeNotification, Stats.DidChangeNotification(StatTypes.HP));
    }

    void OnHPDidChangeNotification(object sender, object args)
    {
        Stats stats = sender as Stats;
        if (stats[StatTypes.HP] == 0)
        {
            Status status = stats.unit.status;
            StatComparisonCondition c = status.Add<KnockedOutStatusEffect, StatComparisonCondition>();
            c.Init(StatTypes.HP, 0, c.EqualTo);
        }
    }
}