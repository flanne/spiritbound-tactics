﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunOnHighHit : Passive
{
    public int stunAmount;

    protected override void AddListeners()
    {
        this.AddObserver(OnHit, Skill.HitNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnHit, Skill.HitNotification);
    }

    void OnHit(object sender, object args)
    {
        var info = args as Info<Unit, BoardTile>;
        var actor = info.arg0;
        var defender = info.arg1?.content?.GetComponent<Unit>();
        var skill = sender as Skill;

        if (owner == actor && defender != null && skill.ContainsTag(SkillTag.Weapon))
        {
            int totalStun = Mathf.Max(0, (actor.tile.z - defender.tile.z) * stunAmount);
            defender.stats[StatTypes.CTR] += totalStun;
        }
    }
}