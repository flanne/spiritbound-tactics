﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

public class EquipChangeState : WorldMapState
{
    #region EventHandlers
    void OnClick(object sender, InfoEventArgs<int> e)
    {
        Equippable selectedItem = owner.filteredInventory[e.info].item;

        if (MasterSingleton.main.Inventory.IsAvailable(selectedItem))
        {
            if (owner.menuUnit.equipment.Equip(selectedItem, owner.menuEquipSlot))
            {
                owner.ChangeState<EquipManageState>();

                audioPlayer.Play(owner.confirmSFX);
                return;
            }
        }

        audioPlayer.Play(owner.cancelSFX);
    }

    void OnSelect(object sender, InfoEventArgs<int> e)
    {
        Equippable selectedItem = owner.filteredInventory[e.info].item;

        equipCompareUI.CompareToEquippable(selectedItem, owner.menuEquipSlot);

        descriptionBox.SetToDescribable(selectedItem);

        audioPlayer.Play(owner.selectSFX);
    }

    void OnCancel(InputAction.CallbackContext obj)
    {
        owner.ChangeState<EquipManageState>();

        audioPlayer.Play(owner.cancelSFX);
    }
    #endregion

    #region LifeCycle
    public override void Enter()
    {
        equipmentMenu.interactable = false;

        equipCompareUI.SetUnit(owner.menuUnit);

        // Show description box if iventory is not empty
        if (owner.filteredInventory.Count > 0)
            descriptionBox.Show(); 

        // Add listeners
        equipChangeMenu.ClickEvent += OnClick;
        equipChangeMenu.SelectEvent += OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;

        equipChangeMenu.AddEntriesFromItemStacks(owner.filteredInventory);

        equipChangeMenu.SelectFirstAvailable();
        equipChangeMenu.Show();

        // Format equipment if can't equip
        for (int i = 0; i < owner.filteredInventory.Count; i++)
        {
            if (!owner.menuUnit.equipment.CanEquip(owner.filteredInventory[i].item))
            {
                var widget = equipChangeMenu.GetEntry(i).GetComponent<ItemStackWidget>();
                if (widget != null)
                    widget.FormatUnEquippable();
            }
        }

        // Show keybinds
        keybindPromptUI.Show();
        keybindPromptUI.SetText(0, "Select");
        keybindPromptUI.SetText(1, "Cancel");
        keybindPromptUI.HidePrompt(2);
    }

    public override void Exit()
    {
        equipmentMenu.interactable = true;

        // Remove listeners
        equipChangeMenu.ClickEvent -= OnClick;
        equipChangeMenu.SelectEvent -= OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;

        equipChangeMenu.Hide();

        descriptionBox.Hide();
    }
    #endregion
}
