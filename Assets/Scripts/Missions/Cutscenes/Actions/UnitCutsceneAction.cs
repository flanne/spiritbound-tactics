﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UnitCutsceneAction : CutsceneAction
{
    [SerializeField]
    IDEnum unitID;

    protected Unit unit { get { return UnitID.GetUnitWithID(unitID); } }
}
