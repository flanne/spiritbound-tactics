﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillUnitCutsceneAction : UnitCutsceneAction
{
    public override void Play()
    {
        unit.stats[StatTypes.HP] = 0;
    }
}
