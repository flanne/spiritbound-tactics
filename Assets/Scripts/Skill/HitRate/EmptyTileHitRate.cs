﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyTileHitRate : HitRate
{
    public override bool IsAngleBased { get { return false; } }

    public override int Calculate(Unit actor, Unit targetUnit, Skill skill)
    {
        return 100;
    }
}
