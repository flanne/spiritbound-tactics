﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Empty state to wait for scene transitions
public class MapTransitionSceneState : WorldMapState
{
    public override void Enter()
    {
        MasterSingleton.main.AudioPlayer.FadeOutMusic(1f);
    }
}
