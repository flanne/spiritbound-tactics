﻿using UnityEngine;
using System.Collections;

public enum StatTypes
{
	LVL, // Level
	EXP, // Experience
	HP,  // Hit Points
	MHP, // Max Hit Points
	ATK, // Physical Attack
	DEF, // Physical Defense
	MAT, // Magic Attack
	MDF, // Magic Defense
	MOV, // Move Range
	JMP, // Jump Height
	EVD, // Evade
	CTR, // Time till turn
	Count
}