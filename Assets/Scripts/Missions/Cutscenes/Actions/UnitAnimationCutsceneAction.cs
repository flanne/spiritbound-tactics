﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAnimationCutsceneAction : UnitCutsceneAction
{
    [SerializeField]
    UnitAnimState unitAnimState;

    public override void Play()
    {
        unit.SetAnimationState(unitAnimState);
        isPlaying = false;
    }
}
