﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using TMPro;

namespace flanne.UI
{
    public class ShopConfirmUI : Panel
    {
        #region Events
        public event EventHandler<InfoEventArgs<uint>> selectEvent;
        public event EventHandler cancelEvent;
        #endregion

        #region Fields
        [System.NonSerialized] public bool isSelling;

        public Equippable selectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value; Refresh(); }
        }
        Equippable _selectedItem;

        [SerializeField] PlayerInput playerInput;
        [SerializeField] Image itemIcon;
        [SerializeField] TMP_Text itemName;
        [SerializeField] TMP_Text ownedText;
        [SerializeField] TMP_Text amountText;
        public uint amount { get; private set; }

        [SerializeField] TMP_Text priceText;

        GameStrings gameStrings { get { return MasterSingleton.main.GameStrings; } }
        Inventory inventory { get { return MasterSingleton.main.Inventory; } }
        #endregion

        #region Eventhandlers
        void OnMove(InputAction.CallbackContext obj)
        {
            Vector2 vec = obj.ReadValue<Vector2>();
            if (vec.x < 0 || vec.y < 0)
                DecreaseAmount();
            else if (vec.x > 0 || vec.y > 0)
                IncreaseAmount();
        }

        protected virtual void OnFire(object sender, InfoEventArgs<int> e)
        {
            if (e.info == 0)
            {
                if (selectEvent != null)
                    selectEvent(this, new InfoEventArgs<uint>(amount));
            }
            else if (e.info == 1)
            {
                if (cancelEvent != null)
                    cancelEvent(this, EventArgs.Empty);
            }
        }
        #endregion

        #region Public
        public override void Show()
        {
            amount = 1;
            Refresh();

            base.Show();

            playerInput.currentActionMap.FindAction("Navigate").performed += OnMove;
        }

        public override void Hide()
        {
            base.Hide();

            playerInput.currentActionMap.FindAction("Navigate").performed -= OnMove;
        }

        public void IncreaseAmount()
        {
            if (amount < 999)
                amount++;

            Refresh();
        }

        public void DecreaseAmount()
        {
            if (amount > 1)
                amount--;

            Refresh();
        }
        #endregion

        #region Private
        void Refresh()
        {
            itemIcon.sprite = _selectedItem.icon;
            itemName.text = gameStrings.Get(_selectedItem.nameStringID);

            uint inUse = 0;
            uint total = 0;
            ItemStack inventoryStack = inventory.itemList.Find(x => x.item == _selectedItem);
            if (inventoryStack != null)
            {
                inUse = inventoryStack.inUse;
                total = inventoryStack.total;
            }

            ownedText.text = inUse + "/" + total;

            amountText.text = amount.ToString();

            uint price;
            if (isSelling)
            {
                price = (amount * _selectedItem.cost) / 2;
                priceText.color = Color.white;
            }
            else
            {
                price = (amount * _selectedItem.cost);
                if (price > inventory.money)
                    priceText.color = Color.red;
                else
                    priceText.color = Color.white;
            }
            priceText.text = price.ToString();
        }
        #endregion
    }
}
