﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// How much job xp a mission gives out. 
/// </summary>
public enum RewardJobXP
{
    None = 0,
    Small = 40,
    Medium = 80,
    Large = 160
}
