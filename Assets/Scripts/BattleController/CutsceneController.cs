﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneController : MonoBehaviour
{
    public DialogueController dialogueController;
    private Coroutine cutsceneCoroutine;
    private CutsceneSequence queuedCutscene;

    // Cutscene prefabs have a cutscene sequence and cutscene trigger
    public GameObject LoadCutscene(GameObject cutscenePrefab)
    {
        GameObject instance = GameObject.Instantiate(cutscenePrefab);
        instance.name = instance.name.Replace("(Clone)", "");

        instance.transform.SetParent(this.transform);
        var cutsceneSequence = instance.GetComponent<CutsceneSequence>();

        if (cutsceneSequence != null)
            cutsceneSequence.owner = this;
        else
            Debug.LogError("No cutscene sequence attached cutscene prefab: " + instance.name);

        return instance;
    }

    // Called by child cutscene trigger components
    public void TriggerCutscene(CutsceneSequence cutscene)
    {
        queuedCutscene = cutscene;
    }

    // Play queue'd cutscene
    public bool Play()
    {
        if (queuedCutscene)
        {
            cutsceneCoroutine = StartCoroutine(queuedCutscene.PlaySequence());
            queuedCutscene = null;
            return true;
        }
        else
            return false;
    }

    public bool CutsceneQueued()
    {
        return queuedCutscene != null;
    }

    public bool isPlaying()
    {
        CutsceneSequence[] cutscenes = GetComponentsInChildren<CutsceneSequence>();
        foreach (var c in cutscenes)
        {
            if (c.isPlaying)
                return true;
        }

        return false;
    }

    public void Stop()
    {
        StopCoroutine(cutsceneCoroutine);

        CutsceneSequence[] cutscenes = GetComponentsInChildren<CutsceneSequence>();
        foreach (var c in cutscenes)
        {
            c.isPlaying = false;
            c.Stop();
        }
    }
}
