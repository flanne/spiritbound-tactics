﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrantSkillFeature : Feature
{
	#region Fields / Properties
	public Skill Skill { get { return skill; } }
	[SerializeField]
	Skill skill;
	#endregion

	#region Protected
	protected override void OnApply(Unit target)
	{
		if (skill == null)
        {
			Debug.Log("No skill attached to grant skill feature");
			return;
        }

		target.skillCatalog.AddSkill(skill);
	}

	protected override void OnRemove(Unit target)
	{
		if (skill == null)
		{
			Debug.Log("No skill attached to grant skill feature");
			return;
		}

		target.skillCatalog.RemoveSkill(skill);
	}
	#endregion
}
