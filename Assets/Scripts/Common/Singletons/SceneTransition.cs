﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneTransition : MonoBehaviour
{
    [SerializeField]
    ScreenFilter screenFilter;

    [SerializeField]
    GameLevel gameLevel;

    string sceneToLoad;

    public void LoadScene(string sceneName, float fadeSpeed = 2f)
    {
        sceneToLoad = sceneName;
        screenFilter.FadeOut(fadeSpeed, FadeSceneIn);
    }

    void FadeSceneIn()
    {
        StartCoroutine(FadeSceneInCoroutine());
    }

    IEnumerator FadeSceneInCoroutine()
    {
        yield return new WaitForSeconds(1);

        gameLevel.ClearBoard();
        SceneManager.LoadScene(sceneToLoad);
        screenFilter.FadeIn(0.5f);
    }

}
