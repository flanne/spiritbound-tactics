﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class JobXPRewardWidget : UnitWidget
    {
        public TMP_Text rankTMP;
        public JobXPBar jobXPBar;

        [SerializeField] Image unitSprite;
        [SerializeField] Image classIcon;
        [SerializeField] TMP_Text unitNameTMP;
        [SerializeField] TMP_Text classNameTMP;

        public override void SetProperties(UnitProperties properties)
        {
            Unit unit = properties.unit;

            unitSprite.sprite = unit.unitSpriteSheet.GetSprite("f_still");
            classIcon.sprite = unit.jobs.primary.icon;

            unitNameTMP.text = unit.unitName;
            classNameTMP.text = MasterSingleton.main.GameStrings.Get(unit.jobs.primary.nameStringID);

            JobRank primraryJobRank = unit.jobRanks.Get(unit.jobs.primary);
            rankTMP.text = primraryJobRank.rank.ToString();

            if (primraryJobRank.rank < 4)
                jobXPBar.SetTo(primraryJobRank.XP, JobRank.XPToLevel[primraryJobRank.rank - 1]);
            else
                jobXPBar.SetTo(1, 1); // set xp bar to full
        }
    }
}
