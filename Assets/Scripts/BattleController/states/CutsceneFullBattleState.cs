﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spiritlink.Battle
{
    public class CutsceneFullBattleState : CutsceneState
    {
        public override void Enter()
        {
            LoadLevel();
            StartCutscene();
        }

        protected override void GoToNextState()
        {
            MasterSingleton.main.SceneTransition.LoadScene("WorldMap");
            PanCamera();
        }

        void StartCutscene()
        {
            GameObject cutscenePrefab = MasterSingleton.main.MissionLog.current.fullPostCutscene;
            GameObject cutsceneObj = cutsceneController.LoadCutscene(cutscenePrefab);

            FullCutscene cutscene = cutsceneObj.GetComponent<FullCutscene>();
            cutsceneController.TriggerCutscene(cutscene);
            cutsceneController.Play();

            MasterSingleton.main.ScreenFilter.FadeIn(2f);
            letterBox.Show();
            StartCoroutine(WaitForCutscene());
            StartCoroutine(InitControlLock());
        }

        void PanCamera()
        {
            MasterSingleton.main.MainCameraRig.follow = null;
            var pos = MasterSingleton.main.MainCameraRig.transform.position;
            pos.y += 1;
            LeanTween.move(MasterSingleton.main.MainCameraRig.gameObject, pos, 2f);
        }

        void LoadLevel()
        {
            MasterSingleton.main.Party.DeactivateAll();

            FullCutscene cutscene = MasterSingleton.main.MissionLog.current.fullPostCutscene.GetComponent<FullCutscene>();

            // Create board
            MasterSingleton.main.GameLevel.ClearBoard();
            MasterSingleton.main.GameLevel.Load(cutscene.level.boardPrefab);

            // Spawn party units
            foreach (var p in cutscene.partySpawns)
            {
                Unit unit = MasterSingleton.main.UnitFactory.Spawn(p.spawnData, p.unitID);

                if (unit)
                    units.Add(unit);
            }

            // Spawn other units
            foreach (var recipe in cutscene.unitRecipes)
            {
                Unit unit = MasterSingleton.main.UnitFactory.Spawn(recipe);
                units.Add(unit);
            }
        }
    }
}