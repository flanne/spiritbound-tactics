﻿using UnityEngine;
using System.Collections;

public class AttackHitRate : HitRate
{
    public override int Calculate(Unit actor, Unit targetUnit, Skill skill)
    {
        if (AutomaticHit(actor, targetUnit, skill))
            return 100;
        if (AutomaticMiss(actor, targetUnit, skill))
            return 0;

        int evade = Mathf.Clamp(targetUnit.stats[StatTypes.EVD], 0, 100);
        evade = AdjustForRelativeFacing(actor, targetUnit, evade);
        evade = AdjustForStatusEffects(actor, targetUnit, evade);
        evade = Mathf.Clamp(evade, 5, 99);
        return Final(evade);
    }

    int AdjustForRelativeFacing(Unit actor, Unit target, int rate)
    {
        switch (actor.GetFacing(target))
        {
            case Facings.Front:
                return rate;
            case Facings.Side:
                return rate / 2;
            default:
                return 0;
        }
    }
}