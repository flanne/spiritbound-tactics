﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Equippable : DescribableObject
{
	#region Fields
	public uint cost { get { return _cost; } }
	[SerializeField]
	uint _cost;

	/// <summary>
	/// The EquipSlots flag which is the default
	/// equip location(s) for this item.
	/// For example, a normal weapon would only specify 
	/// primary, but a two-handed weapon would specify
	/// both primary and secondary.
	/// </summary>
	public virtual EquipSlots defaultSlots { get { return EquipSlots.None; } }

	/// <summary>
	/// Some equipment may be allowed to be equipped in
	/// more than one slot location, such as when 
	/// dual-wielding swords.
	/// </summary>
	public EquipSlots[] secondarySlots;
	
	[SerializeField]
	int MHP;
	[SerializeField]
	int ATK;
	[SerializeField]
	int MAT;
	[SerializeField]
	int DEF;
	[SerializeField]
	int MDF;
	[SerializeField]
	int MOV;
	[SerializeField]
	int JMP;
	[SerializeField]
	int EVD;

	public int this[StatTypes s]
	{
		get
		{
			int stat = 0;
			switch (s)
            {
				case StatTypes.MHP:
					stat = MHP;
					break;
				case StatTypes.ATK:
					stat = ATK;
					break;
				case StatTypes.MAT:
					stat = MAT;
					break;
				case StatTypes.DEF:
					stat = DEF;
					break;
				case StatTypes.MDF:
					stat = MDF;
					break;
				case StatTypes.MOV:
					stat = MOV;
					break;
				case StatTypes.JMP:
					stat = JMP;
					break;
				case StatTypes.EVD:
					stat = EVD;
					break;
			}

			return stat;
		}
	}

	[SerializeField]
	Feature[] features;
	#endregion

	#region Public
	public override string GetDescription()
	{
		string description = "";
		if (MHP > 0)
			description += "HP" + " <color=#dc9042>+" + MHP +"</color>  ";
		if (ATK > 0)
			description += MasterSingleton.main.GameStrings.Get("CHARACTER_STAT_PATK_SHORT") + " <color=#dc9042>+" + ATK + "</color>  ";
		if (MAT > 0)
			description += MasterSingleton.main.GameStrings.Get("CHARACTER_STAT_MATK_SHORT") + " <color=#dc9042>+" + MAT + "</color>  ";
		if (DEF > 0)
			description += MasterSingleton.main.GameStrings.Get("CHARACTER_STAT_PDEF_SHORT") + " <color=#dc9042>+" + DEF + "</color>  ";
		if (MDF > 0)
			description += MasterSingleton.main.GameStrings.Get("CHARACTER_STAT_MDEF_SHORT") + " <color=#dc9042>+" + MDF + "</color>  ";
		if (MOV > 0)
			description += MasterSingleton.main.GameStrings.Get("CHARACTER_STAT_MOVE") + " <color=#dc9042>+" + MOV + "</color>  ";
		if (JMP > 0)
			description += MasterSingleton.main.GameStrings.Get("CHARACTER_STAT_JUMP") + " <color=#dc9042>+" + JMP + "</color>  ";
		if (EVD > 0)
			description += MasterSingleton.main.GameStrings.Get("CHARACTER_STAT_EVADE") + " <color=#dc9042>+" + EVD + "</color>  ";

		if (description != "")
			description += "\n";

		if (descriptionStringID != "")
			description += MasterSingleton.main.GameStrings.Get(descriptionStringID);

		return description;
	}

	public virtual void OnEquip(Unit unit)
	{
		AddStats(unit);
		for (int i = 0; i < features.Length; ++i)
			features[i].Activate(unit);
	}

	public virtual void OnUnEquip(Unit unit)
	{
		RemoveStats(unit);
		for (int i = 0; i < features.Length; ++i)
			features[i].Deactivate(unit);
	}
	#endregion

	#region private
	void AddStats(Unit unit)
	{
		Stats stats = unit.stats;
		stats[StatTypes.MHP] += MHP;
		stats[StatTypes.ATK] += ATK;
		stats[StatTypes.MAT] += MAT;
		stats[StatTypes.DEF] += DEF;
		stats[StatTypes.MDF] += MDF;
		stats[StatTypes.MOV] += MOV;
		stats[StatTypes.JMP] += JMP;
		stats[StatTypes.EVD] += EVD;
	}

	void RemoveStats(Unit unit)
	{
		Stats stats = unit.stats;
		stats[StatTypes.MHP] -= MHP;
		stats[StatTypes.ATK] -= ATK;
		stats[StatTypes.MAT] -= MAT;
		stats[StatTypes.DEF] -= DEF;
		stats[StatTypes.MDF] -= MDF;
		stats[StatTypes.MOV] -= MOV;
		stats[StatTypes.JMP] -= JMP;
		stats[StatTypes.EVD] -= EVD;
	}
	#endregion
}