﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

public class EquipManageState : WorldMapState
{
    EquipSlots _currentSlot = EquipSlots.Primary;

    #region Eventhandlers
    void OnClick(object sender, InfoEventArgs<EquipSlots> e)
    {
        List<ItemStack> inventoryItems = MasterSingleton.main.Inventory.itemList;

        // Set equip change menu to equip slot selected
        owner.menuEquipSlot = e.info;
        owner.filteredInventory = MasterSingleton.main.Inventory.GetItemsOfSlot(owner.menuEquipSlot);

        owner.ChangeState<EquipChangeState>();

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnSelect(object sender, InfoEventArgs<EquipSlots> e)
    {
        Equippable selectedItem = owner.menuUnit.equipment.GetItem(e.info);

        if (selectedItem != null)
        {
            descriptionBox.Show();
            descriptionBox.SetToDescribable(selectedItem, DescriptionBox.Side.Right);
        }
        else
        {
            descriptionBox.Hide();
        }

        _currentSlot = e.info;

        audioPlayer.Play(owner.selectSFX);
    }

    void OnSecondary(InputAction.CallbackContext obj)
    {
        owner.menuUnit.equipment.UnEquip(owner.equipmentMenu.currentSlot);

        // Refresh
        owner.equipmentMenu.SetToUnit(owner.menuUnit);
        descriptionBox.Hide();

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnCancel(InputAction.CallbackContext obj)
    {
        equipmentMenu.Hide();
        owner.ChangeState<UnitListMenuState>();

        audioPlayer.Play(owner.cancelSFX);
    }
    #endregion

    #region LifeCycle
    public override void Enter()
    {
        equipmentMenu.SetToUnit(owner.menuUnit);

        // Add listeners
        equipmentMenu.SlotClickEvent += OnClick;
        equipmentMenu.SlotSelectEvent += OnSelect;
        playerInput.currentActionMap.FindAction("Secondary").performed += OnSecondary;
        playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;

        equipmentMenu.Show();
        equipmentMenu.Select(_currentSlot);

        // Init Description box
        Equippable primaryEquip = owner.menuUnit.equipment.GetItem(EquipSlots.Primary);
        if (primaryEquip != null)
        {
            descriptionBox.Show();
            descriptionBox.SetToDescribable(primaryEquip, DescriptionBox.Side.Right);
        }

        // Show keybinds
        keybindPromptUI.Show();
        keybindPromptUI.SetText(0, "Select");
        keybindPromptUI.SetText(1, "Save & Back");
        keybindPromptUI.SetText(2, "Unequip");
    }

    public override void Exit()
    {
        // Remove listeners
        equipmentMenu.SlotClickEvent -= OnClick;
        equipmentMenu.SlotSelectEvent -= OnSelect;
        playerInput.currentActionMap.FindAction("Secondary").performed -= OnSecondary;
        playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;

        descriptionBox.Hide();
    }
    #endregion
}
