﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

[RequireComponent(typeof(Light2D))]
public class PulsingLight : MonoBehaviour
{
    Light2D myLight;

    [SerializeField]
    float startIntensity;

    [SerializeField]
    float endIntensity;

    [SerializeField]
    float secondsPerPulse;

    // Start is called before the first frame update
    void Start()
    {
        myLight = GetComponent<Light2D>();
        LeanTween.value(gameObject, startIntensity, endIntensity, secondsPerPulse).setLoopPingPong().setEase(LeanTweenType.easeInOutQuad).setOnUpdate((float val) => {
            myLight.intensity = val;
        });
    }
}
