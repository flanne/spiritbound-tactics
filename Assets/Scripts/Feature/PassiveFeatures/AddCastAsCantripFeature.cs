﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddCastAsCantripFeature : AddPassiveFeature<CastAsCantripPassive>
{
    [SerializeField]
    SkillTag skillTag;

    protected override void SetParameters(CastAsCantripPassive passive)
    {
        passive.skillTag = this.skillTag;
    }
}
