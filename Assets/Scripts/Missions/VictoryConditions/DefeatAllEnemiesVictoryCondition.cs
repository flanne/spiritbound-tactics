﻿using UnityEngine;
using System.Collections;

namespace Spiritlink.Battle
{
    public class DefeatAllEnemiesVictoryCondition : BaseVictoryCondition
    {
        protected override void CheckForVictory()
        {
            if (PartyDefeated(enemyAlliance))
                victoryAchieved = true;
        }
    }
}