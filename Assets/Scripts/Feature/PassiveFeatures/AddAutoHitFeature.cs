﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddAutoHitFeature : AddPassiveFeature<AutoHitPassive>
{
    [SerializeField]
    SkillTag skillTag;

    protected override void SetParameters(AutoHitPassive passive)
    {
        passive.skillTag = this.skillTag;
    }
}
