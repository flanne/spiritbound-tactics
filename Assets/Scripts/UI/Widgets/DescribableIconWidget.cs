﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace flanne.UI
{
    public class DescribableIconWidget : DescribableWidget
    {
        [SerializeField] protected Image icon;

        public override void SetProperties(DescribableProperties properties)
        {
            if (properties.des == null)
            {
                icon.enabled = false;
            }
            else
            {
                icon.enabled = true;
                icon.sprite = properties.des.icon;
            }
        }
    }
}
