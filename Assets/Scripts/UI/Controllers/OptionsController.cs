﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class OptionsController : MonoBehaviour
    {
        #region Fields
        public event EventHandler CancelEvent;

        [SerializeField] Slider bgmSlider;
        [SerializeField] Slider sfxSlider;

        [SerializeField] TMP_Text bgmTMP;
        [SerializeField] TMP_Text sfxTMP;

        [SerializeField] Toggle fullscreenToggle;

        [SerializeField] AudioClip testSFX;

        [SerializeField] Panel panel;
        #endregion

        #region EventHandlers
        public void OnMusicSliderChanged()
        {
            MasterSingleton.main.AudioPlayer.MusicVolume = bgmSlider.value;

            int volumePercent = Mathf.FloorToInt(100 * bgmSlider.value);
            bgmTMP.text = volumePercent + "%";
        }

        public void OnEffectsSliderChanged()
        {
            MasterSingleton.main.AudioPlayer.EffectsVolume = sfxSlider.value;

            int volumePercent = Mathf.FloorToInt(100 * sfxSlider.value);
            sfxTMP.text = volumePercent + "%";
        }

        public void OnEffectsSliderEndDrag()
        {
            MasterSingleton.main.AudioPlayer.Play(testSFX);
        }

        public void OnFullscreenToggle()
        {
            Screen.fullScreen = fullscreenToggle.isOn;
        }

        public void OnSaveAndExitButton()
        {
            // Save settings
            int isFullscreen;
            if (fullscreenToggle.isOn)
                isFullscreen = 1;
            else
                isFullscreen = 0;

            PlayerPrefs.SetInt("isFullscreen", isFullscreen);
            PlayerPrefs.SetFloat("bgmVolume", bgmSlider.value);
            PlayerPrefs.SetFloat("sfxVolume", sfxSlider.value);
            PlayerPrefs.Save();

            if (CancelEvent != null)
                CancelEvent(this, EventArgs.Empty);
        }
        #endregion

        #region Public
        public void Show()
        {
            if (panel != null)
                panel.Show();

            bgmSlider.Select();
        }

        public void Hide()
        {
            if (panel != null)
                panel.Hide();
        }

        public void SetToPrefs()
        {
            fullscreenToggle.isOn = (PlayerPrefs.GetInt("isFullscreen") == 1);
            bgmSlider.value = PlayerPrefs.GetFloat("bgmVolume");
            sfxSlider.value = PlayerPrefs.GetFloat("sfxVolume");
        }
        #endregion
    }
}
