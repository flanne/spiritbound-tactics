﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialFlags : MonoBehaviour
{
    public bool wishlist = false;
    public bool combat = false;
    public bool action = false;
    public bool mana = false;
    public bool item = false;
    public bool ability = false;
    public bool pub = false;
    public bool worldMap = false;
    public bool partyPlacement = false;
    public bool job = false;
    public bool patrol = false;

    public void LoadFromSave(SaveData data)
    {
        this.wishlist = data.wishlistTut;
        this.combat = data.combatTut;
        this.action = data.actionTut;
        this.mana = data.manaTut;
        this.item = data.itemTut;
        this.ability = data.abilityTut;
        this.pub = data.pubTut;
        this.worldMap = data.worldMapTut;
        this.partyPlacement = data.partyPlacementTut;
        this.job = data.jobTut;
        this.patrol = data.patrolTut;
    }

    public void InitNew()
    {
        wishlist = false;
        combat = false;
        action = false;
        mana = false;
        item = false;
        ability = false;
        pub = false;
        worldMap = false;
        partyPlacement = false;
        job = false;
        patrol = false;
    }
}
