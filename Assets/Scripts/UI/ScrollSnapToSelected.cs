﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace flanne.UI
{
    public class ScrollSnapToSelected : MonoBehaviour
    {
        [SerializeField] Menu menu;
        [SerializeField] ScrollRect scroller;

        void OnSelect(object sender, InfoEventArgs<int> e)
        {
            var rect = menu.GetEntry(e.info).GetComponent<RectTransform>();

            scroller.EnsureVisibility(rect);
        }

        void OnEnable()
        {
            if (menu != null)
                menu.SelectEvent += OnSelect;
            else
                Debug.LogError("No menu attached.");
        }

        void OnDisable()
        {
            if (menu != null)
                menu.SelectEvent -= OnSelect;
        }
    }
}
