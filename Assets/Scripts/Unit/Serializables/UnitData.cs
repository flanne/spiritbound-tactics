﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public struct UnitData
{
    [LabelWidth(100)]
    [GUIColor(1f, 0.75f, 0.5f)]
    public string unitName;

    [HorizontalGroup("Split", 300)]
    [BoxGroup("Split/Basic Info", false)]
    [LabelWidth(100)]
    public int level;

    [BoxGroup("Split/Basic Info")]
    [LabelWidth(100)]
    public UnitModel modelRef;

    [BoxGroup("Split/Basic Info")]
    [LabelWidth(100)]
    public Job primaryJob;

    [BoxGroup("Split/Basic Info")]
    [LabelWidth(100)]
    public Job secondaryJob;

    [BoxGroup("Split/Basic Info")]
    [LabelWidth(100)]
    public JobRank[] jobRanks;

    [BoxGroup("Split/Equipment", false)]
    [LabelWidth(75)]
    public Equippable primary, secondary, body, accessory, consume1, consume2, consume3;

    [HideInEditorMode]
    public int xp;
}
