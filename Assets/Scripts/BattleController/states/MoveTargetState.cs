﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class MoveTargetState : BattleState
    {
        #region Fields
        List<BoardTile> tiles;
        #endregion

        #region Eventhandlers
        void OnClick(object sender, InfoEventArgs<BoardTile> e)
        {
            if (tiles.Contains(e.info))
            {
                owner.ChangeState<MoveSequenceState>();

                audioPlayer.Play(owner.confirmSFX);
            }
        }

        void OnSelect(object sender, InfoEventArgs<BoardTile> e)
        {
            if (e.info.content != null)
            {
                Unit unit = e.info.content.GetComponent<Unit>();

                if (unit != null)
                {
                    primaryHUD.SetProperties(new UnitProperties(unit));
                    primaryHUD.Show();
                    turnOrderUI.SetHighlight(unit, true);
                }
            }
            else
            {
                turnOrderUI.UnHighlightAll();
                primaryHUD.Hide();
            }
        }

        void OnCancel(InputAction.CallbackContext obj)
        {
            owner.ChangeState<ActionSelectState>();

            audioPlayer.Play(owner.cancelSFX);
        }
        #endregion

        #region LifeCycle
        public override void Enter()
        {
            base.Enter();

            Movement mover = owner.turn.actor.movement;
            tiles = mover.GetTilesInRange(board);

            tileHighlighter.Highlight(tiles, new Color(0.2f, 1f, 0.6f, 1f));

            if (driver.Current == DriverType.Computer)
                StartCoroutine(ComputerHighlightMoveTarget());
        }

        public override void Exit()
        {
            base.Exit();

            turnOrderUI.UnHighlightAll();

            tileHighlighter.Clear();
            tileSelectIndicator.Hide();
            tiles = null;

            primaryHUD.Hide();
        }

        protected override void OnEnterUI()
        {
            // Add listeners
            tileSelectIndicator.ClickEvent += OnClick;
            tileSelectIndicator.SelectEvent += OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed += OnCancel;

            tileSelectIndicator.SelectTile(turn.actor.tile.xy);
            tileSelectIndicator.Show();

            // Show keybinds
            keybindPromptUI.Show();
            keybindPromptUI.SetText(0, "Select");
            keybindPromptUI.SetText(1, "Cancel");

            cameraRig.follow = null;
            cameraPanner.active = true;
        }

        protected override void OnExitUI()
        {
            // Remove Listeners
            tileSelectIndicator.ClickEvent -= OnClick;
            tileSelectIndicator.SelectEvent -= OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed -= OnCancel;

            tileSelectIndicator.Hide();

            cameraRig.follow = turn.actor.transform;
            cameraPanner.active = false;
        }
        #endregion

        #region AI
        IEnumerator ComputerHighlightMoveTarget()
        {
            yield return new WaitForSeconds(0.5f);
            tileSelectIndicator.SelectTile(turn.plan.moveLocation);

            audioPlayer.Play(owner.confirmSFX);

            owner.ChangeState<MoveSequenceState>();
        }
        #endregion
    }
}