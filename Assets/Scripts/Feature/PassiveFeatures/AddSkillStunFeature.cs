﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddSkillStunFeature : AddPassiveFeature<SkillStunPassive>
{
    [SerializeField]
    SkillTag skillTag;

    [SerializeField]
    int stunAmount;

    protected override void SetParameters(SkillStunPassive passive)
    {
        passive.skillTag = this.skillTag;
        passive.stunAmount = this.stunAmount;
    }
}
