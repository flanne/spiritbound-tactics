﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Turn
{
	public Unit actor;
	public bool hasUnitMoved;
	public bool hasUnitActed;
	public bool hasUnitCantriped;
	public bool lockMove;
	public Skill skill;
	public int consumableIndex;
	public List<BoardTile> targets;
	public PlanOfAttack plan;

	BoardTile startTile;
	Direction startDirection;

	public void Change(Unit current)
	{
		actor = current;
		hasUnitMoved = false;
		hasUnitActed = false;
		hasUnitCantriped = false;
		plan = null;
		lockMove = false;
		startTile = actor.tile;
		startDirection = actor.direction;
	}

	public void UndoMove()
	{
		hasUnitMoved = false;
		actor.Place(startTile);
		actor.direction = startDirection;
		actor.MatchToTile();
	}

	public void NewStartTile()
    {
		startTile = actor.tile;
    }
}