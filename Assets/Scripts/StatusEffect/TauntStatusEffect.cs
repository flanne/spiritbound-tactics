﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TauntStatusEffect : StatusEffect
{
    protected override string nameID { get { return "KEYWORD_SHORT_TAUNTED"; } }
    public override bool isBuff { get { return false; } }
    public override string spriteFileName { get { return "icon_sts_taunted"; } }
    public override Keyword keyword { get { return Resources.Load<Keyword>("Taunt"); } }

    protected override void OnApply()
    {
        this.AddObserver(OnTargetCheck, Skill.CanTargetCheck);
        this.AddObserver(OnStatusAdded, Status.AddedNotification);
    }

    protected override void OnRemove()
    {
        this.RemoveObserver(OnTargetCheck, Skill.CanTargetCheck);
        this.RemoveObserver(OnStatusAdded, Status.AddedNotification);
    }

    void OnTargetCheck(object sender, object args)
    {
        var info = args as Info<Unit, BoardTile, BaseException>;
        Unit actor = info.arg0;
        BoardTile targetTile = info.arg1;
        BaseException exc = info.arg2;

        Unit target = targetTile.content?.GetComponent<Unit>();

        if (actor == this.owner && target != this.inflictor)
            exc.FlipToggle();
    }

    void OnStatusAdded(object sender, object args)
    {
        if ((sender as Unit) == inflictor && args is KnockedOutStatusEffect)
            owner.status.Remove(this);
    }
}
