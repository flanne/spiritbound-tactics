﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleTween : UITweener
{
    [SerializeField]
    LeanTweenType easeType = LeanTweenType.linear;

    [SerializeField]
    float startScaleX = 1;
    [SerializeField]
    float startScaleY = 1;

    void Awake()
    {
        this.transform.localScale = new Vector3(startScaleX, startScaleY, 1);
    }

    public override void Show()
    {
        LeanTween.scaleX(this.gameObject, 1, duration).setEase(easeType);
        LeanTween.scaleY(this.gameObject, 1, duration).setEase(easeType);
    }

    public override void Hide()
    {
        LeanTween.scaleX(this.gameObject, startScaleX, duration);
        LeanTween.scaleY(this.gameObject, startScaleY, duration);
    }

    public override void SetOff()
    {
        Hide();
    }
}
