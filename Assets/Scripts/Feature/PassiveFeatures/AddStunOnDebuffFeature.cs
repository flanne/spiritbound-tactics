﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddStunOnDebuffFeature : AddPassiveFeature<StunOnDebuffPassive>
{
    [SerializeField]
    int stunAmount;

    protected override void SetParameters(StunOnDebuffPassive passive)
    {
        passive.stunAmount = this.stunAmount;
    }
}
