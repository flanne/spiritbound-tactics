﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace flanne.UI
{
    public class JobProperties : IUIProperties
    {
        public Job job;

        public JobProperties(Job j)
        {
            job = j;
        }
    }

    public class JobEquippablesWidget : Widget<JobProperties>
    {
        [SerializeField] TMP_Text[] _entries;

        GameStrings gameStrings { get { return MasterSingleton.main.GameStrings; } }

        public override void SetProperties(JobProperties properties)
        {
            Job job = properties.job;

            int index = 0;

            foreach (var armorType in job.usableArmors)
            {
                if (armorType == ArmorType.Cloth)
                    _entries[index].text = gameStrings.Get("ARMORTYPE_CLOTH");
                else if (armorType == ArmorType.Medium)
                    _entries[index].text = gameStrings.Get("ARMORTYPE_MEDIUM");
                else if (armorType == ArmorType.Heavy)
                    _entries[index].text = gameStrings.Get("ARMORTYPE_HEAVY");

                index++;
            }

            foreach (var weaponType in job.usableWeapons)
            {
                if (weaponType == WeaponType.Sword)
                    _entries[index].text = gameStrings.Get("WEAPONTYPE_SWORD");
                else if (weaponType == WeaponType.Dagger)
                    _entries[index].text = gameStrings.Get("WEAPONTYPE_DAGGER");
                else if (weaponType == WeaponType.Axe)
                    _entries[index].text = gameStrings.Get("WEAPONTYPE_AXE");
                else if (weaponType == WeaponType.Spear)
                    _entries[index].text = gameStrings.Get("WEAPONTYPE_SPEAR");
                else if (weaponType == WeaponType.Staff)
                    _entries[index].text = gameStrings.Get("WEAPONTYPE_STAFF");
                else if (weaponType == WeaponType.Bow)
                    _entries[index].text = gameStrings.Get("WEAPONTYPE_BOW");

                index++;
            }

            for (; index < _entries.Length; index++)
                _entries[index].text = "";
        }
    }
}
