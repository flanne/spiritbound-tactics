﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class ManaController : MonoBehaviour
    {
        #region Properties
        public ManaBar manaBar;

        const int maxMana = 12;
        int heroMana = 0;
        int villainMana = 0;
        #endregion

        #region Notifications
        public const string WillCalculateMana = "ManaController.WillCalculateMana";
        #endregion

        #region Monobehaviour
        void Awake()
        {
            manaBar.SetProperties(new IntProperty(heroMana));
        }

        void OnEnable()
        {
            this.AddObserver(OnCanPerformCheck, Skill.CanPerformCheck);
            this.AddObserver(OnDidPerform, Skill.DidPerformNotification);

            this.AddObserver(OnTurnBegan, TurnOrderController.TurnBeganNotification);
        }

        void OnDisable()
        {
            this.RemoveObserver(OnCanPerformCheck, Skill.CanPerformCheck);
            this.RemoveObserver(OnDidPerform, Skill.DidPerformNotification);

            this.RemoveObserver(OnTurnBegan, TurnOrderController.TurnBeganNotification);
        }
        #endregion

        #region Notification Handlers
        void OnCanPerformCheck(object sender, object args)
        {
            var info = args as Info<Unit, BaseException>; // Actor, Exception for whether the unit can cast
            Unit actor = info.arg0;
            BaseException exc = info.arg1;

            // Check for modifiers
            Skill skill = sender as Skill;
            int finalManaCost = skill.manaCost.NotifyModifiers(WillCalculateMana, actor);
            finalManaCost = Mathf.Clamp(finalManaCost, 0, maxMana);

            if (actor.alliance.type == AllianceType.Hero && finalManaCost > heroMana)
                exc.FlipToggle();
            else if (actor.alliance.type == AllianceType.Villain && finalManaCost > villainMana)
                exc.FlipToggle();
        }

        void OnDidPerform(object sender, object args)
        {
            Skill skill = sender as Skill;
            Unit actor = args as Unit;

            // Check for modifiers
            int finalManaCost = skill.manaCost.NotifyModifiers(WillCalculateMana, actor);
            finalManaCost = Mathf.Clamp(finalManaCost, 0, maxMana);

            if (actor.alliance.type == AllianceType.Hero)
            {
                heroMana -= finalManaCost;
                manaBar.SetProperties(new IntProperty(heroMana));
            }
            else if (actor.alliance.type == AllianceType.Villain)
            {
                villainMana -= finalManaCost;
            }
        }

        void OnTurnBegan(object sender, object args)
        {
            Unit unit = sender as Unit;

            if (unit.alliance.type == AllianceType.Hero && heroMana < maxMana)
            {
                heroMana++;
                manaBar.SetProperties(new IntProperty(heroMana));
            }
            else if (unit.alliance.type == AllianceType.Villain && villainMana < maxMana)
                villainMana++;
        }
        #endregion
    }
}
