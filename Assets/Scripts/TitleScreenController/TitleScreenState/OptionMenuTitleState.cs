﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionMenuTitleState : TitleScreenState
{
    public override void Enter()
    {
        optionsMenu.Show();
        optionsMenu.CancelEvent += OnCancel;
    }

    public override void Exit()
    {
        optionsMenu.Hide();
        optionsMenu.CancelEvent -= OnCancel;

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnCancel(object sender, System.EventArgs e)
    {
        owner.ChangeState<TitleMenuState>();
    }
}
