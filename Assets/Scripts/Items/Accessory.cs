﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accessory : Equippable
{
    public override EquipSlots defaultSlots { get { return EquipSlots.Accessory; } }
}
