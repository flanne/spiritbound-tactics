﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;

namespace flanne.UI
{
    public class BindingDisplay : MonoBehaviour
    {
        #region Fields
        [Header("Bindings")]
        public InputActionReference actionRef;

        [ValueDropdown("myBindings")]
        [SerializeField] string bindingId;
        ValueDropdownList<string> myBindings
        {
            get
            {
                var dl = new ValueDropdownList<string>();
                if (actionRef != null)
                {
                    var bindingCount = actionRef.action.bindings.Count;
                    var displayOptions =
                    InputBinding.DisplayStringOptions.DontUseShortDisplayNames | InputBinding.DisplayStringOptions.IgnoreBindingOverrides;

                    for (var i = 0; i < bindingCount; ++i)
                    {
                        string bindingId = actionRef.action.bindings[i].id.ToString();
                        string displayString = actionRef.action.GetBindingDisplayString(i, displayOptions);
                        dl.Add(displayString, bindingId);
                    }
                }
                return dl;
            }
        }

        [SerializeField] InputBinding.DisplayStringOptions displayStringOptions;

        [Header("UI")]
        [SerializeField] Image icon;
        [SerializeField] TMP_Text bindingTMP;
        [SerializeField] TMP_Text labelTMP;
        [SerializeField] Sprite keyboardIcon;
        [SerializeField] Sprite lmbIcon;
        [SerializeField] Sprite rmbIcon;
        #endregion

        #region LifeCycle
        void Start()
        {
            RefreshBindingDisplay();
        }
        #endregion

        #region Public
        public void SetActionReference(InputActionReference actionRef)
        {
            this.actionRef = actionRef;
            RefreshBindingDisplay();
        }

        public void SetLabel(string str)
        {
            labelTMP.text = str;
        }
        #endregion

        #region Private
        void RefreshBindingDisplay()
        {
            string displayString = "";
            string deviceLayoutName = default(string);
            string controlPath = default(string);

            if (actionRef != null)
            {
                var bindingIndex = actionRef.action.bindings.IndexOf(x => x.id.ToString() == bindingId);
                if (bindingIndex != -1)
                    displayString = actionRef.action.GetBindingDisplayString(bindingIndex, out deviceLayoutName, out controlPath, displayStringOptions);
            }

            if (bindingTMP != null && icon != null)
            {
                if (deviceLayoutName == "Keyboard")
                {
                    bindingTMP.text = displayString;
                    icon.sprite = keyboardIcon;
                }
                else if (deviceLayoutName == "Mouse")
                {
                    bindingTMP.text = "";
                    if (controlPath == "leftButton")
                        icon.sprite = lmbIcon;
                    else if (controlPath == "rightButton")
                        icon.sprite = rmbIcon;
                }
            }
        }
        #endregion

        #if UNITY_EDITOR
        protected void OnValidate()
        {
            RefreshBindingDisplay();
        }

        #endif
    }
}
