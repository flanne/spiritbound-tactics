﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCounterPassive : CounterPassive
{
    public override int priority { get { return 0; } }
    public override Skill counterSkill { get { return owner.basicAttack; } }

    Board board { get { return MasterSingleton.main.GameLevel.board; } }

    public override bool CanCounter(Unit actor, Skill skill)
    {
        // Get direction the attack is from (back hits do not counter)
        Facings facing = actor.GetFacing(owner);

        // Check if aggressor is in range to get countered
        List<BoardTile> tilesInRange = counterSkill.GetTilesInRange(board, owner);

        // Do not counter if hit from back or if aggressor is the same alliance
        if (tilesInRange.Contains(actor.tile) && facing != Facings.Back && skill.ContainsTag(SkillTag.Weapon) && owner.alliance.IsMatch(actor.alliance, AITargeting.Foe))
            return true;

        return false;
    }
}
