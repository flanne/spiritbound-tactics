﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillStunPassive : Passive
{
    public SkillTag skillTag;
    public int stunAmount;

    protected override void AddListeners()
    {
        this.AddObserver(OnHit, Skill.HitNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnHit, Skill.HitNotification);
    }

    void OnHit(object sender, object args)
    {
        var info = args as Info<Unit, BoardTile>;
        Unit actor = info.arg0;

        if (actor != owner)
            return;

        BoardTile targetTile = info.arg1;
        Skill skill = sender as Skill;

        if (skill.ContainsTag(skillTag) && targetTile.content != null)
        {
            Unit target = targetTile.content.GetComponent<Unit>();

            if (target != null)
            {
                target.stats[StatTypes.CTR] += stunAmount;

                if (target.stats[StatTypes.CTR] < 0)
                    target.stats[StatTypes.CTR] = 0;
            }
        }
    }
}
