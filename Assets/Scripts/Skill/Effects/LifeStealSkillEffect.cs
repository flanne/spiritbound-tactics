﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeStealSkillEffect : PhysicalDamageSkillEffect
{
	protected override int OnApply(Unit actor, BoardTile target, int power, Skill skill)
	{
		Unit defender = target.content.GetComponent<Unit>();

		// Start with the predicted damage value
		int value = Predict(actor, target, power, skill);

		// Add some random variance
		value = Mathf.FloorToInt(value * UnityEngine.Random.Range(0.9f, 1.1f));

		// Clamp the damage to a range
		value = Mathf.Clamp(value, minDamage, maxDamage);

		// Check modifiers for healing
		var healModInfo = new Info<Unit, Unit, Skill>(actor, actor, skill);
		value = value.NotifyModifiers<Info<Unit, Unit, Skill>>(TweakHealNotification, this, healModInfo);

		// Notification about the healing
		var willHealInfo = new Info<Unit, Unit, int>(actor, actor, -1 * value);
		this.PostNotification(WillHealNotification, willHealInfo);

		// Heal the user
		actor.stats[StatTypes.HP] -= value;
		return value;
	}
}