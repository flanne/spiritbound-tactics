﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public struct SpawnData
{
    [VerticalGroup("Placement")]
    [LabelText("Spawned")]
    [LabelWidth(50)]
    [HorizontalGroup("Placement/Bool")]
    [ToggleLeft]
    public bool spawnedOnInitialize;

    [VerticalGroup("Placement")]
    [LabelText("Non-Combat")]
    [LabelWidth(50)]
    [HorizontalGroup("Placement/Bool")]
    [ToggleLeft]
    public bool nonCombat;

    [VerticalGroup("Placement")]
    [LabelText("Start Anim")]
    [LabelWidth(60)]
    [ShowIf("nonCombat", true)]
    public UnitAnimState startingState;

    [VerticalGroup("Placement")]
    [LabelText("CutsceneOnly")]
    [LabelWidth(50)]
    [ToggleLeft]
    public bool cutsceneOnly;

    [VerticalGroup("Placement")]
    [HideLabel]
    public Vector2Int location;

    [VerticalGroup("Placement")]
    [EnumToggleButtons]
    [LabelText("Dir")]
    [LabelWidth(20)]
    public Direction direction;
}
