﻿using UnityEngine;
using System.Collections;

public class StatModifierFeature : Feature
{
	#region Fields / Properties
	public StatTypes type;
	public int amount;
	#endregion

	#region Protected
	protected override void OnApply(Unit target)
	{
		target.stats[type] += amount;
	}

	protected override void OnRemove(Unit target)
	{
		target.stats[type] -= amount;
	}
	#endregion
}