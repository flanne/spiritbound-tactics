﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperPassive : Passive
{
    const float multiplier = 0.05f;

    protected override void AddListeners()
    {
        this.AddObserver(OnDealingDamage, DamageSkillEffect.TweakDamageNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnDealingDamage, DamageSkillEffect.TweakDamageNotification);
    }

    void OnDealingDamage(object sender, object args)
    {
        var info = args as Info<List<ValueModifier>, Info<Unit, Unit, Skill>>;
        Unit actor = info.arg1.arg0;
        Unit defender = info.arg1.arg1;

        int distance = Mathf.Abs(actor.tile.xy.x - defender.tile.xy.x) + Mathf.Abs(actor.tile.xy.y - defender.tile.xy.y);
        float totalMultiplier = 1 + (Mathf.Max(0, (distance - 1)) * multiplier);

        info.arg0.Add(new MultValueModifier(1, totalMultiplier));
    }
}
