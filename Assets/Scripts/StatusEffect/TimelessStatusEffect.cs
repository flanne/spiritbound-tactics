﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spiritlink.Battle;

public class TimelessStatusEffect : StatusEffect
{
    protected override string nameID { get { return ""; } }
    public override bool isBuff { get { return true; } }
    public override string spriteFileName { get { return ""; } }
    public override Keyword keyword { get { return null; } }

    protected override void OnApply()
    {
        this.AddObserver(OnPerformSkill, Skill.DidPerformNotification);
        this.AddObserver(OnEndTurn, AfterTurnState.EndOfTurnEffectsNotification, owner);
    }

    protected override void OnRemove()
    {
        this.RemoveObserver(OnPerformSkill, Skill.DidPerformNotification);
        this.RemoveObserver(OnEndTurn, AfterTurnState.EndOfTurnEffectsNotification, owner);
    }

    void OnPerformSkill(object sender, object args)
    {
        Unit unit = args as Unit;
        unit.stats[StatTypes.CTR] = 0;
    }

    void OnEndTurn(object sender, object args)
    {
        owner.stats[StatTypes.CTR] = 0;
    }
}
