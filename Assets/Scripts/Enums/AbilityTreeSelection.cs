﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AbilityTreeSelection 
{
    None,
    Left,
    Right
}