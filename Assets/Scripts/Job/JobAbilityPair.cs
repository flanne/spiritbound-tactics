﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class JobAbilityPair
{
    public JobAbility left { get { return _left; } }
    [SerializeField]
    JobAbility _left;

    public JobAbility right { get { return _right; } }
    [SerializeField]
    JobAbility _right;
}
