﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAnimator : MonoBehaviour
{
    [SerializeField]
    Animator unitAnimator;

    [SerializeField]
    Animator weaponAnimator;

    [SerializeField]
    SpriteRenderer spriteRenderer;

    [SerializeField]
    Unit myUnit;

    bool isPlaying = false;

    void Update()
    {
        if (unitAnimator.GetCurrentAnimatorStateInfo(0).IsName("f_idle") || unitAnimator.GetCurrentAnimatorStateInfo(0).IsName("b_idle"))
        {
            isPlaying = false;
            spriteRenderer.enabled = false;
        }
        else if (!isPlaying && unitAnimator.GetCurrentAnimatorStateInfo(0).IsName("f_attack"))
        {
            Play("f_swing");
        }
        else if (!isPlaying && unitAnimator.GetCurrentAnimatorStateInfo(0).IsName("b_attack"))
        {
            Play("b_swing");
        }
        else if (!isPlaying && unitAnimator.GetCurrentAnimatorStateInfo(0).IsName("f_bow"))
        {
            Play("f_bow");
        }
        else if (!isPlaying && unitAnimator.GetCurrentAnimatorStateInfo(0).IsName("b_bow"))
        {
            Play("b_bow");
        }
        else if (!isPlaying && unitAnimator.GetCurrentAnimatorStateInfo(0).IsName("f_spear"))
        {
            Play("f_spear");
        }
        else if (!isPlaying && unitAnimator.GetCurrentAnimatorStateInfo(0).IsName("b_spear"))
        {
            Play("b_spear");
        }
    }

    public void Play(string animationName)
    {
        isPlaying = true;
        spriteRenderer.enabled = true;

        if (myUnit.direction == Direction.Up || myUnit.direction == Direction.Left)
            spriteRenderer.flipX = false;
        else
            spriteRenderer.flipX = true;

        AnimatorStateInfo animationState = unitAnimator.GetCurrentAnimatorStateInfo(0);

        weaponAnimator.Play(animationName, 0, animationState.normalizedTime);
    }
}
