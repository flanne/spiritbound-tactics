﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spiritlink.Battle
{
    public class BeforeTurnState : BattleState
    {
        #region Notifications
        public const string StartOfTurnEffectsNotification = "BeforeTurnState.StartOfTurnEffectsNotification";
        #endregion

        public override void Enter()
        {
            owner.cameraRig.follow = turn.actor.transform;

            StartCoroutine(GetEffects());
        }

        IEnumerator GetEffects()
        {
            yield return null;

            while (!owner.cameraRig.ReachedFollowPosition())
                yield return null;

            yield return PlayAutoEffects(StartOfTurnEffectsNotification);


            // check if actor got knocked out
            if (turn.actor.IsKnockedOut())
            {
                // check if battle is over
                if (IsBattleOver())
                    owner.ChangeState<EndRewardLootState>();
                else
                    owner.ChangeState<SelectUnitState>();
            }
            else
            {
                // check for tutorial
                if (MasterSingleton.main.TutorialShown.mana == false && turn.actor.uniqueID.id == IDEnum.MC)
                {
                    owner.ChangeState<ManaTutorialState>();
                    MasterSingleton.main.TutorialShown.mana = true;
                }
                else
                    owner.ChangeState<ActionSelectState>();
            }
        }
    }
}