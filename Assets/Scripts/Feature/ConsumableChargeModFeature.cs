﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumableChargeModFeature : Feature
{
	[SerializeField]
	int amount;

	protected override void OnApply(Unit target)
	{
		target.consumableSkills.maxChargeMod += amount;
	}

	protected override void OnRemove(Unit target)
	{
		target.consumableSkills.maxChargeMod -= amount;
	}
}
