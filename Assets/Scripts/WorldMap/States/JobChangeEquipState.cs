﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class JobChangeEquipState : WorldMapState
{
    public override void Enter()
    {
        jobChangeEquipPrompt.Show();
        playerInput.currentActionMap.FindAction("AnyButton").performed += OnAnyKey;
    }

    public override void Exit()
    {
        jobChangeEquipPrompt.Hide();
        jobChangeController.Hide();
    }

    void OnAnyKey(InputAction.CallbackContext obj)
    {
        playerInput.currentActionMap.FindAction("AnyButton").performed -= OnAnyKey;
        StartCoroutine(WaitToExitState());
    }

    IEnumerator WaitToExitState()
    {
        // Wait frame to make sure input doesn't carry
        yield return null;
        owner.ChangeState<JobManageState>();

        audioPlayer.Play(owner.confirmSFX);
    }
}
