﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnUnitCutsceneAction : UnitCutsceneAction
{
    [SerializeField]
    Direction entranceFrom;

    [SerializeField]
    Vector2Int tilePos;

    [SerializeField]
    Direction facing;

    Board board { get { return MasterSingleton.main.GameLevel.board; } }

    public override void Play()
    {
        StartCoroutine(SpawnUnit(unit));
    }

    IEnumerator SpawnUnit(Unit unit)
    {
        isPlaying = true;

        unit.Place(board.GetTile(tilePos));
        unit.MatchToTile();
        unit.direction = facing;

        // Set unit position depending on entrance direction
        Vector3 offset;
        if (entranceFrom == Direction.Up)
            offset = board.CellToWorld(new Vector3Int(0, 1, 0));
        else if(entranceFrom == Direction.Down)
            offset = board.CellToWorld(new Vector3Int(0, -1, 0));
        else if (entranceFrom == Direction.Left)
            offset = board.CellToWorld(new Vector3Int(-1, 0, 0));
        else // Direction.Right
            offset = board.CellToWorld(new Vector3Int(1, 0, 0));

        unit.transform.position += offset;

        // Tweak color
        LeanTween.color(unit.gameObject, Color.black, 0f);
        LeanTween.alpha(unit.gameObject, 1f, 0.3f);
        int leanTweenID = LeanTween.color(unit.gameObject, Color.white, 0.3f).id;

        while (LeanTween.isTweening(leanTweenID))
            yield return null;

        unit.UnHide();
        isPlaying = false;
    }
}