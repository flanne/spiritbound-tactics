﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddSilenceOnBackstabFeature : AddPassiveFeature<SilenceOnBackstabPassive>
{
    [SerializeField]
    [Range(0, 1)]
    float chanceToHit;

    [SerializeField]
    int turnDuration;

    protected override void SetParameters(SilenceOnBackstabPassive passive)
    {
        passive.chanceToHit = this.chanceToHit;
        passive.turnDuration = this.turnDuration;
    }
}

