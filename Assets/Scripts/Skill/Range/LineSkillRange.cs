﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineSkillRange : BaseSkillRange
{
	public override bool directionOriented { get { return true; } }

	public override List<BoardTile> GetTilesInRange(Board board, Unit unit, int horizontal, int vertical)
	{
		Vector2Int startPos = unit.tile.xy;
		Vector2Int endPos;
		List<BoardTile> retValue = new List<BoardTile>();

		switch (unit.direction)
		{
			case Direction.Up:
				endPos = new Vector2Int(startPos.x, board.max.y);
				break;
			case Direction.Right:
				endPos = new Vector2Int(board.max.x, startPos.y);
				break;
			case Direction.Down:
				endPos = new Vector2Int(startPos.x, board.min.y);
				break;
			default: // Left
				endPos = new Vector2Int(board.min.x, startPos.y);
				break;
		}

		int dist = 0;
		while (startPos != endPos)
		{
			if (startPos.x < endPos.x) startPos.x++;
			else if (startPos.x > endPos.x) startPos.x--;

			if (startPos.y < endPos.y) startPos.y++;
			else if (startPos.y > endPos.y) startPos.y--;

			if (board.HasTile(startPos))
			{
				BoardTile t = board.GetTile(startPos);
				if (Mathf.Abs(t.z - unit.tile.z) <= vertical)
					retValue.Add(t);

				dist++;
				if (dist >= horizontal)
					break;
			}
		}

		return retValue;
	}
}