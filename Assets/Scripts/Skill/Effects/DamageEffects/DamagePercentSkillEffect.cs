﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePercentSkillEffect : DamageSkillEffect
{
	public override int GetAttack(Unit actor, Unit targetUnit)
	{
		return targetUnit.stats[StatTypes.MHP];
	}

	public override int GetDefence(Unit actor, Unit targetUnit)
	{
		return 0;
	}
}

