﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace flanne.UI
{
    public class DirectionArrowController : MonoBehaviour
    {
        [SerializeField] Panel panel;
        [SerializeField] Image arrowImage;
        [SerializeField] Sprite upArrow, downArrow;

        public void SetDirection(Direction dir)
        {
            if (dir == Direction.Up || dir == Direction.Left)
                arrowImage.transform.localScale = new Vector3(1, 1, 1);
            else
                arrowImage.transform.localScale = new Vector3(-1, 1, 1);

            if (dir == Direction.Down || dir == Direction.Left)
                arrowImage.sprite = downArrow;
            else
                arrowImage.sprite = upArrow;
        }

        public void Show()
        {
            panel.Show();
        }

        public void Hide()
        {
            panel.Hide();
        }
    }
}
