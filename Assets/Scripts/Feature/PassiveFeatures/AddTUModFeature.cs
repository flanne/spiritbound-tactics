﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddTUModFeature : AddPassiveFeature<TUCostModPassive>
{
    [SerializeField] int amount;
    [SerializeField] SkillTag skillTag;

    protected override void SetParameters(TUCostModPassive passive)
    {
        passive.amount = this.amount;
        passive.skillTag = this.skillTag;
    }
}
