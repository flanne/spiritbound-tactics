﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace flanne.UI
{
    public class AbilityIconWidget : DescribableIconWidget
    {
        [SerializeField] GameObject iconTransform;
        [SerializeField] Image iconHighlight;
        [SerializeField] Image lockIcon;
        [SerializeField] Image glare;
        [SerializeField] Image whiteImage;

        /*public int abilityRank { get { return _abilityRank; } }
        [SerializeField]
        int _abilityRank;

        public AbilityTreeSelection side { get { return _side; } }
        [SerializeField]
        AbilityTreeSelection _side;*/

        int tweenID;

        public void Activate()
        {
            iconHighlight.enabled = true;
            icon.color = new Color32(255, 255, 255, 255);
        }

        public void Deactivate()
        {
            iconHighlight.enabled = false;
            icon.color = new Color32(155, 155, 155, 255);
        }

        public void SetGlare(bool isGlaring)
        {
            glare.enabled = isGlaring;
        }

        public void Lock()
        {
            lockIcon.enabled = true;
            icon.color = new Color32(75, 75, 75, 255);
        }

        public void Reset()
        {
            lockIcon.enabled = false;
            glare.enabled = false;
            Deactivate();
        }

        public void PlayClickFeedback()
        {
            if (!LeanTween.isTweening(tweenID))
            {
                tweenID = LeanTween.scale(iconTransform, new Vector3(1.1f, 1.1f, 1.1f), 0.1f).setLoopPingPong(1).id;
                StartCoroutine(Flash());
            }
        }

        IEnumerator Flash()
        {
            whiteImage.enabled = true;
            yield return new WaitForSeconds(0.1f);
            whiteImage.enabled = false;
        }
    }
}
