﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeMusicCutsceneAction : CutsceneAction
{
    [SerializeField]
    float fadeTime = 0.5f;

    public override void Play()
    {
        MasterSingleton.main.AudioPlayer.FadeOutMusic(fadeTime);
    }

    /*IEnumerator FadeOutMusicCoroutine()
    {
        isPlaying = true;
        MasterSingleton.main.AudioPlayer.FadeOutMusic(fadeTime);
        yield return new WaitForSeconds(fadeTime);

        yield return null;

        isPlaying = false;
    }*/

}
