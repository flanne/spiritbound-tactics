﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace flanne.UI
{
    public class EquipmentCompareUI : MonoBehaviour
    {
        [SerializeField] TMP_Text mhpCurrent, mhpNew, patkCurrent, patkNew, matkCurrent, matkNew, pdefCurrent, pdefNew, mdefCurrent, mdefNew, movCurrent, movNew, evdCurrent, evdNew;

        [SerializeField] Color worseStatColor, betterStatColor, neutralStatColor;

        Unit unit;

        public void SetUnit(Unit u)
        {
            unit = u;

            mhpCurrent.text = unit.stats[StatTypes.MHP].ToString();
            mhpNew.text = unit.stats[StatTypes.MHP].ToString();

            patkCurrent.text = unit.stats[StatTypes.ATK].ToString();
            patkNew.text = unit.stats[StatTypes.ATK].ToString();

            matkCurrent.text = unit.stats[StatTypes.MAT].ToString();
            matkNew.text = unit.stats[StatTypes.MAT].ToString();

            pdefCurrent.text = unit.stats[StatTypes.DEF].ToString();
            pdefNew.text = unit.stats[StatTypes.DEF].ToString();

            mdefCurrent.text = unit.stats[StatTypes.MDF].ToString();
            mdefNew.text = unit.stats[StatTypes.MDF].ToString();

            movCurrent.text = unit.stats[StatTypes.MOV].ToString();
            movNew.text = unit.stats[StatTypes.MOV].ToString();

            evdCurrent.text = unit.stats[StatTypes.EVD].ToString();
            evdNew.text = unit.stats[StatTypes.EVD].ToString();
        }

        public void CompareToEquippable(Equippable equippable, EquipSlots equipSlots)
        {
            CompareStat(equipSlots, equippable, StatTypes.MHP, mhpNew);

            CompareStat(equipSlots, equippable, StatTypes.ATK, patkNew);

            CompareStat(equipSlots, equippable, StatTypes.MAT, matkNew);

            CompareStat(equipSlots, equippable, StatTypes.DEF, pdefNew);

            CompareStat(equipSlots, equippable, StatTypes.MDF, mdefNew);

            CompareStat(equipSlots, equippable, StatTypes.MOV, movNew);

            CompareStat(equipSlots, equippable, StatTypes.EVD, evdNew);
        }

        void CompareStat(EquipSlots equipSlots, Equippable newEquippable, StatTypes stat, TMP_Text newText)
        {
            int currentTotalStat = unit.stats[stat];

            int currentEquipStat = 0;
            if (unit.equipment.GetItem(equipSlots) != null)
                currentEquipStat = unit.equipment.GetItem(equipSlots)[stat];

            int newTotalStat = (currentTotalStat - currentEquipStat) + newEquippable[stat];

            newText.text = newTotalStat.ToString();

            // SetColor Depending of if improve stat or not
            if (newTotalStat > currentTotalStat)
                newText.color = betterStatColor;
            else if (newTotalStat < currentTotalStat)
                newText.color = worseStatColor;
            else
                newText.color = neutralStatColor;
        }
    }
}
