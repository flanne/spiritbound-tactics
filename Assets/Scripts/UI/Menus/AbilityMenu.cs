﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace flanne.UI
{
    public class AbilityMenu : Menu
    {
        [SerializeField] bool isTree;
        [ShowIf("isTree")]
        [SerializeField] Image[] arrowsToAbilities;

        public void Activate(int index)
        {
            var widget = entries[index].GetComponent<AbilityIconWidget>();
            widget?.Activate();

            if (isTree)
                arrowsToAbilities[index].enabled = true;
        }

        public void Deactivate(int index)
        {
            var widget = entries[index].GetComponent<AbilityIconWidget>();
            widget?.Deactivate();

            if (isTree)
                arrowsToAbilities[index].enabled = false;
        }

        public void SetGlare(int index, bool isGlare)
        {
            var widget = entries[index].GetComponent<AbilityIconWidget>();
            widget?.SetGlare(isGlare);
        }

        public override void Lock(int index)
        {
            var widget = entries[index].GetComponent<AbilityIconWidget>();
            widget?.Lock();
        }

        public void ResetAll()
        {
            for (int i = 0; i < entries.Count; i++)
            {
                var widget = entries[i].GetComponent<AbilityIconWidget>();
                widget?.Reset();

                if (isTree)
                    arrowsToAbilities[i].enabled = false;
            }
        }
    }
}
