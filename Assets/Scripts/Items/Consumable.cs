﻿using UnityEngine;
using System.Collections;

public class Consumable : Equippable
{
    [SerializeField]
    int maxCharge;

    public override EquipSlots defaultSlots { get { return EquipSlots.Consumable1; } }

    public Skill skill { get { return _skill; } }
    [SerializeField]
    Skill _skill;

    public override string GetDescription()
    {
        string description = "";
        description += maxCharge + " <color=#eec39a>Use(s)</color> ";
        description += skill.timeCost + " <color=#eec39a>TU</color> \n";
        description += MasterSingleton.main.GameStrings.Get(descriptionStringID);

        return description;
    }

    public string GetDescription(Unit actor)
    {
        string description = "";
        description += maxCharge + " <color=#eec39a>Use(s)</color> ";

        int finalTimeCost = skill.GetTimeCost(actor);
        if (finalTimeCost == skill.timeCost)
            description += skill.GetTimeCost(actor) + " <color=#eec39a>TU</color> \n";
        else
            description += " <color=#fbf236>" + skill.GetTimeCost(actor) + "</color>" + " <color=#eec39a>TU</color> \n";

        description += MasterSingleton.main.GameStrings.Get(descriptionStringID);

        return description;
    }

    public override void OnEquip(Unit unit)
	{
        unit.consumableSkills.AddConsumable(this, maxCharge);
	}

	public override void OnUnEquip(Unit unit)
	{
        unit.consumableSkills.RemoveConsumable(this);
    }
}