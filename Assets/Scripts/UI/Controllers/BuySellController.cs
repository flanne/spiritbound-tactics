﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace flanne.UI
{
    public class BuySellController : MonoBehaviour
    {
        #region Fields
        public EventHandler<InfoEventArgs<Equippable>> ClickEvent;
        public EventHandler<InfoEventArgs<Equippable>> SelectEvent;

        [SerializeField] ItemStackMenu itemStackMenu;
        [SerializeField] Panel panel;

        [System.NonSerialized] public bool isSelling = false;

        Equippable[] _allItems;
        List<Equippable> _filteredItems;

        enum Filter
        {
            Weapon,
            Offhand,
            Armor,
            Accessory,
            Consumable
        }

        Filter currentFilter = Filter.Weapon;
        #endregion

        #region Eventhandlers
        void OnClick(object sender, InfoEventArgs<int> e)
        {
            if (ClickEvent != null)
                ClickEvent(this, new InfoEventArgs<Equippable>(_filteredItems[e.info]));
        }

        void OnSelect(object sender, InfoEventArgs<int> e)
        {
            if (SelectEvent != null)
                SelectEvent(this, new InfoEventArgs<Equippable>(_filteredItems[e.info]));
        }
        #endregion

        #region Callbacks
        public void FilterWeapons()
        {
            _filteredItems = new List<Equippable>();
            foreach (var i in _allItems)
            {
                if (i is Weapon)
                    _filteredItems.Add(i);
            }

            currentFilter = Filter.Weapon;
            Refresh();
        }

        public void FilterOffhands()
        {
            _filteredItems = new List<Equippable>();
            foreach (var i in _allItems)
            {
                if (i is OffHand)
                    _filteredItems.Add(i);
            }


            currentFilter = Filter.Offhand;
            Refresh();
        }

        public void FilterArmors()
        {
            _filteredItems = new List<Equippable>();
            foreach (var i in _allItems)
            {
                if (i.defaultSlots == EquipSlots.Body)
                    _filteredItems.Add(i);
            }

            currentFilter = Filter.Armor;
            Refresh();
        }

        public void FilterAccessories()
        {
            _filteredItems = new List<Equippable>();
            foreach (var i in _allItems)
            {
                if (i.defaultSlots == EquipSlots.Accessory)
                    _filteredItems.Add(i);
            }

            currentFilter = Filter.Accessory;
            Refresh();
        }

        public void FilterConsumables()
        {
            _filteredItems = new List<Equippable>();
            foreach (var i in _allItems)
            {
                if (i is Consumable)
                    _filteredItems.Add(i);
            }

            currentFilter = Filter.Consumable;
            Refresh();
        }
        #endregion

        #region Public
        public void SetToItems(Equippable[] items)
        {
            _allItems = items;

            FilterType(currentFilter);
            SelectFirstAvailable();
        }

        public void SelectFirstAvailable()
        {
            itemStackMenu.SelectFirstAvailable();
        }

        public void Show()
        {
            if (panel != null)
                panel.Show();

            if (itemStackMenu != null)
                itemStackMenu.Show();

            itemStackMenu.ClickEvent += OnClick;
            itemStackMenu.SelectEvent += OnSelect;
        }

        public void Hide()
        {
            if (panel != null)
                panel.Hide();

            if (itemStackMenu != null)
                itemStackMenu.Hide();

            itemStackMenu.ClickEvent -= OnClick;
            itemStackMenu.SelectEvent -= OnSelect;
        }
        #endregion

        #region Private
        void Refresh()
        {
            itemStackMenu.Clear();

            foreach (var i in _filteredItems)
            {
                ItemStack itemStack = MasterSingleton.main.Inventory.itemList.Find(x => x.item == i);
                if (itemStack == null)
                {
                    itemStack = new ItemStack(i, 0);
                    itemStack.inUse = 0;
                    itemStack.total = 0;
                }

                itemStackMenu.AddEntry(new ItemStackProperties(itemStack, isSelling));
            }

            if (SelectEvent != null)
                SelectEvent(this, new InfoEventArgs<Equippable>(null));
        }

        void FilterType(Filter filter)
        {
            if (filter == Filter.Weapon)
                FilterWeapons();
            else if (filter == Filter.Offhand)
                FilterOffhands();
            else if (filter == Filter.Armor)
                FilterArmors();
            else if (filter == Filter.Accessory)
                FilterAccessories();
            else if (filter == Filter.Consumable)
                FilterConsumables();
        }
        #endregion
    }
}
