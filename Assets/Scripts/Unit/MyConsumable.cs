﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyConsumable
{
	public Consumable consumable { get { return _consumable; } }
	Consumable _consumable;
	public Skill consumableSkill { get { return consumable.skill; } }
	public int charge { get; private set; }
	public int maxCharge { get; private set; }

	public MyConsumable(Consumable consumable, int charge)
	{
		this._consumable = consumable;
		this.charge = charge;
		this.maxCharge = charge;
	}

	public void Use()
	{
		this.charge--;
		if (this.charge < 0)
			this.charge = 0;
	}

	public void Fill()
    {
		charge = maxCharge;
	}

	public void ModifyMaxCharge(int amount)
	{
		maxCharge += amount;
		maxCharge = Mathf.Max(maxCharge, 1);

		charge += amount;
		charge = Mathf.Max(maxCharge, 0);
	}
}
