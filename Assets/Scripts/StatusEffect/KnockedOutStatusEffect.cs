﻿using UnityEngine;
using System.Collections;
using Spiritlink.Battle;

public class KnockedOutStatusEffect : StatusEffect
{
    protected override string nameID { get { return ""; } } // No visual for this effect
    public override bool isBuff { get { return false; } }
    public override string spriteFileName { get { return ""; } }
    public override Keyword keyword { get { return null; } }

    protected override void OnApply()
    {
        // Remove from board
        owner.tile.content = null;

        // Add Observers
        this.AddObserver(OnTurnCheck, TurnOrderController.TurnCheckNotification, owner);
        this.AddObserver(OnStatCounterWillChange, Stats.WillChangeNotification(StatTypes.CTR), owner.stats);
    }

    protected override void OnRemove()
    {
        this.RemoveObserver(OnTurnCheck, TurnOrderController.TurnCheckNotification, owner);
        this.RemoveObserver(OnStatCounterWillChange, Stats.WillChangeNotification(StatTypes.CTR), owner.stats);
    }

    void OnTurnCheck(object sender, object args)
    {
        // Dont allow a KO'd unit to take turns
        BaseException exc = args as BaseException;
        if (exc.defaultToggle == true)
            exc.FlipToggle();
    }

    void OnStatCounterWillChange(object sender, object args)
    {
        // Dont allow a KO'd unit to increment the turn order counter
        ValueChangeException exc = args as ValueChangeException;
        if (exc.toValue < exc.fromValue)
            exc.FlipToggle();
    }
}