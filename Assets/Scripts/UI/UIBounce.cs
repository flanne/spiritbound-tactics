﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBounce : MonoBehaviour
{
    [SerializeField]
    float magnitude = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        LeanTween.moveLocalY(this.gameObject, this.transform.localPosition.y + magnitude, 0.5f).setLoopPingPong().setEase(LeanTweenType.easeInOutQuad);
    }
}
