﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class DescribableObject : SerializableScriptableObject
{
    public virtual Sprite icon { get { return _icon; } }
    [HorizontalGroup("Description", 50)]
    [PreviewField(50)]
    [HideLabel]
    [SerializeField]
    Sprite _icon;

    public virtual string nameStringID { get { return _nameStringID; } }
    [VerticalGroup("Description/Basic Info")]
    [LabelWidth(100)]
    [SerializeField]
    string _nameStringID;

    [VerticalGroup("Description/Basic Info")]
    [LabelWidth(100)]
    [SerializeField]
    protected string descriptionStringID;

    public Keyword[] keywords { get { return _keywords; } }
    [SerializeField]
    Keyword[] _keywords;

    public string fullDescription { get { return GetDescription(); } }

    public virtual string GetDescription()
    {
        return MasterSingleton.main.GameStrings.Get(descriptionStringID);
    }
}
