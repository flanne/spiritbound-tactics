﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spiritlink.Battle;

public class UnitTurnCutsceneTrigger : CutsceneTrigger
{
    [SerializeField] IDEnum unitID;

    void Start()
    {
        this.AddObserver(OnUnitTurnStart, SelectUnitState.TurnWillStartNotification);
    }

    void OnDestroy()
    {
        this.RemoveObserver(OnUnitTurnStart, SelectUnitState.TurnWillStartNotification);
    }

    void OnUnitTurnStart(object sender, object args)
    {
        if (UnitID.GetUnitWithID(unitID) == sender as Unit)
        {
            Trigger();
            this.RemoveObserver(OnUnitTurnStart, SelectUnitState.TurnWillStartNotification);
        }
    }
}
