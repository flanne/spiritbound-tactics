﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddStunOnHighHitFeature : AddPassiveFeature<StunOnHighHit>
{
    [SerializeField]
    int stunAmount;

    protected override void SetParameters(StunOnHighHit passive)
    {
        passive.stunAmount = this.stunAmount;
    }
}
