﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StatusOnHitPassive<T> : Passive where T : StatusEffect
{
    public float chanceToHit = 1;
    public int turnDuration;

    protected override void AddListeners()
    {
        this.AddObserver(OnHit, BaseSkillEffect.HitNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnHit, BaseSkillEffect.HitNotification);
    }

    protected virtual bool ConditionsMet(object sender, Info<Unit, Unit, int, Skill> info)
    {
        return true;
    }

    void OnHit(object sender, object args)
    {
        var info = args as Info<Unit, Unit, int, Skill>;
        Unit actor = info.arg0;
        Unit defender = info.arg1;

        if (actor != owner)
            return;

        if (Random.Range(0.01f, 1.0f) <= chanceToHit && ConditionsMet(sender, info))
        {
            if (turnDuration <= 0)
            {
                defender.status.Add<T, StatusCondition>();
            }
            else
            {
                TurnStatusCondition statusCondition = defender.status.Add<T, TurnStatusCondition>(owner);

                int finalTurns = turnDuration;
                var sendInfo = new Info<Unit, Unit>(actor, defender);
                finalTurns = finalTurns.NotifyModifiers<Info<Unit, Unit>>(StatusCondition.TweakStatusDurationNotification, this, sendInfo);

                statusCondition.turns = finalTurns;
            }
        }
    }
}
