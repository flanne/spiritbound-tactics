﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeModPassive : Passive
{
    public SkillTag skillTag;
    public int amount;

    protected override void AddListeners()
    {
        this.AddObserver(OnCalculateRange, SkillRangeData.ModifySkillRangeNotification, owner);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnCalculateRange, SkillRangeData.ModifySkillRangeNotification, owner);
    }

    void OnCalculateRange(object sender, object args)
    {
        var info = args as Info<List<ValueModifier>, Skill>;
        Skill skill = info.arg1;

        if (skill.ContainsTag(skillTag))
            info.arg0.Add(new AddValueModifier(0, amount));
    }
}

