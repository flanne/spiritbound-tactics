﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMusicCutsceneAction : CutsceneAction
{
    [SerializeField]
    AudioClip musicClip;

    [SerializeField] float fadeInTime = 0.5f;

    public override void Play()
    {
        MasterSingleton.main.AudioPlayer.PlayMusic(musicClip, fadeInTime);
    }
}
