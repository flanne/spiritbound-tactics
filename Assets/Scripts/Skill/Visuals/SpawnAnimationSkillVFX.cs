﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAnimationSkillVFX : SpawnGameObjectSkillVFX
{
    protected override bool DespawnCondition(GameObject obj)
    {
        Animator animator = obj.GetComponent<Animator>();

        if (animator?.GetCurrentAnimatorStateInfo(0).normalizedTime < 1 && !animator.IsInTransition(0))
            return false;

        return true;
    }
}
