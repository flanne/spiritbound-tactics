﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    #region Fields/Properties
    public List<ItemStack> itemList { get; private set; }
    public uint money { get; private set; }
    #endregion

    #region EventHandlers
    void OnEquip(object sender, object args)
    {
        Equipment e = sender as Equipment;
        if (e.unit.alliance.type != AllianceType.Hero)
            return;

        Equippable item = args as Equippable;
        ItemStack itemStack = itemList.Find(x => x.item == item);

        if (itemStack == null || ((itemStack.inUse) >= itemStack.total))
            return;

        itemStack.inUse += 1;
    }

    void OnUnEquip(object sender, object args)
    {
        Equipment e = sender as Equipment;
        if (e.unit.alliance.type != AllianceType.Hero)
            return;

        Equippable item = args as Equippable;
        ItemStack itemStack = itemList.Find(x => x.item == item);

        if (itemStack == null || (int)(itemStack.inUse - 1) < 0)
            return;

        itemStack.inUse -= 1;
    }
    #endregion

    #region LifeCycle
    void Awake()
    {
        money = 999999;
        itemList = new List<ItemStack>();
        this.AddObserver(OnEquip, Equipment.EquippedNotification);
        this.AddObserver(OnUnEquip, Equipment.UnEquippedNotification);
    }
    #endregion

    #region Public
    public void AcquireMoney(uint amount)
    {
        money += amount;
    }

    public bool IsAvailable(Equippable item)
    {
        ItemStack itemStack = itemList.Find(x => x.item == item);

        if (itemStack != null && (int)(itemStack.inUse) < (int)(itemStack.total))
            return true;

        return false;
    }

    public bool Buy(Equippable item, uint amount = 1)
    {
        // Check if can afford
        uint totalCost = item.cost * amount;
        if (money < totalCost)
            return false;

        money -= totalCost;
        Add(item, amount);

        return true;
    }

    public bool Sell(Equippable item, uint amount = 1)
    {
        // Sell for half of buy cost
        if (Remove(item, amount))
        {
            money += (item.cost * amount) / 2;
            return true;
        }
        else 
            return false;
    }

    public ItemStack Add(Equippable item, uint amount = 1)
    {
        ItemStack itemStack = itemList.Find(x => x.item == item);

        // if item stack of this item is not in inventory, make a new item stack, otherwise add to total of existing item stack
        if (itemStack == null)
        {
            itemStack = new ItemStack(item, amount);
            itemList.Add(itemStack);
        }
        else
            itemStack.total += amount;

        return itemStack;
    }

    public ItemStack AddAndEquip(Equippable item, uint amount = 1)
    {
        ItemStack i = Add(item, amount);
        i.inUse++;

        return i;
    }

    public void InitNew()
    {
        itemList = new List<ItemStack>();
        money = 1000;
    }

    public void SetInventory(List<ItemStack> items, uint money)
    {
        itemList = items;
        this.money = money;
    }

    public List<ItemStack> GetItemsOfSlot(EquipSlots slot)
    {
        List<ItemStack> slotItems = new List<ItemStack>();

        foreach (var i in itemList)
        {
            if ((i.item.defaultSlots & slot) == slot)
                slotItems.Add(i);

            foreach (var s in i.item.secondarySlots)
                if ((s & slot) == slot)
                    slotItems.Add(i);
        }

        return slotItems;
    }
    #endregion

    #region Private
    bool Remove(Equippable item, uint amount = 1)
    {
        ItemStack itemStack = itemList.Find(x => x.item == item);

        // Check if item stack of this item does not exist or trying to remove items that are in use
        if (itemStack == null || amount > (itemStack.total - itemStack.inUse))
            return false;

        // if all items in the stack is to be removed, then remove the stack from the inventory
        if (amount >= itemStack.total)
            itemList.Remove(itemStack);
        else
            itemStack.total -= amount;

        return true;
    }
    #endregion
}
