﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public class PartyUnitSpawn
{
    public IDEnum unitID { get { return _unitID; } }
    [TableColumnWidth(350, Resizable = false)]
    [VerticalGroup("Unit")]
    [LabelWidth(75)]
    [GUIColor(1f, 0.75f, 0.5f)]
    [SerializeField]
    IDEnum _unitID;

    public SpawnData spawnData { get { return _spawnData; } }
    [VerticalGroup("Unit")]
    [HideLabel]
    [SerializeField]
    SpawnData _spawnData;
}
