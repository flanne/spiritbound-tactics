﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public class SkillRangeData
{
    public const string ModifySkillRangeNotification = "SkillRangeData.ModifySkillRangeNotification";

    enum SkillRangeType
    {
        ConstantNoSelf,
        Constant,
        Cone,
        Infinite,
        Line,
        Self,
        Weapon,
    };

    [LabelWidth(80)]
    [SerializeField]
    private SkillRangeType rangeType;

    [HorizontalGroup("Range"), LabelWidth(70)]
    [HideIf("@rangeType == SkillRangeType.Infinite || rangeType == SkillRangeType.Weapon")]
    [SerializeField]
    private int horizontal = 1;

    [HorizontalGroup("Range"), LabelWidth(70)]
    [HideIf("@rangeType == SkillRangeType.Infinite || rangeType == SkillRangeType.Weapon")]
    [SerializeField]
    private int vertical = int.MaxValue;

    public bool directionOriented { get { return RangeGetters[rangeType].directionOriented; } }
    public bool positionOriented { get { return RangeGetters[rangeType].positionOriented; } }

    public List<BoardTile> GetTilesInRange(Board board, Unit unit, Skill skill)
    {
        int finalSkillRange = horizontal.NotifyModifiers<Skill>(ModifySkillRangeNotification, unit, skill);
        finalSkillRange = Mathf.Max(0, finalSkillRange);//Skill range cannot be negative

        return RangeGetters[rangeType].GetTilesInRange(board, unit, finalSkillRange, vertical);
    }

    private static Dictionary<SkillRangeType, BaseSkillRange> RangeGetters = new Dictionary<SkillRangeType, BaseSkillRange>()
    {
        { SkillRangeType.ConstantNoSelf,    new ConstantNoSelfSkillRange() },
        { SkillRangeType.Constant,          new ConstantSkillRange() },
        { SkillRangeType.Cone,              new ConeSkillRange() },
        { SkillRangeType.Infinite,          new InfiniteSkillRange() },
        { SkillRangeType.Line,              new LineSkillRange() },
        { SkillRangeType.Self,              new SelfSkillRange() },
        { SkillRangeType.Weapon,            new WeaponSkillRange() },
    };
}
