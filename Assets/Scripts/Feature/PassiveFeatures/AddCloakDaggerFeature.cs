﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddCloakDaggerFeature : AddPassiveFeature<CloakDaggerPassive>
{
    [SerializeField]
    Skill shadowStep;

    protected override void SetParameters(CloakDaggerPassive passive)
    {
        passive.shadowStep = this.shadowStep;
    }
}
