﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    public SaveableItemStack[] items;
    public uint money;

    public SaveableUnitData[] _saveableUnits;

    public string currentLocationGuid;
    public string[] unlockedLocationsGuids;

    public string[] completedMissionGuids;
    public string[] activeMissionGuids;

    public bool wishlistTut;
    public bool combatTut;
    public bool actionTut;
    public bool manaTut;
    public bool itemTut;
    public bool abilityTut;
    public bool pubTut;
    public bool worldMapTut;
    public bool partyPlacementTut;
    public bool jobTut;
    public bool patrolTut;

    #region Party
    // Set this party save data
    public void SetParty(List<Unit> party)
    {
        _saveableUnits = new SaveableUnitData[party.Count];
        for (int i = 0; i < party.Count; i++)
        {
            SaveableUnitData sUnitData = new SaveableUnitData();

            // Unique unit id
            sUnitData.id = party[i].uniqueID.id;

            // Basic Info
            sUnitData.unitName = party[i].unitName;
            sUnitData.level = party[i].level.rank;
            sUnitData.xp = party[i].level.xp;

            // Set the model guid
            sUnitData.modelGuid = party[i].modelRef.Guid;

            // Set jobs guid
            sUnitData.primaryJobGuid = party[i].jobs.primary.Guid;
            sUnitData.hasSecondary = party[i].jobs.hasSecondary;
            if (party[i].jobs.hasSecondary)
                sUnitData.secondaryJobGuid = party[i].jobs.secondary.Guid;

            // Convert Jobranks job references into saveable guids
            JobRank[] jobRanks = party[i].jobRanks.GetData();
            sUnitData.saveableJobRanks = new SaveableJobRank[jobRanks.Length];
            for (int j = 0; j < jobRanks.Length; j++)
            {
                SaveableJobRank sJobRank = new SaveableJobRank();
                sJobRank.jobGuid = jobRanks[j].job.Guid;
                sJobRank.rank = jobRanks[j].rank;
                sJobRank.xp = jobRanks[j].XP;
                sJobRank.abilityTree = jobRanks[j].abilityTree;

                sUnitData.saveableJobRanks[j] = sJobRank;
            }

            // Set equipment guid
            sUnitData.eqPrimaryGuid     = party[i].equipment.GetItem(EquipSlots.Primary)?.Guid;
            sUnitData.eqSecondaryGuid   = party[i].equipment.GetItem(EquipSlots.Secondary)?.Guid;
            sUnitData.eqBodyGuid        = party[i].equipment.GetItem(EquipSlots.Body)?.Guid;
            sUnitData.eqAccessoryGuid   = party[i].equipment.GetItem(EquipSlots.Accessory)?.Guid;
            sUnitData.eqConsume1Guid    = party[i].equipment.GetItem(EquipSlots.Consumable1)?.Guid;
            sUnitData.eqConsume2Guid    = party[i].equipment.GetItem(EquipSlots.Consumable2)?.Guid;
            sUnitData.eqConsume3Guid    = party[i].equipment.GetItem(EquipSlots.Consumable3)?.Guid;

            _saveableUnits[i] = sUnitData;
        }
    }

    public PlayableUnit[] GetParty(JobRegistry jobRegistry, ModelRegistry modelRegistry, EquipRegistry equipRegistry)
    {
        PlayableUnit[] party = new PlayableUnit[_saveableUnits.Length];

        for (int i = 0; i < _saveableUnits.Length; i++)
        {
            UnitData unitData = new UnitData();
            unitData.unitName = _saveableUnits[i].unitName;
            unitData.level = _saveableUnits[i].level;
            unitData.xp = _saveableUnits[i].xp;

            unitData.modelRef = modelRegistry.FindByGuid(_saveableUnits[i].modelGuid);

            // Get jobs
            unitData.primaryJob = jobRegistry.FindByGuid(_saveableUnits[i].primaryJobGuid);
            if (_saveableUnits[i].hasSecondary)
                unitData.secondaryJob = jobRegistry.FindByGuid(_saveableUnits[i].secondaryJobGuid);
            else
                unitData.secondaryJob = null;

            // Get jobranks
            unitData.jobRanks = new JobRank[_saveableUnits[i].saveableJobRanks.Length];
            for (int j = 0; j < _saveableUnits[i].saveableJobRanks.Length; j++)
            {
                JobRank jobRank = new JobRank(jobRegistry.FindByGuid(_saveableUnits[i].saveableJobRanks[j].jobGuid));
                jobRank.rank = _saveableUnits[i].saveableJobRanks[j].rank;
                jobRank.XP = _saveableUnits[i].saveableJobRanks[j].xp;
                jobRank.abilityTree = _saveableUnits[i].saveableJobRanks[j].abilityTree;

                unitData.jobRanks[j] = jobRank;
            }

            // Get equipment
            unitData.primary    = equipRegistry.FindByGuid(_saveableUnits[i].eqPrimaryGuid);
            unitData.secondary  = equipRegistry.FindByGuid(_saveableUnits[i].eqSecondaryGuid);
            unitData.body       = equipRegistry.FindByGuid(_saveableUnits[i].eqBodyGuid);
            unitData.accessory  = equipRegistry.FindByGuid(_saveableUnits[i].eqAccessoryGuid);
            unitData.consume1   = equipRegistry.FindByGuid(_saveableUnits[i].eqConsume1Guid);
            unitData.consume2   = equipRegistry.FindByGuid(_saveableUnits[i].eqConsume2Guid);
            unitData.consume3   = equipRegistry.FindByGuid(_saveableUnits[i].eqConsume3Guid);

            party[i] = new PlayableUnit(_saveableUnits[i].id, unitData);
        }

        return party;
    }
    #endregion

    #region Inventory
    public void SetInventory(Inventory inventory)
    {
        money = inventory.money;

        items = new SaveableItemStack[inventory.itemList.Count];
        for (int i = 0; i < inventory.itemList.Count; i++)
        {
            SaveableItemStack sItemStack = new SaveableItemStack();
            sItemStack.itemGuid = inventory.itemList[i].item.Guid;
            sItemStack.total = inventory.itemList[i].total;
            sItemStack.inUse = inventory.itemList[i].inUse;

            items[i] = sItemStack;
        }
    }

    public List<ItemStack> GetInventoryItems(EquipRegistry equipRegistry)
    {
        List<ItemStack> itemList = new List<ItemStack>();

        for (int i = 0; i < items.Length; i++)
        {
            ItemStack itemStack = new ItemStack(equipRegistry.FindByGuid(items[i].itemGuid), items[i].total);
            itemStack.inUse = items[i].inUse;
            itemList.Add(itemStack);
        }

        return itemList;
    }
    #endregion

    #region WorldMap
    public void SetWorldMap(WorldMapData worldMapData)
    {
        this.unlockedLocationsGuids = new string[worldMapData.unlockedLocations.Count];
        for (int i = 0; i < worldMapData.unlockedLocations.Count; i++)
            this.unlockedLocationsGuids[i] = worldMapData.unlockedLocations[i].Guid;

        currentLocationGuid = worldMapData.currentLocation.Guid;
    }

    public List<WorldMapLocation> GetUnlockedLocations(LocationRegistry locationRegistry)
    {
        List<WorldMapLocation> unlocked = new List<WorldMapLocation>();
        foreach (var guid in unlockedLocationsGuids)
            unlocked.Add(locationRegistry.FindByGuid(guid));

        return unlocked;
    }

    public WorldMapLocation GetCurrentLocation(LocationRegistry locationRegistry)
    {
        return locationRegistry.FindByGuid(currentLocationGuid);
    }
    #endregion

    #region Missions
    public void SetMissions(MissionLog missionLog)
    {
        this.completedMissionGuids = new string[missionLog.completedMissions.Count];
        for (int i = 0; i < missionLog.completedMissions.Count; i++)
            this.completedMissionGuids[i] = missionLog.completedMissions[i].Guid;

        this.activeMissionGuids = new string[missionLog.activeMissions.Count];
        for (int i = 0; i < missionLog.activeMissions.Count; i++)
            this.activeMissionGuids[i] = missionLog.activeMissions[i].Guid;
    }

    public List<MissionData> GetCompletedMission(MissionRegistry missionRegistry)
    {
        List<MissionData> completed = new List<MissionData>();
        foreach (var guid in completedMissionGuids)
            completed.Add(missionRegistry.FindByGuid(guid));

        return completed;
    }

    public List<MissionData> GetActiveMission(MissionRegistry missionRegistry)
    {
        List<MissionData> active = new List<MissionData>();
        foreach (var guid in activeMissionGuids)
            active.Add(missionRegistry.FindByGuid(guid));

        return active;
    }
    #endregion

    #region TutorialFlags
    public void SetTutorialFlags(TutorialFlags tutorialFlags)
    {
        // Tutorial flags
        wishlistTut = tutorialFlags.wishlist;
        combatTut = tutorialFlags.combat;
        actionTut = tutorialFlags.action;
        manaTut = tutorialFlags.mana;
        itemTut = tutorialFlags.item;
        abilityTut = tutorialFlags.ability;
        pubTut = tutorialFlags.pub;
        worldMapTut = tutorialFlags.worldMap;
        partyPlacementTut = tutorialFlags.partyPlacement;
        jobTut = tutorialFlags.job;
        patrolTut = tutorialFlags.patrol;
    }
    #endregion
}
