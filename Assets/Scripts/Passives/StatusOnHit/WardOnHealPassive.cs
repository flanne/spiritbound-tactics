﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WardOnHealPassive : StatusOnHitPassive<WardStatusEffect>
{
    protected override bool ConditionsMet(object sender, Info<Unit, Unit, int, Skill> info)
    {
        return sender is HealSkillEffect || sender is HealPercentSkillEffect;
    }
}

