﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CutsceneAction : MonoBehaviour
{
    [System.NonSerialized]
    public bool isPlaying = false;
    public CutsceneController owner;

    public abstract void Play();

    public virtual void Stop() { }
}
