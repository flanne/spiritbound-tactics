﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMagicCircleCutsceneAction : UnitCutsceneAction
{
    public override void Play()
    {
        unit.GetComponentInChildren<MagicCircleEffect>().Activate();

        isPlaying = false;
    }
}
