﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class EquippableProperties : IUIProperties
    {
        public Equippable equippable;

        public EquippableProperties(Equippable e)
        {
            equippable = e;
        }
    }

    public class EquippableWidget : Widget<EquippableProperties>
    {
        [SerializeField] Image icon;
        [SerializeField] TMP_Text nameTMP;
        [SerializeField] TMP_Text priceTMP;

        public override void SetProperties(EquippableProperties properties)
        {
            Equippable equippable = properties.equippable;

            icon.sprite = equippable.icon;
            nameTMP.text = MasterSingleton.main.GameStrings.Get(equippable.nameStringID);
            priceTMP.text = equippable.cost.ToString();
        }

        public void FormatUnavailable()
        {
            nameTMP.color = new Color(150, 150, 150, 255);
        }
    }
}
