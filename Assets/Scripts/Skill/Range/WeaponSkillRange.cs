﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// THIS CLASS I NO LONGER BEING USED, INSTEAD USE BOOL isWeaponSkill on Skill class
public class WeaponSkillRange : BaseSkillRange
{
	public override List<BoardTile> GetTilesInRange(Board board, Unit unit, int horizontal, int vertical)
	{
		return unit.basicAttack.GetTilesInRange(board, unit);
	}
}