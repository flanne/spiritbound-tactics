﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewGameWarningState : TitleScreenState
{
    void OnClick(object sender, InfoEventArgs<int> e)
    {
        if (e.info == 0)
            owner.ChangeState<InitNewGameState>();
        else if (e.info == 1)
            owner.ChangeState<TitleMenuState>();

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnSelect(object sender, InfoEventArgs<int> e)
    {
        audioPlayer.Play(owner.selectSFX);
    }

    public override void Enter()
    {
        newGameWarningUI.Show();
        newGameWarningUI.Select(1);
        newGameWarningUI.ClickEvent += OnClick;
        newGameWarningUI.SelectEvent += OnSelect;
    }

    public override void Exit()
    {
        newGameWarningUI.Hide();
        newGameWarningUI.ClickEvent -= OnClick;
        newGameWarningUI.SelectEvent -= OnSelect;
    }
}
