﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneUseCondition : StatusCondition
{
    public override void Remove()
    {
        Status status = GetComponentInParent<Status>();

        // Destroy all other conditions
        foreach (var condition in status.GetComponentsInChildren<StatusCondition>())
        {
            if (condition == this)
                return;

            condition.transform.SetParent(null);
            Destroy(condition.gameObject);
        }

        // Remove this
        if (status)
            status.Remove(this);
    }
}
