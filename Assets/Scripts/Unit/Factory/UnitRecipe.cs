﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public struct UnitRecipe
{
    [TableColumnWidth(600, Resizable = false)]
    public UnitData unitData;

    [TableColumnWidth(300, Resizable = false)]
    [VerticalGroup("Other Data")]

    [BoxGroup("Other Data/Placement", false)]
    [LabelWidth(60)]
    [HideLabel]
    public SpawnData spawnData;

    [BoxGroup("Other Data/Misc", false)]
    [EnumToggleButtons]
    [LabelWidth(60)]
    public AllianceType alliance;

    [BoxGroup("Other Data/Misc", false)]
    [LabelWidth(60)]
    public AIStrategy strategy;

    [BoxGroup("Other Data/Misc", false)]
    [LabelWidth(60)]
    public IDEnum unitID;
}