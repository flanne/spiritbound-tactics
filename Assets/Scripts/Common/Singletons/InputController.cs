﻿using UnityEngine;
using System;
using System.Collections;

public class InputController : MonoBehaviour
{
    Repeater _hor = new Repeater("Horizontal");
    Repeater _ver = new Repeater("Vertical");
    string[] _buttons = new string[] { "Fire1", "Fire2", "Fire3" };

    public static event EventHandler<InfoEventArgs<Vector2>> mouseEvent;
    public static event EventHandler<InfoEventArgs<Vector2Int>> moveEvent;
    public static event EventHandler<InfoEventArgs<int>> fireEvent;
    public static event EventHandler<InfoEventArgs<float>> mouseScrollEvent;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        int x = _hor.Update();
        int y = _ver.Update();

        // Check if arrow keys or wasd pressed
        if (x != 0 || y != 0)
        {
            if (moveEvent != null)
                moveEvent(this, new InfoEventArgs<Vector2Int>(new Vector2Int(x, y)));
        }

        // Check if ctrl, alt, or cmd keys are pressed
        for (int i = 0; i < 3; ++i)
        {
            if (Input.GetButtonUp(_buttons[i]))
            {
                if (fireEvent != null)
                    fireEvent(this, new InfoEventArgs<int>(i));
            }
        }

        // Check if mouse moved
        if ((Input.GetAxis("Mouse X") != 0) || (Input.GetAxis("Mouse Y") != 0))
        {
            if (mouseEvent != null)
                mouseEvent(this, new InfoEventArgs<Vector2>(Input.mousePosition));

        }

        // Check for scroll wheel
        var v = Input.GetAxis("Mouse ScrollWheel");
        if (v != 0)
        {
            // Positive v is mouse scroll up. Negative vis mouse scroll down.
            if (mouseScrollEvent != null)
                mouseScrollEvent(this, new InfoEventArgs<float>(v));
        }
    }
}

class Repeater
{
    const float threshold = 0.5f;
    const float rate = 0.25f;
    float _next;
    bool _hold;
    string _axis;

    public Repeater(string axisName)
    {
        _axis = axisName;
    }

    public int Update()
    {
        int retValue = 0;
        int value = Mathf.RoundToInt(Input.GetAxisRaw(_axis));
        if (value != 0)
        {
            if (Time.time > _next)
            {
                retValue = value;
                _next = Time.time + (_hold ? rate : threshold);
                _hold = true;
            }
        }
        else
        {
            _hold = false;
            _next = 0;
        }
        return retValue;
    }
}