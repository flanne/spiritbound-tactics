﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

public class TryEquipUnitInfoState : ShopState
{
    int _totalCost;

    #region Eventhandlers
    void OnClick(object sender, InfoEventArgs<EquipSlots> e)
    {
        EquipSlots slot = e.info;

        // Get possible equippables that can go in that slot
        List<Equippable> items = new List<Equippable>();
        foreach (var i in shopItems)
        {
            if ((i.defaultSlots & slot) == slot)
                items.Add(i);

            foreach (var s in i.secondarySlots)
                if ((s & slot) == slot)
                    items.Add(i);
        }

        owner.currentTrySlot = slot;

        tryBuyMenu.AddItems(items, owner.menuUnit);

        owner.ChangeState<TryShopState>();

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnSelect(object sender, InfoEventArgs<EquipSlots> e)
    {
        // Check which slot was selected
        EquipSlots selectedSlot = e.info;

        // Set description box
        Equippable selectedEquippable = owner.menuUnit.equipment.GetItem(selectedSlot);
        if (selectedEquippable != null)
        {
            owner.descriptionBox.SetToDescribable(selectedEquippable, DescriptionBox.Side.Right);
            owner.descriptionBox.Show();
        }
        else
        {
            owner.descriptionBox.Hide();
        }

        audioPlayer.Play(owner.selectSFX);
    }

    void OnPurchase(object sender, EventArgs e)
    {
        // Can't buy if not enough money
        if (_totalCost > MasterSingleton.main.Inventory.money)
            return;

        owner.ChangeState<SelectUnitTryMenuState>();
        tryEquipController.Hide();

        // Unequip tried items
        foreach (var item in owner.tryEquips)
            owner.menuUnit.equipment.UnEquip(item.Key, false);

        // Re-equip original gear
        foreach (var equips in owner.tryOriginalEquips)
            owner.menuUnit.equipment.Equip(equips.Value, equips.Key);

        // Buy and equip tried items
        foreach (var item in owner.tryEquips)
        {
            if (MasterSingleton.main.Inventory.Buy(item.Value, 1))
                owner.menuUnit.equipment.Equip(item.Value, item.Key);
        }

        // Reset try equip list
        owner.tryEquips = new Dictionary<EquipSlots, Equippable>();

        // Update funds
        fundsText.text = MasterSingleton.main.Inventory.money.ToString();

        // Play sound effect
        AudioClip coinSound = MasterSingleton.main.SFXLibrary.CoinSound;
        MasterSingleton.main.AudioPlayer.Play(coinSound);

        // Hide Description box
        descriptionBox.Hide();

        audioPlayer.Play(coinSound);
    }

    void OnPurchaseSelect(object sender, EventArgs e)
    {
        descriptionBox.Hide();

        audioPlayer.Play(owner.selectSFX);
    }

    void OnSecondary(InputAction.CallbackContext obj)
    {
        if (owner.tryEquips.ContainsKey(tryEquipController.currentSlot))
        {
            // Unequip tried items
            foreach (var item in owner.tryEquips)
                owner.menuUnit.equipment.UnEquip(item.Key, false);

            // Re-equip original gear
            foreach (var equips in owner.tryOriginalEquips)
                owner.menuUnit.equipment.Equip(equips.Value, equips.Key);

            // Remove selected try equip
            owner.tryEquips.Remove(tryEquipController.currentSlot);

            // Re-equip tried items
            foreach (var item in owner.tryEquips)
                owner.menuUnit.equipment.Equip(item.Value, item.Key, false);

            Refresh();
        }

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnCancel(InputAction.CallbackContext obj)
    {
        owner.ChangeState<SelectUnitTryMenuState>();
        tryEquipController.Hide();
        descriptionBox.Hide();

        // Un-equip tried gear
        foreach (var tryEquip in owner.tryEquips)
            owner.menuUnit.equipment.UnEquip(tryEquip.Key);

        owner.tryEquips = new Dictionary<EquipSlots, Equippable>();

        // Re-equip original gear
        foreach (var equips in owner.tryOriginalEquips)
            owner.menuUnit.equipment.Equip(equips.Value, equips.Key);

        audioPlayer.Play(owner.cancelSFX);
    }
    #endregion

    #region LifeCycle
    public override void Enter()
    {
        tryEquipController.Show();
        tryEquipController.Select(owner.currentTrySlot);

        if (owner.menuUnit.equipment.GetItem(EquipSlots.Primary) != null)
        {
            owner.descriptionBox.SetToDescribable(owner.menuUnit.equipment.GetItem(EquipSlots.Primary), DescriptionBox.Side.Right);
            owner.descriptionBox.Show();
        }

        tryEquipController.SlotClickEvent += OnClick;
        tryEquipController.SlotSelectEvent += OnSelect;
        tryEquipController.PurchaseEvent += OnPurchase;
        tryEquipController.PurchaseSelectEvent += OnPurchaseSelect;
        playerInput.currentActionMap.FindAction("Secondary").performed += OnSecondary;
        playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;

        owner.keybindPrompt.Show();
        owner.keybindPrompt.SetText(0, "Select");
        owner.keybindPrompt.SetText(1, "Cancel");
        owner.keybindPrompt.SetText(2, "Remove");

        Refresh();
    }

    public override void Exit()
    {
        tryEquipController.interactable = false;

        tryEquipController.SlotClickEvent -= OnClick;
        tryEquipController.SlotSelectEvent -= OnSelect;
        tryEquipController.PurchaseEvent -= OnPurchase;
        tryEquipController.PurchaseSelectEvent -= OnPurchaseSelect;
        playerInput.currentActionMap.FindAction("Secondary").performed -= OnSecondary;
        playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;

        owner.keybindPrompt.Hide();
    }
    #endregion

    #region Private
    void Refresh()
    {
        // Recalculate cost
        _totalCost = 0;
        foreach (var e in owner.tryEquips.Values)
            _totalCost += (int)e.cost;

        owner.tryCostText.text = _totalCost.ToString();
        if (_totalCost > MasterSingleton.main.Inventory.money)
            owner.tryCostText.color = new Color32(172, 50, 50, 255);
        else
            owner.tryCostText.color = Color.white;

        // Refresh menu
        // Unequip tried items
        foreach (var item in owner.tryEquips)
            owner.menuUnit.equipment.UnEquip(item.Key, false);

        // Re-equip original gear
        foreach (var equips in owner.tryOriginalEquips)
            owner.menuUnit.equipment.Equip(equips.Value, equips.Key);

        // Re-equip tried items
        foreach (var item in owner.tryEquips)
            owner.menuUnit.equipment.Equip(item.Value, item.Key, false);

        tryEquipController.SetToUnit(owner.menuUnit);
        foreach (var tryEquip in owner.tryEquips)
            tryEquipController.FormatTry(tryEquip.Key);
    }
    #endregion
}
