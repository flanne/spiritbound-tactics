﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntenseFocusPassive : Passive
{
    protected override void AddListeners()
    {
        this.AddObserver(OnCheckAutoHit, HitRate.AutomaticHitCheckNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnCheckAutoHit, HitRate.AutomaticHitCheckNotification);
    }

    void OnCheckAutoHit(object sender, object args)
    {
        var info = args as Info<MatchException, Skill>;
        var exc = info.arg0;

        if (exc.attacker == owner && owner.status.GetComponentInChildren<FocusStatusEffect>() != null)
            exc.FlipToggle();
    }
}
