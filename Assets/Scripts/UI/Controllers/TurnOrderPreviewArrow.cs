﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class TurnOrderPreviewArrow : Panel
    {
        [SerializeField] GameObject arrowObj;
        [SerializeField] TMP_Text tuTMP;

        public void SetArrowToAboveRect(Transform transform)
        {
            Vector2 botPos = arrowObj.transform.position;
            botPos.y = transform.position.y + 14f;
            arrowObj.transform.position = botPos;
        }

        public void SetArrowToUnderRect(Transform transform)
        {
            Vector3 botPos = arrowObj.transform.position;
            botPos.y = transform.position.y - 14f;
            arrowObj.transform.position = botPos;
        }

        public void SetTUText(int amount)
        {
            tuTMP.text = amount.ToString();
        }
    }
}
