﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InfiniteSkillRange : BaseSkillRange
{
	public override bool positionOriented { get { return false; } }

	public override List<BoardTile> GetTilesInRange(Board board, Unit unit, int horizontal, int vertical)
	{
		return new List<BoardTile>(board.GetAllTiles());
	}
}
