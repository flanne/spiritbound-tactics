﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunOnDebuffPassive : Passive
{
    public int stunAmount;

    protected override void AddListeners()
    {
        this.AddObserver(OnDebuff, Status.AddedNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnDebuff, Status.AddedNotification);
    }

    void OnDebuff(object sender, object args)
    {
        var effects = args as StatusEffect;
        var defender = sender as Unit;

        if (effects.inflictor == owner)
            defender.stats[StatTypes.CTR] += stunAmount;
    }
}