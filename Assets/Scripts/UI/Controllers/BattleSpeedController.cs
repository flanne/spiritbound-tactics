﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using flanne.UI;

public class BattleSpeedController : MonoBehaviour
{
    #region Fields
    [SerializeField] TMP_Text speedNumber;
    [SerializeField] Image speedSprite;
    [SerializeField] TMP_Text label;

    [SerializeField] Sprite sprite1x, sprite2x, sprite3x;
    #endregion

    #region Properties
    float battleSpeed
    {
        get { return MasterSingleton.main.GameSettings.battleSpeed; }
        set { MasterSingleton.main.GameSettings.battleSpeed = value; }
    }
    #endregion

    #region Eventhandlers
    public void OnClick()
    {
        if (battleSpeed == 1)
            battleSpeed = 1.5f;
        else if (battleSpeed == 1.5f)
            battleSpeed = 2;
        else
            battleSpeed = 1;

        Refresh();

        // Button animation
        LeanTween.scale(this.gameObject, new Vector3(0.9f, 0.9f, 1), 0.05f).setLoopPingPong(1).setEase(LeanTweenType.easeOutBack);
    }
    #endregion

    #region LifeCycle
    void Start()
    {
        Refresh();
    }
    #endregion

    #region Public
    public void Refresh()
    {
        string speedStr;

        if (battleSpeed == 1)
        {
            speedStr = "1x";
            speedSprite.sprite = sprite1x;
        }
        else if (battleSpeed == 1.5f)
        {
            speedStr = "2x";
            speedSprite.sprite = sprite2x;
        }
        else
        {
            speedStr = "3x";
            speedSprite.sprite = sprite3x;
        }

        speedNumber.text = speedStr;
    }
    #endregion
}
