﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddWardOnHealFeature : AddPassiveFeature<WardOnHealPassive>
{
    [SerializeField]
    [Range(0, 1)]
    float chanceToHit;

    [SerializeField]
    int turnDuration;

    protected override void SetParameters(WardOnHealPassive passive)
    {
        passive.chanceToHit = this.chanceToHit;
        passive.turnDuration = this.turnDuration;
    }
}
