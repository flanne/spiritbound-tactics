﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class IntExtensions
{
    public static int NotifyModifiers<T>(this int value, string notification, System.Object notifier, T e)
    {
        var mods = new List<ValueModifier>();
        var info = new Info<List<ValueModifier>, T>(mods, e);
        notifier.PostNotification(notification, info);
        mods.Sort(Compare);

        float floatVal = value;
        for (int i = 0; i < mods.Count; ++i)
            floatVal = mods[i].Modify(value, floatVal);

        return Mathf.CeilToInt(floatVal);
    }

    public static int NotifyModifiers(this int value, string notification, System.Object notifier)
    {
        var mods = new List<ValueModifier>();
        notifier.PostNotification(notification, mods);
        mods.Sort(Compare);

        float floatVal = value;
        for (int i = 0; i < mods.Count; ++i)
            floatVal = mods[i].Modify(value, floatVal);

        return Mathf.FloorToInt(floatVal);
    }

    static int Compare(ValueModifier x, ValueModifier y)
    {
        return x.sortOrder.CompareTo(y.sortOrder);
    }
}
