﻿using UnityEngine;

[CreateAssetMenu(menuName = "SpiritlinkTactics/Registry/Model Registry")]
public class ModelRegistry : Registry<UnitModel>
{
}