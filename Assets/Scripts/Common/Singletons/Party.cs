﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Party : MonoBehaviour
{
    [SerializeField] UnitFactory unitFactory;
    [SerializeField] SaveSystem saveSystem;
    [SerializeField] Inventory inventory;

    [SerializeField] PlayableUnit[] newGameParty;

    [SerializeField] PlayableUnit[] devParty;

    public List<Unit> myParty;

    public void InitNewParty()
    {
        myParty = new List<Unit>();
        LoadParty(newGameParty);

        // add equipment to inventory
        foreach (var unit in myParty)
        {
            foreach (var equippable in unit.equipment.GetAllItems())
                inventory.AddAndEquip(equippable);
        }
    }

    public void InitDevParty()
    {
        myParty = new List<Unit>();
        LoadParty(devParty);

        // add equipment to inventory
        foreach (var unit in myParty)
        {
            foreach (var equippable in unit.equipment.GetAllItems())
                inventory.AddAndEquip(equippable);
        }
    }

    public Unit GetByID(IDEnum id)
    {
        foreach (var u in myParty)
        {
            if (u.uniqueID?.id == id)
                return u;
        }
        return null;
    }

    public void DeactivateAll()
    {
        foreach (var u in myParty)
        {
            u.Reset();
            u.Hide();
        }
    }

    public void LoadParty(PlayableUnit[] units)
    {
        ClearParty();
        foreach (var u in units)
        {
            Unit unit = unitFactory.Create(u.unitData, u.id);
            unit.level.xp = u.unitData.xp;
            unit.transform.SetParent(this.transform);
            myParty.Add(unit);
        }

        DeactivateAll();
    }

    void ClearParty()
    {
        foreach (var unit in myParty)
        {
            unit.transform.SetParent(null);
            Destroy(unit.gameObject);
        }

        myParty = new List<Unit>();
    }
}
