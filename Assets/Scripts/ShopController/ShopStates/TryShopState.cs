﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public class TryShopState : ShopState
{
    #region Eventhandlers
    void OnClick(object sender, InfoEventArgs<int> e)
    {
        Equippable selectedEquippable = tryBuyMenu.items[e.info];

        // Don't try on if item is same as unit's currently equipped item
        if (selectedEquippable == owner.menuUnit.equipment.GetItem(owner.currentTrySlot))
        {
            owner.ChangeState<TryEquipUnitInfoState>();
            return;
        }

        // Temporarily equip item
        if (owner.menuUnit.equipment.Equip(selectedEquippable, owner.currentTrySlot, false))
        {
            owner.tryEquips[owner.currentTrySlot] = selectedEquippable;

            // Change text color to indicate temporarily equipped
            tryEquipController.FormatTry(owner.currentTrySlot);

            // Check if any tried equip needs to be unequiped
            List<EquipSlots> toRemove = new List<EquipSlots>();
            foreach (var tryEquip in owner.tryEquips)
            {
                if (owner.menuUnit.equipment.GetItem(tryEquip.Key) != tryEquip.Value)
                    toRemove.Add(tryEquip.Key);
            }
            foreach (var r in toRemove)
                owner.tryEquips.Remove(r);

            //Check if any open slots to re-equip
                foreach (var equips in owner.tryOriginalEquips)
            {
                if (owner.menuUnit.equipment.GetItem(equips.Key) == null && owner.menuUnit.equipment.GetOccupyingItem(equips.Key) == null)
                    owner.menuUnit.equipment.Equip(equips.Value, equips.Key);
            }
            owner.ChangeState<TryEquipUnitInfoState>();
        }

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnSelect(object sender, InfoEventArgs<int> e)
    {
        Equippable selectedItem = tryBuyMenu.items[e.info];
        owner.equipCompareUI.CompareToEquippable(selectedItem, owner.currentTrySlot);

        owner.descriptionBox.SetToDescribable(selectedItem);

        audioPlayer.Play(owner.selectSFX);
    }

    void OnCancel(InputAction.CallbackContext obj)
    {
        owner.ChangeState<TryEquipUnitInfoState>();

        audioPlayer.Play(owner.cancelSFX);
    }
    #endregion

    #region LifeCycle
    public override void Enter()
    {
        tryBuyMenu.Show();
        tryBuyMenu.SelectFirstAvailable();
        tryBuyMenu.ClickEvent += OnClick;
        tryBuyMenu.SelectEvent += OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;

        descriptionBox.Show();
    }

    public override void Exit()
    {
        tryBuyMenu.Hide();
        tryBuyMenu.ClickEvent -= OnClick;
        tryBuyMenu.SelectEvent -= OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;

        descriptionBox.Hide();
    }
    #endregion
}
