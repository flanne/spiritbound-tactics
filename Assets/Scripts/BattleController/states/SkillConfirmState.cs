﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class SkillConfirmState : BattleState
    {
        #region Properties
        int index = 0;
        List<Unit> targetedUnits;
        #endregion

        #region Eventhandlers
        void OnMove(InputAction.CallbackContext obj)
        {
            Vector2 vec = obj.ReadValue<Vector2>();
            if (targetedUnits.Count > 0)
            {
                if (vec.y > 0 || vec.x > 0)
                    SetTarget(index + 1);
                else if (vec.y < 0 || vec.x < 0)
                    SetTarget(index - 1);
            }
        }

        void OnClick(InputAction.CallbackContext obj)
        {
            if (turn.skill.AreAnyTargetsValid(turn.actor, turn.targets))
            {
                if (turn.skill == turn.actor.consumableSkills.GetSkill(turn.consumableIndex)) // Use charges if consumable skill
                    turn.actor.consumableSkills.Use(turn.consumableIndex);

                owner.ChangeState<PerformSkillState>();

                audioPlayer.Play(owner.confirmSFX);
            }
        }

        void OnCancel(InputAction.CallbackContext obj)
        {
            owner.ChangeState<SkillTargetState>();

            audioPlayer.Play(owner.cancelSFX);
        }
        #endregion

        #region LifeCycle
        public override void Enter()
        {
            base.Enter();

            if (turn.skill.isWeaponSkill)
                turn.targets = turn.actor.basicAttack.GetTilesInArea(board, currentTile.xy, turn.actor);
            else
                turn.targets = turn.skill.GetTilesInArea(board, currentTile.xy, turn.actor);

            tileHighlighter.Highlight(turn.targets, new Color(1f, 0.6f, 0.25f, 1f));

            targetedUnits = new List<Unit>();
            foreach (var t in turn.targets)
                if (t.content != null && t.content.GetComponent<Unit>() != null && turn.skill.IsTarget(turn.actor, t))
                    targetedUnits.Add(t.content.GetComponent<Unit>());

            if (turn.skill.AreAnyTargetsValid(turn.actor, turn.targets))
            {
                primaryHUD.SetProperties(new UnitProperties(turn.actor));
                primaryHUD.Show();

                if (targetedUnits.Count > 0)
                {
                    SetTarget(0);
                    secondaryHUD.Show();

                    damagePreviewPanel.Show();
                }
            }
            else
            {
                uiLabel.Show();
                TMP_Text tmp = uiLabel.GetComponentInChildren<TMP_Text>();
                tmp.text = "No valid target(s).";
                manaBar.Hide();
                primaryHUD.Hide();
            }

            if (driver.Current == DriverType.Computer)
                StartCoroutine(ComputerHighlightTarget());

            if (turn.actor.alliance.type == AllianceType.Hero)
                manaBar.Preview(turn.skill.manaCost);

            // Set all targets to glowing
            foreach (var u in targetedUnits)
                u.spriteMaterialSwapper.SetToNew();
        }

        public override void Exit()
        {
            base.Exit();
            tileHighlighter.Clear();
            uiLabel.Hide();
            manaBar.Show();

            primaryHUD.Hide();
            secondaryHUD.Hide();
            damagePreviewPanel.Hide();

            turnOrderUI.UnHighlightAll();

            manaBar.ClearPreview();

            // Remove glowing from targets
            foreach (var u in targetedUnits)
                u.spriteMaterialSwapper.SetToDefault();
        }

        protected override void OnEnterUI()
        {
            // Add Listeners
            playerInput.currentActionMap.FindAction("Navigate").performed += OnMove;
            playerInput.currentActionMap.FindAction("Submit").performed += OnClick;
            playerInput.currentActionMap.FindAction("Click").performed += OnClick;
            playerInput.currentActionMap.FindAction("Cancel").performed += OnCancel;

            // Show keybinds
            keybindPromptUI.Show();
            keybindPromptUI.SetText(0, "Confirm");
            keybindPromptUI.SetText(1, "Cancel");
        }

        protected override void OnExitUI()
        {
            // Remove Listeners
            playerInput.currentActionMap.FindAction("Navigate").performed -= OnMove;
            playerInput.currentActionMap.FindAction("Submit").performed -= OnClick;
            playerInput.currentActionMap.FindAction("Click").performed -= OnClick;
            playerInput.currentActionMap.FindAction("Cancel").performed -= OnCancel;
        }
        #endregion

        #region Private
        void SetTarget(int target)
        {
            index = target;
            if (index < 0)
                index = targetedUnits.Count - 1;
            if (index >= targetedUnits.Count)
                index = 0;

            if (targetedUnits.Count > 0)
            {
                secondaryHUD.SetProperties(new UnitProperties(targetedUnits[index]));
                turnOrderUI.SetHighlight(targetedUnits[index], true);
            }

            UpdateDamagePreview();
        }

        void UpdateDamagePreview()
        {
            // Get hitrate and damage predictions
            int chance = turn.skill.GetHitRate(turn.actor, targetedUnits[index].tile);
            int amount = turn.skill.GetPrediction(turn.actor, targetedUnits[index].tile);
            damagePreviewLeft.SetProperties(new DamagePreviewProperties(chance.ToString(), amount));

            // Get counter predictions
            List<CounterPassive> counters = targetedUnits[index].passives.GetComponentsInChildren<CounterPassive>().ToList();
            counters.Sort();

            if (counters.Any())
            {
                for (int i = 0; i < counters.Count; i++)
                {
                    if (counters[i].CanCounter(turn.actor, turn.skill))
                    {
                        int counterChance = counters[i].counterSkill.GetHitRate(targetedUnits[index], turn.actor.tile);
                        int counterAmount = counters[i].counterSkill.GetPrediction(targetedUnits[index], turn.actor.tile);
                        damagePreviewRight.SetProperties(new DamagePreviewProperties(counterChance.ToString(), counterAmount));
                        return;
                    }
                }
            }

            // If no valid counters found display "No Counter"
            damagePreviewRight.SetProperties(new DamagePreviewProperties("No Counter", 0));
        }

        #endregion

        #region AI
        IEnumerator ComputerHighlightTarget()
        {
            yield return new WaitForSeconds(1f);
            owner.ChangeState<PerformSkillState>();
        }
        #endregion
    }
}