﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Flags]
public enum DamageTypes
{
	None = 0,
	Magic = 1 << 0,
	Physical = 1 << 1
}
