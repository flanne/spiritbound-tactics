﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoSelfAllySkillEffectTarget : SkillEffectTarget
{
    public override bool IsTarget(Unit actor, BoardTile target)
    {
        Stats s = target?.content?.GetComponent<Unit>()?.stats;
        Unit targetUnit = target?.content?.GetComponent<Unit>();
        Alliance a = targetUnit?.alliance;
        return (s != null && s[StatTypes.HP] > 0) && (a != null && actor != targetUnit && actor.alliance.IsMatch(a, AITargeting.Ally));
    }
}
