﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PatrolTutorialState : WorldMapState
{
    public override void Enter()
    {
        owner.patrolTutorial.Show();

        StartCoroutine(WaitLockout());
    }

    public override void Exit()
    {
        playerInput.currentActionMap.FindAction("AnyButton").canceled -= OnAnyKey;

        owner.patrolTutorial.Hide();
    }

    void OnAnyKey(InputAction.CallbackContext obj)
    {
        owner.ChangeState<MapActionState>();
    }

    IEnumerator WaitLockout()
    {
        yield return new WaitForSeconds(1f);

        playerInput.currentActionMap.FindAction("AnyButton").canceled += OnAnyKey;
    }
}