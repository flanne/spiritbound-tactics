﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spiritlink.Battle;

public class TurnStatusCondition : StatusCondition
{
    public int turns = 3;
    Unit owner;

    void OnEnable()
    {
        owner = GetComponentInParent<Unit>();

        if (owner)
            this.AddObserver(OnTurnEnd, TurnOrderController.TurnCompletedNotification, owner);
    }

    void OnDisable()
    {
        if (owner)
            this.RemoveObserver(OnTurnEnd, TurnOrderController.TurnCompletedNotification, owner);
    }

    void OnTurnEnd(object sender, object args)
    {
        turns--;
        if (turns <= 0)
            Remove();
    }
}
