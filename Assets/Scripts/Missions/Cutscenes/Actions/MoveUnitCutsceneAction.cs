﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUnitCutsceneAction : UnitCutsceneAction
{
    Board board { get { return MasterSingleton.main.GameLevel.board; } }

    [SerializeField]
    Vector2Int position;

    [SerializeField]
    float moveSpeed = 4;

    [SerializeField] UnitAnimState afterMoveState = UnitAnimState.Idle;

    [SerializeField]
    bool exitScene;

    IEnumerator mainCoroutine;
    IEnumerator moveCoroutine;

    public override void Play()
    {
        mainCoroutine = MoveUnit(unit);
        StartCoroutine(mainCoroutine);
    }

    IEnumerator MoveUnit(Unit unit)
    {
        isPlaying = true;

        BoardTile tile = board.GetTile(position);

        unit.SetAnimationState(UnitAnimState.Idle);

        yield return null;

        Movement m = unit.movement;
        moveCoroutine = m.Traverse(tile, moveSpeed, true);
        yield return StartCoroutine(moveCoroutine);

        if (exitScene)
            yield return StartCoroutine(UnitFadeOut());

        unit.SetAnimationState(afterMoveState);
        isPlaying = false;
    }

    IEnumerator UnitFadeOut()
    {
        yield return null;

        int leanTweenID = LeanTween.color(unit.gameObject, new Color32(0, 0, 0, 0), 0.3f).id;

        while (LeanTween.isTweening(leanTweenID))
            yield return null;
    }
}
