﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class UnitHUD : UnitWidget
    {
        [SerializeField] Image portrait;
        [SerializeField] TMP_Text unitName;
        [SerializeField] TMP_Text jobName;
        [SerializeField] TMP_Text levelText;
        [SerializeField] TMP_Text xpText;
        [SerializeField] HealthBar healthBar;
        [SerializeField] DescribableMenu statusIcons;

        [SerializeField] Image panelImage;
        [SerializeField] Sprite heroPanelSprite;
        [SerializeField] Sprite villainPanelSprite;

        public override void SetProperties(UnitProperties properties)
        {
            Unit unit = properties.unit;

            if (unit.uniqueSprite != null)
                this.portrait.sprite = unit.uniqueSprite.portrait;
            else
                this.portrait.sprite = unit.jobs.primary.portrait;

            unitName.text = unit.unitName;
            jobName.text = MasterSingleton.main.GameStrings.Get(unit.jobs.primary.nameStringID);
            if (unit.jobs.secondary)
                jobName.text += "/" + MasterSingleton.main.GameStrings.Get(unit.jobs.secondary.nameStringID);

            levelText.text = "Lv. " + unit.level.rank;
            xpText.text = "Xp. " + unit.level.xp;

            healthBar.SetMaxHealth(unit.health.MHP);
            healthBar.SetHealth(unit.health.HP, unit.health.SHP);

            if (unit.alliance.type == AllianceType.Hero || unit.alliance.type == AllianceType.Ally || unit.alliance.type == AllianceType.None)
                panelImage.sprite = heroPanelSprite;
            else
                panelImage.sprite = villainPanelSprite;

            // Add status effect icons
            List<StatusEffect> buffs = new List<StatusEffect>();
            List<StatusEffect> debuffs = new List<StatusEffect>();
            foreach (var status in unit.status.GetComponentsInChildren<StatusEffect>())
            {
                if (status.isBuff)
                    buffs.Add(status);
                else
                    debuffs.Add(status);
            }

            statusIcons.Clear();
            foreach (var buff in buffs)
            {
                if (buff.keyword != null)
                    AddStatusIcon(buff.keyword);
            }

            foreach (var debuff in debuffs)
            {
                if (debuff.keyword != null)
                    AddStatusIcon(debuff.keyword);
            }
        }

        public override void Show()
        {
            base.Show();

            if (statusIcons != null)
                statusIcons.Show();
        }

        public override void Hide()
        {
            base.Hide();

            if (statusIcons != null)
                statusIcons.Hide();
        }

        void AddStatusIcon(Keyword keyword)
        {
            var entry = statusIcons.AddEntry(new DescribableProperties(keyword));
            var tooltipText = entry.GetComponent<ToolTipText>();

            if (tooltipText != null)
                tooltipText.tooltip = keyword.GetDescription();
        }
    }
}
