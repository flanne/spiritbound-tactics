﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TUCostModPassive : Passive
{
    public int amount;
    public SkillTag skillTag;

    protected override void AddListeners()
    {
        this.AddObserver(OnCalculateTU, Skill.GetTimeCostNotification, owner);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnCalculateTU, Skill.GetTimeCostNotification, owner);
    }

    void OnCalculateTU(object sender, object args)
    {
        var info = args as Info<List<ValueModifier>, Skill>;
        var mods = info.arg0;
        var skill = info.arg1;

        if (skill.ContainsTag(skillTag))
            mods.Add(new AddValueModifier(0, amount));
    }
}
