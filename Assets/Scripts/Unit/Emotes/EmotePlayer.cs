﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmotePlayer : MonoBehaviour
{
    [SerializeField]
    Sprite[] emoteSprites; // index of sprites correspond to th EmoteType enum

    [SerializeField]
    GameObject emotePrefab;

    public void Play(EmoteType emoteType)
    {
        GameObject emote = Instantiate(emotePrefab);
        emote.transform.localPosition = this.transform.position;

        emote.GetComponent<Emote>()?.SetEmoteSprite(emoteSprites[(int)emoteType]);

        emote.transform.SetParent(this.transform);
    }
}
