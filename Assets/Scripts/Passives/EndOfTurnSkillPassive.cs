﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spiritlink.Battle;

public class EndOfTurnSkillPassive : Passive
{
    public Skill skill;

    protected override void AddListeners()
    {
        this.AddObserver(OnEndTurn, AfterTurnState.EndOfTurnEffectsNotification, owner);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnEndTurn, AfterTurnState.EndOfTurnEffectsNotification, owner);
    }

    void OnEndTurn(object sender, object args)
    {
        var effects = args as List<SkillDirections>;
        effects.Add(new SkillDirections(skill, owner, owner.tile));
    }
}
