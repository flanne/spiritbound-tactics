﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spiritlink.Battle
{
    public class CutsceneEndBattleState : CutsceneState
    {
        public override void Enter()
        {
            if (cutsceneController.CutsceneQueued())
                MasterSingleton.main.ScreenFilter.FadeOut(2f, StartCutscene);
            else
                GoToNextState();
        }

        protected override void GoToNextState()
        {
            PanCamera();

            if (MasterSingleton.main.MissionLog.current.fullPostCutscene) // play post cutscene
                MasterSingleton.main.ScreenFilter.FadeOut(2f, ChangeToPostBattleCutscene);
            else // if no post cutscene, go to world map
                MasterSingleton.main.SceneTransition.LoadScene("WorldMap");
        }

        void ChangeToPostBattleCutscene()
        {
            owner.ChangeState<CutsceneFullBattleState>();
        }

        void StartCutscene()
        {
            cutsceneController.Play();
            MasterSingleton.main.ScreenFilter.FadeIn(2f);
            letterBox.Show();
            StartCoroutine(WaitForCutscene());
            StartCoroutine(InitControlLock());
        }

        void PanCamera()
        {
            MasterSingleton.main.MainCameraRig.follow = null;
            var pos = MasterSingleton.main.MainCameraRig.transform.position;
            pos.y += 1;
            LeanTween.move(MasterSingleton.main.MainCameraRig.gameObject, pos, 2f);
        }
    }
}
