﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadGameState : TitleScreenState
{
    public override void Enter()
    {
        MasterSingleton.main.SaveSystem.Load(0);
        MasterSingleton.main.SceneTransition.LoadScene("WorldMap");
    }
}
