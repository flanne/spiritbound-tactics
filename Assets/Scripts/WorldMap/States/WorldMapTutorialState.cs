﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class WorldMapTutorialState : WorldMapState
{
    public override void Enter()
    {
        owner.worldMapTutorial.Show();

        StartCoroutine(WaitLockout());
    }

    public override void Exit()
    {
        playerInput.currentActionMap.FindAction("AnyButton").canceled -= OnAnyKey;

        owner.worldMapTutorial.Hide();
    }

    void OnAnyKey(InputAction.CallbackContext obj)
    {
        owner.ChangeState<TraverseWorldMapState>();
    }

    IEnumerator WaitLockout()
    {
        yield return new WaitForSeconds(1f);

        playerInput.currentActionMap.FindAction("AnyButton").canceled += OnAnyKey;
    }
}
