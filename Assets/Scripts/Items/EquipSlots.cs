﻿using UnityEngine;
using System.Collections;

[System.Flags]
public enum EquipSlots
{
	None = -1,
	Primary = 1 << 0,   // usually a weapon (sword etc)
	Secondary = 1 << 1, // usually a shield, but could be another sword (dual-wield) or occupied by two-handed weapon
	Body = 1 << 2,      // body armor, robe, etc
	Accessory = 1 << 3, // ring, belt, etc
	Consumable1 = 1 << 4,
	Consumable2 = 1 << 5,
	Consumable3 = 1 << 6
}