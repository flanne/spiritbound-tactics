﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace flanne.UI
{
    public class ControllerCursor : MonoBehaviour
    {
        #region Fields
        public EventHandler<InfoEventArgs<Vector2>> MoveEvent;
        public EventHandler<InfoEventArgs<bool>> SwitchEvent;

        [SerializeField] SpriteRenderer cursorRenderer;
        [SerializeField] PlayerInput playerInput;
        [SerializeField] float speed;

        public Transform magnetTo = null;
        #endregion

        #region Eventhandlers
        void OnPoint(InputAction.CallbackContext obj)
        {
            if (cursorRenderer.enabled)
                Hide();
        }
        #endregion

        #region LifeCylce
        void OnEnable()
        {
            playerInput.currentActionMap.FindAction("Point").performed += OnPoint;
            this.transform.position = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
        }

        void OnDisable()
        {
            if (playerInput != null)
                playerInput.currentActionMap.FindAction("Point").performed -= OnPoint;

            Hide();
        }

        void FixedUpdate()
        {
            Vector2 moveInput = playerInput.currentActionMap.FindAction("Navigate").ReadValue<Vector2>();

            if (moveInput != Vector2.zero)
            {
                if (!cursorRenderer.enabled)
                    Show();

                this.transform.position += speed * new Vector3(moveInput.x, moveInput.y, 0);

                ConfineToScreen();

                if (MoveEvent != null)
                    MoveEvent(this, new InfoEventArgs<Vector2>(new Vector2(this.transform.position.x, this.transform.position.y)));
            }
            else if (magnetTo != null && Vector3.Distance(this.transform.position, magnetTo.position) > 0.01)
            {
                float step = speed/2;
                this.transform.position = Vector3.MoveTowards(this.transform.position, magnetTo.position, step);

                ConfineToScreen();

                if (MoveEvent != null)
                    MoveEvent(this, new InfoEventArgs<Vector2>(new Vector2(this.transform.position.x, this.transform.position.y)));
            }
        }
        #endregion

        #region Private
        void Show()
        {
            this.transform.position = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
            cursorRenderer.enabled = true;

            // turn off mouse cursor
            Cursor.visible = false;

            if (SwitchEvent != null)
                SwitchEvent(this, new InfoEventArgs<bool>(true));
        }

        void Hide()
        {
            if (cursorRenderer != null)
                cursorRenderer.enabled = false;

            // turn on mouse cursor
            Cursor.visible = true;

            if (SwitchEvent != null)
                SwitchEvent(this, new InfoEventArgs<bool>(false));
        }

        void ConfineToScreen()
        {
            Vector3 viewPos = Camera.main.WorldToViewportPoint(this.transform.position);
            viewPos.x = Mathf.Clamp01(viewPos.x);
            viewPos.y = Mathf.Clamp01(viewPos.y);
            this.transform.position = Camera.main.ViewportToWorldPoint(viewPos);
        }
        #endregion
    }
}
