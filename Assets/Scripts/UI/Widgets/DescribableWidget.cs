﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace flanne.UI
{
    public class DescribableProperties : IUIProperties
    {
        public DescribableObject des;

        public DescribableProperties(DescribableObject d)
        {
            des = d;
        }
    }

    public abstract class DescribableWidget : Widget<DescribableProperties>
    {
        public override void SetProperties(DescribableProperties properties)
        {

        }
    }
}
