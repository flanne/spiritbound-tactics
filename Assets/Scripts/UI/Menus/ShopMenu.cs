﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace flanne.UI
{
    public class ShopMenu : DynamicMenu<EquippableProperties>
    {
        public List<Equippable> items { get; private set; }

        public void AddItems(List<Equippable> i, Unit unit = null)
        {
            items = i;
            Clear();

            foreach (var item in items)
            {
                var entry = AddEntry(new EquippableProperties(item));

                if (unit != null && !unit.equipment.CanEquip(item))
                {
                    var widget = entry.GetComponent<EquippableWidget>();
                    widget.FormatUnavailable();
                }
            }
        }
    }
}

