﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroLoopMusicPlayer
{
    AudioClip musicClip;
    AudioSource musicSource;
    AudioSource loopSource;
    int loopStart;
    int loopLength;

    AudioClip cutClip;

    public IntroLoopMusicPlayer(AudioSource musicSource, AudioSource loopSource)
    {
        this.musicSource = musicSource;
        this.loopSource = loopSource;
    }

    public void Play(AudioClip musicClip, int loopStart, int loopLength)
    {
        this.musicClip = musicClip;
        this.loopStart = loopStart;
        this.loopLength = loopLength;

        float[] sample = new float[loopLength * musicClip.channels];
        musicClip.GetData(sample, loopStart);

        cutClip = AudioClip.Create("loop", loopLength, musicClip.channels, musicClip.frequency, false);
        cutClip.SetData(sample, 0);

        double loopStartTime = loopLength / cutClip.frequency;

        musicSource.clip = musicClip;
        musicSource.Play();
        musicSource.SetScheduledEndTime(loopStartTime);

        loopSource.clip = cutClip;
        loopSource.PlayScheduled(loopStartTime);
        loopSource.loop = true;
    }
}
