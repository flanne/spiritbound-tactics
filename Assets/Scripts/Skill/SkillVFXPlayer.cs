﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillVFXPlayer : MonoBehaviour
{
    #region Visuals
    public void Play(Unit actor, List<BoardTile> targets, bool anyEffectsHit, Skill skill)
	{
		// turn towards targeted tile
		if (actor.tile != targets[0]) // dont change directions if targeting self
			actor.direction = actor.tile.xy.GetDirection(targets[0].xy);

		// Play All Visual Effects
		foreach (BaseSkillVisual v in GetComponentsInChildren<BaseSkillVisual>())
		{
			if (v.onHitOnly && !anyEffectsHit)
				continue;

			List<BoardTile> t = new List<BoardTile>();

			switch (v.targetType)
			{
				case BaseSkillVisual.TargetType.Actor:
					t.Add(actor.tile);
					break;
				case BaseSkillVisual.TargetType.Targets:
					foreach ( var target in targets )
						if (skill.IsTarget(actor, target, false))
							t.Add(target);
					break;
				case BaseSkillVisual.TargetType.Tile:
					t.Add(targets[0]);
					break;
				case BaseSkillVisual.TargetType.None:
					t.Add(targets[0]);
					break;
				case BaseSkillVisual.TargetType.Special:
					v.SpecialPlay(actor, targets[0]);
					break;
				case BaseSkillVisual.TargetType.TargetNoSelf:
					foreach (var target in targets)
						if (skill.IsTarget(actor, target, false) && target.content.GetComponent<Unit>() != actor)
							t.Add(target);
					break;
			}
			v.Play(t);
		}
	}

	public bool IsPlaying()
	{
		foreach (BaseSkillVisual v in GetComponentsInChildren<BaseSkillVisual>())
		{
			if (v.isPlaying)
				return true;
		}

		return false;
	}

	public void ExitAnimations()
	{
		foreach (BaseSkillVisual v in GetComponentsInChildren<BaseSkillVisual>())
			v.Exit();
	}
	#endregion
}
