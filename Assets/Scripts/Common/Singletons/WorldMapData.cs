﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldMapData : MonoBehaviour
{
    public WorldMapLocation currentLocation;

    [SerializeField] WorldMapLocation startingLocation;
    [SerializeField] List<WorldMapLocation> defaultUnlockedLocations;

    public List<WorldMapLocation> unlockedLocations;

    public void InitNew()
    {
        currentLocation = startingLocation;
        unlockedLocations = new List<WorldMapLocation>(defaultUnlockedLocations);
    }

    public void GetDefault()
    {
        unlockedLocations = new List<WorldMapLocation>(defaultUnlockedLocations);
    }

    public void SetWorldMap(List<WorldMapLocation> unlocked, WorldMapLocation current)
    {
        this.currentLocation = current;
        this.unlockedLocations = unlocked;
    }

    public void Unlock(WorldMapLocation location)
    {
        unlockedLocations.Add(location);
    }
}
