﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefillConsumablesSkillEffect : BaseSkillEffect
{
	public override int Predict(Unit actor, BoardTile target, int power, Skill skill)
	{
		return 0;
	}

	protected override int OnApply(Unit actor, BoardTile target, int power, Skill skill)
	{
		Unit defender = target.content.GetComponent<Unit>();

		// Apply turn delay
		var consumables = defender.consumableSkills.GetConsumables();
		foreach (var c in consumables)
			c.Fill();

		return 0;
	}
}
