﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfSkillEffectTarget : SkillEffectTarget
{
    public override bool IsTarget(Unit actor, BoardTile target)
    {
        Stats s = target?.content?.GetComponent<Unit>()?.stats;
        Unit targetUnit = target?.content?.GetComponent<Unit>();
        return (s != null && s[StatTypes.HP] > 0) && (targetUnit != null && actor == targetUnit);
    }
}
