﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitJobRanks : MonoBehaviour
{
    Dictionary<Job, JobRank> jobRanks;

    public JobRank Get(Job job)
    {
        if (jobRanks.ContainsKey(job))
            return jobRanks[job];

        JobRank jobRank = new JobRank(job);
        jobRanks.Add(job, jobRank);
        return jobRank;
    }

    public void LoadRecipe(JobRank[] jobRanksData)
    {
        jobRanks = new Dictionary<Job, JobRank>();
        foreach (JobRank j in jobRanksData)
            jobRanks.Add(j.job, j);
    }

    public JobRank[] GetData()
    {
        List<JobRank> ranks = new List<JobRank>();
        foreach (var j in jobRanks)
            ranks.Add(j.Value);

        return ranks.ToArray();
    }
}
