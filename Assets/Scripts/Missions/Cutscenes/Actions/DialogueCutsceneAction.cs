﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueCutsceneAction : UnitCutsceneAction
{
    public string stringID;

    enum Side { Left, Right};
    [SerializeField] Side dialogueSide;

    [SerializeField] bool continues = false;

    public override void Play()
    {
        StartCoroutine(ActivateDialogue());
    }

    public override void Stop()
    {
        isPlaying = false;
    }

    IEnumerator ActivateDialogue()
    {
        isPlaying = true;

        DialogueController dialogueController = owner.dialogueController;
        if (dialogueSide == Side.Left)
            dialogueController.PlayDialogueLeft(unit, stringID);
        else if (dialogueSide == Side.Right)
            dialogueController.PlayDialogueRight(unit, stringID);

        while (dialogueController.isPlaying)
            yield return null;

        if (!continues)
            dialogueController.Hide();

        isPlaying = false;
    }
}
