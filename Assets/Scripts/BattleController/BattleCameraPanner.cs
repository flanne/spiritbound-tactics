﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Spiritlink.Battle
{
    public class BattleCameraPanner : MonoBehaviour
    {
        public bool active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
                if (_active == true)
                    AddListeners();
                else
                    RemoveListeners();
            }
        }
        bool _active = false;
        [SerializeField] TileSelectIndicator tileSelectIndicator;
        [SerializeField] PlayerInput playerInput;

        void OnNavigate(InputAction.CallbackContext obj)
        {
            Vector2 tileScreenPos = Camera.main.WorldToScreenPoint(tileSelectIndicator.currentTile.GetWorldPosition());
            float screenEdge = 100f;
            float rightEdge = 200f;

            if (tileScreenPos.x < screenEdge || tileScreenPos.y < screenEdge || tileScreenPos.x > Screen.width - rightEdge || tileScreenPos.y > Screen.height - screenEdge)
            {
                Vector3 moveVector = Vector3.zero;
                moveVector = Vector3.Normalize(tileSelectIndicator.transform.position - MasterSingleton.main.MainCameraRig.transform.position);
                MasterSingleton.main.MainCameraRig.transform.position += moveVector;
            }
        }

        void Update()
        {
            if (active)
            {
                float camSpeed = 5f;
                Vector2 mousePos = Mouse.current.position.ReadValue();
                float screenEdge = 5f;

                Board board = MasterSingleton.main.GameLevel.board;
                BoundsInt bounds = board.tileMap.cellBounds;
                Vector3 top = board.CellToWorld(bounds.max);
                Vector3 bot = board.CellToWorld(bounds.min);
                Vector3 left = board.CellToWorld(new Vector3Int(bounds.xMin, 0, 0));
                Vector3 right = board.CellToWorld(new Vector3Int(bounds.xMax, 0, 0));

                CameraRig cameraRig = MasterSingleton.main.MainCameraRig;

                if (mousePos.x < screenEdge && cameraRig.transform.position.x > left.x)
                    cameraRig.transform.position += new Vector3(-(camSpeed * Time.deltaTime), 0, 0);

                if (mousePos.x > Screen.width - screenEdge && cameraRig.transform.position.x < right.x)
                    cameraRig.transform.position += new Vector3(camSpeed * Time.deltaTime, 0, 0);

                if (mousePos.y < screenEdge && cameraRig.transform.position.y > bot.y)
                    cameraRig.transform.position += new Vector3(0, -(camSpeed * Time.deltaTime), 0);

                if (mousePos.y > Screen.height - screenEdge && cameraRig.transform.position.y < top.y)
                    cameraRig.transform.position += new Vector3(0, camSpeed * Time.deltaTime, 0);
            }
        }

        void AddListeners()
        {
            playerInput.currentActionMap.FindAction("Navigate").performed += OnNavigate;
        }

        void RemoveListeners()
        {
            playerInput.currentActionMap.FindAction("Navigate").performed -= OnNavigate;
        }
    }
}
