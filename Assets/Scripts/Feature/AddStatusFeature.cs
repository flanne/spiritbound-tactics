﻿using UnityEngine;
using System.Collections;

public abstract class AddStatusFeature<T> : Feature where T : StatusEffect
{
    #region Protected
    protected override void OnApply(Unit target)
    {
        target.status.Add<T, StatusCondition>();
    }

    protected override void OnRemove(Unit target)
    {
        T statusEffect = target.GetComponentInChildren<T>();
        StatusCondition statusCondition = statusEffect?.GetComponentInChildren<StatusCondition>();

        if (statusCondition != null)
            statusCondition.Remove();
    }
    #endregion
}