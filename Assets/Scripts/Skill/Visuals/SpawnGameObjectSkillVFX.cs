﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public abstract class SpawnGameObjectSkillVFX : BaseSkillVisual
{
    [SerializeField]
    GameObject prefab;

    [SerializeField]
    float yOffset = 0.75f;

    [SerializeField]
    bool cameraFollow = false;
    Transform originalCameraFollow;

    [SerializeField]
    bool randomRotation = false;

    [SerializeField]
    TargetType _targetType;
    public override TargetType targetType { get { return _targetType; } }

    public override void Exit()
    {
    }

    protected override IEnumerator Animate(BoardTile target)
    {
        GameObject obj = Instantiate(prefab);
        Initialize(obj);

        Vector3 pos = target.GetWorldPosition();
        pos.y += yOffset;
        pos.z += 10;
        obj.transform.position = pos;

        if (randomRotation)
            obj.transform.Rotate(0.0f, 0.0f, Random.Range(0.0f, 360.0f), Space.Self);

        if (cameraFollow)
        {
            originalCameraFollow = MasterSingleton.main.MainCameraRig.follow;
            MasterSingleton.main.MainCameraRig.follow = obj?.transform;
        }

        while (!DespawnCondition(obj))
            yield return null;

        Destroy(obj);

        isPlaying = false;
    }

    protected abstract bool DespawnCondition(GameObject obj);
    protected virtual void Initialize(GameObject obj) { }
}
