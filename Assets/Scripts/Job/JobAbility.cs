﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JobAbility : DescribableObject
{
    [SerializeField] int rank;

    [SerializeField]
    Feature[] features;

    public override string GetDescription()
    {
        string description;

        if (rank < 1)
            description = "<color=#8d8d8d>Innate</color>  ";
        else
            description = "<color=#8d8d8d>Rank " + rank + "</color>  ";

        if (features.Length == 1 && features[0] is GrantSkillFeature)
            description += (features[0] as GrantSkillFeature).Skill.GetDescription();
        else
            description += base.GetDescription();

        return description;
    }

    public void Activate(Unit unit)
    {
        foreach (var feat in features)
            feat.Activate(unit);
    }

    public void Deactivate(Unit unit)
    {
        foreach(var feat in features)
            feat.Deactivate(unit);
    }
}
