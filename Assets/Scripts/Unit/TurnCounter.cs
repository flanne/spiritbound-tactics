﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnCounter : MonoBehaviour
{
	#region Fields & Properties
	[SerializeField]
	Unit owner;
	Stats stats { get { return owner.stats; } }

	public int counter
	{
		get { return stats[StatTypes.CTR]; }
		set { stats[StatTypes.CTR] = value; }
	}

	const int minCounter = 0;
	const int maxCounter = 999;
	#endregion

	#region Monobheaviour
	void OnEnable()
	{
		this.AddObserver(OnCTRWillChange, Stats.WillChangeNotification(StatTypes.CTR), stats);
	}

	void OnDisable()
	{
		this.RemoveObserver(OnCTRWillChange, Stats.WillChangeNotification(StatTypes.CTR), stats);
	}
    #endregion

    #region Eventhandlers
    void OnCTRWillChange(object sender, object args)
	{
		ValueChangeException vce = args as ValueChangeException;

		// Clamp hp to Max HP and 0
		vce.AddModifier(new ClampValueModifier(int.MaxValue, minCounter, maxCounter));
	}
    #endregion
}
