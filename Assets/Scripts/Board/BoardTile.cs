﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BoardTile
{
    public Vector2Int xy;
    public int z;
    public GameObject content;
    private Tilemap tileMap;

    // Used for pathfinding
    [HideInInspector] public BoardTile prev;
    [HideInInspector] public int distance;

    public BoardTile(Vector2Int xy, int z, Tilemap tileMap, GameObject content = null)
    {
        this.xy = xy;
        this.z = z;
        this.tileMap = tileMap;
        this.content = content;
    }

    public Vector3 GetWorldPosition()
    {
        Vector3 worldPosition = tileMap.CellToWorld(new Vector3Int(xy.x, xy.y, z));
        worldPosition.y += .5f;
        worldPosition.z += 1.5f;
        return worldPosition;
    }
}
