﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spiritlink.Battle;

public class DivineProtectionPassive : Passive
{
    Skill skill { get { return Resources.Load<Skill>("DivineProtectionSkill"); } }

    bool available = true;
    bool triggered = false;

    protected override void AddListeners()
    {
        this.AddObserver(OnHPWillChange, Stats.WillChangeNotification(StatTypes.HP), owner.stats);
        this.AddObserver(OnAfterSkill, BattleState.AfterSkillEffectsNotification);
        this.AddObserver(OnBattleOver, EndRewardLootState.PostBattleNotifaction);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnHPWillChange, Stats.WillChangeNotification(StatTypes.HP), owner.stats);
        this.RemoveObserver(OnAfterSkill, BattleState.AfterSkillEffectsNotification);
        this.RemoveObserver(OnBattleOver, EndRewardLootState.PostBattleNotifaction);
    }

    void OnHPWillChange(object sender, object args)
    {
        if (available)
        {
            ValueChangeException vce = args as ValueChangeException;
            if (vce.toValue < 1)
            {
                vce.AddModifier(new ClampValueModifier(int.MaxValue - 1, 1, int.MaxValue));

                available = false;
                triggered = true;
            }
        }
    }

    void OnAfterSkill(object sender, object args)
    {
        if (triggered)
        {
            var effects = args as List<SkillDirections>;
            effects.Add(new SkillDirections(skill, owner, owner.tile));

            triggered = false;
        }
    }

    void OnBattleOver(object sender, object args)
    {
        available = true;
    }
}
