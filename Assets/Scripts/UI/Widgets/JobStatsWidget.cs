﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class JobRankProperties : IUIProperties
    {
        public JobRank jobRank;

        public JobRankProperties(JobRank j)
        {
            jobRank = j;
        }
    }

    public class JobStatsWidget : Widget<JobRankProperties>
    {
        [SerializeField] Image icon;
        [SerializeField] TMP_Text nameTMP;
        [SerializeField] TMP_Text rankTMP;
        [SerializeField] TMP_Text xpTMP;
        [SerializeField] JobXPBar xpBar;

        public override void SetProperties(JobRankProperties properties)
        {
            JobRank jobRank = properties.jobRank;

            icon.sprite = jobRank.job.icon;
            nameTMP.text = MasterSingleton.main.GameStrings.Get(jobRank.job.nameStringID);

            rankTMP.text = jobRank.rank.ToString();

            if (jobRank.rank < 4)
                xpTMP.text = jobRank.XP.ToString() + "/" + JobRank.XPToLevel[jobRank.rank - 1];
            else
                xpTMP.text = "Maxed!";

            if (jobRank.rank < 4)
                xpBar.SetTo(jobRank.XP, JobRank.XPToLevel[jobRank.rank - 1]);
            else
                xpBar.SetTo(1, 1);
        }

        public void FormatUnavailable()
        {
            if (icon != null)
                icon.color = new Color32(150, 150, 150, 255);

            if (nameTMP != null)
                nameTMP.color = new Color32(150, 150, 150, 255);
        }
    }
}
