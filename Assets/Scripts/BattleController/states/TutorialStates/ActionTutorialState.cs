﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class ActionTutorialState : TutorialState<SkillTargetState>
    {
        protected override Panel tutorialPanel { get { return owner.actionTutorial; } }
    }
}