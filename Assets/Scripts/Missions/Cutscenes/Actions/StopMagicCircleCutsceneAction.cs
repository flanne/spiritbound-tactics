﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopMagicCircleCutsceneAction : UnitCutsceneAction
{
    public override void Play()
    {
        unit.GetComponentInChildren<MagicCircleEffect>().Deactivate();

        isPlaying = false;
    }
}
