﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using TMPro;

public class Tooltip : MonoBehaviour
{
    [SerializeField] Camera uiCamera;

    [SerializeField] TMP_Text tooltipText;
    [SerializeField] RectTransform backgroundRectTransform;

    void Awake()
    {
        HideTooltip();
    }

    void Update() 
    {
        Vector2 mousePos = Mouse.current.position.ReadValue();
        transform.position = mousePos;

        if (mousePos.x > Screen.width/2)
            backgroundRectTransform.pivot = new Vector2(1f, 0f);
        else
            backgroundRectTransform.pivot = new Vector2(0f, 0f);
    }

    public void ShowTooltip(string tooltipString) 
    {
        gameObject.SetActive(true);

        tooltipText.text = tooltipString;
        float textPaddingSize = 8f;
        float width = 0;
        if (tooltipText.preferredWidth < ((RectTransform) tooltipText.transform).rect.width)
            width = tooltipText.preferredWidth;
        else
            width = ((RectTransform) tooltipText.transform).rect.width;

        Vector2 backgroundSize = new Vector2(width + (textPaddingSize * 2f), tooltipText.preferredHeight + (textPaddingSize * 2f));
        backgroundRectTransform.sizeDelta = backgroundSize;
        Update();
    }

    public void HideTooltip() 
    {
        gameObject.SetActive(false);
    }
}
