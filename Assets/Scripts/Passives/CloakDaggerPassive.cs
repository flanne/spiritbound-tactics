﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloakDaggerPassive : Passive
{
    public Skill shadowStep;

    protected override void AddListeners()
    {
        this.AddObserver(OnSkillPerform, Skill.DidPerformNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnSkillPerform, Skill.DidPerformNotification);
    }

    void OnSkillPerform(object sender, object args)
    {
        Unit actor = args as Unit;
        Skill skill = sender as Skill;

        if (actor == owner && skill == shadowStep)
        {
            var statusCondition = actor.status.Add<MightStatusEffect, TurnStatusCondition>(actor);
			statusCondition.turns = 1;
        }    
    }
}
