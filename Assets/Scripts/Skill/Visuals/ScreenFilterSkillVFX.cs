﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenFilterSkillVFX : BaseSkillVisual
{
    private ScreenFilter screenFilter { get { return MasterSingleton.main.ScreenFilter; } }

    public override TargetType targetType { get { return TargetType.None; } }

    [SerializeField]
    Color color;

    [SerializeField]
    float transitionInTime;

    [SerializeField]
    float duration;

    [SerializeField]
    float transitionOutTime;

    public override void Exit()
    {
    }

    protected override IEnumerator Animate(BoardTile target)
    {
        yield return null;
        screenFilter.TransitionToColor(color, transitionInTime, FadeOut);
    }

    void FadeOut()
    {
        StartCoroutine(WaitThenFade());
    }

    IEnumerator WaitThenFade()
    {
        yield return new WaitForSeconds(duration);

        screenFilter.TransitionToClear(transitionOutTime);
        isPlaying = false;
    }
}