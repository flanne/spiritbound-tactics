﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace flanne.UI
{

    public class DescriptionBox : Panel
    {
        #region Fields
        [SerializeField] DescribableMenu keywordLeft;
        [SerializeField] DescribableMenu keywordRight;
        [SerializeField] DescribableFullWidget describableWidget;


        public enum Side
        {
            Left,
            Right
        }
        #endregion

        #region Public
        public void SetToDescribable(DescribableObject des, Side side = Side.Left)
        {
            keywordLeft.Clear();
            keywordRight.Clear();

            // Set to widget
            describableWidget.SetProperties(new DescribableProperties(des));

            // Set Keywords
            DescribableMenu keywordMenu;
            if (side == Side.Left)
                keywordMenu = keywordLeft;
            else
                keywordMenu = keywordRight;

            foreach (var keyword in des.keywords)
                keywordMenu.AddEntry(new DescribableProperties(keyword));
        }

        public void SetDescriptionOverride(string description)
        {
            describableWidget.SetDescriptionOverride(description);
        }

        public override void Show()
        {
            base.Show();

            keywordLeft.Show();
            keywordRight.Show();
        }

        public override void Hide()
        {
            base.Hide();

            keywordLeft.Hide();
            keywordRight.Hide();
        }
        #endregion
    }
}
