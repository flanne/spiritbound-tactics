﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SkillTag 
{
    Weapon,
    Skill,
    Spell,
    Item,
    Passive,
    Status,
    Movement,
    Potion,
    Flask
}
