﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Spiritlink.Battle
{
    [InlineProperty(LabelWidth = 0)]
    [System.Serializable]
    public class VictoryConditionData
    {
        enum ConditionType
        {
            DefeatAll,
            DefeatAny,
            DefeatTarget
        };

        [EnumToggleButtons]
        [HideLabel]
        [SerializeField]
        ConditionType conditionType;

        [ShowIf("conditionType", ConditionType.DefeatTarget)]
        [SerializeField]
        IDEnum unitTargetID;

        GameStrings gameStrings { get { return MasterSingleton.main.GameStrings; } }

        public BaseVictoryCondition AddComponent(GameObject obj, AllianceType alliance)
        {
            BaseVictoryCondition vc = null;

            switch (conditionType)
            {
                case ConditionType.DefeatAll:
                    vc = obj.AddComponent<DefeatAllEnemiesVictoryCondition>();
                    break;
                case ConditionType.DefeatAny:
                    vc = obj.AddComponent<DefeatAnyEnemiesVictoryCondition>();
                    break;
                case ConditionType.DefeatTarget:
                    vc = obj.AddComponent<DefeatTargetVictoryCondition>();
                    (vc as DefeatTargetVictoryCondition).target = UnitID.GetUnitWithID(unitTargetID);
                    break;
            }

            vc.alliance = alliance;
            vc.bc = obj.GetComponent<BattleController>();

            return vc;
        }

        public string GetVictoryConditionDescription(AllianceType alliance)
        {
            string vc = "";

            if (alliance == AllianceType.Hero)
            {
                switch (conditionType)
                {
                    case ConditionType.DefeatAll:
                        vc = gameStrings.Get("HERO_VICTORY_DEFEAT_ALL");
                        break;
                    case ConditionType.DefeatAny:
                        vc = gameStrings.Get("HERO_VICTORY_DEFEAT_ANY");
                        break;
                    case ConditionType.DefeatTarget:
                        vc = gameStrings.Get("HERO_VICTORY_DEFEAT_TARGET") + " " + UnitID.GetUnitWithID(unitTargetID).unitName;
                        break;
                }
            }
            else
            {
                switch (conditionType)
                {
                    case ConditionType.DefeatAll:
                        vc = gameStrings.Get("VILLAIN_VICTORY_DEFEAT_ALL");
                        break;
                    case ConditionType.DefeatAny:
                        vc = gameStrings.Get("VILLAIN_VICTORY_DEFEAT_ANY");
                        break;
                    case ConditionType.DefeatTarget:
                        vc = UnitID.GetUnitWithID(unitTargetID).unitName + " " + gameStrings.Get("VILLAIN_VICTORY_DEFEAT_TARGET");
                        break;
                }

            }

            return vc;
        }
    }
}
