﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace flanne.UI
{
    public class JobChangeController : MonoBehaviour
    {
        #region Fields
        public event EventHandler<InfoEventArgs<DescribableObject>> DescribableSelectEvent;
        public event EventHandler<InfoEventArgs<Job>> JobClickEvent;

        [SerializeField] JobListMenu jobListMenu;
        [SerializeField] ScrollRect scroller;
        [SerializeField] AbilityTreeController abilityTreePreview;
        [SerializeField] JobEquippablesWidget jobEquippablesWidget;
        [SerializeField] Panel panel;

        Unit _unit;
        #endregion

        #region EventHandlers
        void OnJobClick(object sender, InfoEventArgs<int> e)
        {
            Job job = GetPlayableJobs()[e.info];
            
            if (JobClickEvent != null)
                JobClickEvent(this, new InfoEventArgs<Job>(job));
        }

        void OnJobSelect(object sender, InfoEventArgs<int> e)
        {
            Job job = GetPlayableJobs()[e.info];
            JobRank jobRank = null;
            if (_unit != null)
                jobRank = _unit.jobRanks.Get(job);

            abilityTreePreview.SetToJob(job);
            abilityTreePreview.SetToJobRank(jobRank);

            if(DescribableSelectEvent != null)
                DescribableSelectEvent(this, new InfoEventArgs<DescribableObject>(job));

            // Update equippables
            jobEquippablesWidget.SetProperties(new JobProperties(job));
        }

        void OnAbilitySelect(object sender, InfoEventArgs<DescribableObject> e)
        {
            if (DescribableSelectEvent != null)
                DescribableSelectEvent(this, new InfoEventArgs<DescribableObject>(e.info));
        }
        #endregion

        #region Public
        public void SetToUnit(Unit unit)
        {
            _unit = unit;

            jobListMenu.Clear();

            foreach(var j in GetPlayableJobs())
            {
                JobRank jr = unit.jobRanks.Get(j);
                var entry = jobListMenu.AddEntry(new JobRankProperties(jr));
                var widget = entry.GetComponent<JobStatsWidget>();
                if (widget != null)
                {
                    if (!j.PrereqsMet(_unit))
                        widget.FormatUnavailable();
                }
            }
        }

        public void Show()
        {
            if (panel != null)
                panel.Show();

            jobListMenu.Show();
            abilityTreePreview.Show();

            jobListMenu.ClickEvent += OnJobClick;
            jobListMenu.SelectEvent += OnJobSelect;
            abilityTreePreview.SelectEvent += OnAbilitySelect;

            jobListMenu.SelectFirstAvailable();
        }

        public void Hide()
        {
            jobListMenu.Clear();

            if (panel != null)
                panel.Hide();

            jobListMenu.Hide();
            abilityTreePreview.Hide();

            jobListMenu.ClickEvent -= OnJobClick;
            jobListMenu.SelectEvent -= OnJobSelect;
            abilityTreePreview.SelectEvent -= OnAbilitySelect;
        }

        public void ShowEquippables()
        {
            jobEquippablesWidget.Show();
        }

        public void HideEquippables()
        {
            jobEquippablesWidget.Hide();
        }
        #endregion

        #region Private
        Job[] GetPlayableJobs()
        {
            return MasterSingleton.main.PlayableJobList.Get();
        }
        #endregion
    }
}
