﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AttackOption
{
	#region Classes
	class Mark
	{
		public BoardTile tile;
		public bool isMatch;

		public Mark(BoardTile tile, bool isMatch)
		{
			this.tile = tile;
			this.isMatch = isMatch;
		}
	}
	#endregion

	#region Fields
	public BoardTile target;
	public Direction direction;
	public List<BoardTile> areaTargets = new List<BoardTile>();
	public bool isCasterMatch;
	public BoardTile bestMoveBoardTile { get; private set; }
	public int bestAngleBasedScore { get; private set; }
	List<Mark> marks = new List<Mark>();
	List<BoardTile> moveTargets = new List<BoardTile>();
	#endregion

	#region Public
	public void AddMoveTarget(BoardTile tile)
	{
		// Dont allow moving to a tile that would negatively affect the caster
		if (!isCasterMatch && areaTargets.Contains(tile))
			return;
		moveTargets.Add(tile);
	}

	public void AddMark(BoardTile tile, bool isMatch)
	{
		marks.Add(new Mark(tile, isMatch));
	}

	// Scores the option based on how many of the targets are of the desired type
	public int GetScore(Unit caster, Skill skill)
	{
		GetBestMoveTarget(caster, skill);
		if (bestMoveBoardTile == null)
			return 0;

		int score = 0;
		for (int i = 0; i < marks.Count; ++i)
		{
			if (Mathf.Abs(skill.GetPredictionClamped(caster, marks[i].tile)) > 0)
			{
				if (marks[i].isMatch)
					score += 1;
				else
					score -= 1;
			}
		}

		if (isCasterMatch && areaTargets.Contains(bestMoveBoardTile))
		{
			if (Mathf.Abs(skill.GetPredictionClamped(caster, caster.tile)) > 0)
				score++;
		}

		return score;
	}
	#endregion

	#region Private
	// Returns the tile which is the most effective point for the caster to attack from
	void GetBestMoveTarget(Unit caster, Skill skill)
	{
		if (moveTargets.Count == 0)
			return;

		if (skill.IsAngleBased())
		{
			bestAngleBasedScore = int.MinValue;
			BoardTile startBoardTile = caster.tile;
			Direction startDirection = caster.direction;
			caster.direction = direction;

			List<BoardTile> bestOptions = new List<BoardTile>();
			for (int i = 0; i < moveTargets.Count; ++i)
			{
				caster.Place(moveTargets[i]);
				int score = GetAngleBasedScore(caster);
				if (score > bestAngleBasedScore)
				{
					bestAngleBasedScore = score;
					bestOptions.Clear();
				}

				if (score == bestAngleBasedScore)
				{
					bestOptions.Add(moveTargets[i]);
				}
			}

			caster.Place(startBoardTile);
			caster.direction = startDirection;

			FilterBestMoves(bestOptions);
			bestMoveBoardTile = bestOptions[UnityEngine.Random.Range(0, bestOptions.Count)];
		}
		else
		{
			bestMoveBoardTile = moveTargets[UnityEngine.Random.Range(0, moveTargets.Count)];
		}
	}

	// Scores the option based on how many of the targets are a match
	// and considers the angle of attack to each mark
	int GetAngleBasedScore(Unit caster)
	{
		int score = 0;
		for (int i = 0; i < marks.Count; ++i)
		{
			int value = marks[i].isMatch ? 1 : -1;
			int multiplier = MultiplierForAngle(caster, marks[i].tile);
			score += value * multiplier;
		}
		return score;
	}

	void FilterBestMoves(List<BoardTile> list)
	{
		if (!isCasterMatch)
			return;

		bool canTargetSelf = false;
		for (int i = 0; i < list.Count; ++i)
		{
			if (areaTargets.Contains(list[i]))
			{
				canTargetSelf = true;
				break;
			}
		}

		if (canTargetSelf)
		{
			for (int i = list.Count - 1; i >= 0; --i)
			{
				if (!areaTargets.Contains(list[i]))
					list.RemoveAt(i);
			}
		}
	}

	int MultiplierForAngle(Unit caster, BoardTile tile)
	{
		if (tile.content == null)
			return 0;

		Unit defender = tile.content.GetComponentInChildren<Unit>();
		if (defender == null)
			return 0;

		Facings facing = caster.GetFacing(defender);
		if (facing == Facings.Back)
			return 90;
		if (facing == Facings.Side)
			return 75;
		return 50;
	}
	#endregion
}