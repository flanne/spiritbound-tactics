﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class ScrollRectExtensions
{
    public static void SnapTo(this ScrollRect scroller, RectTransform child)
    {
        Canvas.ForceUpdateCanvases();

        var contentPos = (Vector2)scroller.transform.InverseTransformPoint(scroller.content.position);
        var childPos = (Vector2)scroller.transform.InverseTransformPoint(child.position);
        var endPos = contentPos - childPos;
        // If no horizontal scroll, then don't change contentPos.x
        if (!scroller.horizontal) endPos.x = scroller.content.anchoredPosition.x;
        // If no vertical scroll, then don't change contentPos.y
        if (!scroller.vertical) endPos.y = scroller.content.anchoredPosition.y;
        scroller.content.anchoredPosition = endPos;
    }

    public static void EnsureVisibility(this ScrollRect scrollRect, RectTransform child, float padding = 0)
    {
        Debug.Assert(child.parent == scrollRect.content,
            "EnsureVisibility assumes that 'child' is directly nested in the content of 'scrollRect'");

        float viewportHeight = scrollRect.viewport.rect.height;
        Vector2 scrollPosition = scrollRect.content.anchoredPosition;

        float elementTop = child.anchoredPosition.y;
        float elementBottom = elementTop - child.rect.height;

        float visibleContentTop = -scrollPosition.y - padding;
        float visibleContentBottom = -scrollPosition.y - viewportHeight + padding;

        float scrollDelta =
            elementTop > visibleContentTop ? visibleContentTop - elementTop :
            elementBottom < visibleContentBottom ? visibleContentBottom - elementBottom :
            0f;

        scrollPosition.y += scrollDelta;
        scrollRect.content.anchoredPosition = scrollPosition;
    }
}
