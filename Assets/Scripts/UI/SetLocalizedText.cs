﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SetLocalizedText : MonoBehaviour
{
    [SerializeField] string stringID;
    [SerializeField] TMP_Text tmp;

    void Start()
    {
        tmp.text = MasterSingleton.main.GameStrings.Get(stringID);
    }
}
