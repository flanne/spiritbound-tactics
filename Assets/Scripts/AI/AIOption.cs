﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AIOption
{
    public AIOptionCondition condition;
    public AITargeting targeting;
    public Skill skill;

    int counter = 1; // used for skills that are cast every so many turns
    
    public bool Count()
    {
        counter--;

        if (counter == 0)
        {
            counter = condition.turns;
            return true;
        }
        else
            return false;
    }
}
