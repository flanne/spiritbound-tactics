﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetShadowCutsceneAction : UnitCutsceneAction
{
    [SerializeField] bool on;

    public override void Play()
    {
        unit.shadow.enabled = on;
    }
}
