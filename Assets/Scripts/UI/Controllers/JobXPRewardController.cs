﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace flanne.UI
{
    public class JobXPRewardController : DynamicMenu<UnitProperties>
    {
        public void RewardXP(int index, int oldRank, int newRank, int oldXP, int newXP)
        {
            var widget = entries[index]?.GetComponent<JobXPRewardWidget>();
            if (widget == null)
                return;

            StartCoroutine(XPBarAnimCoroutine(widget, oldRank, newRank, oldXP, newXP));
        }

        IEnumerator XPBarAnimCoroutine(JobXPRewardWidget widget, int oldRank, int newRank, int oldXP, int newXP)
        {
            // Fill up full bar for each level gained
            for (int i = 0; i < newRank - oldRank; i++)
            {
                float fillAnimDuration = 1.0f;
                widget.jobXPBar.AnimateChange(oldXP, JobRank.XPToLevel[oldRank + i], JobRank.XPToLevel[oldRank + i], fillAnimDuration);

                yield return new WaitForSeconds(fillAnimDuration);

                widget.rankTMP.text = (i + 1 + oldRank).ToString();

                // animate from 0 if gained more than one rank
                oldXP = 0;
            }

            // Finish animating xp bar
            float animDuration = 2.0f;
            if (newRank < 4)
                widget.jobXPBar.AnimateChange(oldXP, newXP, JobRank.XPToLevel[newRank - 1], animDuration, LeanTweenType.easeOutQuad);
        }
    }
}
