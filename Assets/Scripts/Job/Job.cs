﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

public class Job : DescribableObject
{
	#region Fields / Properties
	[System.Serializable]
	struct Prereq
	{
		public Job job;
		public int rank;
	}

	[SerializeField] bool unobtainableClass;
	[HideIf("unobtainableClass")]
	[SerializeField] Prereq[] prereqs;

	public Sprite[] spriteSheet = new Sprite[0];

	[HorizontalGroup("BasicleInfo",100)]

	[VerticalGroup("BasicleInfo/Visuals")]
	[PreviewField(100)]
	[HideLabel]
	public Sprite portrait;

	[VerticalGroup("BasicleInfo/Stats Multipliers"), LabelWidth(50)]
	public float MHP, ATK, DEF, MAT, MDF;

	[VerticalGroup("BasicleInfo/Flat Stats"), LabelWidth(50)]
	public int MOV, JMP, EVD, initiative;

	[BoxGroup("Equipment")]
	public WeaponType[] usableWeapons;

	[BoxGroup("Equipment")]
	public ArmorType[] usableArmors;

	[BoxGroup("Equipment")]
	public bool useAccessories = true;

	[BoxGroup("Equipment")]
	public bool useConsumables = true;

	[BoxGroup("Equipment")]
	public bool useOffhands = true;

	[BoxGroup("Abilities")]
	[LabelWidth(75)]
	public JobAbility innate, Rank1;

	[BoxGroup("Abilities")]
	[TableList]
	public JobAbilityPair[] abilityPairs;
	#endregion

	#region Public
	public override string GetDescription()
	{
		string description = "";
		if (prereqs.Length > 0)
		{
			description += "Requires: ";
			foreach (var p in prereqs)
			{
				description += (MasterSingleton.main.GameStrings.Get(p.job.nameStringID) + " " + p.rank + ", ");
			}

			// remove comma and add new line
			description = description.Remove(description.Length - 2);
			description += "\n";
		}

		description += base.GetDescription();
		return description;
	}

	public void Employ(Unit unit)
	{
		JobRank jobRank = unit.jobRanks.Get(this);
		AbilityTree abilityTree = jobRank.abilityTree;

		Rank1?.Activate(unit);

		// Activate abilities for rank 2-5 on the ability tree
		for (int i=2; i<=4; i++)
        {
			if (abilityTree.GetSelection(i) == AbilityTreeSelection.Left)
				abilityPairs[i - 2].left.Activate(unit);
			else if (abilityTree.GetSelection(i) == AbilityTreeSelection.Right)
				abilityPairs[i - 2].right.Activate(unit);
		}
	}

	public void UnEmploy(Unit unit)
	{
		JobRank jobRank = unit.jobRanks.Get(this);
		AbilityTree abilityTree = jobRank.abilityTree;

		Rank1?.Deactivate(unit);

		// Deactivate abilities for rank 2-5 on the ability tree
		for (int i = 2; i <= 4; i++)
		{
			if (abilityTree.GetSelection(i) == AbilityTreeSelection.Left)
				abilityPairs[i - 2].left.Deactivate(unit);
			else if (abilityTree.GetSelection(i) == AbilityTreeSelection.Right)
				abilityPairs[i - 2].right.Deactivate(unit);
		}
	}

	public void ActivateInnate(Unit unit)
	{
		innate?.Activate(unit);
	}

	public void DeactivateInnate(Unit unit)
	{
		innate?.Deactivate(unit);
	}

	public void LoadSpriteSheet(Unit unit)
	{
		if (unit.GetComponent<UniqueSprite>() == null)
			unit.unitSpriteSheet.LoadSpriteSheet(spriteSheet);
	}

	public void LoadDefaultStats(Unit unit)
	{
		int level = unit.level.rank;
		Stats stats = unit.stats;

		stats.SetValue(StatTypes.MHP, (int)(Stats.GetBaseStat(level) * 2 * MHP), false, false);
		stats.SetValue(StatTypes.ATK, (int)(Stats.GetBaseStat(level) * ATK), false, false);
		stats.SetValue(StatTypes.DEF, (int)(Stats.GetBaseStat(level) * DEF), false, false);
		stats.SetValue(StatTypes.MAT, (int)(Stats.GetBaseStat(level) * MAT), false, false);
		stats.SetValue(StatTypes.MDF, (int)(Stats.GetBaseStat(level) * MDF), false, false);

		stats.SetValue(StatTypes.HP, stats[StatTypes.MHP], false, false);

		stats.SetValue(StatTypes.MOV, MOV, false, false);
		stats.SetValue(StatTypes.JMP, JMP, false, false);
		stats.SetValue(StatTypes.EVD, EVD, false, false);
	}

	public int GetDefaultStat(StatTypes statType, int level)
    {
		int stat = 0;
		switch(statType)
        {
			case StatTypes.MHP:
				stat = (int)(Stats.GetBaseStat(level) * MHP);
				break;
			case StatTypes.ATK:
				stat = (int)(Stats.GetBaseStat(level) * ATK);
				break;
			case StatTypes.DEF:
				stat = (int)(Stats.GetBaseStat(level) * DEF);
				break;
			case StatTypes.MAT:
				stat = (int)(Stats.GetBaseStat(level) * MAT);
				break;
			case StatTypes.MDF:
				stat = (int)(Stats.GetBaseStat(level) * MDF);
				break;
			case StatTypes.MOV:
				stat = MOV;
				break;
			case StatTypes.JMP:
				stat = JMP;
				break;
		}

		return stat;
    }

	public bool PrereqsMet(Unit unit)
    {
		if (unobtainableClass)
			return false;

		foreach (var p in prereqs)
        {
			JobRank jobRank = unit.jobRanks.Get(p.job);
			if (jobRank.rank < p.rank)
				return false;
        }

		return true;
    }
	#endregion
}