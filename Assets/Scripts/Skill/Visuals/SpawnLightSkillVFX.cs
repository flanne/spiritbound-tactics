﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;


public class SpawnLightSkillVFX : BaseSkillVisual
{
    [SerializeField]
    float transitionInTime;

    [SerializeField]
    float duration;

    [SerializeField]
    float transitionOutTime;

    [SerializeField]
    GameObject prefab;

    [SerializeField]
    float yOffset = 0.75f;

    [SerializeField]
    float xOffset = 0;

    [SerializeField]
    TargetType _targetType;
    public override TargetType targetType { get { return _targetType; } }

    protected override IEnumerator Animate(BoardTile target)
    {
        GameObject lightObj = Instantiate(prefab);
        var pos = target.GetWorldPosition();
        pos.x += xOffset;
        pos.y += yOffset;
        lightObj.transform.position = pos;
        Light2D light = lightObj.GetComponent<Light2D>();

        float intensity = light.intensity;
        light.intensity = 0;

        float timer = 0;

        while (timer < transitionInTime)
        {
            light.intensity = (timer / transitionInTime) * intensity;

            timer += Time.deltaTime;
            yield return null;
        }

        light.intensity = intensity;

        while ((timer - transitionInTime) < duration)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        while ((timer - (transitionInTime + duration)) < transitionOutTime)
        {
            light.intensity = intensity - (((timer - (transitionInTime + duration)) / transitionOutTime) * intensity);

            timer += Time.deltaTime;
            yield return null;
        }

        yield return null;

        Destroy(lightObj);
        isPlaying = false;
    }
}