﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Spiritlink.Battle;

public class BleedStatusEffect : StatusEffect
{
    protected override string nameID { get { return "KEYWORD_BLEED"; } }
    public override bool isBuff { get { return false; } }
    public override string spriteFileName { get { return "icon_sts_bleed"; } }
    public override Keyword keyword { get { return Resources.Load<Keyword>("Bleed"); } }

    Skill bleedSkillSmall { get { return Resources.Load<Skill>("sk_bleed_small"); } }
    Skill bleedSkillMed { get { return Resources.Load<Skill>("sk_bleed_med"); } }
    Skill bleedSkillLarge { get { return Resources.Load<Skill>("sk_bleed_large"); } }

    int turnCounter = 0;

    protected override void OnApply()
    {
        this.AddObserver(OnNewTurn, BeforeTurnState.StartOfTurnEffectsNotification, owner);
    }

    protected override void OnRemove()
    {
        this.RemoveObserver(OnNewTurn, BeforeTurnState.StartOfTurnEffectsNotification, owner);
    }

    void OnNewTurn(object sender, object args)
    {
        Skill skill;
        if (turnCounter == 0)
            skill = bleedSkillSmall;
        else if (turnCounter == 1)
            skill = bleedSkillMed;
        else
            skill = bleedSkillLarge;

        var effects = args as List<SkillDirections>;
        effects.Add(new SkillDirections(skill, owner, owner.tile));

        turnCounter++;
    }
}