﻿using UnityEngine;
using System.Collections;
using Spiritlink.Battle;

public class DurationStatusCondition : StatusCondition
{
    public int duration = 10;

    void OnEnable()
    {
        this.AddObserver(OnNewRound, TurnOrderController.RoundBeganNotification);
    }

    void OnDisable()
    {
        this.RemoveObserver(OnNewRound, TurnOrderController.RoundBeganNotification);
    }

    void OnNewRound(object sender, object args)
    {
        duration--;
        if (duration <= 0)
            Remove();
    }
}