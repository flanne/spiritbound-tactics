﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyStatusSkillEffect<T> : BaseSkillEffect where T : StatusEffect
{
	#region Public
	public override int Predict(Unit actor, BoardTile target, int power, Skill skill)
	{
		return 0;
	}

	public override int PredictClamped(Unit actor, BoardTile target, int power, Skill skill)
	{
		return 1;
	}

	// Power specifies duration
	protected override int OnApply(Unit actor, BoardTile target, int power, Skill skill)
	{
		Unit targetUnit = target.content.GetComponent<Unit>();
		// negative power indicates infinite duration
		if (power <= 0)
		{
			targetUnit.status.Add<T, StatusCondition>(actor);
		}
		else
		{
			var statusCondition = targetUnit.status.Add<T, TurnStatusCondition>(actor);

			int turns = power;
			var info = new Info<Unit, Unit>(actor, targetUnit);
			turns = turns.NotifyModifiers<Info<Unit, Unit>>(StatusCondition.TweakStatusDurationNotification, this, info);

			statusCondition.turns = turns;
		}

		return power;
	}
    #endregion
}
