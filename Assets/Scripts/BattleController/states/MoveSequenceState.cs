﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spiritlink.Battle
{
    public class MoveSequenceState : BattleState
    {
        #region LifeCycle
        public override void Enter()
        {
            base.Enter();
            turn.hasUnitMoved = true;

            // Set animation speed
            if (!MasterSingleton.main.MissionLog.current.disableBattleSpeed)
                Time.timeScale = MasterSingleton.main.GameSettings.battleSpeed;

            StartCoroutine("Sequence");

            // Hide Keybinds
            keybindPromptUI.Hide();
        }

        public override void Exit()
        {
            base.Exit();

            Time.timeScale = 1;
        }
        #endregion

        #region Private
        IEnumerator Sequence()
        {
            Movement m = owner.turn.actor.movement;
            yield return StartCoroutine(m.Traverse(currentTile));
            owner.ChangeState<ActionSelectState>();
        }
        #endregion
    }
}