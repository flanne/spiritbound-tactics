﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddDmgUpPerStatusFeature : AddPassiveFeature<DmgUpPerStatusPassive>
{
    [SerializeField]
    float perStackBonus;

    [SerializeField]
    int maxBonus;

    protected override void SetParameters(DmgUpPerStatusPassive passive)
    {
        passive.perStackBonus = this.perStackBonus;
        passive.maxBonus = this.maxBonus;
    }
}

