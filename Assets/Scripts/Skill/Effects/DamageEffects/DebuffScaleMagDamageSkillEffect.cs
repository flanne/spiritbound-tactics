﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebuffScaleMagDamageSkillEffect : MagicDamageSkillEffect
{
	public override int GetAttack(Unit actor, Unit targetUnit)
	{
		var statusEffects = targetUnit.status.GetComponentsInChildren<StatusEffect>();
		int debuffCount = 0;

		foreach (var s in statusEffects)
			if (!s.isBuff)
				debuffCount++;

		return Mathf.FloorToInt(actor.stats[StatTypes.MAT] * (1f + (debuffCount * 0.1f)));
	}

	public override int GetDefence(Unit actor, Unit targetUnit)
	{
		return targetUnit.stats[StatTypes.MDF];
	}
}
