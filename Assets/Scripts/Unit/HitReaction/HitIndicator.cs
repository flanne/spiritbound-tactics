﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HitIndicator : MonoBehaviour
{
    public TextMeshPro text;

    float duration = 0.6f;
    float fadeOutTime = 0.1f;

    float timer = 0.0f;

    public bool startedFade { get; private set; }

    void Start()
    {
        startedFade = false;

        LeanTween.moveLocal(this.gameObject, this.transform.localPosition + new Vector3(0f, 0.1f, 0f), duration).setEase(LeanTweenType.easeOutElastic);
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= duration)
        {
            startedFade = true;

            float alpha = 1 - ((timer - duration) / fadeOutTime);

            if (alpha > 0)
            {
                Color textColor = text.color;
                textColor.a = alpha;
                text.color = textColor;
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
    }
}
