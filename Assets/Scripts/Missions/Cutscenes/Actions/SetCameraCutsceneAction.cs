﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetCameraCutsceneAction : CutsceneAction
{
    [SerializeField]
    Vector3 position;

    public override void Play()
    {
        MasterSingleton.main.MainCameraRig.follow = null;
        MasterSingleton.main.MainCameraRig.transform.position = position;
    }
}