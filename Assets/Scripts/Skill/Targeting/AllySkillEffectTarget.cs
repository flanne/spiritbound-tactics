﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllySkillEffectTarget : SkillEffectTarget
{
    public override bool IsTarget(Unit actor, BoardTile target)
    {
        Stats s = target?.content?.GetComponent<Unit>()?.stats;
        Alliance a = target?.content?.GetComponent<Unit>()?.alliance;
        return (s != null && s[StatTypes.HP] > 0) && (a != null && actor.alliance.IsMatch(a, AITargeting.Ally));
    }
}
