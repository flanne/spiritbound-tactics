﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackstabDamagePassive : Passive
{
    float multiplier = 1.5f;

    protected override void AddListeners()
    {
        this.AddObserver(OnDamage, DamageSkillEffect.TweakDamageNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnDamage, DamageSkillEffect.TweakDamageNotification);
    }

    void OnDamage(object sender, object args)
    {
        var info = args as Info<List<ValueModifier>, Info<Unit, Unit, Skill>>;
        Unit actor = info.arg1.arg0;
        Unit target = info.arg1.arg1;
        Skill skill = info.arg1.arg2;

        if (actor != owner)
            return;

        // Backstab are weapon attacks from back side
        if (actor.GetFacing(target) == Facings.Back 
            && skill.ContainsTag(SkillTag.Weapon))
            info.arg0.Add(new MultValueModifier(1, multiplier));
    }
}
