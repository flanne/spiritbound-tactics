﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddStatBonusDamageFeature : AddPassiveFeature<StatBonusDamagePassive>
{
    [SerializeField]
    StatTypes stat;

    [SerializeField]
    float percentStatToAdd;

    protected override void SetParameters(StatBonusDamagePassive passive)
    {
        passive.stat = this.stat;
        passive.percentStatToAdd = this.percentStatToAdd;
    }
}
