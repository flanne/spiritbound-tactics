﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class ConsumableSkillProperties : IUIProperties
    {
        public DescribableObject des;
        public int charges;
        public int max;

        public ConsumableSkillProperties(DescribableObject des, int charges, int max)
        {
            this.des = des;
            this.charges = charges;
            this.max = max;
        }
    }

    public class ConsumableSkillWidget : Widget<ConsumableSkillProperties>
    {
        [SerializeField] Image icon;
        [SerializeField] TMP_Text chargesTMP;

        public override void SetProperties(ConsumableSkillProperties properties)
        {
            icon.sprite = properties.des.icon;
            chargesTMP.text = properties.charges + "/" + properties.max;
        }
    }
}
