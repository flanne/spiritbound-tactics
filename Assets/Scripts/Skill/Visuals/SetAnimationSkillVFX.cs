﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAnimationSkillVFX : BaseSkillVisual
{
    [SerializeField]
    UnitAnimState animState;

    [SerializeField]
    float duration;

    [SerializeField]
    TargetType _targetType;
    public override TargetType targetType { get { return _targetType; } }

    public override void Exit()
    {
    }

    protected override IEnumerator Animate(BoardTile target)
    {
        if (target.content == null || target.content.GetComponent<Unit>() == null)
        {
            Debug.Log("No unit found at target tile.");
            yield break;
        }

        Unit unit = target.content.GetComponent<Unit>();

        unit.SetAnimationState(animState);
        yield return new WaitForSeconds(duration);

        if (unit.GetAnimationState() == animState)
            unit.SetAnimationState(UnitAnimState.Idle);

        isPlaying = false;
    }
}
