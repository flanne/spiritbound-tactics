﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddHobbleOnHighHitFeature : AddPassiveFeature<HobbleOnHighHitPassive>
{
    [SerializeField]
    [Range(0, 1)]
    float chanceToHit;

    [SerializeField]
    int turnDuration;

    protected override void SetParameters(HobbleOnHighHitPassive passive)
    {
        passive.chanceToHit = this.chanceToHit;
        passive.turnDuration = this.turnDuration;
    }
}
