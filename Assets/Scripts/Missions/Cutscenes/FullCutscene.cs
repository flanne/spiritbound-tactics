﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class FullCutscene : CutsceneSequence
{
    public WorldMapLocation level { get { return _level; } }
    [SerializeField]
    WorldMapLocation _level;

    public PartyUnitSpawn[] partySpawns { get { return _partySpawns; } }
    [TableList]
    [SerializeField]
    PartyUnitSpawn[] _partySpawns;

    [TableList]
    public UnitRecipe[] unitRecipes;
}
