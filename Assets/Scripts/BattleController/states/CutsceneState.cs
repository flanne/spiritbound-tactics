﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spiritlink.Battle
{
    public class CutsceneState : BattleState
    {
        protected bool controlLockedOut = true;

        #region BattleState
        public override void Enter()
        {
            base.Enter();

            if (cutsceneController.Play())
            {
                letterBox.Show();
                StartCoroutine(WaitForCutscene());

                StartCoroutine(InitControlLock());
            }
            else
                GoToNextState();
        }

        public override void Exit()
        {
            base.Exit();
            letterBox.Hide();
        }
        #endregion

        #region EventHandlers
        /*protected override void OnFire(object sender, InfoEventArgs<int> e)
        {
            if (e.info == 1 && !controlLockedOut)
            {
                cutsceneController.Stop();
                //dialogueBox.Hide();
                controlLockedOut = true;

                GoToNextState();
            }
        }*/
        #endregion

        #region Protected
        protected virtual void GoToNextState()
        {
            owner.ChangeState<BeforeTurnState>();
        }

        protected IEnumerator InitControlLock()
        {
            yield return new WaitForSeconds(1);
            controlLockedOut = false;
        }

        protected IEnumerator WaitForCutscene()
        {
            yield return null;

            while (cutsceneController.isPlaying())
                yield return null;

            GoToNextState();
        }
        #endregion
    }
}
