﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitPlaceCutsceneAction : UnitCutsceneAction
{
    [SerializeField]
    Vector2Int position;

    public override void Play()
    {
        Board board = MasterSingleton.main.GameLevel.board;
        
        board.GetTile(position).content = unit.gameObject;

        LeanTween.color(unit.gameObject, Color.white, 0f);
        unit.Place(board.GetTile(position));
        unit.MatchToTile();

        isPlaying = false;
    }
}
