﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class EndScreenBanner : Panel
    {
        [SerializeField] Image panel;
        [SerializeField] TMP_Text endText;

        bool didWin = true;

        public void SetDidWin(bool w)
        {
            didWin = w;

            if (didWin)
                endText.text = "Battle Won!";
            else
                endText.text = "Defeat";
        }

        public IEnumerator MoveBannerUp()
        {
            yield return new WaitForSeconds(1f);

            Vector3 newPostion = panel.transform.localPosition;
            newPostion.y += 90;
            LeanTween.moveLocal(panel.gameObject, newPostion, 0.2f);
        }
    }
}
