﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldSkillEffect : BaseSkillEffect
{
	#region Notifications
	public const string TweakShieldNotification = "ShieldSkillEffect.TweakShieldNotification";
	#endregion

	#region Public
	public override int Predict(Unit actor, BoardTile target, int power, Skill skill)
	{
		Unit targetUnit = target.content.GetComponent<Unit>();

		int actorMDF = actor.stats[StatTypes.MDF];

		// Calculate base shielding
		int shield = actorMDF / 2;
		shield = Mathf.Max(shield, 1);

		// Apply power bonus
		shield = power * shield / 100;
		shield = Mathf.Max(shield, 1);

		// Tweak the damage based on a variety of other checks like
		// Elemental damage, Critical Hits, Damage multipliers, etc.
		var info = new Info<Unit, Unit, Skill>(actor, targetUnit, skill);
		shield = shield.NotifyModifiers<Info<Unit, Unit, Skill>>(TweakHealNotification, this, info);

		// Clamp the damage to a range
		shield = Mathf.Clamp(shield, minDamage, maxDamage);
		return shield;
	}

	protected override int OnApply(Unit actor, BoardTile target, int power, Skill skill)
	{
		Unit defender = target.content.GetComponent<Unit>();

		// Start with the predicted damage value
		int value = Predict(actor, target, power, skill);

		// Add some random variance
		value = Mathf.FloorToInt(value * UnityEngine.Random.Range(0.9f, 1.1f));

		// Clamp the damage to a range
		value = Mathf.Clamp(value, minDamage, maxDamage);

		// Apply the damage to the target
		defender.health.SHP += value;
		return value;
	}
	#endregion
}
