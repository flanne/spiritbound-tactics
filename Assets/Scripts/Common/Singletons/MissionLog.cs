﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionLog : MonoBehaviour
{
    public MissionData FirstMission { get { return firstMission; } }

    [SerializeField] MissionData firstMission;
    [SerializeField] MissionData startingActiveMission;

    // Current mission that is playing
    public MissionData current;

    // List of completed missions
    public List<MissionData> completedMissions;

    // List of active missions
    public List<MissionData> activeMissions;

    MissionData[] allMissions;

    void Awake()
    {
        // Loads the full list of missions from the Resources Folder
        allMissions = Resources.LoadAll("", typeof(MissionData)).Cast<MissionData>().ToArray();
    }

    // Called when current mission should be marked as completed
    public void CompleteCurrent()
    {
        if (current == null)
        {
            Debug.Log("Current mission is null and cannot be completed");
            return;
        }

        if (!current.isPatrolBattle)
            completedMissions.Add(current);
    }

    public void AcceptMission(MissionData mission)
    {
        activeMissions.Add(mission);
    }

    public void StartMission(MissionData mission)
    {
        activeMissions.Remove(mission);
        current = mission;
    }

    public List<MissionData> GetAvailable()
    {
        List<MissionData> availableMissions = new List<MissionData>();

        foreach (var m in allMissions)
        {
            // check if mission is already active or completed or is a patroll mission
            if (completedMissions.Contains(m) || activeMissions.Contains(m) || m.isPatrolBattle)
                continue;

            // check if prereqs for missions are met
            if (PreReqsMet(m))
                availableMissions.Add(m);
        }

        return availableMissions;
    }

    public void InitNew()
    {
        current = firstMission;
        completedMissions = new List<MissionData>();
        activeMissions = new List<MissionData>();
        activeMissions.Add(startingActiveMission);
    }

    public void SetMissions(List<MissionData> completed, List<MissionData> active)
    {
        completedMissions = completed;
        activeMissions = active;
    }

    bool PreReqsMet(MissionData mission)
    {
        if (mission.preReq == null)
            return true;

        foreach (var pr in mission.preReq)
        {
            if (!completedMissions.Contains(pr))
                return false;
        }

        return true;
    }
}
