﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class EndRewardJobXpState : BattleState
    {
        #region Properties
        MissionData currentMission { get { return MasterSingleton.main.MissionLog.current; } }
        #endregion

        #region EventHandlers
        void OnAnyKey(InputAction.CallbackContext obj)
        {
            owner.ChangeState<CutsceneEndBattleState>();

            audioPlayer.Play(owner.confirmSFX);
        }
        #endregion

        #region LifeCycle
        public override void Enter()
        {
            jobXPRewardController.Show();

            StartCoroutine(PlayRewardAnimation());
        }

        public override void Exit()
        {
            // Remove listeners
            playerInput.currentActionMap.FindAction("AnyButton").performed -= OnAnyKey;

            // Hide all job reward xp ui
            jobXPRewardController.Hide();

            // Complete mission in mission log
            MasterSingleton.main.MissionLog.CompleteCurrent();

            // Fade music out
            audioPlayer.FadeOutMusic(1);

            endScreenBanner.Hide();
        }
        #endregion

        #region Private
        IEnumerator PlayRewardAnimation()
        {
            jobXPRewardController.Clear();

            // Give Player Job XP
            for (int i = 0; i < currentMission.partySpawns.Length; i++)
            {
                // Get participating party unit
                Unit partyUnit = MasterSingleton.main.Party.GetByID(currentMission.partySpawns[i].unitID);

                // Add unit xp widget
                jobXPRewardController.AddEntry(new UnitProperties(partyUnit));

                // Get requirements for JobXPRewardController
                JobRank jobRank = partyUnit.jobRanks.Get(partyUnit.jobs.primary);
                int oldXP = jobRank.XP;
                int oldRank = jobRank.rank;

                // REWARD XP
                int jobXpReward = (int)currentMission.jobXpReward;
                jobRank.GainXP(jobXpReward);

                int newXP = jobRank.XP;
                int newRank = jobRank.rank;

                jobXPRewardController.RewardXP(i, oldRank, newRank, oldXP, newXP);
            }

            yield return new WaitForSeconds(3f);

            // Add listeners
            playerInput.currentActionMap.FindAction("AnyButton").performed += OnAnyKey;
        }
        #endregion
    }
}