﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reaction : MonoBehaviour
{
    public Skill skill { get; private set; }
    public BoardTile target { get; private set; }
    int priority;

    public void Queue(Skill reaction, BoardTile target, int priority)
    {
        if (this.skill == null || this.priority <= priority)
        {
            this.skill = reaction;
            this.target = target;
            this.priority = priority;
        }
    }

    public void Clear()
    {
        skill = null;
        target = null;
        priority = 0;
    }
}
