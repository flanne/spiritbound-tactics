﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spiritlink.Battle;

public class ManaCostModPassive : Passive
{
    public int amount;

    protected override void AddListeners()
    {
        this.AddObserver(OnCalculateManaCost, ManaController.WillCalculateMana, owner);
        this.AddObserver(OnCalculateManaCost, Skill.GetManaCost, owner);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnCalculateManaCost, ManaController.WillCalculateMana, owner);
        this.RemoveObserver(OnCalculateManaCost, Skill.GetManaCost, owner);
    }

    void OnCalculateManaCost(object sender, object args)
    {
        var mods = args as List<ValueModifier>;
        mods.Add(new AddValueModifier(0, amount));
    }
}
