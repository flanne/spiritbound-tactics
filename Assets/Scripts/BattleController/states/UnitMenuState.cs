﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class UnitMenuState : BattleState
    {
        #region Eventhandlers
        void OnDescribableSelect(object sender, InfoEventArgs<DescribableObject> e)
        {
            if (e.info == null)
            {
                descriptionBox.Hide();
            }
            else
            {
                // Abilities are on right side of screen so move description keywords to left
                if (e.info is JobAbility)
                    descriptionBox.SetToDescribable(e.info, DescriptionBox.Side.Left);
                else
                    descriptionBox.SetToDescribable(e.info, DescriptionBox.Side.Right);

                descriptionBox.Show();
            }

            audioPlayer.Play(owner.selectSFX);
        }

        void OnCancel(InputAction.CallbackContext obj)
        {
            owner.ChangeState<ExploreState>();

            audioPlayer.Play(owner.cancelSFX);
        }
        #endregion

        #region LifeCycle
        protected override void OnEnterUI()
        {
            Unit selectedUnit = currentTile.content.GetComponent<Unit>();
            unitInfoController.SetToUnit(selectedUnit);
            unitInfoController.Show();
            manaBar.Hide();
            turnOrderUI.Hide();
            battleSpeedButton.Hide();

            // Set descriptionbox
            Equippable primaryEquip = selectedUnit.equipment.GetItem(EquipSlots.Primary);
            if (primaryEquip != null)
            {
                descriptionBox.SetToDescribable(primaryEquip, DescriptionBox.Side.Right);
                descriptionBox.Show();
            }

            // Add Listeners
            unitInfoController.DescribableSelectEvent += OnDescribableSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed += OnCancel;
        }

        protected override void OnExitUI()
        {
            unitInfoController.Hide();
            manaBar.Show();
            descriptionBox.Hide();
            turnOrderUI.Show();

            if (!MasterSingleton.main.MissionLog.current.disableBattleSpeed)
                battleSpeedButton.Show();

            // Remove Listeners
            unitInfoController.DescribableSelectEvent -= OnDescribableSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed -= OnCancel;
        }
        #endregion
    }
}