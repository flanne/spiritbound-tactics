﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraConsumableFeature : Feature
{
	protected override void OnApply(Unit target)
	{
		target.equipment.extraConsumable = true;
	}

	protected override void OnRemove(Unit target)
	{
		target.equipment.extraConsumable = false;
		target.equipment.UnEquip(EquipSlots.Consumable3);
	}
}