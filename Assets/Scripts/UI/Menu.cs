﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace flanne.UI
{
    public class Menu : Panel
    {
        #region Fields
        public event EventHandler<InfoEventArgs<int>> ClickEvent;
        public event EventHandler<InfoEventArgs<int>> SelectEvent;

        [SerializeField] protected List<MenuEntry> entries;
        #endregion

        #region Eventhandlers
        public virtual void OnEntryClicked(int index)
        {
            if (ClickEvent != null)
                ClickEvent(this, new InfoEventArgs<int>(index));
        }

        public virtual void OnEntrySelected(int index)
        {
            if (SelectEvent != null)
                SelectEvent(this, new InfoEventArgs<int>(index));
        }
        #endregion

        #region LifeCycle
        protected override void Start()
        {
            base.Start();
            // Add listeners to button
            for (int i = 0; i < entries.Count; i++)
            {
                int closureIndex = i;
                entries[closureIndex].onClick.AddListener(() => OnEntryClicked(closureIndex));
                entries[closureIndex].onSelect.AddListener(() => OnEntrySelected(closureIndex));
            }
        }

        void OnDestroy()
        {
            // Remove listeners to button
            for (int i = 0; i < entries.Count; i++)
            {
                int closureIndex = i;
                entries[closureIndex].onClick.RemoveListener(() => OnEntryClicked(closureIndex));
                entries[closureIndex].onSelect.RemoveListener(() => OnEntrySelected(closureIndex));
            }
        }
        #endregion

        #region Public
        public void SetProperties<T>(int index, T properties) where T:IUIProperties 
        {
            if (index >= 0 && index < entries.Count)
            {
                var widget = entries[index].GetComponent<Widget<T>>();

                if (widget != null)
                    widget.SetProperties(properties);
            }
            else
                Debug.LogError("Cannot set property of entry " + index + ". Index out of bounds.");
        }

        public void Select(int index)
        {
            if (index >= 0 && index < entries.Count)
                entries[index].Select();
            else
                Debug.LogError("Cannot select menu entry " + index + ". Index out of bounds.");
        }

        public int SelectFirstAvailable()
        {
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].interactable == true && entries[i].gameObject.activeSelf)
                {
                    Select(i);
                    return i;
                }
            }

            return -1;
        }

        public virtual void Lock(int index)
        {
            entries[index].interactable = false;
        }

        public void UnLock(int index)
        {
            entries[index].interactable = true;
        }

        public void SetEntryActive(int index, bool active)
        {
            entries[index].gameObject.SetActive(active);
        }

        public void UnlockAll()
        {
            foreach (var entry in entries)
                entry.interactable = true;
        }

        public Vector2 GetEntryPosition(int index)
        {
            return entries[index].transform.position;
        }

        public MenuEntry GetEntry(int index)
        {
            return entries[index];
        }

        public void SetActive(int index, bool active)
        {
            entries[index].gameObject.SetActive(active);
        }

        public void SetAllActive(bool active)
        {
            foreach (var entry in entries)
                entry.gameObject.SetActive(active);
        }
        #endregion
    }
}
