﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudGenerator : MonoBehaviour
{
    [SerializeField]
    float xStart;
    [SerializeField]
    float xEnd;
    [SerializeField]
    float yMin;
    [SerializeField]
    float yMax;

    [SerializeField]
    Sprite[] cloudSprites;
    
    void Start()
    {
        AttemptSpawn();
    }

    void AttemptSpawn()
    {
        SpawnCloud();

        float spawnInterval = Random.Range(3f, 6f);
        Invoke("AttemptSpawn", spawnInterval);
    }

    void SpawnCloud()
    {
        GameObject cloud = new GameObject("Cloud");

        // random Y pos
        cloud.transform.position = new Vector3(xStart, Random.Range(yMin, yMax), 0);

        // random scale
        cloud.transform.localScale = new Vector2(Random.Range(0.9f, 1.1f), Random.Range(0.9f, 1.1f));

        // random cloud sprite
        SpriteRenderer renderer = cloud.AddComponent<SpriteRenderer>();
        renderer.sprite = cloudSprites[Random.Range(0, cloudSprites.Length)];

        // random transparency
        int alpha = Random.Range(50, 125);
        renderer.color = new Color32(255, 255, 255, (byte) alpha);

        // random life time;
        float lifeTime = Random.Range(20f, 40f);
        LeanTween.moveX(cloud, xEnd, lifeTime);
        Destroy(cloud, lifeTime);
    }
}
