﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public abstract class BaseSkillEffect
{
	#region Consts & Notifications
	protected const int minDamage = -999;
	protected const int maxDamage = 999;

	public const string MissedNotification = "BaseSkillEffect.MissedNotification";
	public const string HitNotification = "BaseSkillEffect.HitNotification";
	public const string TweakTargetNotifcation = "BaseSkillEffect.TweakTargetNotifcation";
	public const string TweakHealNotification = "BaseSkillEffect.TweakHealNotification";
	public const string WillHealNotification = "BaseSkillEffect.WillHealNotification";
	#endregion

	#region Abstract
	public abstract int Predict(Unit actor, BoardTile target, int power, Skill skill);
	protected abstract int OnApply(Unit Actor, BoardTile target, int power, Skill skill);
	#endregion

	#region Public
	public virtual int PredictClamped(Unit actor, BoardTile target, int power, Skill skill)
	{
		return Predict(actor, target, power, skill);
	}

	public bool Apply(Unit actor, BoardTile target, bool isHit, int power, Skill skill)
	{
		Unit targetUnit = target.content?.GetComponent<Unit>();

		if (isHit)
		{
			// Check for target changing effects
			TargetTileException exc = new TargetTileException(target);
			this.PostNotification(TweakTargetNotifcation, exc);
			target = exc.targetTile;
			targetUnit = target.content?.GetComponent<Unit>();

			// Apply effect
			this.PostNotification(HitNotification, new Info<Unit, Unit, int, Skill>(actor, targetUnit, OnApply(actor, target, power, skill), skill));
			return true;
		}
		else
		{
			this.PostNotification(MissedNotification, new Info<Unit, Unit>(targetUnit, actor));
			return false;
		}
	}

	public bool Apply(Unit actor, BoardTile target, HitRate hitRoller, int power, Skill skill)
	{
		Unit targetUnit = target.content?.GetComponent<Unit>();
		bool isHit = hitRoller.RollForHit(actor, targetUnit, skill);

		return Apply(actor, target, isHit, power, skill);
	}
	#endregion
}