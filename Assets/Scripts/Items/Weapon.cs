﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Equippable
{
    public WeaponType weaponType;
    public Skill basicAttack;

	public override EquipSlots defaultSlots 
	{ 
		get 
		{
			if (weaponType == WeaponType.Dagger || weaponType == WeaponType.Sword || weaponType == WeaponType.Spear) // 1h weapons
				return EquipSlots.Primary;
			else if (weaponType == WeaponType.Axe || weaponType == WeaponType.Bow || weaponType == WeaponType.Staff || weaponType == WeaponType.Monster) // 2h weapons
				return EquipSlots.Primary | EquipSlots.Secondary;

			return EquipSlots.None;
		} 
	}

	[SerializeField] Material spriteMaterial;

	[SerializeField] Sprite[] spritesheet;

	public override string GetDescription()
	{
		string description;
		if (weaponType == WeaponType.Dagger || weaponType == WeaponType.Sword || weaponType == WeaponType.Spear) // 1h weapons
			description = "<color=#969696>1-Hand</color> ";
		else
			description = "<color=#969696>2-Hand</color> ";

		description += base.GetDescription();
		return description;
	}

	public override void OnEquip(Unit unit)
	{
		base.OnEquip(unit);

		unit.weaponSpriteSheet?.LoadSpriteSheet(spritesheet);
		var weaponRender = unit.weaponSpriteSheet?.GetComponent<SpriteRenderer>();
		if (weaponRender != null)
			weaponRender.material = spriteMaterial;
	}

	public override void OnUnEquip(Unit unit)
	{
		base.OnUnEquip(unit);
	}
}
