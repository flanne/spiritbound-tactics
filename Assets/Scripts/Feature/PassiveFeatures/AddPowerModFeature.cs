﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddPowerModFeature : AddPassiveFeature<PowerModPassive>
{
    [SerializeField]
    SkillTag skillTag;

    [SerializeField]
    float multiplier = 1;

    protected override void SetParameters(PowerModPassive passive)
    {
        passive.skillTag = this.skillTag;
        passive.multiplier = this.multiplier;
    }
}
