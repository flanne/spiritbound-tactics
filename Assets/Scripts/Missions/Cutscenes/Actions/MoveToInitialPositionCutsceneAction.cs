﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToInitialPositionCutsceneAction : UnitCutsceneAction
{
    Board board { get { return MasterSingleton.main.GameLevel.board; } }

    [SerializeField]
    float moveSpeed = 4;

    [SerializeField] UnitAnimState afterMoveState = UnitAnimState.Idle;

    IEnumerator mainCoroutine;
    IEnumerator moveCoroutine;

    public override void Play()
    {
        mainCoroutine = MoveUnit(unit);
        StartCoroutine(mainCoroutine);
    }

    public override void Stop()
    {
        if (mainCoroutine != null)
            StopCoroutine(mainCoroutine);

        if (moveCoroutine != null)
            StopCoroutine(moveCoroutine);
    }

    IEnumerator MoveUnit(Unit unit)
    {
        isPlaying = true;

        board.Search(unit.tile, SearchAllTiles);

        Movement m = unit.movement;
        moveCoroutine = m.Traverse(unit.initialTile, moveSpeed, true);
        yield return StartCoroutine(moveCoroutine);

        unit.direction = unit.initialDirection;
        unit.SetAnimationState(afterMoveState);
        isPlaying = false;
    }

    bool SearchAllTiles(BoardTile from, BoardTile to)
    {
        return true;
    }
}
