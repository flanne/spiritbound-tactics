﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

public class SelectUnitTryMenuState : ShopState
{
    #region Properties
    int _index = -1;

    List<Unit> myParty { get { return MasterSingleton.main.Party.myParty; } }
    #endregion

    #region Eventhandler
    void OnClick(object sender, InfoEventArgs<int> e)
    {
        Unit selectedUnit = myParty[e.info];

        // remember what original equipment this unit had
        owner.tryOriginalEquips = new Dictionary<EquipSlots, Equippable>();
        foreach (var slot in Enum.GetValues(typeof(EquipSlots)).Cast<EquipSlots>())
        {
            Equippable equip = selectedUnit.equipment.GetItem(slot);
            if (equip != null)
                owner.tryOriginalEquips.Add(slot, equip);
        }

        unitListMenu.interactable = false;

        owner.menuUnit = selectedUnit;
        tryEquipController.SetToUnit(selectedUnit);
        equipCompareUI.SetUnit(selectedUnit);

        owner.ChangeState<TryEquipUnitInfoState>();

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnSelect(object sender, InfoEventArgs<int> e)
    {
        // Remeber last selected index
        _index = e.info;

        audioPlayer.Play(owner.selectSFX);
    }

    void OnCancel(InputAction.CallbackContext obj)
    {
        owner.ChangeState<ShopMenuState>();
        unitListMenu.Hide();

        audioPlayer.Play(owner.cancelSFX);
    }
    #endregion

    #region LifeCylce
    public override void Enter()
    {
        // Set up unit list menu
        unitListMenu.Clear();
        for (int i = 0; i < myParty.Count; i++)
            unitListMenu.AddEntry(new UnitProperties(myParty[i]));

        if (_index == -1)
            unitListMenu.SelectFirstAvailable();
        else
            unitListMenu.Select(_index);

        unitListMenu.Show();

        unitListMenu.ClickEvent += OnClick;
        unitListMenu.SelectEvent += OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;
    }

    public override void Exit()
    {
        unitListMenu.ClickEvent -= OnClick;
        unitListMenu.SelectEvent -= OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;
    }
    #endregion
}
