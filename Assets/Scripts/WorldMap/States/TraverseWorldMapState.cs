﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TraverseWorldMapState : WorldMapState
{
    #region Fields/Properties
    bool isMoving = false;
    IEnumerator _moveAvatarCoroutine;
    #endregion

    #region Eventhandlers
    void OnCursorMove(object sender, InfoEventArgs<Vector2> e)
    {
        controllerCursor.magnetTo = null;

        foreach (var node in map.nodes)
        {
            if (node.IsPointOn(e.info))
                node.PointerEnterAnimation();
            else
                node.PointerExitAnimation();

            if (node.gameObject.activeSelf && Vector3.Distance(e.info, node.transform.position) < 0.35f)
                controllerCursor.magnetTo = node.transform;
        }
    }

    void OnCursorSwitch(object sender, InfoEventArgs<bool> e)
    {
        if (e.info == false)
        {
            foreach (var node in map.nodes)
                node.PointerExitAnimation();
        }
    }

    void OnClick(InputAction.CallbackContext obj)
    {
        if (isMoving)
            return;

        // Check if any node is clicked on
        foreach (var node in map.nodes)
        {
            if (node.gameObject.activeSelf && node.selected)
            {
                // AutoSave
                MasterSingleton.main.SaveSystem.Save();

                audioPlayer.Play(owner.confirmSFX);

                _moveAvatarCoroutine = MoveAvatar(node);
                StartCoroutine(_moveAvatarCoroutine);
                return;
            }
        }
    }

    void OnSecondary(InputAction.CallbackContext obj)
    {
        if (isMoving)
            return;

        //bring up menu on right R
        owner.ChangeState<ExitMenuState>();

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnCancel(InputAction.CallbackContext obj)
    {
        if (isMoving)
            return;

        //bring up party menu on right click
        owner.ChangeState<UnitListMenuState>();

        audioPlayer.Play(owner.confirmSFX);
    }
    #endregion

    #region LifeCycle
    public override void Enter()
    {
        controllerCursor.enabled = true;

        controllerCursor.MoveEvent += OnCursorMove;
        controllerCursor.SwitchEvent += OnCursorSwitch;

        playerInput.currentActionMap.FindAction("Click").canceled += OnClick;
        playerInput.currentActionMap.FindAction("Submit").canceled += OnClick;
        playerInput.currentActionMap.FindAction("Secondary").canceled += OnSecondary;
        playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;

        isMoving = false;

        // Show keybinds
        keybindPromptUI.Show();
        keybindPromptUI.SetText(0, "Move To");
        keybindPromptUI.SetText(1, "Party");
        keybindPromptUI.SetText(2, "Menu");
    }

    public override void Exit()
    {
        controllerCursor.enabled = false;

        controllerCursor.MoveEvent -= OnCursorMove;
        controllerCursor.SwitchEvent += OnCursorSwitch;

        playerInput.currentActionMap.FindAction("Click").canceled -= OnClick;
        playerInput.currentActionMap.FindAction("Submit").canceled -= OnClick;
        playerInput.currentActionMap.FindAction("Secondary").canceled -= OnSecondary;
        playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;
    }
    #endregion

    #region Private
    IEnumerator MoveAvatar(WorldMapNode destination)
    {
        isMoving = true;

        map.Search(avatar.node);

        // Build list of nodes to move to
        List<WorldMapNode> targets = new List<WorldMapNode>();
        while (destination != null)
        {
            targets.Insert(0, destination);
            destination = destination.prev;
        }

        // Move to each way point in succession
        for (int i = 1; i < targets.Count; ++i)
        {
            yield return StartCoroutine(avatar.MoveTo(targets[i]));

            audioPlayer.Play(owner.confirmSFX);
        }
        yield return null;

        isMoving = false;

        if (MapActionAvailable())
            owner.ChangeState<MapActionState>();
    }

    bool MapActionAvailable()
    {
        return avatar.node.level.shopItems.Length > 0 || avatar.node.level.hasPub || avatar.node.level.patrolBattles.Length > 0 || CheckForActiveMission();
    }
    #endregion
}
