﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

public class UnitListMenuState : WorldMapState
{
    #region Properties
    int _index = 0;

    List<Unit> myParty { get { return MasterSingleton.main.Party.myParty; } }
    #endregion

    #region Eventhandlers
    void OnClick(object sender, InfoEventArgs<int> e)
    {
        owner.menuUnit = myParty[e.info];

        // Set unit options menu position to clicked entry
        Vector2 pos = unitListMenu.GetEntryPosition(e.info);
        pos.x += 300;
        pos.y -= 50;
        owner.unitOptionsMenu.transform.position = pos;

        // Change State
        owner.ChangeState<UnitOptionsState>();

        // Remove listeners
        unitListMenu.interactable = false;
        unitListMenu.ClickEvent -= OnClick;
        unitListMenu.SelectEvent -= OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnSelect(object sender, InfoEventArgs<int> e)
    {
        _index = e.info;

        audioPlayer.Play(owner.selectSFX);
    }

    void OnCancel(InputAction.CallbackContext obj)
    {
        owner.ChangeState<TraverseWorldMapState>();

        // Remove listeners
        unitListMenu.Hide();
        unitListMenu.ClickEvent -= OnClick;
        unitListMenu.SelectEvent -= OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;

        audioPlayer.Play(owner.cancelSFX);
    }
    #endregion

    #region LifeCycle
    public override void Enter()
    {
        // Set up unit list menu
        unitListMenu.Clear();
        for (int i = 0; i < myParty.Count; i ++)
            unitListMenu.AddEntry(new UnitProperties(myParty[i]));

        unitListMenu.Select(_index);
        unitListMenu.Show();

        unitListMenu.ClickEvent += OnClick;
        unitListMenu.SelectEvent += OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;

        // Show keybinds
        keybindPromptUI.Show();
        keybindPromptUI.SetText(0, "Select");
        keybindPromptUI.SetText(1, "Back");
        keybindPromptUI.HidePrompt(2);
    }

    public override void Exit()
    {
    }
    #endregion
}
