﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DamageSkillEffect : BaseSkillEffect
{
	#region Notifications
	public const string TweakDamageNotification = "DamageSkillEffect.TweakDamageNotification";
	#endregion

	#region Abstract
	public abstract int GetAttack(Unit actor, Unit targetUnit);
	public abstract int GetDefence(Unit actor, Unit targetUnit);
    #endregion

    #region Public
    public override int Predict(Unit actor, BoardTile target, int power, Skill skill)
	{
		Unit targetUnit = target.content.GetComponent<Unit>();

		int attack = GetAttack(actor, targetUnit);
		int defense = GetDefence(actor, targetUnit);

		// Calculate base damage
		int damage = attack - (defense / 2);
		damage = Mathf.Max(damage, 1);

		// Apply power bonus
		damage = power * damage / 100;
		damage = Mathf.Max(damage, 1);

		// Tweak the damage based on a variety of other checks like
		// Elemental damage, Critical Hits, Damage multipliers, etc.
		var info = new Info<Unit, Unit, Skill>(actor, targetUnit, skill);
		damage = damage.NotifyModifiers<Info<Unit, Unit, Skill>>(TweakDamageNotification, this, info);

		// Clamp the damage to a range
		damage = Mathf.Clamp(damage, minDamage, maxDamage);
		return -damage;
	}

	protected override int OnApply(Unit actor, BoardTile target, int power, Skill skill)
	{
		Unit defender = target.content.GetComponent<Unit>();

		// Start with the predicted damage value
		int value = Predict(actor, target, power, skill);

		// Add some random variance
		value = Mathf.FloorToInt(value * UnityEngine.Random.Range(0.9f, 1.1f));

		// Clamp the damage to a range
		value = Mathf.Clamp(value, minDamage, maxDamage);

		// Apply the damage to the target
		defender.stats[StatTypes.HP] += value;
		return value;
	}
    #endregion
}
