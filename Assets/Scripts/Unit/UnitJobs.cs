﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitJobs : MonoBehaviour
{
	public Job primary { get; private set; }
	public Job secondary { get; private set; }
	public bool hasSecondary { get { return secondary != null; } }
	[SerializeField]
	private Unit unit;

	public void SetPrimary(Job job)
	{
		primary?.UnEmploy(unit);
		primary?.DeactivateInnate(unit);

		primary = job;
		primary.Employ(unit);

		primary.LoadSpriteSheet(unit);
		primary.ActivateInnate(unit);
		primary.LoadDefaultStats(unit);
	}

	public void SetSecondary(Job job)
	{
		secondary?.UnEmploy(unit);

		secondary = job;
		secondary.Employ(unit);
	}

	public void DeactivateFeatures()
	{
		primary.UnEmploy(unit);
		primary.DeactivateInnate(unit);

		secondary?.UnEmploy(unit);
	}

	public void ReactivateFeatures()
	{
		primary.Employ(unit);
		primary.ActivateInnate(unit);

		secondary?.Employ(unit);
	}
}

