﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Spiritlink.Battle
{
    public class ComputerPlayer : MonoBehaviour
    {
        BattleController bc;
        Unit actor { get { return bc.turn.actor; } }
        Alliance alliance { get { return actor.alliance; } }
        Unit nearestFoe;

        void Awake()
        {
            bc = GetComponent<BattleController>();
        }

        #region Public
        public PlanOfAttack Evaluate()
        {
            // Create and fill out a plan of attack
            PlanOfAttack poa = new PlanOfAttack();

            // Decide what skill to use
            AIStrategy strategy = actor.GetComponentInChildren<AIStrategy>();

            foreach (var option in strategy.skillOptions)
            {
                if (OptionConditionsMet(option))
                {
                    poa.skill = option.skill;
                    poa.targeting = option.targeting;
                    break;
                }
            }

            if (poa.skill == null)
            {
                MoveTowardOpponent(poa);
            }
            else
            {
                //Determine where to move and aim to best use the skill
                if (IsPositionIndependent(poa))
                    PlanPositionIndependent(poa);
                else if (IsFireLocationIndependent(poa))
                    PlanFireLocationIndepndent(poa);
                else if (IsDirectionIndependent(poa))
                    PlanDirectionIndependent(poa);
                else
                    PlanDirectionDependent(poa);
                if (poa.skill == null)
                    MoveTowardOpponent(poa);
            }

            // Return the completed plan
            return poa;
        }

        public Direction DetermineEndFacingDirection()
        {
            Direction direction = (Direction)UnityEngine.Random.Range(0, 4);
            FindNearestFoe();
            if (nearestFoe != null)
            {
                Direction start = actor.direction;
                for (int i = 0; i < 4; ++i)
                {
                    actor.direction = (Direction)i;
                    if (nearestFoe.GetFacing(actor) == Facings.Front)
                    {
                        direction = actor.direction;
                        break;
                    }
                }
                actor.direction = start;
            }
            return direction;
        }
        #endregion

        #region Private
        bool OptionConditionsMet(AIOption option)
        {
            // Can only target enemies when taunted
            if (option.targeting != AITargeting.Foe && actor.status.GetComponentInChildren<TauntStatusEffect>() != null)
                return false;

            // Check if able to cast skill
            if (option.skill == null || !option.skill.CanPerform(actor))
                return false;

            switch (option.condition.type)
            {
                case ConditionType.IfAvailable:
                    return true;
                case ConditionType.AllyUnderHPPercent:
                    foreach (var unit in bc.units)
                    {
                        if (unit.alliance.type == actor.alliance.type && unit.health.HP > 0 && ((float)unit.health.HP / (float)unit.health.MHP) <= option.condition.amount)
                        {
                            int distance = Mathf.Abs(unit.tile.xy.x - actor.tile.xy.x) + Mathf.Abs(unit.tile.xy.y - actor.tile.xy.y);
                            if (distance <= option.condition.range)
                                return true;
                        }
                    }
                    return false;
                case ConditionType.SelfUnderHPPercent:
                    if (((float)actor.health.HP / (float)actor.health.MHP) <= option.condition.amount)
                        return true;
                    else
                        return false;
                case ConditionType.NoMight:
                    if (actor.status.GetComponentInChildren<MightStatusEffect>() == null)
                        return true;
                    else
                        return false;

                case ConditionType.EveryCertainTurns:
                    return option.Count();
            }

            return false;
        }

        bool IsPositionIndependent(PlanOfAttack poa)
        {
            return !poa.skill.positionOriented;
        }

        bool IsFireLocationIndependent(PlanOfAttack poa)
        {
            return !poa.skill.fireLocationOriented;
        }

        bool IsDirectionIndependent(PlanOfAttack poa)
        {
            return !poa.skill.directionOriented;
        }

        void PlanPositionIndependent(PlanOfAttack poa)
        {
            List<BoardTile> moveOptions = GetMoveOptions();
            BoardTile tile = moveOptions[Random.Range(0, moveOptions.Count)];
            poa.moveLocation = poa.fireLocation = tile.xy;
        }

        void PlanFireLocationIndepndent(PlanOfAttack poa)
        {
            BoardTile startBoardTile = actor.tile;
            List<AttackOption> list = new List<AttackOption>();
            List<BoardTile> moveOptions = GetMoveOptions();

            for (int i = 0; i < moveOptions.Count; ++i)
            {
                BoardTile moveBoardTile = moveOptions[i];
                actor.Place(moveBoardTile);

                AttackOption ao = new AttackOption();
                ao.target = moveBoardTile;
                RateFireLocation(poa, ao);
                ao.AddMoveTarget(moveBoardTile);
                list.Add(ao);
            }

            actor.Place(startBoardTile);
            PickBestOption(poa, list);
        }

        void PlanDirectionIndependent(PlanOfAttack poa)
        {
            BoardTile startBoardTile = actor.tile;
            Dictionary<BoardTile, AttackOption> map = new Dictionary<BoardTile, AttackOption>();
            List<BoardTile> moveOptions = GetMoveOptions();

            for (int i = 0; i < moveOptions.Count; ++i)
            {
                BoardTile moveBoardTile = moveOptions[i];
                actor.Place(moveBoardTile);
                List<BoardTile> fireOptions = poa.skill.GetTilesInRange(bc.board, actor);

                for (int j = 0; j < fireOptions.Count; ++j)
                {
                    BoardTile fireBoardTile = fireOptions[j];
                    AttackOption ao = null;
                    if (map.ContainsKey(fireBoardTile))
                    {
                        ao = map[fireBoardTile];
                    }
                    else
                    {
                        ao = new AttackOption();
                        map[fireBoardTile] = ao;
                        ao.target = fireBoardTile;
                        ao.direction = actor.direction;
                        RateFireLocation(poa, ao);
                    }
                    ao.AddMoveTarget(moveBoardTile);
                }
            }

            actor.Place(startBoardTile);
            List<AttackOption> list = new List<AttackOption>(map.Values);
            PickBestOption(poa, list);
        }

        void PlanDirectionDependent(PlanOfAttack poa)
        {
            BoardTile startBoardTile = actor.tile;
            Direction startDirection = actor.direction;
            List<AttackOption> list = new List<AttackOption>();
            List<BoardTile> moveOptions = GetMoveOptions();

            for (int i = 0; i < moveOptions.Count; ++i)
            {
                BoardTile moveBoardTile = moveOptions[i];
                actor.Place(moveBoardTile);

                for (int j = 0; j < 4; ++j)
                {
                    actor.direction = (Direction)j;
                    AttackOption ao = new AttackOption();
                    ao.target = moveBoardTile;
                    ao.direction = actor.direction;
                    RateFireLocation(poa, ao);
                    ao.AddMoveTarget(moveBoardTile);
                    list.Add(ao);
                }
            }

            actor.Place(startBoardTile);
            actor.direction = startDirection;
            PickBestOption(poa, list);
        }

        void PickBestOption(PlanOfAttack poa, List<AttackOption> list)
        {
            int bestScore = 1;
            List<AttackOption> bestOptions = new List<AttackOption>();
            for (int i = 0; i < list.Count; ++i)
            {
                AttackOption option = list[i];
                int score = option.GetScore(actor, poa.skill);
                if (score > bestScore)
                {
                    bestScore = score;
                    bestOptions.Clear();
                    bestOptions.Add(option);
                }
                else if (score == bestScore)
                {
                    bestOptions.Add(option);
                }
            }
            if (bestOptions.Count == 0)
            {
                poa.skill = null; // Clear skill as a sign not to perform it
                return;
            }

            List<AttackOption> finalPicks = new List<AttackOption>();
            bestScore = 0;
            for (int i = 0; i < bestOptions.Count; ++i)
            {
                AttackOption option = bestOptions[i];
                int score = option.bestAngleBasedScore;
                if (score > bestScore)
                {
                    bestScore = score;
                    finalPicks.Clear();
                    finalPicks.Add(option);
                }
                else if (score == bestScore)
                {
                    finalPicks.Add(option);
                }
            }

            if (finalPicks.Count > 0)
            {
                AttackOption choice = finalPicks[UnityEngine.Random.Range(0, finalPicks.Count)];
                poa.fireLocation = choice.target.xy;
                poa.attackDirection = choice.direction;
                poa.moveLocation = choice.bestMoveBoardTile.xy;
            }
        }

        bool IsSkillTargetMatch(PlanOfAttack poa, BoardTile tile)
        {
            bool isMatch = false;
            if (poa.targeting == AITargeting.Tile)
                isMatch = true;
            else if (poa.targeting != AITargeting.None)
            {
                Alliance other = tile.content.GetComponent<Unit>().alliance;
                if (other != null && alliance.IsMatch(other, poa.targeting))
                    isMatch = true;
            }
            return isMatch;
        }

        void RateFireLocation(PlanOfAttack poa, AttackOption option)
        {
            List<BoardTile> tiles = poa.skill.GetTilesInArea(bc.board, option.target.xy, actor);
            option.areaTargets = tiles;
            option.isCasterMatch = IsSkillTargetMatch(poa, actor.tile);

            for (int i = 0; i < tiles.Count; ++i)
            {
                BoardTile tile = tiles[i];
                if (actor.tile == tiles[i] || !poa.skill.IsTarget(actor, tile))
                    continue;

                bool isMatch = IsSkillTargetMatch(poa, tile);
                option.AddMark(tile, isMatch);
            }
        }

        void FindNearestFoe()
        {
            nearestFoe = null;
            bc.board.Search(actor.tile, delegate (BoardTile arg1, BoardTile arg2)
            {
                if (nearestFoe == null && arg2.content != null)
                {
                    Unit unit = arg2.content.GetComponent<Unit>();
                    if (unit == null)
                        return false;

                    Alliance other = unit.alliance;
                    if (other != null && alliance.IsMatch(other, AITargeting.Foe) && actor.movement.TraversalConditionMet(arg1, arg2, true))
                    {
                        if (unit.stats[StatTypes.HP] > 0)
                        {
                            nearestFoe = unit;
                        }
                    }
                }

                return actor.movement.TraversalConditionMet(arg1, arg2, true);
            });
        }

        void MoveTowardOpponent(PlanOfAttack poa)
        {
            List<BoardTile> moveOptions = GetMoveOptions();
            FindNearestFoe();
            if (nearestFoe != null)
            {
                BoardTile toCheck = nearestFoe.tile;
                while (toCheck != null)
                {
                    if (moveOptions.Contains(toCheck))
                    {
                        poa.moveLocation = toCheck.xy;
                        return;
                    }
                    toCheck = toCheck.prev;
                }
            }
            poa.moveLocation = actor.tile.xy;
        }

        List<BoardTile> GetMoveOptions()
        {
            List<BoardTile> tiles = actor.movement.GetTilesInRange(bc.board);
            tiles.Add(actor.tile);
            return tiles;
        }
        #endregion
    }
}