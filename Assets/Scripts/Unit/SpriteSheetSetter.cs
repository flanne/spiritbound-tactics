﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpriteSheetSetter : MonoBehaviour
{
    private string LoadedSpriteSheetName;

    private Dictionary<string, Sprite> spriteSheet;

    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        this.spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void LateUpdate()
    {
        // Swap out the sprite to be rendered by its name
        // Important: The name of the sprite must be the same!
        if (this.spriteSheet.ContainsKey(this.spriteRenderer.sprite.name))
            this.spriteRenderer.sprite = this.spriteSheet[this.spriteRenderer.sprite.name];
    }

    // Loads the sprites from a sprite sheet
    public void LoadSpriteSheet(Sprite[] sprites)
    {
        // Load the sprites from a sprite sheet file (png). 
        // Note: The file specified must exist in a folder named Resources
        this.spriteSheet = sprites.ToDictionary(x => x.name, x => x);
    }

    public Sprite GetSprite(string file_name)
    {
        if (spriteSheet.ContainsKey(file_name))
            return spriteSheet[file_name];

        return null;
    }
}
