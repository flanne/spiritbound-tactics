﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DmgUpPerStatusPassive : Passive
{
    public float perStackBonus;
    public int maxBonus;

    protected override void AddListeners()
    {
        this.AddObserver(OnDealingDamage, DamageSkillEffect.TweakDamageNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnDealingDamage, DamageSkillEffect.TweakDamageNotification);
    }

    void OnDealingDamage(object sender, object args)
    {
        var info = args as Info<List<ValueModifier>, Info<Unit, Unit, Skill>>;
        Unit actor = info.arg1.arg0;
        Unit target = info.arg1.arg1;

        if (actor != owner)
            return;

        int numStatus = 0;
        foreach (var s in target.status.GetComponentsInChildren<StatusEffect>())
            if (!s.isBuff)
                numStatus++;

        float multiplier = perStackBonus * Mathf.Min(maxBonus, numStatus);
        multiplier += 1;

        info.arg0.Add(new MultValueModifier(1, multiplier));
    }
}
