﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
	#region Notifications
	public const string HealthChangeNotification = "Health.HealthChangeNotification";
	public const string ShieldGainNotification = "Health.ShieldGainNotification";
	#endregion

	#region Fields & Properties
	[SerializeField]
	Unit owner;
	Stats stats { get { return owner.stats; } }

	public int HP
	{
		get { return stats[StatTypes.HP]; }
		set { stats[StatTypes.HP] = value; }
	}

	public int MHP
	{
		get { return stats[StatTypes.MHP]; }
		set { stats[StatTypes.MHP] = value; }
	}

	public int SHP
    {
        get { return _SHP; }
        set
        {
			value = Mathf.Clamp(value, 0, MHP);

			if (value <= _SHP)
				lastSHPChange = value - _SHP;
			else if (value > _SHP)
			{
				var info = new Info<int, int>(_SHP, value);
				this.PostNotification(ShieldGainNotification, info);
			}

			_SHP = value;
        }
    }
	int _SHP;

	public const int MinHP = 0;

	int lastSHPChange = 0;
    #endregion

    #region Monobheaviour
    void OnEnable()
	{
		this.AddObserver(OnHPWillChange, Stats.WillChangeNotification(StatTypes.HP), stats);
		this.AddObserver(OnHPDidChange, Stats.DidChangeNotification(StatTypes.HP), stats);
		this.AddObserver(OnMaxHPDidChange, Stats.DidChangeNotification(StatTypes.MHP), stats);
	}

	void OnDisable()
	{
		this.RemoveObserver(OnHPWillChange, Stats.WillChangeNotification(StatTypes.HP), stats);
		this.RemoveObserver(OnHPDidChange, Stats.DidChangeNotification(StatTypes.HP), stats);
		this.RemoveObserver(OnMaxHPDidChange, Stats.DidChangeNotification(StatTypes.MHP), stats);
	}
	#endregion

	#region Event Handlers
	void OnHPWillChange(object sender, object args)
	{
		ValueChangeException vce = args as ValueChangeException;

		// Check if unit is going losing HP, and instead divert it to lose shield
		int hpChange = Mathf.FloorToInt(vce.toValue - vce.fromValue);
		if (hpChange < 0)
		{
			vce.AddModifier(new AddValueModifier(0, Mathf.Min(SHP, -1 * hpChange)));

			SHP = Mathf.Max(SHP + hpChange, 0);

			// If shield blocks all damage, send out health change notification now otherwise wait until hp change is finaliized to send out notificiation
			if (lastSHPChange == hpChange && hpChange < 0)
            {
				var info = new Info<int, int, int, int>(HP, HP, _SHP - lastSHPChange, SHP);
				this.PostNotification(HealthChangeNotification, info);

				lastSHPChange = 0;
			}
		}

		// Clamp hp to Max HP and 0
		vce.AddModifier(new ClampValueModifier(int.MaxValue, MinHP, stats[StatTypes.MHP]));
	}

	void OnHPDidChange(object sender, object args)
	{
		var info = args as Info<int, int>;
		int hpChange = info.arg1 - info.arg0;

		var sendInfo = new Info<int, int, int, int>(info.arg0, info.arg1, _SHP - lastSHPChange, SHP);
		this.PostNotification(HealthChangeNotification, sendInfo);

		lastSHPChange = 0;
	}

	void OnMaxHPDidChange(object sender, object args)
	{
		var info = args as Info<int, int>;

		// Set health's hp values
		int oldMHP = info.arg0;
		if (MHP > oldMHP)
			HP += MHP - oldMHP;
		else
			HP = Mathf.Clamp(HP, MinHP, MHP);
	}
    #endregion

    #region Public
	public void Reset()
    {
		owner.stats.SetValue(StatTypes.HP, MHP, false, false);
		_SHP = 0;
	}
    #endregion
}
