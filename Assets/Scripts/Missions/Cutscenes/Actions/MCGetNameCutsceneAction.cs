﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using flanne.UI;

public class MCGetNameCutsceneAction : CutsceneAction
{
    public override void Play()
    {
        StartCoroutine(ShowGetNameUI());
    }

    public override void Stop()
    {
        MasterSingleton.main.NameChangeUI.Hide();
        isPlaying = false;
    }

    IEnumerator ShowGetNameUI()
    {
        isPlaying = true;

        yield return null;

        NameChangeUI nameChangeUI = MasterSingleton.main.NameChangeUI;
        nameChangeUI.Show();

        while (!nameChangeUI.isNameSet)
            yield return null;

        nameChangeUI.Hide();

        yield return new WaitForSeconds(1f);

        isPlaying = false;
    }
}
