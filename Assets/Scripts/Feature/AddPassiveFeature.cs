﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AddPassiveFeature<T> : Feature where T : Passive
{
    [SerializeField]
    System.Type passive;

    protected override void OnApply(Unit target)
    {
        T passive = target.passives.Add<T>();
        SetParameters(passive);
    }

    protected override void OnRemove(Unit target)
    {
        target.passives.Remove<T>();
    }

    protected virtual void SetParameters(T passive)
    {
    }
}
