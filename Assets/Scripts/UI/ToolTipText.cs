﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace flanne.UI
{
    public class ToolTipText : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] string tooltipStringId;
        [System.NonSerialized] public string tooltip;

        void Awake()
        {
            tooltip = MasterSingleton.main.GameStrings.Get(tooltipStringId);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            MasterSingleton.main.Tooltip.ShowTooltip(tooltip);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            MasterSingleton.main.Tooltip.HideTooltip();
        }
    }
}
