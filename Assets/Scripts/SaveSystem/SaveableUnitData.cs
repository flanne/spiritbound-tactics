﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveableUnitData
{
    public IDEnum id;
    public string unitName;
    public int level;
    public int xp;

    public string modelGuid;

    public string primaryJobGuid;
    public bool hasSecondary;
    public string secondaryJobGuid;

    public SaveableJobRank[] saveableJobRanks;

    public string eqPrimaryGuid;
    public string eqSecondaryGuid;
    public string eqBodyGuid;
    public string eqAccessoryGuid;
    public string eqConsume1Guid;
    public string eqConsume2Guid;
    public string eqConsume3Guid;
}
