﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoveToCutsceneAction : CutsceneAction
{
    [SerializeField]
    Vector3 position;

    [SerializeField]
    float secondsToMove;

    [SerializeField]
    LeanTweenType easeType = LeanTweenType.linear;

    int tweenId;

    public override void Play()
    {
        MasterSingleton.main.MainCameraRig.follow = null;
        StartCoroutine(MoveCamera());
    }

    public override void Stop()
    {
        if (LeanTween.isTweening(tweenId))
            LeanTween.cancel(tweenId);

        isPlaying = false;
    }

    IEnumerator MoveCamera()
    {
        isPlaying = true;

        tweenId = LeanTween.move(MasterSingleton.main.MainCameraRig.gameObject, position, secondsToMove).setEase(easeType).id;

        while(LeanTween.isTweening(tweenId))
            yield return null;

        isPlaying = false;
    }
}
