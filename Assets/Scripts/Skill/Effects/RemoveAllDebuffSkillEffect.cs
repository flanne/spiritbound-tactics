﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveAllDebuffSkillEffect : BaseSkillEffect
{
	#region Public
	public override int Predict(Unit actor, BoardTile target, int power, Skill skill)
	{
		return power;
	}

	protected override int OnApply(Unit actor, BoardTile target, int power, Skill skill)
	{
		Unit defender = target.content.GetComponent<Unit>();

		var statusEffects = defender.status.GetComponentsInChildren<StatusEffect>();
		foreach (var s in statusEffects)
			if (!s.isBuff)
				defender.status.Remove(s);

		return power;
	}
	#endregion
}