﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class ConsumablesMenuState : BattleState
    {
        #region Properties
        List<MyConsumable> myConsumables = new List<MyConsumable>();
        #endregion

        #region Eventhandlers
        void OnClick(object sender, InfoEventArgs<int> e)
        {
            if (myConsumables[e.info].consumableSkill.CanPerform(turn.actor))
            {
                turn.skill = myConsumables[e.info].consumableSkill;
                turn.consumableIndex = e.info;
                owner.ChangeState<SkillTargetState>();

                audioPlayer.Play(owner.confirmSFX);
            }
            else
            {
                audioPlayer.Play(owner.cancelSFX);
            }
        }

        void OnSelect(object sender, InfoEventArgs<int> e)
        {
            descriptionBox.SetToDescribable(myConsumables[e.info].consumable, DescriptionBox.Side.Right);
            descriptionBox.SetDescriptionOverride(myConsumables[e.info].consumable.GetDescription(turn.actor)); // Manually set description to see mana/tu mods

            audioPlayer.Play(owner.selectSFX);
        }

        void OnCancel(InputAction.CallbackContext obj)
        {
            owner.ChangeState<ActionSelectState>();

            audioPlayer.Play(owner.cancelSFX);
        }
        #endregion

        #region LifeCycle
        public override void Enter()
        {
            myConsumables = turn.actor.consumableSkills.GetConsumables();
            base.Enter();
        }

        protected override void OnEnterUI()
        {
            //Add listeners
            consumablesMenu.ClickEvent += OnClick;
            consumablesMenu.SelectEvent += OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed += OnCancel;

            consumablesMenu.Clear();

            for (int i = 0; i < myConsumables.Count; i++)
            {
                consumablesMenu.AddEntry(new ConsumableSkillProperties(myConsumables[i].consumableSkill, myConsumables[i].charge, myConsumables[i].maxCharge));

                // Can't use if not enough charges
                if (myConsumables[i].charge <= 0)
                    consumablesMenu.Lock(i);
                else
                    consumablesMenu.UnLock(i);
            }

            // Select
            consumablesMenu.SelectFirstAvailable();
            consumablesMenu.Show();

            descriptionBox.Show();
        }

        protected override void OnExitUI()
        {
            //Remove
            consumablesMenu.ClickEvent -= OnClick;
            consumablesMenu.SelectEvent -= OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed -= OnCancel;

            consumablesMenu.Hide();

            descriptionBox.Hide();
        }
        #endregion
    }
}
