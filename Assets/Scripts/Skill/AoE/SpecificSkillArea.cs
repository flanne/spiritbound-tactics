﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecificSkillArea : BaseSkillArea
{
	int horizontal;
	int vertical;
	BoardTile tile;

	public override List<BoardTile> GetTilesInArea(Board board, Vector2Int pos, Unit unit, SkillRangeData skillRange, int horizontal, int vertical, Skill skill)
	{
		this.horizontal = horizontal;
		this.vertical = vertical;
		tile = board.GetTile(pos);
		return board.Search(tile, ExpandSearch);
	}

	bool ExpandSearch(BoardTile from, BoardTile to)
	{
		return (from.distance + 1) <= horizontal && Mathf.Abs(to.z - tile.z) <= vertical;
	}
}
