﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtectorPassive : Passive
{
    protected override void AddListeners()
    {
        this.AddObserver(OnTargetCheck, BaseSkillEffect.TweakTargetNotifcation);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnTargetCheck, BaseSkillEffect.TweakTargetNotifcation);
    }

    void OnTargetCheck(object sender, object args)
    {
        if (owner.health.SHP == 0 || !(sender is DamageSkillEffect))
            return;

        var exc = args as TargetTileException;
        Unit targetUnit = exc.targetTile.content?.GetComponent<Unit>();

        if (targetUnit)
        {
            int distance = Mathf.Abs(owner.tile.xy.x - targetUnit.tile.xy.x) + Mathf.Abs(owner.tile.xy.y - targetUnit.tile.xy.y);
            if (distance <= 1 && targetUnit.alliance.type == owner.alliance.type)
            {
                exc.targetTile = owner.tile;
            }
        }
    }
}
