﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddRangeModFeature : AddPassiveFeature<RangeModPassive>
{
    [SerializeField]
    SkillTag skillTag;
    [SerializeField]
    int amount;

    protected override void SetParameters(RangeModPassive passive)
    {
        passive.skillTag = this.skillTag;
        passive.amount = this.amount;
    }
}
