﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public struct AbilityTree
{
    [EnumToggleButtons]
    [LabelText("R2")]
    [LabelWidth(20)]
    [SerializeField]
    AbilityTreeSelection abilityR2;

    [EnumToggleButtons]
    [LabelText("R3")]
    [LabelWidth(20)]
    [SerializeField]
    AbilityTreeSelection abilityR3;

    [EnumToggleButtons]
    [LabelText("R4")]
    [LabelWidth(20)]
    [SerializeField]
    AbilityTreeSelection abilityR4;

    public AbilityTreeSelection GetSelection(int rank)
    {
        if (rank == 2)
            return abilityR2;
        else if (rank == 3)
            return abilityR3;
        else if (rank == 4)
            return abilityR4;

        return AbilityTreeSelection.None;
    }

    public void SetSelection(int rank, AbilityTreeSelection selection)
    {
        if (rank == 2)
            abilityR2 = selection;
        else if (rank == 3)
            abilityR3 = selection;
        else if (rank == 4)
            abilityR4 = selection;
    }
}
