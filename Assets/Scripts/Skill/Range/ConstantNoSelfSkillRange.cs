﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantNoSelfSkillRange : ConstantSkillRange
{
	public override List<BoardTile> GetTilesInRange(Board board, Unit unit, int horizontal, int vertical)
	{
		List<BoardTile> tilesInRange = base.GetTilesInRange(board, unit, horizontal, vertical);
		tilesInRange.Remove(unit.tile);

		return tilesInRange;
	}
}
