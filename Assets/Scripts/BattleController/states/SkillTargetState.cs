﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class SkillTargetState : BattleState
    {
        #region Eventhandlers
        void OnClick(object sender, InfoEventArgs<BoardTile> e)
        {
            if (tiles.Contains(currentTile))
            {
                if (directionOriented)
                {
                    Direction dir = turn.actor.tile.xy.GetDirection(currentTile.xy);
                    if (turn.actor.direction != dir)
                    {
                        turn.actor.direction = dir;
                        turn.actor.MatchToTile();
                    }
                }
                owner.ChangeState<SkillConfirmState>();

                audioPlayer.Play(owner.confirmSFX);
            }
        }

        void OnSelect(object sender, InfoEventArgs<BoardTile> e)
        {
            // Check if mouse is on any unit to display hud/ turn opder highlight
            if (e.info.content != null)
            {
                Unit unit = currentTile.content.GetComponent<Unit>();

                if (unit != null)
                {
                    primaryHUD.SetProperties(new UnitProperties(unit));
                    primaryHUD.Show();
                    turnOrderUI.SetHighlight(unit, true);
                    return;
                }
            }
            else
            {
                turnOrderUI.UnHighlightAll();
            }

            primaryHUD.Hide();
        }

        void OnCancel(InputAction.CallbackContext obj)
        {
            if (turn.skill = turn.actor.basicAttack)
            {
                owner.ChangeState<ActionSelectState>();
            }
            else
            {
                owner.ChangeState<SkillSelectState>();
            }

            // Hide turnorder arrow preview if unit at 0 ctr
            if (turn.actor.stats[StatTypes.CTR] == 0)
                turnOrderUI.HidePreviewArrow();
            else
                turnOrderUI.ShowPreviewArrow(turn.actor.stats[StatTypes.CTR]); // Reset preview to current ctr

            audioPlayer.Play(owner.cancelSFX);
        }
        #endregion

        #region Properties
        List<BoardTile> tiles;

        bool directionOriented
        {
            get
            {
                if (turn.skill.isWeaponSkill)
                    return turn.actor.basicAttack.directionOriented;
                else
                    return turn.skill.directionOriented;
            }
        }
        #endregion

        #region LifeCycle
        public override void Enter()
        {
            base.Enter();

            // Check for tutorials
            if (MasterSingleton.main.TutorialShown.action == false && driver.Current != DriverType.Computer)
                StartCoroutine(StartActionTutorial());

            RefreshHighlightTiles();

            turnOrderUI.ShowPreviewArrow(turn.actor.stats[StatTypes.CTR] + turn.skill.GetTimeCost(turn.actor));

            if (driver.Current == DriverType.Computer)
                StartCoroutine(ComputerHighlightTarget());
        }

        public override void Exit()
        {
            base.Exit();

            turnOrderUI.UnHighlightAll();

            tileHighlighter.Clear();
            secondaryHUD.Hide();
        }

        protected override void OnEnterUI()
        {
            //Add Listeners
            tileSelectIndicator.ClickEvent += OnClick;
            tileSelectIndicator.SelectEvent += OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed += OnCancel;

            tileSelectIndicator.Show();
            tileSelectIndicator.SelectTile(turn.actor.tile.xy);

            manaBar.Preview(turn.skill.manaCost);

            // Show keybinds
            keybindPromptUI.Show();
            keybindPromptUI.SetText(0, "Select");
            keybindPromptUI.SetText(1, "Cancel");

            cameraRig.follow = null;
            cameraPanner.active = true;
        }

        protected override void OnExitUI()
        {
            //Remove Listeners
            tileSelectIndicator.ClickEvent -= OnClick;
            tileSelectIndicator.SelectEvent -= OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed -= OnCancel;

            tileSelectIndicator.Hide();

            directionArrowUI.Hide();

            manaBar.ClearPreview();

            cameraRig.follow = turn.actor.transform;
            cameraPanner.active = false;
        }
        #endregion

        #region Private
        void ChangeDirection(Vector2Int p)
        {
            Direction dir = p.GetDirection();
            if (turn.actor.direction != dir)
            {
                turn.actor.direction = dir;
                turn.actor.MatchToTile();
            }
        }

        void RefreshHighlightTiles()
        {
            tileHighlighter.Clear();

            Skill skill = null;
            if (turn.skill.isWeaponSkill)
                skill = turn.actor.basicAttack;
            else
                skill = turn.skill;

            if (skill.directionOriented)
            {
                tiles = new List<BoardTile>();
                Direction originaDir = turn.actor.direction;
                // Highlight tiles of every direction
                foreach (var direction in Enum.GetValues(typeof(Direction)).Cast<Direction>())
                {
                    turn.actor.direction = direction;
                    tiles.AddRange(skill.GetTilesInRange(board, turn.actor));
                }
                turn.actor.direction = originaDir;
            }
            else
                tiles = skill.GetTilesInRange(board, turn.actor);

            // Remove tiles occupied by props
            List<BoardTile> tilesToRemove = new List<BoardTile>();
            foreach (var tile in tiles)
            {
                if (tile.content != null && tile.content.GetComponent<Unit>() == null) // if tile occupied but not by a unit
                    tilesToRemove.Add(tile);
            }

            foreach (var tile in tilesToRemove)
                tiles.Remove(tile);

            tileHighlighter.Highlight(tiles, new Color(1f, 0.3f, 0.15f, 1f));
        }

        IEnumerator StartActionTutorial()
        {
            yield return null;

            owner.ChangeState<ActionTutorialState>();
            MasterSingleton.main.TutorialShown.action = true;
        }
        #endregion

        #region AI
        IEnumerator ComputerHighlightTarget()
        {
            if (directionOriented)
            {
                ChangeDirection(turn.plan.attackDirection.GetNormal());
            }
            else
            {
                tileSelectIndicator.SelectTile(turn.plan.fireLocation);
            }

            yield return new WaitForSeconds(0.5f);

            owner.ChangeState<SkillConfirmState>();
        }
        #endregion
    }
}