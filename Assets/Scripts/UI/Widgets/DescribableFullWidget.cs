﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class DescribableFullWidget : DescribableWidget
    {
        [SerializeField] Image icon;
        [SerializeField] TMP_Text nameTMP;
        [SerializeField] TMP_Text descriptionTMP;

        public override void SetProperties(DescribableProperties properties)
        {
            if (properties.des == null)
            {
                if (icon != null)
                    icon.enabled = false;

                nameTMP.text = "None";
                descriptionTMP.text = "";
            }
            else
            {
                if (icon != null)
                {
                    icon.enabled = true;
                    icon.sprite = properties.des.icon;
                }
                nameTMP.text = MasterSingleton.main.GameStrings.Get(properties.des.nameStringID);
                descriptionTMP.text = properties.des.GetDescription();
            }
        }

        public void SetDescriptionOverride(string description)
        {
            descriptionTMP.text = description;
        }
    }
}
