﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HobbleOnHighHitPassive : StatusOnHitPassive<HobbleStatusEffect>
{ 
    // Check if is a backstab
    protected override bool ConditionsMet(object sender, Info<Unit, Unit, int, Skill> info)
    {
        Unit actor = info.arg0;
        Unit target = info.arg1;

        if (target != null && actor.tile.z > target.tile.z)
            return true;
        else
            return false;
    }
}
