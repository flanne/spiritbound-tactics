﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModStatusDurationPassive : Passive
{
    public int amount;

    protected override void AddListeners()
    {
        this.AddObserver(OnTweakStatusDuration, StatusCondition.TweakStatusDurationNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnTweakStatusDuration, StatusCondition.TweakStatusDurationNotification);
    }

    void OnTweakStatusDuration(object sender, object args)
    {
        var info = args as Info<List<ValueModifier>, Info<Unit, Unit>>;
        var actor = info.arg1.arg0;
        var mods = info.arg0;

        if (actor == owner)
            mods.Add(new AddValueModifier(0, amount));
    }
}