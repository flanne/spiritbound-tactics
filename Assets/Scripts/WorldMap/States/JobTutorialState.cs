﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class JobTutorialState : WorldMapState
{
    public override void Enter()
    {
        owner.jobTutorial.Show();

        StartCoroutine(WaitLockout());
    }

    public override void Exit()
    {
        playerInput.currentActionMap.FindAction("AnyButton").canceled -= OnAnyKey;

        owner.jobTutorial.Hide();
    }

    void OnAnyKey(InputAction.CallbackContext obj)
    {
        if (MasterSingleton.main.TutorialShown.ability == false)
        {
            JobRank primary = owner.menuUnit.jobRanks.Get(owner.menuUnit.jobs.primary);

            JobRank secondary = null;
            if (owner.menuUnit.jobs.secondary != null)
                secondary = owner.menuUnit.jobRanks.Get(owner.menuUnit.jobs.secondary);

            if (primary.rank > 1 || (secondary != null && secondary.rank > 1))
            {
                MasterSingleton.main.TutorialShown.ability = true;

                owner.ChangeState<AbilityTutorialState>();
                return;
            }
        }

        owner.ChangeState<JobManageState>();
    }

    IEnumerator WaitLockout()
    {
        yield return new WaitForSeconds(1f);

        playerInput.currentActionMap.FindAction("AnyButton").canceled += OnAnyKey;
    }
}