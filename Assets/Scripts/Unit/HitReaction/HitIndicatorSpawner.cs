﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitIndicatorSpawner : MonoBehaviour
{
    #region Properties
    public Unit unit { get { return _unit; } }
    [SerializeField]
    Unit _unit;

    [SerializeField]
    GameObject hitIndicatorPrefab;

    [SerializeField]
    GameObject smallHPBarPrefab;

    [SerializeField]
    Color damageColor;
    [SerializeField]
    Color healColor;
    [SerializeField]
    Color missColor;
    [SerializeField]
    Color buffColor;
    [SerializeField]
    Color debuffColor;
    [SerializeField]
    Color xpColor;
    [SerializeField]
    Color levelUpColor;

    GameObject indicator;
    #endregion

    #region Monobehaviour
    void OnEnable()
    {
        this.AddObserver(OnHealthChange, Health.HealthChangeNotification, unit.health);
        this.AddObserver(OnShieldGain, Health.ShieldGainNotification, unit.health);

        this.AddObserver(OnMiss, BaseSkillEffect.MissedNotification);

        // status notifications
        this.AddObserver(OnStatus, Status.AddedNotification, unit);

        this.AddObserver(OnXPGain, Level.XPGainedNotification, unit);
        this.AddObserver(OnLevelUp, Level.LevelUpNotification, unit);
    }

    void OnDisable()
    {
        this.RemoveObserver(OnHealthChange, Health.HealthChangeNotification, unit.health);
        this.RemoveObserver(OnShieldGain, Health.ShieldGainNotification, unit.health);

        this.RemoveObserver(OnMiss, BaseSkillEffect.MissedNotification);

        this.RemoveObserver(OnStatus, Status.AddedNotification, unit);

        this.RemoveObserver(OnXPGain, Level.XPGainedNotification, unit);
        this.RemoveObserver(OnLevelUp, Level.LevelUpNotification, unit);
    }
    #endregion

    #region EventHandlers
    void OnHealthChange(object sender, object args)
    {
        // Don't show hp changes for cutscene units
        if (unit.cutsceneOnly)
            return;

        var info = args as Info<int, int, int, int>;
        int hpChange = info.arg1- info.arg0;
        int shieldChange = info.arg3 - info.arg2;
        int totalChange = hpChange + shieldChange;

        if (info.arg0 == 0)
            return;

        if (totalChange < 0)
            StartCoroutine(QueueIndicator((-1 * totalChange).ToString(), damageColor));
        else
            StartCoroutine(QueueIndicator((totalChange).ToString(), healColor));

        StartCoroutine(SpawnHPBar(info.arg0, info.arg1, info.arg2, info.arg3, unit.stats[StatTypes.MHP]));
    }

    void OnShieldGain(object sender, object args)
    {
        var info = args as Info<int, int>;
        int shieldChange = info.arg1 - info.arg0;
        
        StartCoroutine(QueueIndicator((shieldChange).ToString(), buffColor));
        StartCoroutine(SpawnHPBar(unit.stats[StatTypes.HP], unit.stats[StatTypes.HP], info.arg0, info.arg1, unit.stats[StatTypes.MHP]));
    }

    void OnMiss(object sender, object args)
    {
        var info = args as Info<Unit>;
        if (info.arg0 != this.unit)
            return;

        StartCoroutine(QueueIndicator("Miss!", missColor));
    }

    void OnStatus(object sender, object args)
    {
        StatusEffect s = args as StatusEffect;

        string statusName = s.statusName;
        if (statusName == "")
            return;

        Color statusColor;
        if (s.isBuff)
            statusColor = buffColor;
        else
            statusColor = debuffColor;

        StartCoroutine(QueueIndicator(s.statusName, statusColor));
    }

    void OnXPGain(object sender, object args)
    {
        int xp = (int) args;

        StartCoroutine(QueueIndicator(xp + "XP", xpColor));
    }

    void OnLevelUp(object sender, object args)
    {
        StartCoroutine(QueueIndicator("Lv Up", levelUpColor));
    }
    #endregion

    #region Private
    IEnumerator QueueIndicator(string text, Color color)
    {
        while (indicator != null && !indicator.GetComponent<HitIndicator>().startedFade)
            yield return null;

        SpawnIndicator(text, color);
    }

    void SpawnIndicator(string text, Color color)
    {
        indicator = Instantiate(hitIndicatorPrefab);
        indicator.transform.localPosition = this.transform.position;

        indicator.GetComponent<HitIndicator>().text.text = text;
        indicator.GetComponent<HitIndicator>().text.color = color;

        indicator.transform.SetParent(this.transform);
    }

    IEnumerator SpawnHPBar(int fromHP, int toHP, int fromSHP, int toSHP, int maxHP)
    {
        GameObject hpBarObj = Instantiate(smallHPBarPrefab);
        var unitPos = this.transform.position;
        unitPos.y += 1.25f;
        hpBarObj.transform.localPosition = unitPos;
        hpBarObj.transform.SetParent(this.transform);

        var hpBar = hpBarObj.GetComponent<HealthBarSprite>();
        hpBar.AnimateHPChange(fromHP, toHP, maxHP);
        hpBar.AnimateSHPChange(fromSHP, toSHP, maxHP);

        yield return null;
        while(hpBar.isAnimating)
            yield return null;

        Destroy(hpBarObj);
    }
    #endregion
}
