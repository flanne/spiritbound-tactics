﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldMapLocation : DescribableObject
{
    public Board boardPrefab;

    public Equippable[] shopItems { get { return _shopItems; } }
    [SerializeField] Equippable[] _shopItems;

    public bool hasPub { get { return _hasPub; } }
    [SerializeField] bool _hasPub;

    public MissionData[] patrolBattles;
}
