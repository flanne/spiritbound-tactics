﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddWaitSkillFeature : AddPassiveFeature<WaitSkillPassive>
{
    [SerializeField]
    Skill skill;

    protected override void SetParameters(WaitSkillPassive passive)
    {
        passive.skill = this.skill;
    }
}
