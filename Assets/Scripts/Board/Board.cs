﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Board : MonoBehaviour
{
    public Vector3Int max;
    public Vector3Int min;

    public Grid grid;
    public Tilemap tileMap;
    public Tilemap props;

    Dictionary<Vector2Int, BoardTile> tiles = new Dictionary<Vector2Int, BoardTile>();

    #region Monobehaviour
    void Awake()
    {
        tileMap.CompressBounds();
        props.CompressBounds();

        BoundsInt bounds = tileMap.cellBounds;
        max = bounds.max;
        min = bounds.min;

        // Get highest terrain at each grid location
        foreach (var position in tileMap.cellBounds.allPositionsWithin)
        {
            if (!tileMap.HasTile(position))
                continue;

            var xyPos = new Vector2Int(position.x, position.y);
            if (!tiles.ContainsKey(xyPos))
            {
                tiles.Add(xyPos, new BoardTile(xyPos, position.z, tileMap));
            }
            else if (tiles[xyPos].z < position.z)
            {
                tiles[xyPos].z = position.z;
            }
        }

        // Add props to tiles
        foreach (var position in props.cellBounds.allPositionsWithin)
        {
            if (!props.HasTile(position))
                continue;

            var xyPos = new Vector2Int(position.x, position.y);
            tiles[xyPos].content = new GameObject("Prop");
        }
    }
    #endregion

    #region Public
    public BoardTile GetTile(int x, int y)
    {
        return GetTile(new Vector2Int(x, y));
    }

    public BoardTile GetTile(Vector2Int pos)
    {
        return tiles[pos];
    }

    public List<BoardTile> GetAllTiles()
    {
        return tiles.Values.ToList();
    }

    public bool HasTile(Vector3Int pos)
    {
        Vector2Int xy = new Vector2Int(pos.x, pos.y);
        return tiles.ContainsKey(xy) && tiles[xy].z == pos.z;
    }

    public bool HasTile(Vector2Int pos)
    {
        return tiles.ContainsKey(pos);
    }

    public Vector3Int WorldToCell(Vector3 pos)
    {
        return grid.WorldToCell(pos);
    }

    public Vector3 CellToWorld(Vector3Int pos)
    {
        return tileMap.CellToWorld(pos);
    }
    #endregion

    #region Search
    public List<BoardTile> Search(BoardTile start, System.Func<BoardTile, BoardTile, bool> isReachableTile)
    {
        List<BoardTile> tilesInSearch = new List<BoardTile>();
        tilesInSearch.Add(start);

        // Clear previous searches
        ClearSearch();
        Queue<BoardTile> checkNext = new Queue<BoardTile>();
        Queue<BoardTile> checkNow = new Queue<BoardTile>();

        start.distance = 0;
        checkNow.Enqueue(start);

        // Set 4 Cardinal directions
        Vector2Int[] dirs = new Vector2Int[4]
        {
            new Vector2Int(0, 1),
            new Vector2Int(0, -1),
            new Vector2Int(1, 0),
            new Vector2Int(-1, 0)
        };

        while (checkNow.Count > 0)
        {
            BoardTile t = checkNow.Dequeue();

            // Add a new tile to be searched in every direction
            for (int i = 0; i < 4; ++i)
            {
                BoardTile next = null;
                if (tiles.ContainsKey(t.xy + dirs[i])) // Check if this neighbor tile on board
                    next = tiles[t.xy + dirs[i]];

                if (next == null || next.distance <= t.distance + 1) // If tile is not on board or if this neighbor are also a close distance to target 
                    continue;

                if (isReachableTile(t, next)) // Check if reachable
                {
                    next.distance = t.distance + 1;
                    next.prev = t;
                    checkNext.Enqueue(next);
                    tilesInSearch.Add(next);
                }
            }

            // Check next set of tiles
            if (checkNow.Count == 0)
                SwapReference(ref checkNow, ref checkNext);
        }
        return tilesInSearch;
    }

    private void ClearSearch()
    {
        foreach (BoardTile t in tiles.Values)
        {
            t.prev = null;
            t.distance = int.MaxValue;
        }
    }

    private void SwapReference(ref Queue<BoardTile> a, ref Queue<BoardTile> b)
    {
        Queue<BoardTile> temp = a;
        a = b;
        b = temp;
    }
    #endregion
}
