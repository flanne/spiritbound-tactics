﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace flanne.UI
{
    public class ManaBar : Widget<IntProperty>
    {
        [SerializeField] Material previewMaterial;

        [SerializeField] ManaNode[] nodes;

        private int manaAmount = 0;

        public override void SetProperties(IntProperty properties)
        {
            int amount = properties.i;
            manaAmount = amount;
            GetComponentInChildren<TMP_Text>().text = manaAmount.ToString();

            StartCoroutine(AnimateManaNodes());
        }

        public void Preview(int useAmount)
        {
            ClearPreview();

            if (useAmount > manaAmount)
                return;

            for (int i = manaAmount - 1; i > manaAmount - 1 - useAmount; i--)
                nodes[i].image.material = previewMaterial;
        }

        public void ClearPreview()
        {
            foreach (var n in nodes)
                n.image.material = null;
        }

        IEnumerator AnimateManaNodes()
        {
            for (int i = 0; i < manaAmount; i++)
            {
                if (nodes[i].FillUp())
                    yield return new WaitForSeconds(0.1f);
            }

            for (int i = nodes.Length - 1; i >= manaAmount; i--)
            {
                if (nodes[i].UseUp())
                    yield return new WaitForSeconds(0.1f);
            }
        }
    }
}
