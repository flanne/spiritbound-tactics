﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : Equippable
{
    public ArmorType armorType { get { return type; } }
    [SerializeField] ArmorType type;

    public override EquipSlots defaultSlots { get { return EquipSlots.Body; } }
}

public enum ArmorType
{
    None = 0,
    Cloth = 1,
    Medium = 2,
    Heavy = 3
}
