﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatBonusDamagePassive : Passive
{
    public StatTypes stat;
    public float percentStatToAdd;

    protected override void AddListeners()
    {
        this.AddObserver(OnDealingDamage, DamageSkillEffect.TweakDamageNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnDealingDamage, DamageSkillEffect.TweakDamageNotification);
    }

    void OnDealingDamage(object sender, object args)
    {
        var info = args as Info<List<ValueModifier>, Info<Unit, Unit, Skill>>;
        Unit actor = info.arg1.arg0;

        if (actor != owner)
            return;

        info.arg0.Add(new AddValueModifier(2, actor.stats[stat] * percentStatToAdd));
    }
}
