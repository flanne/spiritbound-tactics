﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillCatalog : MonoBehaviour
{
	List<Skill> skills = new List<Skill>();

	public List<Skill> GetSkills()
    {
		return skills;
    }

	public void AddSkill(Skill skill)
    {
		skills.Add(skill);
	}

	public void RemoveSkill(Skill skill)
	{
		skills.Remove(skill);
	}
}