﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class AbilityTutorialState : WorldMapState
{
    public override void Enter()
    {
        owner.abilityTutorial.Show();

        StartCoroutine(WaitLockout());
    }

    public override void Exit()
    {
        playerInput.currentActionMap.FindAction("AnyButton").canceled -= OnAnyKey;

        owner.abilityTutorial.Hide();
    }

    void OnAnyKey(InputAction.CallbackContext obj)
    {
        owner.ChangeState<JobManageState>();
    }

    IEnumerator WaitLockout()
    {
        yield return new WaitForSeconds(1f);

        playerInput.currentActionMap.FindAction("AnyButton").canceled += OnAnyKey;
    }
}
