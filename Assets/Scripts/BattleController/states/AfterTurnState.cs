﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spiritlink.Battle
{
    public class AfterTurnState : BattleState
    {
        #region Notifications
        public const string EndOfTurnEffectsNotification = "AfterTurnState.EndOfTurnEffectsNotification";
        #endregion

        public override void Enter()
        {
            StartCoroutine(GetEffects());
        }

        IEnumerator GetEffects()
        {
            yield return PlayAutoEffects(EndOfTurnEffectsNotification);

            if (IsBattleOver())
                owner.ChangeState<EndRewardLootState>();
            else
                owner.ChangeState<SelectUnitState>();
        }
    }
}