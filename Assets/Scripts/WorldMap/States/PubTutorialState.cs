﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PubTutorialState : WorldMapState
{
    void OnDialogueFinish(object sender, EventArgs e)
    {
        dialogueBox.Hide();

        owner.ChangeState<TraverseWorldMapState>();
    }

    public override void Enter()
    {
        dialogueBox.SetDialogueBox(UnitID.GetUnitWithID(IDEnum.Bo), owner.pubTutorialDialogueStringID);
        dialogueBox.Show();

        dialogueBox.finishedEvent += OnDialogueFinish;
    }
}
