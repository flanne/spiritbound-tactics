﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Spiritlink.Battle;

public class PoisonStatusEffect : StatusEffect
{
    protected override string nameID { get { return "KEYWORD_POISON"; } }
    public override bool isBuff { get { return false; } }
    public override string spriteFileName { get { return "icon_sts_poison"; } }
    public override Keyword keyword { get { return Resources.Load<Keyword>("Poison"); } }

    int patkReduction;

    Skill poisonSkill { get { return Resources.Load<Skill>("sk_poison"); } }

    protected override void OnApply()
    {
        this.AddObserver(OnNewTurn, BeforeTurnState.StartOfTurnEffectsNotification, owner);

        patkReduction = Mathf.FloorToInt(owner.stats[StatTypes.ATK] * 0.2f);
        owner.stats[StatTypes.ATK] -= patkReduction;
    }

    protected override void OnRemove()
    {
        this.RemoveObserver(OnNewTurn, BeforeTurnState.StartOfTurnEffectsNotification, owner);

        owner.stats[StatTypes.ATK] += patkReduction;
    }

    void OnNewTurn(object sender, object args)
    {
        var effects = args as List<SkillDirections>;
        effects.Add(new SkillDirections(poisonSkill, owner, owner.tile));
    }
}