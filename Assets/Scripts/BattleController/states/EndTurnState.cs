﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Spiritlink.Battle
{
    public class EndTurnState : BattleState
    {
        #region Notifications
        public const string WaitNotification = "EndTurnState.WaitNotification";
        #endregion

        #region Eventhandlers
        void OnMove(InputAction.CallbackContext obj)
        {
            Vector2 vec = obj.ReadValue<Vector2>();
            if (vec.GetDirection() != Direction.None)
                SetDirection(vec.GetDirection());
        }

        void OnPoint(InputAction.CallbackContext obj)
        {
            Vector2 vec = obj.ReadValue<Vector2>();
            var worldPos = Camera.main.ScreenToWorldPoint(vec);
            var unitPos = turn.actor.transform.position;

            if ((worldPos.x - unitPos.x) < 0)
            {
                if ((worldPos.y - unitPos.y) < 1.4f)
                    SetDirection(Direction.Left);
                else
                    SetDirection(Direction.Up);
            }
            else
            {
                if ((worldPos.y - unitPos.y) < 1.4f)
                    SetDirection(Direction.Down);
                else
                    SetDirection(Direction.Right);
            }
        }

        void OnClick(InputAction.CallbackContext obj)
        {
            EndTurn();
        }

        void OnCancel(InputAction.CallbackContext obj)
        {
            audioPlayer.Play(owner.cancelSFX);

            owner.ChangeState<ActionSelectState>();
        }
        #endregion

        #region BattleState
        public override void Enter()
        {
            base.Enter();

            directionArrowUI.Show();

            SetDirection(turn.actor.direction, true); // set direction arrow to actor

            if (driver.Current == DriverType.Computer)
                StartCoroutine(ComputerControl());
            else
                directionPrompt.Show();

            // Hide turnorder arrow preview if unit at 0 ctr
            if (!turn.hasUnitActed)
            {
                int baseWaitTime = 80;
                int finalWaitTime = baseWaitTime.NotifyModifiers(WaitNotification, turn.actor);

                turnOrderUI.ShowPreviewArrow(turn.actor.stats[StatTypes.CTR] + finalWaitTime);
            }
        }

        public override void Exit()
        {
            base.Exit();

            directionArrowUI.Hide();

            directionPrompt.Hide();
            turnOrderUI.HidePreviewArrow();
        }

        protected override void OnEnterUI()
        {
            // Add Listeners
            playerInput.currentActionMap.FindAction("Navigate").performed += OnMove;
            playerInput.currentActionMap.FindAction("Point").performed += OnPoint;
            playerInput.currentActionMap.FindAction("Submit").performed += OnClick;
            playerInput.currentActionMap.FindAction("Click").performed += OnClick;
            playerInput.currentActionMap.FindAction("Cancel").performed += OnCancel;

            // Show keybinds
            keybindPromptUI.Show();
            keybindPromptUI.SetText(0, "Confirm");
            keybindPromptUI.SetText(1, "Cancel");
        }

        protected override void OnExitUI()
        {
            // Remove Listeners
            playerInput.currentActionMap.FindAction("Navigate").performed -= OnMove;
            playerInput.currentActionMap.FindAction("Point").performed -= OnPoint;
            playerInput.currentActionMap.FindAction("Submit").performed -= OnClick;
            playerInput.currentActionMap.FindAction("Click").performed -= OnClick;
            playerInput.currentActionMap.FindAction("Cancel").performed -= OnCancel;

            // Hide keybinds
            keybindPromptUI.Hide();
        }
        #endregion

        #region Private
        void SetDirection(Direction dir, bool forceSet = false)
        {
            if (turn.actor.direction == dir && !forceSet)
                return;

            audioPlayer.Play(owner.selectSFX);

            turn.actor.direction = dir;
            turn.actor.MatchToTile();

            directionArrowUI.SetDirection(dir);
        }

        void EndTurn()
        {
            // Play sound effect
            audioPlayer.Play(owner.confirmSFX);

            if (!turn.hasUnitActed)
            {
                int baseWaitTime = 80;
                int finalWaitTime = baseWaitTime.NotifyModifiers(WaitNotification, turn.actor);

                turn.actor.stats[StatTypes.CTR] += finalWaitTime;
            }

            owner.ChangeState<AfterTurnState>();
        }

        int Compare(ValueModifier x, ValueModifier y)
        {
            return x.sortOrder.CompareTo(y.sortOrder);
        }
        #endregion

        #region AI
        IEnumerator ComputerControl()
        {
            yield return new WaitForSeconds(0.2f);
            SetDirection(owner.cpu.DetermineEndFacingDirection());
            yield return new WaitForSeconds(0.2f);

            EndTurn();
        }
        #endregion
    }
}