﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddModStatusDurationFeature : AddPassiveFeature<ModStatusDurationPassive>
{
    [SerializeField]
    int amount;

    protected override void SetParameters(ModStatusDurationPassive passive)
    {
        passive.amount = this.amount;
    }
}
