﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;
using TMPro;

public class WorldMapNode : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    #region Properties
    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] SpriteRenderer missionIndicator;
    [SerializeField] TMP_Text label;

    public WorldMapLocation level { get { return _level; } }

    [SerializeField] WorldMapLocation _level;

    int tweenID;

    public bool selected = false;
    #endregion

    #region Pathfinding
    public WorldMapNode[] adjacentNodes { get { return _adjacentNodes; } }
    [SerializeField]
    WorldMapNode[] _adjacentNodes;
    
    [System.NonSerialized]
    public WorldMapNode prev;

    [System.NonSerialized]
    public int distance;
    #endregion

    #region Eventhandlers
    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        PointerEnterAnimation();
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        PointerExitAnimation();
    }
    #endregion

    #region Public
    public bool hasMission { 
        get { 
            return _hasMission; 
        } 
        set {
            _hasMission = value;
            missionIndicator.enabled = _hasMission;
        } 
    }
    bool _hasMission = false;

    public bool MouseIsOn()
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());

        Bounds bounds = spriteRenderer.bounds;

        float min_x = bounds.center.x - bounds.extents.x;
        float max_x = bounds.center.x + bounds.extents.x;
        float min_y = bounds.center.y - bounds.extents.y;
        float max_y = bounds.center.y + bounds.extents.y;

        if (mousePosition.x >= min_x && mousePosition.x <= max_x && mousePosition.y >= min_y && mousePosition.y <= max_y)
            return true;
        else
            return false;
    }

    public bool IsPointOn(Vector2 point)
    {
        Bounds bounds = spriteRenderer.bounds;

        float min_x = bounds.center.x - bounds.extents.x;
        float max_x = bounds.center.x + bounds.extents.x;
        float min_y = bounds.center.y - bounds.extents.y;
        float max_y = bounds.center.y + bounds.extents.y;

        if (point.x >= min_x && point.x <= max_x && point.y >= min_y && point.y <= max_y)
            return true;
        else
            return false;
    }

    public void PointerEnterAnimation()
    {
        if (!selected && !LeanTween.isTweening(tweenID))
        {
            tweenID = LeanTween.scale(spriteRenderer.gameObject, new Vector3(1.2f, 1.2f, 0), 0.1f).id;

            label.text = MasterSingleton.main.GameStrings.Get(level.nameStringID);
            label.enabled = true;

            selected = true;
        }
    }

    public void PointerExitAnimation()
    {
        if (spriteRenderer != null)
            LeanTween.scale(spriteRenderer.gameObject, new Vector3(1f, 1f, 0), 0.1f);

        if (label != null)
            label.enabled = false;

        selected = false;
    }
    #endregion
}
