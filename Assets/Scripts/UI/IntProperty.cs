﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace flanne.UI
{
    public class IntProperty : IUIProperties
    {
        public int i;

        public IntProperty(int i)
        {
            this.i = i;
        }
    }
}
