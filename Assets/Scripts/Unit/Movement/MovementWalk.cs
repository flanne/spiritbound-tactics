﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementWalk : Movement
{
    public override bool TraversalConditionMet(BoardTile from, BoardTile to, bool ignoreUnits = false)
    {
        // Skip if the distance in height between the two tiles is more than the unit can jump
        if ((Mathf.Abs(from.z - to.z) > jumpHeight))
            return false;
        // Skip if the tile is occupied by an enemy
        if (to.content != null && !ignoreUnits)
        {
            var occupier = to.content.GetComponent<Unit>();
            if (occupier == null) // Skip if occupied by a non-unit 
                return false;
            else if (occupier.alliance.type != unit.alliance.type)
                return false;
        }

        return true;
    }

    public override IEnumerator Traverse(BoardTile tile, float moveSpeed = DefaultMoveSpeed, bool needSearch = false)
    {
        // Search all tiles if needed
        if (needSearch)
            MasterSingleton.main.GameLevel.board.Search(unit.tile, SearchAllTiles);

        // Build a list of way points from the unit's 
        // starting tile to the destination tile
        List<BoardTile> targets = new List<BoardTile>();
        BoardTile searchTile = tile;
        while (searchTile != null)
        {
            targets.Insert(0, searchTile);
            searchTile = searchTile.prev;
        }
        
        // Set unit tile and animation state
        unit.Place(tile);
        unit.SetAnimationState(UnitAnimState.Idle);
        yield return null;
        unit.SetAnimationState(UnitAnimState.Walk); // Must transition from idle to walk

        // Move to each way point in succession
        for (int i = 1; i < targets.Count; ++i)
        {
            BoardTile from = targets[i - 1];
            BoardTile to = targets[i];

            // Face towards square to move to
            Direction dir = from.xy.GetDirection(to.xy);
            if (unit.direction != dir)
                unit.direction = dir;

            // Check if any unit on the tile should move out of the way
            if (to.content != null)
            {
                Unit u = to.content.GetComponent<Unit>();

                if (u != null && u != unit)
                    MoveOutOfWay(u, dir);
            }

            // Move or jump to square depending on height difference
            if (from.z == to.z)
                yield return StartCoroutine(Walk(to, moveSpeed));
            else
                yield return StartCoroutine(Jump(to));

        }
        yield return null;

        // Set unit animation state back to idle
        unit.SetAnimationState(UnitAnimState.Idle);
    }

    IEnumerator Walk(BoardTile target, float moveSpeed)
    {
        int id = LeanTween.move(unit.gameObject, target.GetWorldPosition(), 1/moveSpeed).id;

        while (LeanTween.isTweening(id))
            yield return null;
    }

    IEnumerator Jump(BoardTile to)
    {
        int id = LeanTween.move(unit.gameObject, to.GetWorldPosition(), 1/DefaultMoveSpeed).id;

        LeanTween.moveLocalY(unit.jumper.gameObject, 0.3f, (1/DefaultMoveSpeed) / 2)
            .setLoopPingPong(1)
            .setEase(LeanTweenType.easeOutQuad);

        LeanTween.moveLocalZ(unit.jumper.gameObject, 0.3f, (1/DefaultMoveSpeed) / 2)
            .setLoopPingPong(1)
            .setEase(LeanTweenType.easeOutQuad);

        while (LeanTween.isTweening(id))
            yield return null;
    }

    void MoveOutOfWay(Unit otherUnit, Direction incomingDir)
    {
        Vector3 dodgeVector = new Vector3(0.2f, -0.1f, 0);
        switch (incomingDir)
        {
            case Direction.Left:
                dodgeVector = new Vector3(0.2f, -0.1f, 0);
                break;
            case Direction.Up:
                dodgeVector = new Vector3(0.2f, 0.1f, 0);
                break;
            case Direction.Right:
                dodgeVector = new Vector3(-0.2f, 0.1f, 0);
                break;
            default:
                dodgeVector = new Vector3(-0.2f, -0.1f, 0);
                break;
        }

        LeanTween.move(otherUnit.gameObject, otherUnit.transform.position + dodgeVector, 1 / DefaultMoveSpeed).setLoopPingPong(1);
    }

    bool SearchAllTiles(BoardTile from, BoardTile to)
    {
        return true;
    }
}
