﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileHighlightController : MonoBehaviour
{
    public GameObject tileHighlightPrefab;

    public List<GameObject> highlightedTiles { get; private set; }

    void Awake()
    {
        highlightedTiles = new List<GameObject>();
    }

    public void Highlight(List<BoardTile> tiles, Color color)
    {
        foreach (BoardTile t in tiles)
        {
            GameObject instance = Instantiate(tileHighlightPrefab) as GameObject;
            highlightedTiles.Add(instance);

            Vector3 worldPosition = t.GetWorldPosition();
            worldPosition.z = t.z + 2.0f;

            instance.GetComponent<Transform>().localPosition = worldPosition;

            instance.GetComponent<SpriteRenderer>().color = color;
        }
    }

    public void Clear()
    {
        foreach (GameObject t in highlightedTiles)
            Destroy(t);

        highlightedTiles.Clear();
    }
}
