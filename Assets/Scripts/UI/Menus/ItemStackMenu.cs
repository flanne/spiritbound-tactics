﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace flanne.UI
{
    public class ItemStackMenu : DynamicMenu<ItemStackProperties>
    {
        public void AddEntriesFromItemStacks(List<ItemStack> items)
        {
            Clear();

            foreach (var itemStack in items)
                AddEntry(new ItemStackProperties(itemStack));
        }
    }
}
