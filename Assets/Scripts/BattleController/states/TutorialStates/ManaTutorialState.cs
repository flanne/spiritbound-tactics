﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class ManaTutorialState : TutorialState<ActionSelectState>
    {
        protected override Panel tutorialPanel { get { return owner.manaTutorial; } }
    }
}