﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayTUSkillEffect : BaseSkillEffect
{
	#region Public
	public override int Predict(Unit actor, BoardTile target, int power, Skill skill)
	{
		return power;
	}

	protected override int OnApply(Unit actor, BoardTile target, int power, Skill skill)
	{
		Unit defender = target.content.GetComponent<Unit>();

		// Start with the predicted damage value
		int value = Predict(actor, target, power, skill);

		// Apply turn delay
		defender.stats[StatTypes.CTR] += value;
		if (defender.stats[StatTypes.CTR] < 0)
			defender.stats[StatTypes.CTR] = 0;

		return value;
	}
	#endregion
}
