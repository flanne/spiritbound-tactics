﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtectedStatusEffect : StatusEffect
{
    protected override string nameID { get { return ""; } } // Deprecated effect
    public override bool isBuff { get { return true; } }
    public override string spriteFileName { get { return ""; } }
    public override Keyword keyword { get { return null; } }
}
