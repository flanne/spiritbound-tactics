﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MDefToHealingPassive : Passive
{
    public const float percentMDef = 0.1f;

    protected override void AddListeners()
    {
        this.AddObserver(OnHeal, BaseSkillEffect.TweakHealNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnHeal, BaseSkillEffect.TweakHealNotification);
    }

    void OnHeal(object sender, object args)
    {
        var info = args as Info<List<ValueModifier>, Info<Unit, Unit, Skill>>;
        Unit actor = info.arg1.arg0;

        if (actor != owner)
            return;

        int mdef = actor.stats[StatTypes.MDF];
        int bonus = Mathf.FloorToInt(mdef * percentMDef);
        info.arg0.Add(new AddValueModifier(2, bonus));
    }
}
