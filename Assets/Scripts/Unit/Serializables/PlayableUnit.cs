﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayableUnit
{
    public IDEnum id { get { return _id; } }
    [SerializeField] IDEnum _id;

    public UnitData unitData { get { return _unitData; } }
    [SerializeField] UnitData _unitData;

    public PlayableUnit(IDEnum id, UnitData unitData)
    {
        _id = id;
        _unitData = unitData;
    }
}
