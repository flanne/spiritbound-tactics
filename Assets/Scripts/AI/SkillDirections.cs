﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillDirections
{
    public Skill skill;
    public Unit actor;
    public BoardTile targetTile;

    public SkillDirections(Skill skill, Unit actor, BoardTile targetTile)
    {
        this.skill = skill;
        this.actor = actor;
        this.targetTile = targetTile;
    }
}
