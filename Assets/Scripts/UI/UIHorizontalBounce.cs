﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHorizontalBounce : MonoBehaviour
{
    [SerializeField] float magnitude = 5f;
    [SerializeField] float bouncePerSecond = 2f;

    // Start is called before the first frame update
    void Start()
    {
        LeanTween.moveLocalX(this.gameObject, this.transform.localPosition.x - magnitude, 1f/bouncePerSecond).setLoopPingPong().setEase(LeanTweenType.easeInOutQuad);
    }
}
