﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spiritlink.Battle;

public class WaitTimeModPassive : Passive
{
    public int amount;

    protected override void AddListeners()
    {
        this.AddObserver(OnGetWaitTime, EndTurnState.WaitNotification, owner);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnGetWaitTime, EndTurnState.WaitNotification, owner);
    }

    void OnGetWaitTime(object sender, object args)
    {
        var mods = args as List<ValueModifier>;
        mods.Add(new AddValueModifier(0, amount));
    }
}