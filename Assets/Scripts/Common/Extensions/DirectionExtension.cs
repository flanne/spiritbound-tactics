﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DirectionExtensions
{
    public static Direction GetDirection(this Vector2Int t1, Vector2Int t2)
    {
        if (Mathf.Abs(t1.y - t2.y) > Mathf.Abs(t1.x - t2.x))
        {
            if (t1.y < t2.y)
                return Direction.Up;
            else
                return Direction.Down;
        }
        else
        {
            if (t1.x < t2.x)
                return Direction.Right;
            else
                return Direction.Left;
        }
    }

    public static Vector3 ToEuler(this Direction d)
    {
        return new Vector3(0, (int)d * 90, 0);
    }

    public static Direction GetDirection(this Vector2Int p)
    {
        if (p.y > 0)
            return Direction.Up;
        if (p.x > 0)
            return Direction.Right;
        if (p.y < 0)
            return Direction.Down;
        return Direction.Left;
    }

    public static Direction GetDirection(this Vector2 p)
    {
        if (p.x <= 0 && p.y > 0)
            return Direction.Up;
        if (p.x > 0 && p.y >= 0)
            return Direction.Right;
        if (p.x >= 0 && p.y < 0)
            return Direction.Down;
        if (p.x < 0 && p.y <= 0)
            return Direction.Left;

        return Direction.None;
    }

    public static Vector2Int GetNormal(this Direction dir)
    {
        switch (dir)
        {
            case Direction.Up:
                return new Vector2Int(0, 1);
            case Direction.Right:
                return new Vector2Int(1, 0);
            case Direction.Down:
                return new Vector2Int(0, -1);
            default: // Direction.West:
                return new Vector2Int(-1, 0);
        }
    }
}

