﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SkillSoundFXData
{
    public AudioClip audioClip { get { return _audioClip; } }
    [SerializeField] AudioClip _audioClip;

    public float delay { get { return _delay; } }
    [SerializeField] float _delay;

    public bool onHitOnly { get { return _onHitOnly; } }
    [SerializeField] bool _onHitOnly = false;
}
