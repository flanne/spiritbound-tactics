﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WardStatusEffect : StatusEffect
{
    protected override string nameID { get { return "KEYWORD_WARD"; } }
    public override bool isBuff { get { return true; } }
    public override string spriteFileName { get { return "icon_sts_ward"; } }
    public override Keyword keyword { get { return Resources.Load<Keyword>("Ward"); } }

    float multiplier = 0.75f;

    protected override void OnApply()
    {
        this.AddObserver(OnDealingDamage, DamageSkillEffect.TweakDamageNotification);
    }

    protected override void OnRemove()
    {
        this.RemoveObserver(OnDealingDamage, DamageSkillEffect.TweakDamageNotification);
    }

    void OnDealingDamage(object sender, object args)
    {
        var info = args as Info<List<ValueModifier>, Info<Unit, Unit, Skill>>;
        Unit defender = info.arg1.arg1;

        if (defender != owner)
            return;

        info.arg0.Add(new MultValueModifier(3, multiplier));
    }
}

