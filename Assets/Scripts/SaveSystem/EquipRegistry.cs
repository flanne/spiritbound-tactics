﻿using UnityEngine;

[CreateAssetMenu(menuName = "SpiritlinkTactics/Registry/Equip Registry")]
public class EquipRegistry : Registry<Equippable>
{
}