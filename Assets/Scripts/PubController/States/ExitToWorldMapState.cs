﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Spiritlink.Pub
{
    public class ExitToWorldMapState : PubState
    {
        public override void Enter()
        {
            MasterSingleton.main.AudioPlayer.FadeOutMusic(1f);
            MasterSingleton.main.ScreenFilter.FadeOut(LoadWorldMap);
        }

        void LoadWorldMap()
        {
            SceneManager.LoadScene("WorldMap");
            MasterSingleton.main.ScreenFilter.FadeIn();
        }
    }
}
