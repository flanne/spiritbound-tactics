﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathFromAbovePassive : Passive
{
    const float threshold = 0.05f;
    protected override void AddListeners()
    {
        this.AddObserver(OnHit, BaseSkillEffect.HitNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnHit, BaseSkillEffect.HitNotification);
    }

    void OnHit(object sender, object args)
    {
        var info = args as Info<Unit, Unit, int, Skill>;
        Unit actor = info.arg0;
        Unit defender = info.arg1;

        if (sender is DamageSkillEffect && actor == owner)
        {
            int zDiff = actor.tile.z - defender.tile.z;
            float totalThreshold = zDiff * threshold;
            float precentHP = (float) defender.health.HP / (float) defender.health.MHP;
            if (precentHP < totalThreshold)
                defender.health.HP = 0;
        }
    }
}
