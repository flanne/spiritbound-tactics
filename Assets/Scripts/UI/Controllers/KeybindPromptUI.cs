﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class KeybindPromptUI : Panel
    {
        [SerializeField] BindingDisplay[] bindings;

        public void SetText(int index, string str)
        {
            bindings[index].SetLabel(str);
            bindings[index].gameObject.SetActive(true);
        }

        public void HidePrompt(int index)
        {
            bindings[index].gameObject.SetActive(false);
        }
    }
}
