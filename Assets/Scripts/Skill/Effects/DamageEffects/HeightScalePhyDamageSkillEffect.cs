﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeightScalePhyDamageSkillEffect : PhysicalDamageSkillEffect
{
	public override int GetAttack(Unit actor, Unit targetUnit)
	{
		int zDiff = actor.tile.z - targetUnit.tile.z;
		float damageMultiplier = (1f + (zDiff * 0.05f));
		damageMultiplier = Mathf.Clamp(damageMultiplier, 1f, 2f);

		return Mathf.FloorToInt(actor.stats[StatTypes.ATK] * damageMultiplier);
	}

	public override int GetDefence(Unit actor, Unit targetUnit)
	{
		return targetUnit.stats[StatTypes.DEF];
	}
}
