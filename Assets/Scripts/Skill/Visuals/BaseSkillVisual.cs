﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public abstract class BaseSkillVisual : MonoBehaviour
{
    public abstract TargetType targetType { get; }
    public enum TargetType
    {
        Actor,
        Targets,
        Tile,
        None,
        Special,
        TargetNoSelf
    };

    public bool onHitOnly { get { return _onHitOnly; } }
    [SerializeField] bool _onHitOnly = false;

    [SerializeField] protected float delay;
    public bool isPlaying { get; protected set; } = false;

    public void Play(List<BoardTile> targets)
    {
        StartCoroutine("PlayAfterDelay", targets);
    }

    public virtual void Exit() { }

    protected abstract IEnumerator Animate(BoardTile target);

    protected IEnumerator PlayAfterDelay(List<BoardTile> targets)
    {
        isPlaying = true;

        yield return new WaitForSeconds(delay);

        foreach (BoardTile t in targets)
        {
            StartCoroutine("Animate", t);
        }
    }

    public virtual void SpecialPlay(Unit actor, BoardTile target)
    {
    }
}
