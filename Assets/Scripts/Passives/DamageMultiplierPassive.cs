﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageMultiplierPassive : Passive 
{
    public DamageTypes damageType;
    public float multiplier;

    protected override void AddListeners()
    {
        this.AddObserver(OnDealingDamage, DamageSkillEffect.TweakDamageNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnDealingDamage, DamageSkillEffect.TweakDamageNotification);
    }

    void OnDealingDamage(object sender, object args)
    {
        var info = args as Info<List<ValueModifier>, Info<Unit, Unit, Skill>>;
        Unit actor = info.arg1.arg0;

        if (actor != owner)
            return;

        if (damageType == DamageTypes.Physical && sender is PhysicalDamageSkillEffect)
            info.arg0.Add(new MultValueModifier(1, multiplier));
        else if (damageType == DamageTypes.Magic && sender is MagicDamageSkillEffect)
            info.arg0.Add(new MultValueModifier(1, multiplier));
    }
}
