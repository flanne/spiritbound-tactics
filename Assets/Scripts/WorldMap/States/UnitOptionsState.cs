﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class UnitOptionsState : WorldMapState
{
    #region Eventhandlers
    void OnClick(object sender, InfoEventArgs<int> e)
    {
        if (e.info == 0)
            owner.ChangeState<EquipManageState>();
        else if (e.info == 1)
            owner.ChangeState<JobManageState>();
        if (e.info == 2)
            owner.ChangeState<UnitListMenuState>();

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnSelect(object sender, InfoEventArgs<int> e)
    {
        audioPlayer.Play(owner.selectSFX);
    }

    void OnCancel(InputAction.CallbackContext obj)
    {
        owner.ChangeState<UnitListMenuState>();

        audioPlayer.Play(owner.cancelSFX);
    }
    #endregion

    #region LifeCycle
    public override void Enter()
    {
        unitOptionsMenu.Show();
        unitOptionsMenu.SelectFirstAvailable();

        unitOptionsMenu.ClickEvent += OnClick;
        unitOptionsMenu.SelectEvent += OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;
    }

    public override void Exit()
    {
        unitOptionsMenu.Hide();

        unitOptionsMenu.ClickEvent -= OnClick;
        unitOptionsMenu.SelectEvent -= OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;
    }
    #endregion
}
