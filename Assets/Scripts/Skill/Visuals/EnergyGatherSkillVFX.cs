﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class EnergyGatherSkillVFX : BaseSkillVisual
{
    [SerializeField]
    [ColorUsage(true, true)]
    Color color;

    GameObject animPrefab { get { return Resources.Load<GameObject>("AnimPrefab-EnergyGather"); } }
    GameObject lightPrefab { get { return Resources.Load<GameObject>("Light-EnergyGather"); } }
    public override TargetType targetType { get { return TargetType.Actor; } }

    Unit unit;

    public override void Exit()
    {
        // reset unit animation to idle
        unit.SetAnimationState(UnitAnimState.Idle);

        Unit[] allUnits = GameObject.FindObjectsOfType<Unit>();
        // deactivate magic cirlce for all allies
        foreach (var u in allUnits)
            if (unit.alliance.type == u.alliance.type)
                u.magicCircle.Deactivate();
    }

    protected override IEnumerator Animate(BoardTile target)
    {
        // activate magic circle for all allies
        unit = target.content.GetComponent<Unit>();
        Unit[] allUnits = GameObject.FindObjectsOfType<Unit>();
        foreach (var u in allUnits)
            if (unit.alliance.type == u.alliance.type && !u.IsKnockedOut()) 
                u.magicCircle.Activate();

        yield return new WaitForSeconds(0.5f);

        // cast animation
        unit.SetAnimationState(UnitAnimState.Cast);

        yield return new WaitForSeconds(0.8f);

        // spawn light
        StartCoroutine(LightFX(target));

        // spawn and play animation
        GameObject obj = SpawnObj(target, animPrefab);
        var render = obj.GetComponent<Renderer>();
        render.material.SetColor("_Color", color);

        Animator animator = obj.GetComponent<Animator>();
        while (animator?.GetCurrentAnimatorStateInfo(0).normalizedTime < 1 && !animator.IsInTransition(0))
            yield return null;

        Destroy(obj);

        isPlaying = false;
    }

    IEnumerator LightFX(BoardTile target)
    {
        GameObject obj = SpawnObj(target, lightPrefab);
        Light2D light = obj.GetComponent<Light2D>();
        light.color = color;

        float intensity = 1f;

        float timer = 0;

        while (timer < 0.33f)
        {
            light.intensity = (timer / 0.33f) * intensity;

            timer += Time.deltaTime;
            yield return null;
        }

        yield return new WaitForSeconds(0.33f);

        float impactIntensity = 1.5f;
        light.intensity = impactIntensity;

        yield return new WaitForSeconds(0.167f);
        timer = 0;

        while (timer < 0.167f)
        {
            light.intensity = impactIntensity - ((timer / 0.167f) * impactIntensity);

            timer += Time.deltaTime;
            yield return null;
        }

        Destroy(obj);
    }

    GameObject SpawnObj(BoardTile target, GameObject prefab)
    {
        GameObject obj = Instantiate(prefab);

        Vector3 pos = target.GetWorldPosition();
        pos.y += 1.75f;
        pos.z += 10;
        obj.transform.position = pos;

        return obj;
    }
}

