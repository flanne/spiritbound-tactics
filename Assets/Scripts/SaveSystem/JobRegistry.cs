﻿using UnityEngine;

[CreateAssetMenu(menuName = "SpiritlinkTactics/Registry/Job Registry")]
public class JobRegistry : Registry<Job>
{
}