﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneSequence : MonoBehaviour
{
    [System.NonSerialized]
    public bool isPlaying = false;

    public CutsceneController owner;

    public IEnumerator PlaySequence()
    {
        isPlaying = true;

        for (int i = 0; i < this.transform.childCount; i++)
        {
            CutsceneAction[] a = transform.GetChild(i).GetComponents<CutsceneAction>();
            foreach (var x in a)
            {
                x.owner = owner;
                x.Play();
            }

            while (AnyActionsPlaying(a))
                yield return null;
        }

        isPlaying = false;
    }

    public void Stop()
    {
        CutsceneAction[] a = transform.GetComponentsInChildren<CutsceneAction>();
        foreach (var x in a)
            x.Stop();
    }

    bool AnyActionsPlaying(CutsceneAction[] actions)
    {
        foreach (var a in actions)
        {
            if (a.isPlaying)
                return true;
        }

        return false;
    }
}
