﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIStrategy : MonoBehaviour
{
    public AIOption[] cantripOptions; // ordered by priority
    public AIOption[] skillOptions; // ordered by priority
}
