﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfSkillRange : BaseSkillRange
{
	public override bool positionOriented { get { return false; } }

	public override List<BoardTile> GetTilesInRange(Board board, Unit unit, int horizontal, int vertical)
	{ 
		List<BoardTile> retValue = new List<BoardTile>(1);
		retValue.Add(unit.tile);
		return retValue;
	}
}
