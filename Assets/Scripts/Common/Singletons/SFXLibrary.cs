﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXLibrary : MonoBehaviour
{
    public AudioClip LevelUpSound { get { return _levelUpSound; } }
    [SerializeField] AudioClip _levelUpSound;

    public AudioClip DialogueBlip { get { return _dialogueBlip; } }
    [SerializeField] AudioClip _dialogueBlip;

    public AudioClip CoinSound { get { return _coinSound; } }
    [SerializeField] AudioClip _coinSound;

    [SerializeField]
    List<AudioClip> sfxClips;

    public AudioClip SkillMissSFX { get { return _skillMissSFX; } }
    [SerializeField] AudioClip _skillMissSFX;

    public AudioClip Get(int index)
    {
        if (index < sfxClips.Count)
            return sfxClips[index];
        else
            return null;
    }
}
