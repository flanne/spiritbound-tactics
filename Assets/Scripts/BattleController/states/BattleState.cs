﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Tilemaps;
using UnityEngine.Experimental.Rendering.Universal;
using TMPro;
using flanne.UI;

namespace Spiritlink.Battle
{
    public abstract class BattleState : State
    {
        #region Notifications
        public const string AfterSkillEffectsNotification = "PerformSkillState.AfterSkillEffectsNotification";
        #endregion

        #region Fields
        protected BattleController owner;
        protected PlayerInput playerInput { get { return owner.playerInput; } }
        protected BattleCameraPanner cameraPanner { get { return owner.cameraPanner; } }

        public CameraRig cameraRig { get { return owner.cameraRig; } }
        public Board board { get { return owner.board; } }
        public AudioPlayer audioPlayer { get { return MasterSingleton.main.AudioPlayer; } }
        public BoardTile currentTile { get { return tileSelectIndicator.currentTile; } }
        public CutsceneController cutsceneController { get { return owner.cutsceneController; } }
        public Turn turn { get { return owner.turn; } }
        public List<Unit> units { get { return owner.units; } }
        protected Driver driver;
        #endregion

        #region UI Objects
        // Menues
        protected flanne.UI.Menu actionMenu { get { return owner.actionMenu; } }
        protected TMP_Text actionMenuLabel { get { return owner.actionMenuLabel; } }

        public DescribableMenu skillMenu { get { return owner.skillMenu; } }
        public ConsumableSkillMenu consumablesMenu { get { return owner.consumablesMenu; } }
        public flanne.UI.Menu startConfirmMenu { get { return owner.startConfirmMenu; } }
        public flanne.UI.Menu exitConfirmMenu { get { return owner.exitConfirmMenu; } }

        // UIControllers
        public TileSelectIndicator tileSelectIndicator { get { return owner.tileSelectIndicator; } }
        public TileHighlightController tileHighlighter { get { return owner.tileHighlighter; } }
        public Panel battleSpeedButton { get { return owner.battleSpeedButton; } }
        public Panel damagePreviewPanel { get { return owner.damagePreviewPanel; } }
        public DirectionArrowController directionArrowUI { get { return owner.directionArrowUI; } }
        public Panel directionPrompt { get { return owner.directionPrompt; } }
        public OptionsController optionsController { get { return owner.optionsController; } }
        public TurnOrderUI turnOrderUI { get { return owner.turnOrderUI; } }
        public EndScreenBanner endScreenBanner { get { return owner.endScreenBanner; } }
        public LootUIController lootUI { get { return owner.lootUI; } }
        public JobXPRewardController jobXPRewardController { get { return owner.jobXPRewardController; } }
        public ObjectiveUIController objectiveUI { get { return owner.objectiveUI; } }
        public UnitInfoController unitInfoController { get { return owner.unitInfoController; } }
        public DescriptionBox descriptionBox { get { return owner.descriptionBox; } }
        public KeybindPromptUI keybindPromptUI { get { return owner.keybindPromptUI; } }
        public Panel letterBox { get { return owner.letterBox; } }
        public Panel uiLabel { get { return owner.uiLabel; } }

        // Widgets
        public UnitHUD primaryHUD { get { return owner.primaryHUD; } }
        public UnitHUD secondaryHUD { get { return owner.secondaryHUD; } }
        public ManaBar manaBar { get { return owner.manaBar; } }
        public DamagePreview damagePreviewLeft { get { return owner.damagePreviewLeft; } }
        public DamagePreview damagePreviewRight { get { return owner.damagePreviewRight; } }
        #endregion

        #region LifeCycle
        protected virtual void Awake()
        {
            owner = GetComponent<BattleController>();
        }
        public override void Enter()
        {
            //Driver must be set before calling base.Enter()
            driver = (turn.actor != null) ? turn.actor.driver : null;
            base.Enter();

            if (driver == null || driver.Current == DriverType.Human)
                OnEnterUI();
        }

        public override void Exit()
        {
            base.Exit();

            if (driver == null || driver.Current == DriverType.Human)
                OnExitUI();
        }

        protected virtual void OnEnterUI()
        {

        }

        protected virtual void OnExitUI()
        {

        }
        #endregion

        #region Protected
        protected bool DidPlayerWin()
        {
            var victoryConditions = owner.GetComponents<BaseVictoryCondition>();
            foreach (var vc in victoryConditions)
                if (vc.isVictor && vc.alliance == AllianceType.Hero)
                    return true;

            return false;
        }

        protected bool IsBattleOver()
        {
            var victoryConditions = owner.GetComponents<BaseVictoryCondition>();
            foreach (var vc in victoryConditions)
                if (vc.isVictor)
                    return true;

            return false;
        }

        protected IEnumerator PlaySkill(Skill skill, Unit actor, List<BoardTile> targets, bool costTime = true)
        {
            // Perform skill
            skill.Perform(actor, targets, costTime);
            yield return null;

            while (skill.IsPlayingVFX())
                yield return null;

            skill.ExitVFX();

            yield return PlayAutoEffects(AfterSkillEffectsNotification);
        }

        protected IEnumerator PlayAutoEffects(string notification)
        {
            List<SkillDirections> effects = new List<SkillDirections>();
            turn.actor.PostNotification(notification, effects);
            yield return null;

            foreach (var e in effects)
            {
                List<BoardTile> targets = e.skill.GetTilesInArea(board, e.targetTile.xy, e.actor);

                // Play effects if valid target in area
                if (e.skill.AreAnyTargetsValid(e.actor, targets))
                {
                    yield return PlaySkill(e.skill, e.actor, targets, false);

                    if (turn.actor.IsKnockedOut())
                    {
                        break;
                    }
                }
            }
        }
        #endregion
    }
}