﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicDamageSkillEffect : DamageSkillEffect
{
    public override int GetAttack(Unit actor, Unit targetUnit)
    {
        return actor.stats[StatTypes.MAT];
    }

    public override int GetDefence(Unit actor, Unit targetUnit)
    {
        return targetUnit.stats[StatTypes.MDF];
    }
}