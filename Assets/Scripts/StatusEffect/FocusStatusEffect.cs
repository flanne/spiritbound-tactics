﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocusStatusEffect : StatusEffect
{
    protected override string nameID { get { return "KEYWORD_FOCUS"; } }
    public override bool isBuff { get { return true; } }
    public override string spriteFileName { get { return "icon_sts_focus"; } }
    public override Keyword keyword { get { return Resources.Load <Keyword>("Focus"); } }

    float multiplier = 1.3f;

    protected override void OnApply()
    {
        this.AddObserver(OnDealingDamage, DamageSkillEffect.TweakDamageNotification);
    }

    protected override void OnRemove()
    {
        this.RemoveObserver(OnDealingDamage, DamageSkillEffect.TweakDamageNotification);
    }

    void OnDealingDamage(object sender, object args)
    {
        var info = args as Info<List<ValueModifier>, Info<Unit, Unit, Skill>>;
        Unit actor = info.arg1.arg0;
        Skill skill = info.arg1.arg2;

        if (actor != owner || skill.ContainsTag(SkillTag.Status))
            return;

        var damageEffect = sender as DamageSkillEffect;
        if (damageEffect is MagicDamageSkillEffect)
            info.arg0.Add(new MultValueModifier(1, multiplier));
    }
}
