﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldMap : MonoBehaviour
{
    public WorldMapNode[] nodes { get; private set; }

    void Awake()
    {
        nodes = GetComponentsInChildren<WorldMapNode>();
    }

    public List<WorldMapNode> Search(WorldMapNode start)
    {
        List<WorldMapNode> nodesInSearch = new List<WorldMapNode>();
        nodesInSearch.Add(start);

        // Clear previous searches
        ClearSearch();
        Queue<WorldMapNode> checkNext = new Queue<WorldMapNode>();
        Queue<WorldMapNode> checkNow = new Queue<WorldMapNode>();

        start.distance = 0;
        checkNow.Enqueue(start);

        while (checkNow.Count > 0)
        {
            WorldMapNode current = checkNow.Dequeue();

            foreach (var next in current.adjacentNodes)
            {
                if (next.distance <= current.distance + 1)
                    continue;

                next.distance = current.distance + 1;
                next.prev = current;
                checkNext.Enqueue(next);
                nodesInSearch.Add(next);
            }

            if (checkNow.Count == 0)
                SwapReference(ref checkNow, ref checkNext);
        }

        return nodesInSearch;
    }

    private void ClearSearch()
    {
        foreach (WorldMapNode n in nodes)
        {
            n.prev = null;
            n.distance = int.MaxValue;
        }
    }

    private void SwapReference(ref Queue<WorldMapNode> a, ref Queue<WorldMapNode> b)
    {
        Queue<WorldMapNode> temp = a;
        a = b;
        b = temp;
    }
}
