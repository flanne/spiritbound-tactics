﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConeSkillRange : BaseSkillRange
{
	public override bool directionOriented { get { return true; } }

	public override List<BoardTile> GetTilesInRange(Board board, Unit unit, int horizontal, int vertical)
	{
		Vector2Int pos = unit.tile.xy;
		List<BoardTile> retValue = new List<BoardTile>();
		int dir = (unit.direction == Direction.Up || unit.direction == Direction.Right) ? 1 : -1;
		int lateral = 1;

		if (unit.direction == Direction.Up || unit.direction == Direction.Down)
		{
			for (int y = 1; y <= horizontal; ++y)
			{
				int min = -(lateral / 2);
				int max = (lateral / 2);
				for (int x = min; x <= max; ++x)
				{
					Vector2Int next = new Vector2Int(pos.x + x, pos.y + (y * dir));

					if (board.HasTile(next))
					{
						BoardTile tile = board.GetTile(next);
						if (Mathf.Abs(tile.z - unit.tile.z) <= vertical)
							retValue.Add(tile);
					}
				}
				lateral += 2;
			}
		}
		else
		{
			for (int x = 1; x <= horizontal; ++x)
			{
				int min = -(lateral / 2);
				int max = (lateral / 2);
				for (int y = min; y <= max; ++y)
				{
					Vector2Int next = new Vector2Int(pos.x + (x * dir), pos.y + y);

					if (board.HasTile(next))
					{
						BoardTile tile = board.GetTile(next);
						if (Mathf.Abs(tile.z - unit.tile.z) <= vertical)
							retValue.Add(tile);
					}
				}
				lateral += 2;
			}
		}

		return retValue;
	}
}
