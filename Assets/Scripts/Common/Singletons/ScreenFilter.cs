﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenFilter : MonoBehaviour
{
    [SerializeField]
    private Image filter;

    [SerializeField]
    private float defaultTransitionTime = 1f;

    public void FadeOut(System.Action callback = null)
    {
        FadeOut(defaultTransitionTime, callback);
    }

    public void FadeOut(float secondsToTransition, System.Action callback = null)
    {
        TransitionToColor(Color.black, secondsToTransition, callback);
    }

    public void FadeIn(System.Action callback = null)
    {
        FadeIn(defaultTransitionTime, callback);
    }

    public void FadeIn(float secondsToTransition, System.Action callback = null)
    {
        filter.color = Color.black;
        StartCoroutine(Transition(Color.clear, secondsToTransition, callback));
    }

    public void TransitionToColor(Color color, System.Action callback = null)
    {
        TransitionToColor(color, defaultTransitionTime, callback);
    }

    public void TransitionToColor(Color color, float secondsToTransition, System.Action callback = null)
    {
        Color c = color;
        c.a = 0;
        filter.color = c;

        StartCoroutine(Transition( color, secondsToTransition, callback));
    }

    public void TransitionToClear(System.Action callback = null)
    {
        TransitionToClear(defaultTransitionTime, callback);
    }

    public void TransitionToClear(float secondsToTransition, System.Action callback = null)
    {
        StartCoroutine(Transition(Color.clear, secondsToTransition, callback));
    }

    IEnumerator Transition(Color color, float secondsToTransition, System.Action callback)
    {
        int id = LeanTween.color(filter.rectTransform, color, secondsToTransition).id;

        while (LeanTween.isTweening(id))
            yield return null;

        if (callback != null)
            callback();
    }

    public void Flash()
    {
        StartCoroutine("FlashCoroutine");
    }

    IEnumerator FlashCoroutine()
    {
        LeanTween.alpha(filter.rectTransform, 0.4f, 0f);
        yield return new WaitForSeconds(0.05f);

        LeanTween.alpha(filter.rectTransform, 0f, 0.1f);
    }
}
