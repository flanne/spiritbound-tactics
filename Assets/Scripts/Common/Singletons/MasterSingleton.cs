﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using flanne.UI;

public class MasterSingleton : MonoBehaviour
{
    public static MasterSingleton main { get; private set; }

    // Child singletons
    public CameraRig MainCameraRig { get { return mainCameraRig; } }
    [SerializeField]
    private CameraRig mainCameraRig;

    public GameStrings GameStrings { get { return gameStrings; } }
    [SerializeField]
    private GameStrings gameStrings;

    public Party Party { get { return party; } }
    [SerializeField]
    private Party party;

    public Inventory Inventory { get { return inventory; } }
    [SerializeField]
    private Inventory inventory;

    public MissionLog MissionLog { get { return missionLog; } }
    [SerializeField]
    private MissionLog missionLog;

    public GameLevel GameLevel { get { return gameLevel; } }
    [SerializeField]
    private GameLevel gameLevel;

    public WorldMapData WorldMapData { get { return worldMapData; } }
    [SerializeField]
    private WorldMapData worldMapData;

    public UnitFactory UnitFactory { get { return unitFactory; } }
    [SerializeField]
    private UnitFactory unitFactory;

    public PlayableJobList PlayableJobList { get { return playableJobList; } }
    [SerializeField]
    private PlayableJobList playableJobList;

    public ScreenFilter ScreenFilter { get { return screenFilter; } }
    [SerializeField]
    private ScreenFilter screenFilter;

    public NameChangeUI NameChangeUI { get { return nameChangeUI; } }
    [SerializeField]
    private NameChangeUI nameChangeUI;

    public Tooltip Tooltip { get { return tooltip; } }
    [SerializeField]
    private Tooltip tooltip;

    public TutorialFlags TutorialShown { get { return tutorialShown; } }
    [SerializeField]
    private TutorialFlags tutorialShown;

    public SaveSystem SaveSystem { get { return saveSystem; } }
    [SerializeField]
    private SaveSystem saveSystem;

    public GameSettings GameSettings { get { return gameSettings; } }
    [SerializeField]
    private GameSettings gameSettings;

    public AudioPlayer AudioPlayer { get { return audioPlayer; } }
    [SerializeField]
    private AudioPlayer audioPlayer;

    public SFXLibrary SFXLibrary { get { return sfxLibrary; } }
    [SerializeField]
    private SFXLibrary sfxLibrary;

    public SceneTransition SceneTransition { get { return sceneTransition; } }
    [SerializeField]
    private SceneTransition sceneTransition;

    void Awake()
    {
        if (main != null)
        {
            Destroy(this.gameObject);
            return;
        }

        main = this;
        this.transform.parent = null;
        DontDestroyOnLoad(this.gameObject);
    }
}
