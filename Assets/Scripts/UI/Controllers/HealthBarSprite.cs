﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarSprite : MonoBehaviour
{
    [SerializeField]
    float changeHPAnimDuration;

    [SerializeField]
    LeanTweenType easeType;

    [SerializeField]
    SpriteRenderer healthFill;

    [SerializeField]
    SpriteRenderer shieldFill;

    public bool isAnimating 
    { 
        get
        {
            return LeanTween.isTweening(hpTweenId) || LeanTween.isTweening(shpTweenId);
        }
    }

    int hpTweenId;
    int shpTweenId;

    public void AnimateHPChange(int from, int to, int maxHP)
    {
        healthFill.transform.localScale = new Vector3((float)from / (float)maxHP, 1, 1);

        hpTweenId = LeanTween.scale(healthFill.gameObject, new Vector3((float)to / (float)maxHP, 1, 1), changeHPAnimDuration).setEase(easeType).id;
    }

    public void AnimateSHPChange(int from, int to, int maxHP)
    {
        shieldFill.transform.localScale = new Vector3((float)from / (float)maxHP, 1, 1);

        shpTweenId = LeanTween.scale(shieldFill.gameObject, new Vector3((float)to / (float)maxHP, 1, 1), changeHPAnimDuration).setEase(easeType).id;
    }
}
