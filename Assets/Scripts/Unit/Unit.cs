﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Unit : MonoBehaviour
{
    #region Properties
    [System.NonSerialized]
    public string unitName;

    public Animator unitAnimator;
    public SpriteRenderer unitSpriteRenderer;
    public SpriteRenderer shadow;
    public SpriteMaterialSwapper spriteMaterialSwapper;
    public SpriteSheetSetter unitSpriteSheet;
    public SpriteSheetSetter weaponSpriteSheet;
    public Alliance alliance;
    public Driver driver;
    public Level level;
    public Stats stats;
    public Health health;
    public UnitJobs jobs;
    public UnitJobRanks jobRanks;
    public Equipment equipment;
    public SkillCatalog skillCatalog;
    public ConsumableCatalog consumableSkills;
    public Status status;
    public StatusIndicator statusIndicator;
    public UnitPassives passives;
    public Reaction reaction;
    public MagicCircleEffect magicCircle;
    public GameObject jumper;
    public Movement movement;
    public EmotePlayer emoter;
    public HitIndicatorSpawner hitIndicator;
    public HitReaction hitReaction;

    public UniqueSprite uniqueSprite;
    public UnitID uniqueID;
    public Skill defaultBasicAttack;

    public Skill basicAttack
    {
        get
        {
            if (equipment.GetItem(EquipSlots.Primary) is Weapon)
                return (equipment.GetItem(EquipSlots.Primary) as Weapon).basicAttack;
            else if (equipment.GetItem(EquipSlots.Secondary) is Weapon)
                return (equipment.GetItem(EquipSlots.Secondary) as Weapon).basicAttack;
            else
                return defaultBasicAttack;
        }
    }

    [NonSerialized]
    public UnitModel modelRef;

    public BoardTile tile { get; protected set; }
    private Direction dir;

    public Direction direction
    {
        get
        {
            return dir;
        }
        set
        {
            if (this.dir == value)
                return;

            this.dir = value;

            if (this.dir == Direction.Up || this.dir == Direction.Left)
                unitSpriteRenderer.flipX = false;
            else
                unitSpriteRenderer.flipX = true;

            // Tell the unitAnimator the current dirction
            unitAnimator.SetInteger("Direction", (int)dir);
        }
    }

    [NonSerialized]
    public BoardTile initialTile; // Used for cutscene and skipping pre-battle cutscene
    [NonSerialized]
    public Direction initialDirection; // Used for cutscene and skipping pre-battle cutscene


    [NonSerialized] public bool nonCombat; // This unit does not get a turn
    [NonSerialized] public bool cutsceneOnly; // This unit does not appear on the field and is only used for cutcenes
    #endregion

    #region MonoBehaviour
    void Start()
    {
        // Start animation on a random frame
        AnimatorStateInfo state = unitAnimator.GetCurrentAnimatorStateInfo(0);
        unitAnimator.Play(state.fullPathHash, -1, UnityEngine.Random.Range(0f, 5f));
    }
    #endregion

    #region Public
    public void Place(BoardTile target)
    {
        // Make sure old tile location is not still pointing to this unit
        if (tile != null && tile.content == gameObject)
            tile.content = null;

        // Link unit and tile references
        tile = target;

        if (target != null)
            target.content = gameObject;
    }

    public void MatchToTile()
    {
        transform.localPosition = tile.GetWorldPosition();
    }

    public void RemoveFromTile()
    {
        if (tile != null)
        {
            tile.content = null;
            tile = null;
        }
    }

    public bool IsKnockedOut()
    {
        return status.GetComponentInChildren<KnockedOutStatusEffect>() != null;
    }

    public bool AnimatorIsPlaying()
    {
        return unitAnimator.GetCurrentAnimatorStateInfo(0).length > unitAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }

    public void SetAnimationState(UnitAnimState state)
    {
        if (state == UnitAnimState.Special1
            || state == UnitAnimState.Special2
            || state == UnitAnimState.Special3
            || state == UnitAnimState.Special4)
        {
            unitAnimator.SetInteger("AnimationState", (int)state);
        }
        else
        {
            StartCoroutine(ResetAnimation(state));
        }
    }

    IEnumerator ResetAnimation(UnitAnimState state)
    {
        unitAnimator.SetInteger("AnimationState", (int)UnitAnimState.Idle);
        yield return null;
        unitAnimator.SetInteger("AnimationState", (int)state);
    }

    public UnitAnimState GetAnimationState()
    {
        return (UnitAnimState)unitAnimator.GetInteger("AnimationState");
    }

    public void UnHide()
    {
        LeanTween.alpha(this.gameObject, 1f, 0f);
        statusIndicator.Show();

        if (hitIndicator != null)
            hitIndicator.gameObject.SetActive(true);

        if (hitReaction != null)
            hitReaction.gameObject.SetActive(true);
    }

    public void Hide()
    {
        LeanTween.alpha(this.gameObject, 0f, 0f);

        if (hitIndicator != null)
            hitIndicator.gameObject.SetActive(false);

        if (hitReaction != null)
            hitReaction.gameObject.SetActive(false);
    }

    public void Reset()
    {
        health.Reset();
        status.ClearAll();
        consumableSkills.Reset();
    }
    #endregion
}
