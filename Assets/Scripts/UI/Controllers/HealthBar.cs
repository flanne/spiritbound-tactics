﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthBar : MonoBehaviour
{
    [SerializeField]
    Slider hpSlider;

    [SerializeField]
    Slider shieldSlider;

    [SerializeField]
    TMP_Text textDisplay;

    int maxHealth;

    public void SetMaxHealth(int health)
    {
        hpSlider.maxValue = health;
        shieldSlider.maxValue = health;
        this.maxHealth = health;

        hpSlider.value = health;

        if (textDisplay)
            textDisplay.text = health + "/" + maxHealth;
    }

    public void SetHealth(int health, int shield)
    {
        hpSlider.value = health;
        shieldSlider.value = shield;

        if (textDisplay)
            textDisplay.text = (health + shield) + "/" + maxHealth;
    }
}
