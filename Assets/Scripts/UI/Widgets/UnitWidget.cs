﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace flanne.UI
{
    public class UnitProperties : IUIProperties
    {
        public Unit unit;

        public UnitProperties(Unit u)
        {
            unit = u;
        }
    }

    public abstract class UnitWidget : Widget<UnitProperties>
    {
        public override void SetProperties(UnitProperties properties)
        {

        }
    }
}
