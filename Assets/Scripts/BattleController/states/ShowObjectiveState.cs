﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class ShowObjectiveState : BattleState
    {
        #region Eventhandler
        void OnAnyKey(InputAction.CallbackContext obj)
        {
            StartCoroutine(ExitState());

            //Remove listeners
            playerInput.currentActionMap.FindAction("AnyButton").performed -= OnAnyKey;


            audioPlayer.Play(owner.confirmSFX);
        }
        #endregion

        #region LifeCycle
        public override void Enter()
        {
            // Play the battle music referenced in mission data
            if (MasterSingleton.main.AudioPlayer.currentMusic != MasterSingleton.main.MissionLog.current.battleMusic)
                MasterSingleton.main.AudioPlayer.PlayMusic(MasterSingleton.main.MissionLog.current.battleMusic);

            // Set victory condition text
            MissionData mission = MasterSingleton.main.MissionLog.current;
            string heroVictoryDecription = mission.heroVictoryCondition.GetVictoryConditionDescription(AllianceType.Hero);
            string villainVictoryDecription = mission.villainVictoryCondition.GetVictoryConditionDescription(AllianceType.Villain);

            objectiveUI.SetProperties(new ObjectiveProperties(heroVictoryDecription, villainVictoryDecription));
            objectiveUI.Show();

            // Place all units
            List<Unit> cutsceneOnlyUnits = new List<Unit>();
            foreach (var unit in units)
            {
                if (unit.cutsceneOnly)
                {
                    unit.Hide();
                    unit.RemoveFromTile();
                    cutsceneOnlyUnits.Add(unit);
                }
                else
                {
                    unit.UnHide();
                    unit.Place(unit.initialTile);
                    unit.MatchToTile();
                    unit.direction = unit.initialDirection;
                    unit.SetAnimationState(UnitAnimState.Idle);
                }
            }

            foreach (var u in cutsceneOnlyUnits)
                units.Remove(u);


            StartCoroutine(StartLockOut());
        }
        #endregion

        #region Private
        IEnumerator StartLockOut()
        {
            yield return new WaitForSeconds(1);

            // Add listeners
            playerInput.currentActionMap.FindAction("AnyButton").performed += OnAnyKey;
        }

        IEnumerator ExitState()
        {
            objectiveUI.Hide();
            yield return new WaitForSeconds(0.2f);

            // go to unit placement state if allowed, otherwise go straight to select unit state
            if (MasterSingleton.main.MissionLog.current.noUnitPlacement)
            {
                owner.ChangeState<SelectUnitState>();
            }
            else
            {
                // check for unit placement tutorial
                if (MasterSingleton.main.TutorialShown.partyPlacement == false)
                {
                    MasterSingleton.main.TutorialShown.partyPlacement = true;
                    owner.ChangeState<PartyPlacementTutorialState>();
                }
                else
                {
                    owner.ChangeState<UnitPlacementState>();
                }
            }
        }
        #endregion
    }
}
