﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FullSkillArea : BaseSkillArea
{
	public override bool fireLocationOriented { get { return false; } }

	public override List<BoardTile> GetTilesInArea(Board board, Vector2Int pos, Unit unit, SkillRangeData skillRange, int horizontal, int vertical, Skill skill)
	{
		return skillRange.GetTilesInRange(board, unit, skill);
	}
}