﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySkillCutsceneAction : UnitCutsceneAction
{
    public Skill skill;
    public Vector2Int target;

    public bool waitForSkill = false;
    public bool isHit = true;

    public override void Play()
    {
        StartCoroutine(PlaySkill());
    }

    IEnumerator PlaySkill()
    {
        if (waitForSkill)
            isPlaying = true;

        // Get units in skill aoe
        Board board = GameObject.FindWithTag("Board").GetComponent<Board>();
        List<BoardTile> targets = skill.GetTilesInArea(board, target, unit);

        skill.PlayFX(unit, targets, isHit, !isHit);
        yield return null;

        while (skill.IsPlayingVFX())
            yield return null;

        skill.ExitVFX();

        isPlaying = false;
    }
}
