﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace flanne.UI
{
	public class AbilityTreeController : MonoBehaviour
	{
		#region Fields
		public event EventHandler<InfoEventArgs<DescribableObject>> SelectEvent;
		public event EventHandler<InfoEventArgs<Info<int, AbilityTreeSelection>>> ClickEvent;

		[SerializeField] JobAbility noneAbility;
		[SerializeField] TMP_Text rankText;
		[SerializeField] AbilityMenu startingAbilities;
		[SerializeField] AbilityMenu treeAbilities;

		Job _job;
		#endregion

		#region Eventhandlers
		void OnStartingAbilitySelect(object sender, InfoEventArgs<int> e)
		{
			if (SelectEvent != null)
				SelectEvent(this, new InfoEventArgs<DescribableObject>(GetStartingAbility(e.info)));
		}

		void OnTreeAbilitySelect(object sender, InfoEventArgs<int> e)
		{
			if (SelectEvent != null)
				SelectEvent(this, new InfoEventArgs<DescribableObject>(GetTreeAbility(e.info)));
		}

		void OnStartingAbilityClick(object sender, InfoEventArgs<int> e)
		{
			if (ClickEvent != null)
            {
				Info<int, AbilityTreeSelection> info;
				if (e.info == 0)
					info = new Info<int, AbilityTreeSelection>(0, AbilityTreeSelection.None);
				else
					info = new Info<int, AbilityTreeSelection>(1, AbilityTreeSelection.None);

				ClickEvent(this, new InfoEventArgs<Info<int, AbilityTreeSelection>>(info));
			}
		}

		void OnTreeAbilityClick(object sender, InfoEventArgs<int> e)
		{
			if (ClickEvent != null)
			{
				int rank = 2 + (e.info / 2);
				AbilityTreeSelection treeSelection;
				if (e.info % 2 == 0)
					treeSelection = AbilityTreeSelection.Left;
				else
					treeSelection = AbilityTreeSelection.Right;

				Info<int, AbilityTreeSelection> info = new Info<int, AbilityTreeSelection>(rank, treeSelection);
				ClickEvent(this, new InfoEventArgs<Info<int, AbilityTreeSelection>>(info));
			}
		}
        #endregion

        #region LifeCycle
		void Awake()
        {
			// Add Listeners
			startingAbilities.SelectEvent += OnStartingAbilitySelect;
			treeAbilities.SelectEvent += OnTreeAbilitySelect;
			startingAbilities.ClickEvent += OnStartingAbilityClick;
			treeAbilities.ClickEvent += OnTreeAbilityClick;
		}

		void OnDestroy()
		{
			// Remove Listeners
			startingAbilities.SelectEvent -= OnStartingAbilitySelect;
			treeAbilities.SelectEvent -= OnTreeAbilitySelect;
			startingAbilities.ClickEvent -= OnStartingAbilityClick;
			treeAbilities.ClickEvent -= OnTreeAbilityClick;
		}
        #endregion

        #region Public
        public void SetToJob(Job job)
		{
			_job = job;

			if (job == null)
			{
				startingAbilities.SetProperties<DescribableProperties>(0, new DescribableProperties(noneAbility));
				startingAbilities.SetProperties<DescribableProperties>(1, new DescribableProperties(noneAbility));

				for (int i = 2; i < 5; i++)
				{
					treeAbilities.SetProperties<DescribableProperties>((i - 2) * 2, new DescribableProperties(noneAbility));
					treeAbilities.SetProperties<DescribableProperties>(((i - 2) * 2) + 1, new DescribableProperties(noneAbility));
				}
			}
			else
			{
				startingAbilities.SetProperties<DescribableProperties>(0, new DescribableProperties(job.innate));
				startingAbilities.SetProperties<DescribableProperties>(1, new DescribableProperties(job.Rank1));

				for (int i = 2; i < 5; i++)
				{
					treeAbilities.SetProperties<DescribableProperties>((i - 2) * 2, new DescribableProperties(job.abilityPairs[i - 2].left));
					treeAbilities.SetProperties<DescribableProperties>(((i - 2) * 2) + 1, new DescribableProperties(job.abilityPairs[i - 2].right));
				}
			}
		}

		public void SetToJobRank(JobRank jobRank, bool isPrimary = false)
		{
			// Set rank text
			rankText.text = "Rank " + jobRank.rank;

			// Reset tree
			startingAbilities.ResetAll();
			treeAbilities.ResetAll();

			// Set activate abilities
			if (isPrimary)
				startingAbilities.Activate(0);

			startingAbilities.Activate(1);

			for (int i = 2; i <= 4; i++)
			{
				if (jobRank.abilityTree.GetSelection(i) == AbilityTreeSelection.Left) // activate left ability
					treeAbilities.Activate((i - 2) * 2);
				else if (jobRank.abilityTree.GetSelection(i) == AbilityTreeSelection.Right) // activate right ability
					treeAbilities.Activate(((i - 2) * 2) + 1);
				else // no abilities active
                {
					// set glare if abilities are pickable
					if (jobRank.rank >= i && (i-1 == 1 || jobRank.abilityTree.GetSelection(i-1) != AbilityTreeSelection.None))
                    {
						treeAbilities.SetGlare((i - 2) * 2, true);
						treeAbilities.SetGlare(((i - 2) * 2) + 1, true);
					}
					else
                    {
						treeAbilities.Lock((i - 2) * 2);
						treeAbilities.Lock(((i - 2) * 2) + 1);
					}
                }
			}
		}

		public void Show()
        {
			startingAbilities.Show();
			treeAbilities.Show();
		}

		public void Hide()
        {
			startingAbilities.Hide();
			treeAbilities.Hide();
		}
        #endregion

        #region Private
		JobAbility GetStartingAbility(int index)
        {
			if (_job == null)
				return noneAbility;

			JobAbility ability;
			if (index == 0)
				ability = _job.innate;
			else
				ability = _job.Rank1;

			return ability;
		}

		JobAbility GetTreeAbility(int index)
        {
			if (_job == null)
				return noneAbility;

			JobAbility ability;

			int rank = 2 + (index / 2);
			if (index % 2 == 0)
				ability = _job.abilityPairs[rank - 2].left;
			else
				ability = _job.abilityPairs[rank - 2].right;

			return ability;
		}
        #endregion
    }
}
