﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseBarrierSkillEffect : BaseSkillEffect
{
	#region Notifications
	public const string TweakShieldNotification = "ShieldSkillEffect.TweakShieldNotification";
	#endregion

	#region Public
	public override int Predict(Unit actor, BoardTile target, int power, Skill skill)
	{
		Unit targetUnit = target.content.GetComponent<Unit>();
		return targetUnit.health.SHP;
	}

	protected override int OnApply(Unit actor, BoardTile target, int power, Skill skill)
	{
		Unit defender = target.content.GetComponent<Unit>();

		int value = defender.health.SHP;
		defender.health.SHP = 0;

		return value;
	}
	#endregion
}