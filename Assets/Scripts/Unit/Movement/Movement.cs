﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Movement : MonoBehaviour
{
    #region Properties
    [SerializeField]
    protected Unit unit;

    protected const float DefaultMoveSpeed = 4f;

    public int range { get { return unit.stats[StatTypes.MOV]; } }
    public int jumpHeight { get { return unit.stats[StatTypes.JMP]; } }

    #endregion

    #region Public
    public virtual List<BoardTile> GetTilesInRange(Board board)
    {
        List<BoardTile> tilesInRange = board.Search(unit.tile, IsReachableTile);
        FilterBlockedTiles(tilesInRange);
        return tilesInRange;
    }

    public abstract IEnumerator Traverse(BoardTile tile, float moveSpeed = DefaultMoveSpeed, bool needSearch = false);

    public virtual bool TraversalConditionMet(BoardTile from, BoardTile to, bool ignoreUnits = false)
    {
        return true;
    }
    #endregion

    #region Protectred
    protected virtual bool IsReachableTile(BoardTile from, BoardTile to)
    {
        if (!TraversalConditionMet(from, to))
            return false;

        return from.distance < range;
    }

    protected virtual void FilterBlockedTiles(List<BoardTile> tiles)
    {
        for (int i = tiles.Count - 1; i >= 0; --i)
            if (tiles[i].content != null)
                tiles.RemoveAt(i);
    }
    #endregion
}
