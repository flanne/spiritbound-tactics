﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum IDEnum
{
    None,
    Actor1,
    Actor2,
    Actor3,
    Actor4,
    Actor5,
    Actor6,
    Actor7,
    Actor8,
    MC,
    Runa,
    Bo,
    Ishta,
    Benny,
    Owen
}
