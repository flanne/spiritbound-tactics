﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;

public class MapActionState : WorldMapState
{
    MissionLog missionLog { get { return MasterSingleton.main.MissionLog; } }

    #region Eventhandlers
    void OnClick(object sender, InfoEventArgs<int> e)
    {
        switch (e.info)
        {
            case 0:
                MissionData currentMission = null;
                foreach (var mission in missionLog.activeMissions)
                {
                    if (avatar.node.level == mission.level)
                    {
                        currentMission = mission;
                    }
                }
                missionLog.StartMission(currentMission);

                owner.ChangeState<ToBattleAnimState>();
                break;
            case 1:
                MissionData[] patrolBattles = avatar.node.level.patrolBattles;

                missionLog.StartMission(patrolBattles[Random.Range(0, patrolBattles.Length)]);
                owner.ChangeState<ToBattleAnimState>();
                break;
            case 2:
                MasterSingleton.main.SceneTransition.LoadScene("Shop", 1f);
                break;
            case 3:
                MasterSingleton.main.SceneTransition.LoadScene("MissionHub", 1f);
                break;
            case 4:
                owner.ChangeState<TraverseWorldMapState>();
                return;
        }

        owner.ChangeState<MapTransitionSceneState>();

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnSelect(object sender, InfoEventArgs<int> e)
    {
        audioPlayer.Play(owner.selectSFX);
    }

    void OnCancel(InputAction.CallbackContext obj)
    {
        owner.ChangeState<TraverseWorldMapState>();
        
        audioPlayer.Play(owner.cancelSFX);
    }
    #endregion

    #region LifeCycle
    public override void Enter()
    {
        SetMapActionMenu();
        mapActionMenu.SelectFirstAvailable();
        mapActionMenu.Show();

        // Check for tutorials
        if (avatar.node.level.patrolBattles.Length > 0 && MasterSingleton.main.TutorialShown.patrol == false)
            StartCoroutine(StartPatrolTutorial());

        mapActionMenu.ClickEvent += OnClick;
        mapActionMenu.SelectEvent += OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;

        // Show keybinds
        keybindPromptUI.Show();
        keybindPromptUI.SetText(0, "Select");
        keybindPromptUI.SetText(1, "Cancel");
        keybindPromptUI.HidePrompt(2);
    }

    public override void Exit()
    {
        mapActionMenu.Hide();

        mapActionMenu.ClickEvent -= OnClick;
        mapActionMenu.SelectEvent -= OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;
    }
    #endregion

    #region Private
    void SetMapActionMenu()
    {
        mapActionMenu.SetAllActive(false);

        mapActionMenu.SetActive(0, CheckForActiveMission());
        mapActionMenu.SetActive(1, avatar.node.level.patrolBattles.Length > 0);
        mapActionMenu.SetActive(2, avatar.node.level.shopItems.Length > 0);
        mapActionMenu.SetActive(3, avatar.node.level.hasPub);
        mapActionMenu.SetActive(4, true);
    }

    IEnumerator StartPatrolTutorial()
    {
        yield return null;

        owner.ChangeState<PatrolTutorialState>();
        MasterSingleton.main.TutorialShown.patrol = true;

        EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);

        mapActionMenu.interactable = false;
    }
    #endregion
}
