﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashingSprite : MonoBehaviour
{
    public Color emissionColor
    {
        get { return GetComponent<Renderer>().material.GetColor("_EmissionColor"); }
        set { GetComponent<Renderer>().material.SetColor("_EmissionColor", value); }
    }

    public void Start()
    {
        LeanTween.value(this.gameObject, updateEmissionColor, new Color32(25, 25, 25, 255), Color.clear, 1f).setLoopPingPong().setEase(LeanTweenType.easeInOutCubic);
    }

    void updateEmissionColor(Color color)
    {
        emissionColor = color;
    }
}
