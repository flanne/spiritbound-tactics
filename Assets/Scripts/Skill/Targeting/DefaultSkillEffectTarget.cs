﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultSkillEffectTarget : SkillEffectTarget
{
    public override bool IsTarget(Unit actor, BoardTile target)
    {
        Stats s = target?.content?.GetComponent<Unit>()?.stats;
        Alliance a = target?.content?.GetComponent<Unit>()?.alliance;
        return s != null && s[StatTypes.HP] > 0 && a.type != AllianceType.None;
    }
}