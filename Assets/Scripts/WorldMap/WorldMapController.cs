﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Sirenix.OdinInspector;
using flanne.UI;

public class WorldMapController : StateMachine
{
    #region Serialized Fields
    public PlayerInput playerInput;

    public AudioClip worldMapMusic;
    public AudioClip toBattleSound;
    public AudioClip confirmSFX;
    public AudioClip selectSFX;
    public AudioClip cancelSFX;

    public MissionData pubTutorialAfterMission;
    public string pubTutorialDialogueStringID;

    [BoxGroup("UI")]
    [FoldoutGroup("UI/Controllers")] public EquipmentCompareUI equipCompareUI;
    [FoldoutGroup("UI/Controllers")] public JobChangeController jobChangeController;
    [FoldoutGroup("UI/Controllers")] public ControllerCursor controllerCursor;
    [FoldoutGroup("UI/Controllers")] public DialogueUI dialogueBox;

    [FoldoutGroup("UI/Menus")] public UnitListMenu unitListMenu;
    [FoldoutGroup("UI/Menus")] public flanne.UI.Menu unitOptionsMenu;
    [FoldoutGroup("UI/Menus")] public EquipmentMenuController equipmentMenu;
    [FoldoutGroup("UI/Menus")] public ItemStackMenu equipChangeMenu;
    [FoldoutGroup("UI/Menus")] public JobOverviewController classMenu;
    [FoldoutGroup("UI/Menus")] public Menu exitMenu;
    [FoldoutGroup("UI/Menus")] public Menu mapActionMenu;

    [FoldoutGroup("UI/Widgets")] public DescriptionBox descriptionBox;
    [FoldoutGroup("UI/Widgets")] public Panel jobChangeEquipPrompt;
    [FoldoutGroup("UI/Widgets")] public Panel worldMapTutorial;
    [FoldoutGroup("UI/Widgets")] public Panel jobTutorial;
    [FoldoutGroup("UI/Widgets")] public Panel abilityTutorial;
    [FoldoutGroup("UI/Widgets")] public Panel patrolTutorial;

    public WorldMapAvatar avatar;

    public WorldMap map;

    public Animator toBattleAnimator;

    public Panel toBattleTextGraphic;

    public MissionData[] missionsToEndDemo;
    public Panel wishListPrompt;

    public OptionsController optionsMenu;

    public KeybindPromptUI keybindPromptUI;

    public float camSpeed { get { return _camSpeed; } }
    [SerializeField] float _camSpeed;
    #endregion

    CameraRig cameraRig { get { return MasterSingleton.main.MainCameraRig; } }

    [System.NonSerialized] public Unit menuUnit;
    [System.NonSerialized] public EquipSlots menuEquipSlot;
    [System.NonSerialized] public List<ItemStack> filteredInventory;
    [System.NonSerialized] public bool isPrimarySlot;

    #region Monobehaviour
    void Start()
    {
        // For debug
        if (MasterSingleton.main.Party.myParty.Count == 0)
            MasterSingleton.main.Party.InitDevParty();

        // Confine cursor to window
        Cursor.lockState = CursorLockMode.Confined;

        // Reset Units
        MasterSingleton.main.Party.DeactivateAll();

        // Play Music
        MasterSingleton.main.AudioPlayer.PlayMusic(worldMapMusic);

        // Set options menu sliders
        optionsMenu.SetToPrefs();

        // Set cam pos
        cameraRig.follow = null;
        Vector2 camTo = new Vector2(0, avatar.transform.position.y);
        float yMin = -1 * 162 / 96;
        float yMax = 162 / 96;
        camTo.y = Mathf.Clamp(camTo.y, yMin, yMax);
        cameraRig.transform.position = camTo;

        List<MissionData> activeMissions = MasterSingleton.main.MissionLog.activeMissions;
        List<WorldMapLocation> unlockedLocations = MasterSingleton.main.WorldMapData.unlockedLocations;

        if (unlockedLocations.Count == 0)
        {
            MasterSingleton.main.WorldMapData.GetDefault();
            unlockedLocations = MasterSingleton.main.WorldMapData.unlockedLocations;
        }

        foreach (var node in map.nodes)
        {
            // Set mission indicator if node has an active mission
            foreach (var mission in activeMissions)
            {
                if (mission.level == node.level)
                    node.hasMission = true;
            }

            // Activate unlocked nodes
            if (unlockedLocations.Contains(node.level))
            {
                node.gameObject.SetActive(true);
            }
            else
            {
                if (node.hasMission)
                {
                    node.gameObject.SetActive(true);
                    MasterSingleton.main.WorldMapData.Unlock(node.level);
                }
                else
                    node.gameObject.SetActive(false);
            }
        }

        // Set worldmap avater to the correct node
        WorldMapLocation currentLocation = MasterSingleton.main.WorldMapData.currentLocation;
        foreach (var node in map.nodes)
        {
            if (node.level == currentLocation)
            {
                Vector3 pos = node.transform.position;
                pos.y += 0.15f;
                avatar.transform.position = pos;
                avatar.node = node;
                break;
            }
        }

        // AutoSave
        MasterSingleton.main.SaveSystem.Save();

        StartCoroutine(StartStateAFterDelay());
    }

    
    IEnumerator StartStateAFterDelay()
    {
        yield return new WaitForSeconds(0.5f);

        // Initial State and check for tutorials
        if (MasterSingleton.main.TutorialShown.wishlist == false)
        {
            // Check if all demo missions are done
            bool demoDone = true;
            foreach (var mission in missionsToEndDemo)
            {
                if (!MasterSingleton.main.MissionLog.completedMissions.Contains(mission))
                    demoDone = false;
            }

            if (demoDone)
            {
                ChangeState<EndOfDemoState>();
                MasterSingleton.main.TutorialShown.wishlist = true;
            }
        }

        if (MasterSingleton.main.TutorialShown.worldMap == false)
        {
            ChangeState<WorldMapTutorialState>();
            MasterSingleton.main.TutorialShown.worldMap = true;
        }
        else if (MasterSingleton.main.TutorialShown.pub == false && MasterSingleton.main.MissionLog.completedMissions.Contains(pubTutorialAfterMission))
        {
            ChangeState<PubTutorialState>();
            MasterSingleton.main.TutorialShown.pub = true;
        }
        else
            ChangeState<TraverseWorldMapState>();
    }

    void Update()
    {
        // Camera follow avatar
        float step = camSpeed * Time.deltaTime;
        Vector2 camTo = new Vector2(0, avatar.transform.position.y);
        float yMin = -1 * 162 / 96;
        float yMax = 162 / 96;
        camTo.y = Mathf.Clamp(camTo.y, yMin, yMax);
        cameraRig.transform.position = Vector2.Lerp(cameraRig.transform.position, camTo, step);
    }
    #endregion
}
