﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealSkillEffect : BaseSkillEffect
{
	#region Public
	public override int Predict(Unit actor, BoardTile target, int power, Skill skill)
	{
		int heal = CalculateHeal(actor, target, power, skill);

		return heal;
	}

	public override int PredictClamped(Unit actor, BoardTile target, int power, Skill skill)
    {
		Unit targetUnit = target.content.GetComponent<Unit>();

		// Heal at most amount of missing hp
		int heal = Predict(actor, target, power, skill);
		int healClamped = Mathf.Min(heal, targetUnit.health.MHP - targetUnit.health.HP);

		return healClamped;
	}

	protected override int OnApply(Unit actor, BoardTile target, int power, Skill skill)
	{
		Unit defender = target.content.GetComponent<Unit>();

		// Start with the predicted damage value
		int value = CalculateHeal(actor, target, power, skill);

		// Add some random variance
		value = Mathf.FloorToInt(value * UnityEngine.Random.Range(0.9f, 1.1f));

		// Clamp the damage to a range
		value = Mathf.Clamp(value, minDamage, maxDamage);

		// Notification about the healing
		var info = new Info<Unit, Unit, int>(actor, defender, value);
		this.PostNotification(WillHealNotification, info);

		// Apply the damage to the target
		defender.stats[StatTypes.HP] += value;

		return value;
	}

	int CalculateHeal(Unit actor, BoardTile target, int power, Skill skill)
    {
		Unit targetUnit = target.content.GetComponent<Unit>();

		int actorMAT = actor.stats[StatTypes.MAT];

		// Calculate base healing
		int heal = actorMAT / 2;
		heal = Mathf.Max(heal, 1);

		// Apply power bonus
		heal = power * heal / 100;
		heal = Mathf.Max(heal, 1);

		// Tweak the damage based on a variety of other checks like
		// Elemental damage, Critical Hits, Damage multipliers, etc.
		var info = new Info<Unit, Unit, Skill>(actor, targetUnit, skill);
		heal = heal.NotifyModifiers<Info<Unit, Unit, Skill>>(TweakHealNotification, this, info);

		// Clamp the damage to a range
		heal = Mathf.Clamp(heal, minDamage, maxDamage);

		return heal;
	}
	#endregion
}
