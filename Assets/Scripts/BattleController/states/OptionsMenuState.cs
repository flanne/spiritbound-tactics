﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spiritlink.Battle
{
    public class OptionsMenuState : BattleState
    {
        public override void Enter()
        {
            optionsController.Show();
            optionsController.CancelEvent += OnCancel;
        }

        public override void Exit()
        {
            optionsController.Hide();
            optionsController.CancelEvent -= OnCancel;

            audioPlayer.Play(owner.confirmSFX);
        }

        void OnCancel(object sender, System.EventArgs e)
        {
            owner.ChangeState<ExploreState>();
        }
    }
}
