﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public class SkillAreaData
{
    enum SkillAreaType
    {
        Specific,
        Full
    };

    [LabelWidth(80)]
    [SerializeField]
    private SkillAreaType areaType;

    [HorizontalGroup("Range"), LabelWidth(70)]
    [HideIf("areaType", SkillAreaType.Full)]
    [SerializeField]
    private int horizontal = 0;

    [HorizontalGroup("Range"), LabelWidth(70)]
    [HideIf("areaType", SkillAreaType.Full)]
    [SerializeField]
    private int vertical = 0;

    public bool fireLocationOriented { get { return AreaGetters[areaType].fireLocationOriented; } }

    public List<BoardTile> GetTilesInArea(Board board, Vector2Int pos, Unit unit, SkillRangeData skillRange, Skill skill)
    {
        return AreaGetters[areaType].GetTilesInArea(board, pos, unit, skillRange, horizontal, vertical, skill);
    }

    private static Dictionary<SkillAreaType, BaseSkillArea> AreaGetters = new Dictionary<SkillAreaType, BaseSkillArea>()
    {
        { SkillAreaType.Specific,  new SpecificSkillArea() },
        { SkillAreaType.Full,      new FullSkillArea() }
    };
}
