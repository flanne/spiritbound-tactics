﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BleedOnHitPassive : StatusOnHitPassive<BleedStatusEffect>
{
    protected override bool ConditionsMet(object sender, Info<Unit, Unit, int, Skill> info)
    {
        Skill skill = info.arg3;

        return sender is PhysicalDamageSkillEffect 
            && skill.ContainsTag(SkillTag.Weapon);
    }
}
