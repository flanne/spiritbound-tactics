﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace flanne.UI
{
    public class SlidingSelectable : MonoBehaviour, ISelectHandler, IDeselectHandler
    {
        [SerializeField]
        Transform slidingTransform;

        [SerializeField]
        float slideAmount;

        Vector3 startingPos;

        void Awake()
        {
            startingPos = slidingTransform.localPosition;
        }

        public void OnSelect(BaseEventData eventData)
        {
            LeanTween.moveLocal(slidingTransform.gameObject, startingPos + new Vector3(slideAmount, 0, 0), 0.1f);
        }

        public void OnDeselect(BaseEventData eventData)
        {
            LeanTween.moveLocal(slidingTransform.gameObject, startingPos, 0.1f);
        }
    }
}
