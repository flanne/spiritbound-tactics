﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastReflexPassive : Passive
{
    protected override void AddListeners()
    {
        this.AddObserver(OnMissCheck, HitRate.AutomaticMissCheckNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnMissCheck, HitRate.AutomaticMissCheckNotification);
    }

    void OnMissCheck(object sender, object args)
    {
        var info = args as Info<MatchException, Skill>;
        var exc = info.arg0;

        if (exc.target == null)
            return;

        int distance = Mathf.Abs(exc.target.tile.xy.x - exc.attacker.tile.xy.x) + Mathf.Abs(exc.target.tile.xy.y - exc.attacker.tile.xy.y);
        int zDiff = exc.target.tile.z - exc.attacker.tile.z;

        if (exc.target == owner && 
            exc.target.alliance.type != exc.attacker.alliance.type &&
            zDiff > 1 &&
            distance > 1)
        {
            exc.FlipToggle();
        }
    }
}
