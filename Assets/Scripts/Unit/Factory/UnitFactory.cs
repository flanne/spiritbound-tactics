﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class UnitFactory : MonoBehaviour
{
    #region Fields
    [SerializeField] GameLevel gameLevel;
	
	Board board { get { return gameLevel?.board;  } }
	GameStrings gameStrings { get { return MasterSingleton.main.GameStrings; } }
    #endregion

    #region Public
	public Unit Spawn(UnitRecipe recipe, int levelScaleTo = -1)
    { 
		// Intantiate gameobject 
		GameObject obj = InstantiatePrefab(recipe.unitData.modelRef.model);
		obj.name = recipe.unitData.unitName;
		Unit unit = obj.GetComponent<Unit>();

		// Set all parameters of the unit
		Create(unit, recipe.unitData, levelScaleTo);

		// Place the unit onto the board
		Place(unit, recipe.spawnData);

		// Set alliance
		unit.alliance.type = recipe.alliance;

		// Set AI
		if (recipe.strategy == null)
		{
			unit.driver.normal = DriverType.Human;
		}
		else
		{
			unit.driver.normal = DriverType.Computer;
			GameObject instance = InstantiatePrefab(recipe.strategy.gameObject);
			instance.transform.SetParent(unit.transform);
		}

		// Give unit a unique ID used for cutscenes if it has one
		unit.uniqueID.id = recipe.unitID;

		// Set counter based on initiative
		unit.stats.SetValue(StatTypes.CTR, 10 - unit.jobs.primary.initiative, false, false);

		return unit;
	}

	// Spawns a unit from party
	public Unit Spawn(SpawnData spawnData, IDEnum unitID)
	{
		Unit unit = MasterSingleton.main.Party.GetByID(unitID);

		if (unit)
		{
			unit.gameObject.SetActive(true);
			LeanTween.color(unit.gameObject, new Color32(255, 255, 255, 255), 0);
			Place(unit, spawnData);
			unit.driver.normal = DriverType.Human;

			// Set counter based on initiative
			unit.stats.SetValue(StatTypes.CTR, 10 - unit.jobs.primary.initiative, false, false);
		}

		return unit;
	}

	// Creating party units
	public Unit Create(UnitData unitData, IDEnum unitID)
	{
		// Intantiate gameobject 
		GameObject obj = InstantiatePrefab(unitData.modelRef.model);
		obj.name = unitData.unitName;
		Unit unit = obj.GetComponent<Unit>();

		// Set alliance
		unit.alliance.type = AllianceType.Hero;

		Create(unit, unitData);

		unit.uniqueID.id = unitID;

		return unit;
	}
	#endregion

	#region Private
	GameObject InstantiatePrefab(GameObject prefab)
	{
		GameObject instance = GameObject.Instantiate(prefab);
		instance.name = instance.name.Replace("(Clone)", "");
		return instance;
	}

	void Create(Unit unit, UnitData unitData, int levelScaleTo = -1)
	{
		unit.modelRef = unitData.modelRef;

		unit.unitName = unitData.unitName;

		if (unitData.level >= levelScaleTo) // no level scalling
			unit.level.rank = unitData.level;
		else
			unit.level.rank = Mathf.Min(unitData.level + 5, levelScaleTo); // scale up to 5 levels higher than set level

		// Set components
		unit.jobRanks.LoadRecipe(unitData.jobRanks);
		unit.jobs.SetPrimary(unitData.primaryJob);
		if (unitData.secondaryJob)
			unit.jobs.SetSecondary(unitData.secondaryJob);

		// Add Equipment
		if (unitData.primary != null)
			unit.equipment.Equip(unitData.primary,		EquipSlots.Primary);

		if (unitData.secondary != null)
			unit.equipment.Equip(unitData.secondary,	EquipSlots.Secondary);

		if (unitData.body != null)
			unit.equipment.Equip(unitData.body,			EquipSlots.Body);

		if (unitData.accessory != null)
			unit.equipment.Equip(unitData.accessory,	EquipSlots.Accessory);

		if (unitData.consume1 != null)
			unit.equipment.Equip(unitData.consume1,		EquipSlots.Consumable1);

		if (unitData.consume2 != null)
			unit.equipment.Equip(unitData.consume2,		EquipSlots.Consumable2);

		if (unitData.consume3 != null)
			unit.equipment.Equip(unitData.consume3,		EquipSlots.Consumable3);
	}

	void Place(Unit unit, SpawnData spawnData)
    {
		if (!board.HasTile(spawnData.location))
		{
			Debug.LogError("Cannot spawn unit, " + unit.name + ", at tile: " + spawnData.location + " as it is not within tilemap bounds.");
			return;
		}

		unit.initialTile = board.GetTile(spawnData.location);
		unit.Place(unit.initialTile);
		unit.MatchToTile();

		unit.initialDirection = spawnData.direction;
		unit.direction = unit.initialDirection;

		// Set to invisible and knocked out if unit is not meant to be spawned right away.
		if (!spawnData.spawnedOnInitialize)
			unit.Hide();

		// Set if combatant or not
		unit.nonCombat = spawnData.nonCombat;
		unit.cutsceneOnly = spawnData.cutsceneOnly;
	}
	#endregion
}