﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spiritlink.Battle
{
    public class InitBattleState : BattleState
    {
        #region Notifications
        public const string BattleBeganNotification = "InitBattleState.battleBegan";
        #endregion

        #region LifeCycle
        public override void Enter()
        {
            keybindPromptUI.HidePrompt(2);

            base.Enter();
            StartCoroutine(Init());

            LoadLevel();
        }
        #endregion

        #region private
        IEnumerator Init()
        {
            owner.round = owner.gameObject.AddComponent<TurnOrderController>().Round();
            yield return null;
            this.PostNotification(BattleBeganNotification);

            if (cutsceneController.CutsceneQueued())
                owner.ChangeState<CutscenePreBattleState>();
            else
                owner.ChangeState<ShowObjectiveState>();
        }

        void LoadLevel()
        {
            // Debug use only 
            if (MasterSingleton.main.Party.myParty.Count == 0)
                MasterSingleton.main.Party.InitDevParty();

            MissionData mission = MasterSingleton.main.MissionLog.current;
            if (mission == null)
            {
                Debug.LogError("No mission data with the name: " + mission.name);
                return;
            }

            // Create board
            MasterSingleton.main.GameLevel.Load(mission.level.boardPrefab);
            tileSelectIndicator.SetBoard(owner.board);

            // Spawn party units
            foreach (var p in mission.partySpawns)
            {
                Unit unit = MasterSingleton.main.UnitFactory.Spawn(p.spawnData, p.unitID);

                if (unit != null)
                    units.Add(unit);
            }

            // Spawn other units
            int levelScaleTo = -1;
            if (!mission.noLevelScaling)
            {
                int sumLevel = 0;
                foreach (var unit in MasterSingleton.main.Party.myParty)
                    sumLevel += unit.level.rank;

                levelScaleTo = sumLevel / MasterSingleton.main.Party.myParty.Count;
            }

            foreach (var recipe in mission.unitRecipes)
            {
                Unit unit = MasterSingleton.main.UnitFactory.Spawn(recipe, levelScaleTo);

                if (recipe.spawnData.nonCombat)
                    unit.SetAnimationState(recipe.spawnData.startingState);
                else
                    units.Add(unit);
            }

            // Add cutscenes
            foreach (var cutscene in mission.cutsceneSequences)
            {
                cutsceneController.LoadCutscene(cutscene);
            }

            // Add victory conditions
            mission.heroVictoryCondition.AddComponent(owner.gameObject, AllianceType.Hero);
            mission.villainVictoryCondition.AddComponent(owner.gameObject, AllianceType.Villain);
        }
        #endregion
    }
}
