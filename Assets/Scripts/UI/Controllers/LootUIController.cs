﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace flanne.UI
{
    public class LootUIController : MonoBehaviour
    {
        [SerializeField] TMP_Text goldText;
        [SerializeField] Panel goldDisplay;
        [SerializeField] LootMenu lootMenu;

        public void SetGold(uint amount)
        {
            goldText.text = amount + " G";
        }

        public void SetLoot(Equippable[] loot)
        {
            lootMenu.Clear();
            for (int i = 0; i < loot.Length; i++)
                lootMenu.AddEntry(new DescribableProperties(loot[i]));
        }

        public void Show()
        {
            StartCoroutine(DisplayLoot());
        }

        public void Hide()
        {
            goldDisplay.Hide();
            lootMenu.Hide();
        }

        IEnumerator DisplayLoot()
        {
            goldDisplay.Show();

            yield return new WaitForSeconds(0.1f);

            lootMenu.Show();
        }
    }
}
