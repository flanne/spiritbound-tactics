﻿using UnityEngine;
using UnityEditor;

public class YourClassAsset
{
	[MenuItem("Assets/Create/SpiritlinkTactics/Mission Data")]
	public static void CreateMissionData()
	{
		ScriptableObjectUtility.CreateAsset<MissionData>();
	}

	[MenuItem("Assets/Create/SpiritlinkTactics/World Map Location")]
	public static void CreateWorldMapLocation()
	{
		ScriptableObjectUtility.CreateAsset<WorldMapLocation>();
	}

	[MenuItem("Assets/Create/SpiritlinkTactics/UnitModel")]
	public static void CreateUnitModel()
	{
		ScriptableObjectUtility.CreateAsset<UnitModel>();
	}

	[MenuItem("Assets/Create/SpiritlinkTactics/Job")]
	public static void CreateJob()
	{
		ScriptableObjectUtility.CreateAsset<Job>();
	}

	[MenuItem("Assets/Create/SpiritlinkTactics/JobAbility")]
	public static void CreateJobAbility()
	{
		ScriptableObjectUtility.CreateAsset<JobAbility>();
	}

	[MenuItem("Assets/Create/SpiritlinkTactics/Skill")]
	public static void CreateSkill()
	{
		ScriptableObjectUtility.CreateAsset<Skill>();
	}

	[MenuItem("Assets/Create/SpiritlinkTactics/Keyword")]
	public static void CreateKeyword()
	{
		ScriptableObjectUtility.CreateAsset<Keyword>();
	}

	#region Items
	[MenuItem("Assets/Create/Items/Weapon")]
	public static void CreateWeapon()
	{
		ScriptableObjectUtility.CreateAsset<Weapon>();
	}

	[MenuItem("Assets/Create/Items/Armor")]
	public static void CreateArmor()
	{
		ScriptableObjectUtility.CreateAsset<Armor>();
	}

	[MenuItem("Assets/Create/Items/OffHand")]
	public static void CreateOffHand()
	{
		ScriptableObjectUtility.CreateAsset<OffHand>();
	}

	[MenuItem("Assets/Create/Items/Accessory")]
	public static void CreateAccessory()
	{
		ScriptableObjectUtility.CreateAsset<Accessory>();
	}

	[MenuItem("Assets/Create/Items/Consumable")]
	public static void CreateConsumable()
	{
		ScriptableObjectUtility.CreateAsset<Consumable>();
	}
	#endregion

	#region Features
	[MenuItem("Assets/Create/Feature/Grant Skill")]
	public static void CreateGrantSkillFeature()
	{
		ScriptableObjectUtility.CreateAsset<GrantSkillFeature>();
	}

	[MenuItem("Assets/Create/Feature/Stat Modifier")]
	public static void CreateStatModifierFeature()
	{
		ScriptableObjectUtility.CreateAsset<StatModifierFeature>();
	}

	[MenuItem("Assets/Create/Feature/Power Multiplier")]
	public static void CreatePowerModFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddPowerModFeature>();
	}

	[MenuItem("Assets/Create/Feature/Damage Multiplier")]
	public static void CreateDamageMultiplierFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddDamageMultiplierFeature>();
	}

	[MenuItem("Assets/Create/Feature/EndOfTurnSkill")]
	public static void CreateEndOfTurnSkillFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddEndOfTurnSkillFeature>();
	}

	[MenuItem("Assets/Create/Feature/Wait Skill")]
	public static void CreateWaitSkillFeaturee()
	{
		ScriptableObjectUtility.CreateAsset<AddWaitSkillFeature>();
	}

	[MenuItem("Assets/Create/Feature/Dmg Up Per Status")]
	public static void CreateDmgUpPerStatusFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddDmgUpPerStatusFeature>();
	}

	[MenuItem("Assets/Create/Feature/Add Weapon Counter")]
	public static void CreateAddWeaponCounterFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddWeaponCounterFeature>();
	}

	[MenuItem("Assets/Create/Feature/Cloak Dagger")]
	public static void CreateCloakDaggerCounterFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddCloakDaggerFeature>();
	}

	[MenuItem("Assets/Create/Feature/Mana Cost Mod")]
	public static void CreateManaCostModFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddManaModFeature>();
	}

	[MenuItem("Assets/Create/Feature/TU Cost Mod")]
	public static void CreateTUCostModFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddTUModFeature>();
	}

	[MenuItem("Assets/Create/Feature/Wait Time Mod")]
	public static void CreateWaitTimeModFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddWaitTimeModFeature>();
	}

	[MenuItem("Assets/Create/Feature/Extra Consumable")]
	public static void CreateExtraConsumableFeature()
	{
		ScriptableObjectUtility.CreateAsset<ExtraConsumableFeature>();
	}

	[MenuItem("Assets/Create/Feature/Consumable Charge Mod")]
	public static void CreateConsumableChargeModFeature()
	{
		ScriptableObjectUtility.CreateAsset<ConsumableChargeModFeature>();
	}

	[MenuItem("Assets/Create/Feature/Range Mod")]
	public static void CreateRangeModFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddRangeModFeature>();
	}

	[MenuItem("Assets/Create/Feature/Bleed on Hit")]
	public static void CreateBleedOnHitFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddBleedOnHitFeature>();
	}

	[MenuItem("Assets/Create/Feature/Ward On Heal")]
	public static void CreateWardOnHealFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddWardOnHealFeature>();
	}

	[MenuItem("Assets/Create/Feature/Hobble On High Hit")]
	public static void CreateHobbleOnHighHitFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddHobbleOnHighHitFeature>();
	}

	[MenuItem("Assets/Create/Feature/Backstab Bonus Damage")]
	public static void CreateBackstabDamageFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddBackstabDamageFeature>();
	}

	[MenuItem("Assets/Create/Feature/Poison on Backstab")]
	public static void CreatePoisonOnBackstabFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddPoisonOnBackstabFeature>();
	}

	[MenuItem("Assets/Create/Feature/Silence on Backstab")]
	public static void CreateSilenceOnBackstabFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddSilenceOnBackstabFeature>();
	}

	[MenuItem("Assets/Create/Feature/Regeneration")]
	public static void CreateRegenerationFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddRegenerationFeature>();
	}

	[MenuItem("Assets/Create/Feature/IntenseFocus")]
	public static void CreateIntenseFocusFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddIntenseFocusFeature>();
	}

	[MenuItem("Assets/Create/Feature/Divine Protection")]
	public static void CreateAddDivineProtectionFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddDivineProtectionFeature>();
	}

	[MenuItem("Assets/Create/Feature/Cast As Cantrip")]
	public static void CreateCastAsCantrip()
	{
		ScriptableObjectUtility.CreateAsset<AddCastAsCantripFeature>();
	}

	[MenuItem("Assets/Create/Feature/Skill Stun")]
	public static void CreateSkillStunPassive()
	{
		ScriptableObjectUtility.CreateAsset<AddSkillStunFeature>();
	}

	[MenuItem("Assets/Create/Feature/Stun On Debuff")]
	public static void CreateStunOnDebuffFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddStunOnDebuffFeature>();
	}

	[MenuItem("Assets/Create/Feature/Stun On High Hit")]
	public static void CreateStunOnHighHitFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddStunOnHighHitFeature>();
	}

	[MenuItem("Assets/Create/Feature/Stat Bonus Damage")]
	public static void CreateStatBonusDamageFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddStatBonusDamageFeature>();
	}

	[MenuItem("Assets/Create/Feature/Reaper")]
	public static void CreateReaperFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddReaperFeature>();
	}

	[MenuItem("Assets/Create/Feature/MDef To Healing")]
	public static void CreateMDefToHealingFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddMDefToHealingFeature>();
	}

	[MenuItem("Assets/Create/Feature/Overheal Feature")]
	public static void CreateOverhealFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddOverhealFeature>();
	}

	[MenuItem("Assets/Create/Feature/Heavy Armor Feature")]
	public static void CreateHeavyArmorFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddHeavyArmorFeature>();
	}

	[MenuItem("Assets/Create/Feature/Protector Feature")]
	public static void CreateProtectorFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddProtectorFeature>();
	}

	[MenuItem("Assets/Create/Feature/TU On Damaged")]
	public static void CreateTUOnDamagedFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddTUOnDamagedFeature>();
	}

	[MenuItem("Assets/Create/Feature/TU On Status Damage")]
	public static void CreateTUOnStatusDamage()
	{
		ScriptableObjectUtility.CreateAsset<AddTUOnStatusDamageFeature>();
	}

	[MenuItem("Assets/Create/Feature/Mod Status Duration")]
	public static void CreateModStatusDurationFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddModStatusDurationFeature>();
	}

	[MenuItem("Assets/Create/Feature/Doom On Debuffs")]
	public static void CreateDoomOnDebuffFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddDoomOnDebuffFeature>();
	}

	[MenuItem("Assets/Create/Feature/Uncleansable Debuffs")]
	public static void CreatUncleansableDebuffs()
	{
		ScriptableObjectUtility.CreateAsset<AddUncleansableDebuffsFeature>();
	}

	[MenuItem("Assets/Create/Feature/Auto Hit")]
	public static void CreateAutoHitFeatures()
	{
		ScriptableObjectUtility.CreateAsset<AddAutoHitFeature>();
	}

	[MenuItem("Assets/Create/Feature/Backstab Auto Hit")]
	public static void CreateBackstabAutoHitFeatures()
	{
		ScriptableObjectUtility.CreateAsset<AddBackstabAutoHitFeature>();
	}

	[MenuItem("Assets/Create/Feature/Fast Reflex")]
	public static void CreateFastReflex()
	{
		ScriptableObjectUtility.CreateAsset<AddFastReflexFeature>();
	}

	[MenuItem("Assets/Create/Feature/Sniper")]
	public static void CreateAddSniperFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddSniperFeature>();
	}

	[MenuItem("Assets/Create/Feature/Death from Above")]
	public static void CreateAddDeathFromAboveFeature()
	{
		ScriptableObjectUtility.CreateAsset<AddDeathFromAboveFeature>();
	}
	#endregion
}
