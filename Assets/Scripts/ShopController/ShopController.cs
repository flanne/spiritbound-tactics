﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using flanne.UI;

public class ShopController : StateMachine
{
    #region Fields
    [SerializeField] AudioClip shopMusic;
    public AudioClip confirmSFX;
    public AudioClip selectSFX;
    public AudioClip cancelSFX;

    public PlayerInput playerInput;

    public DialogueUI dialogueBox;
    public flanne.UI.Menu mainMenu;
    public BuySellController buySellMenu;
    public DescriptionBox descriptionBox;
    public UnitListMenu unitListMenu;
    public TryEquipController tryEquipController;
    public ShopMenu tryBuyMenu;
    public EquipmentCompareUI equipCompareUI;

    public Panel fundsDisplay;

    public TMP_Text fundsText { get { return _fundsText; } }
    [SerializeField] TMP_Text _fundsText;

    public KeybindPromptUI keybindPrompt { get { return _keybindPrompt; } }
    [SerializeField] KeybindPromptUI _keybindPrompt;


    public TMP_Text tryCostText { get { return _tryCostText; } }
    [SerializeField] TMP_Text _tryCostText;

    public ShopConfirmUI confirmUI { get { return _confirmUI; } }
    [SerializeField] ShopConfirmUI _confirmUI;

    public string welcomeDialogueStringID { get { return _welcomeDialogueStringID; } }
    [SerializeField] string _welcomeDialogueStringID;

    [SerializeField] Equippable[] defaultShopItems;
    [System.NonSerialized] public Equippable[] shopItems;

    [System.NonSerialized] public Unit menuUnit;
    [System.NonSerialized] public EquipSlots currentTrySlot = EquipSlots.Primary;
    public Dictionary<EquipSlots, Equippable> tryEquips = new Dictionary<EquipSlots, Equippable>();
    public Dictionary<EquipSlots, Equippable> tryOriginalEquips = new Dictionary<EquipSlots, Equippable>();
    #endregion

    void Start()
    {
        MasterSingleton.main.AudioPlayer.PlayMusic(shopMusic);
        ChangeState<ShopMenuState>();
        InitShopItems();

        // For debug
        if (MasterSingleton.main.Party.myParty.Count == 0)
            MasterSingleton.main.Party.InitDevParty();
    }

    void InitShopItems()
    {
        shopItems = MasterSingleton.main.WorldMapData.currentLocation.shopItems;

        if (shopItems == null || shopItems.Length == 0)
            shopItems = defaultShopItems;
    }
}
