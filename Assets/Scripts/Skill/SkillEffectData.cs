﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public class SkillEffectData
{
    #region Enums
    enum SkillEffectType
    {
        PhyscialDamage,
        MagicDamage,
        Drain,
        Heal,
        HealPercent,
        Teleport,
        ApplyBleed,
        ApplyMight,
        DebuffScalePhysDmg,
        Shielding,
        ApplyTaunt,
        ApplyBurn,
        DelayTU,
        RefillConsumables,
        ApplyPoison,
        DamagePercent,
        ApplyFocus,
        LifeSteal,
        ApplyWard,
        ApplyRegen,
        ApplyHobble,
        BarrierDelayTU,
        BarrierAttack,
        LoseBarrier,
        ApplyVulnerable,
        ApplyWeak,
        DebuffScaleMagDmg,
        RemoveAllDebuff,
        HeightScalePhysDmg,
        RemoveBleed,
        HPScalingDmg
    };

    enum HitRateType
    {
        Default,
        AlwaysHit,
        EmptyTile
    };

    enum TargetType
    {
        All,
        Allies,
        Enemies,
        EmptyTile,
        Self,
        NoSelfAllies,
        Backstab
    };
    #endregion

    #region Static Dictionaries
    private static Dictionary<SkillEffectType, BaseSkillEffect> SkillEffects = new Dictionary<SkillEffectType, BaseSkillEffect>()
    {
        { SkillEffectType.PhyscialDamage,       new PhysicalDamageSkillEffect() },
        { SkillEffectType.MagicDamage,          new MagicDamageSkillEffect() },
        { SkillEffectType.Drain,                new DrainSkillEffect() },
        { SkillEffectType.Heal,                 new HealSkillEffect() },
        { SkillEffectType.HealPercent,          new HealPercentSkillEffect() },
        { SkillEffectType.Teleport,             new TeleportSkillEffect() },
        { SkillEffectType.ApplyBleed,           new ApplyBleedSkillEffect() },
        { SkillEffectType.ApplyMight,           new ApplyMightSkillEffect() },
        { SkillEffectType.DebuffScalePhysDmg,   new DebuffScalePhysDamageSkillEffect() },
        { SkillEffectType.Shielding,            new ShieldSkillEffect() },
        { SkillEffectType.ApplyTaunt,           new ApplyTauntSkillEffect() },
        { SkillEffectType.ApplyBurn,            new ApplyBurnSkillEffect() },
        { SkillEffectType.DelayTU,              new DelayTUSkillEffect() },
        { SkillEffectType.RefillConsumables,    new RefillConsumablesSkillEffect() },
        { SkillEffectType.ApplyPoison,          new ApplyPoisonSkillEffect() },
        { SkillEffectType.DamagePercent,        new DamagePercentSkillEffect() },
        { SkillEffectType.ApplyFocus,           new ApplyFocusSkillEffect() },
        { SkillEffectType.LifeSteal,            new LifeStealSkillEffect() },
        { SkillEffectType.ApplyWard,            new ApplyWardSkillEffect() },
        { SkillEffectType.ApplyRegen,           new ApplyRegenSkillEffect() },
        { SkillEffectType.ApplyHobble,          new ApplyHobbleSkillEffect() },
        { SkillEffectType.BarrierDelayTU,       new BarrierScaleTUDelaySkillEffect() },
        { SkillEffectType.BarrierAttack,        new BarrierAttackSkillEffect() },
        { SkillEffectType.LoseBarrier,          new LoseBarrierSkillEffect() },
        { SkillEffectType.ApplyVulnerable,      new ApplyVulnerableSkillEffect() },
        { SkillEffectType.ApplyWeak,            new ApplyWeakSkillEffect() },
        { SkillEffectType.DebuffScaleMagDmg,    new DebuffScaleMagDamageSkillEffect() },
        { SkillEffectType.RemoveAllDebuff,      new RemoveAllDebuffSkillEffect() },
        { SkillEffectType.HeightScalePhysDmg,   new HeightScalePhyDamageSkillEffect() },
        { SkillEffectType.RemoveBleed,          new RemoveBleedStatusSkillEffect() },
        { SkillEffectType.HPScalingDmg,         new HPDamageSkillEffect() },
    };

    private static Dictionary<HitRateType, HitRate> HitRates = new Dictionary<HitRateType, HitRate>()
    {
        { HitRateType.Default,   new AttackHitRate() },
        { HitRateType.AlwaysHit, new FullHitRate() },
        { HitRateType.EmptyTile, new EmptyTileHitRate() },
    };

    private static Dictionary<TargetType, SkillEffectTarget> Targeters = new Dictionary<TargetType, SkillEffectTarget>()
    {
        { TargetType.All,           new DefaultSkillEffectTarget() },
        { TargetType.Allies,        new AllySkillEffectTarget() },
        { TargetType.Enemies,       new EnemySkillEffectTarget() },
        { TargetType.EmptyTile,     new EmptyTileSkillEffectTarget() },
        { TargetType.Self,          new SelfSkillEffectTarget() },
        { TargetType.NoSelfAllies,  new NoSelfAllySkillEffectTarget() },
        { TargetType.Backstab,      new BackstabSkillEffectTarget() },
    };
    #endregion

    #region Notification
    public const string GetPowerModsNotification = "SkillEffectData.GetPowerModsNotification";
    #endregion

    #region Serialized Field
    [VerticalGroup("Effects")]

    [HorizontalGroup("Effects/Split1", 100), LabelWidth(50)]
    [SerializeField]
    private int power;

    [HorizontalGroup("Effects/Split1"), HideLabel]
    [SerializeField]
    private SkillEffectType effectType;

    [HorizontalGroup("Effects/Split2"), LabelWidth(50), LabelText("Hit Rate")]
    [SerializeField]
    private HitRateType hitRateType;

    [HorizontalGroup("Effects/Split2"), LabelWidth(50), LabelText("Targeting")]
    [SerializeField]
    private TargetType targetType;
    #endregion

    #region NonSerialized Field
    public BaseSkillEffect effect { get { return SkillEffects[effectType]; } }
    private SkillEffectTarget targeter { get { return Targeters[targetType]; } }
    private HitRate hitRoller { get { return HitRates[hitRateType]; } }
    #endregion

    #region Public
    public bool Apply(Unit actor, BoardTile target, bool isHit, Skill skill)
    {
        int finalPower = power.NotifyModifiers<Skill>(GetPowerModsNotification, actor, skill);

        if (targeter.IsTarget(actor, target))
            return effect.Apply(actor, target, isHit, finalPower, skill);

        return false;
    }

    public bool Apply(Unit actor, BoardTile target, Skill skill)
    {
        int finalPower = power.NotifyModifiers<Skill>(GetPowerModsNotification, actor, skill);

        if (targeter.IsTarget(actor, target))
            return effect.Apply(actor, target, hitRoller, finalPower, skill);

        return false;
    }

    public bool RollForHit(Unit actor, BoardTile target, Skill skill)
    {
        Unit targetUnit = target.content?.GetComponent<Unit>();
        return hitRoller.RollForHit(actor, targetUnit, skill);
    }

    public int GetHitRate(Unit actor, BoardTile target, Skill skill)
    {
        return hitRoller.Calculate(actor, target, skill);
    }

    public int GetPrediction(Unit actor, BoardTile target, Skill skill)
    {
        int finalPower = power.NotifyModifiers<Skill>(GetPowerModsNotification, actor, skill);

        return effect.Predict(actor, target, finalPower, skill);
    }

    public int GetPredictionClamped(Unit actor, BoardTile target, Skill skill)
    {
        int finalPower = power.NotifyModifiers<Skill>(GetPowerModsNotification, actor, skill);

        return effect.PredictClamped(actor, target, finalPower, skill);
    }

    public bool IsAngleBased()
    {
        return hitRoller.IsAngleBased;
    }

    public bool IsTarget(Unit actor, BoardTile target)
    {
        return targeter.IsTarget(actor, target);
    }
    #endregion
}
