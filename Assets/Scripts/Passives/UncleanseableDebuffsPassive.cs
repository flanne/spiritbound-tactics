﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UncleanseableDebuffsPassive : Passive
{
    protected override void AddListeners()
    {
        this.AddObserver(OnCleanseCheck, Status.CanCleanseCheck);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnCleanseCheck, Status.CanCleanseCheck);
    }

    void OnCleanseCheck(object sender, object args)
    {
        var info = args as Info<StatusEffect, BaseException>;
        var statusEffect = info.arg0;
        var exc = info.arg1;

        if (statusEffect.inflictor == owner)
            exc.FlipToggle();
    }
}