﻿using UnityEngine;
using System.Collections;

public class FullHitRate : HitRate
{
    public override bool IsAngleBased { get { return false; } }

    public override int Calculate(Unit actor, Unit targetUnit, Skill skill)
    {
        if (AutomaticMiss(actor, targetUnit, skill))
            return Final(100);
        return Final(0);
    }
}