﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace flanne.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class Panel : MonoBehaviour
    {
        public bool interactable 
        { 
            get 
            {
                return canvasGroup.interactable;
            }
            set 
            {
                canvasGroup.interactable = value;
                canvasGroup.blocksRaycasts = value;
            }
        }
        
        [SerializeField] float fadeDuration = 0f;

        CanvasGroup canvasGroup;

        protected virtual void Start()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
            canvasGroup.alpha = 0;

            foreach (UITweener u in GetComponentsInChildren<UITweener>())
                u.SetOff();
        }

        public virtual void Show()
        {
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;

            LeanTween.alphaCanvas(canvasGroup, 1f, fadeDuration);

            foreach (UITweener u in GetComponentsInChildren<UITweener>())
                u.Show();
        }

        public virtual void Hide()
        {
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;

            LeanTween.alphaCanvas(canvasGroup, 0f, fadeDuration);

            foreach (UITweener u in GetComponentsInChildren<UITweener>())
                u.Hide();
        }
    }
}
