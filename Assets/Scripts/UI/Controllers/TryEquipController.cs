﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class TryEquipController : EquipmentMenuController
    {
        public event EventHandler PurchaseEvent;
        public event EventHandler PurchaseSelectEvent;

        [SerializeField] MenuEntry purchaseButton;

        #region Eventhandler
        public void PurchaseClicked()
        {
            if (PurchaseEvent != null)
                PurchaseEvent(this, null);
        }

        public void PurchaseSelected()
        {
            if (PurchaseSelectEvent != null)
                PurchaseSelectEvent(this, null);
        }
        #endregion

        public void Start()
        {
            purchaseButton.onClick.AddListener(() => PurchaseClicked());
            purchaseButton.onSelect.AddListener(() => PurchaseSelected());
        }

        public void FormatTry(EquipSlots slot)
        {
            var entry = GetEntry(slot);
            var widget = entry.GetComponent<DescribableNamedWidget>();

            if (widget != null)
                widget.FormatTry();
        }
    }
}
