﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EmoteType
{
    Provoke     =0,
    Alerted     =1,
    Confused    =2,
    Loving      =3
}