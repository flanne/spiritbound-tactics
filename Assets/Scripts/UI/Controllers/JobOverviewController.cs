﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace flanne.UI
{
    public class JobOverviewController : MonoBehaviour
    {
        #region Fields/Properties
        public event EventHandler<InfoEventArgs<DescribableObject>> DescribableSelectEvent;
        public event EventHandler<InfoEventArgs<int>> JobClickEvent;
        public event EventHandler<InfoEventArgs<Info<int, int, AbilityTreeSelection>>> AbilityClickEvent;

        [SerializeField] flanne.UI.Menu jobSlots;
        [SerializeField] AbilityTreeController primaryTree;
        [SerializeField] AbilityTreeController secondaryTree;
        [SerializeField] UnitBasicInfoWidget basicInfoWidget;
        [SerializeField] Panel panel;

        Unit _unit;

        public bool interactable
        {
            get
            {
                if (panel != null)
                    return panel.interactable;
                else
                    return false;
            }
            set
            {
                if (panel != null)
                    panel.interactable = value;
            }
        }
        #endregion

        #region Eventhandlers
        void OnJobSelect(object sender, InfoEventArgs<int> e)
        {
            if (_unit != null)
            {
                Job job;
                if (e.info == 0)
                    job = _unit.jobs.primary;
                else
                    job = _unit.jobs.secondary;

                if (DescribableSelectEvent != null)
                    DescribableSelectEvent(this, new InfoEventArgs<DescribableObject>(job));
            }
        }

        void OnJobClick(object sender, InfoEventArgs<int> e)
        {
            if (JobClickEvent != null)
                JobClickEvent(this, new InfoEventArgs<int>(e.info));
        }

        void OnAbilitySelect(object sender, InfoEventArgs<DescribableObject> e)
        {
            if (DescribableSelectEvent != null)
                DescribableSelectEvent(this, e);
        }

        void OnPrimaryAbilityClick(object sender, InfoEventArgs<Info<int, AbilityTreeSelection>> e)
        {
            if (AbilityClickEvent != null)
            {
                var info = new Info<int, int, AbilityTreeSelection>(0, e.info.arg0, e.info.arg1);
                AbilityClickEvent(this, new InfoEventArgs<Info<int, int, AbilityTreeSelection>>(info));
            }
        }

        void OnSecondaryAbilityClick(object sender, InfoEventArgs<Info<int, AbilityTreeSelection>> e)
        {
            if (AbilityClickEvent != null)
            {
                var info = new Info<int, int, AbilityTreeSelection>(1, e.info.arg0, e.info.arg1);
                AbilityClickEvent(this, new InfoEventArgs<Info<int, int, AbilityTreeSelection>>(info));
            }
        }
        #endregion

        #region LifeCycle
        void Awake()
        {
            // Add Listeners
            jobSlots.SelectEvent += OnJobSelect;
            jobSlots.ClickEvent += OnJobClick;

            primaryTree.SelectEvent += OnAbilitySelect;
            primaryTree.ClickEvent += OnPrimaryAbilityClick;

            secondaryTree.SelectEvent += OnAbilitySelect;
            secondaryTree.ClickEvent += OnSecondaryAbilityClick;
        }

        void OnDestroy()
        {
            // Remove Listeners
            jobSlots.SelectEvent -= OnJobSelect;
            jobSlots.ClickEvent -= OnJobClick;

            primaryTree.SelectEvent -= OnAbilitySelect;
            primaryTree.ClickEvent -= OnPrimaryAbilityClick;

            secondaryTree.SelectEvent -= OnAbilitySelect;
            secondaryTree.ClickEvent -= OnSecondaryAbilityClick;
        }
        #endregion

        #region Public
        public void SetToUnit(Unit unit)
        {
            _unit = unit;

            if (basicInfoWidget != null)
                basicInfoWidget.SetProperties(new UnitProperties(unit));

            UnitJobs jobs = unit.jobs;

            jobSlots.SetProperties<DescribableProperties>(0, new DescribableProperties(jobs.primary));
            jobSlots.SetProperties<DescribableProperties>(1, new DescribableProperties(jobs.secondary));

            primaryTree.SetToJob(jobs.primary);
            secondaryTree.SetToJob(jobs.secondary);

            UnitJobRanks jobRanks = unit.jobRanks;
            primaryTree.SetToJobRank(jobRanks.Get(jobs.primary), true);
            if (jobs.secondary != null)
                secondaryTree.SetToJobRank(jobRanks.Get(jobs.secondary), false);
        }

        public void Show()
        {
            interactable = true;

            if (panel != null)
                panel.Show();

            jobSlots.Show();
            primaryTree.Show();
            secondaryTree.Show();
        }

        public void Hide()
        {
            if (panel != null)
                panel.Hide();

            jobSlots.Hide();
            primaryTree.Hide();
            secondaryTree.Hide();
        }

        public void SelectDefault()
        {
            jobSlots.Select(0);
        }
        #endregion
    }
}
