﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitEmoteCutsceneAction : UnitCutsceneAction
{
    [SerializeField]
    EmoteType emoteType;

    public override void Play()
    {
        unit.emoter.Play(emoteType);
        isPlaying = false;
    }
}
