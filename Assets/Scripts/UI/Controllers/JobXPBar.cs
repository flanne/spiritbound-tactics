﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JobXPBar : MonoBehaviour
{
    [SerializeField]
    Image fill;

    public bool isAnimating
    {
        get
        {
            return LeanTween.isTweening(tweenId);
        }
    }

    int tweenId;

    public void AnimateChange(int from, int to, int max, float duration, LeanTweenType easeType = LeanTweenType.linear)
    {
        SetTo(from, max);

        tweenId = LeanTween.scale(fill.gameObject, new Vector3((float)to / (float)max, 1, 1), duration).setEase(easeType).id;
    }

    public void SetTo(int amount, int max)
    {
        fill.transform.localScale = new Vector3((float)amount / (float)max, 1, 1);
    }
}
