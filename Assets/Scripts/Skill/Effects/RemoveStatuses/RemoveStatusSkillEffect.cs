﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveStatusSkillEffect<T> : BaseSkillEffect where T : StatusEffect
{
	#region Public
	public override int Predict(Unit actor, BoardTile target, int power, Skill skill)
	{
		return 0;
	}

	// Power specifies duration
	protected override int OnApply(Unit actor, BoardTile target, int power, Skill skill)
	{
		Unit defender = target.content.GetComponent<Unit>();

		var statusEffects = defender.status.GetComponentsInChildren<StatusEffect>();
		foreach (var s in statusEffects)
			if (s is T)
				defender.status.Remove(s);

		return 0;
	}
	#endregion
}
