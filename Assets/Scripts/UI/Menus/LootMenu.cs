﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace flanne.UI
{
    public class LootMenu : DynamicMenu<DescribableProperties>
    {
        [SerializeField] float lootFadeDuration;

        protected override void Start()
        {
            base.Start();

            foreach (var entry in entries)
            {
                CanvasGroup canvasGroup = entry.GetComponent<CanvasGroup>();

                if (canvasGroup != null)
                {
                    canvasGroup.interactable = false;
                    canvasGroup.blocksRaycasts = false;
                    canvasGroup.alpha = 0;
                }
            }
        }

        public override void Show()
        {
            base.Show();

            StartCoroutine(ShowEntriesCoroutine());
        }

        IEnumerator ShowEntriesCoroutine()
        {
            foreach (var entry in entries)
            {
                CanvasGroup canvasGroup = entry.GetComponent<CanvasGroup>();

                if (canvasGroup != null)
                {
                    LeanTween.alphaCanvas(canvasGroup, 1f, lootFadeDuration);
                    canvasGroup.interactable = true;
                    canvasGroup.blocksRaycasts = true;
                }

                yield return new WaitForSeconds(0.1f);
            }
        }
    }
}