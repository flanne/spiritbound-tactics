﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JobDatabase : MonoBehaviour
{
    [SerializeField] Job[] jobs;

    Job GetJob(int index)
    {
        if (index > 0 && index < jobs.Length)
            return jobs[index];
        else
            Debug.Log("Index: " + index + "is not in job database.");

        return null;
    }

    int GetIndex(Job job)
    {
        for (int i = 0; i < jobs.Length; i++)
        {
            if (jobs[i] == job)
                return i;
        }

        return 0;
    }
}
