﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ExitMenuState : WorldMapState
{
    #region Eventhandler
    void OnClick(object sender, InfoEventArgs<int> e)
    {
        if (e.info == 0)
            owner.ChangeState<UnitListMenuState>();
        else if (e.info == 1)
            owner.ChangeState<WorldMapOptionsState>();
        else if (e.info == 2)
        {
            MasterSingleton.main.SaveSystem.Save();
            MasterSingleton.main.SceneTransition.LoadScene("MainMenu");
            owner.ChangeState<MapTransitionSceneState>();
        }

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnSelect(object sender, InfoEventArgs<int> e)
    {
        audioPlayer.Play(owner.selectSFX);
    }

    void OnCancel(InputAction.CallbackContext obj)
    {
        owner.ChangeState<TraverseWorldMapState>();

        audioPlayer.Play(owner.cancelSFX);
    }
    #endregion

    #region LifeCycle
    public override void Enter()
    {
        owner.exitMenu.Show();
        owner.exitMenu.SelectFirstAvailable();
        owner.exitMenu.ClickEvent += OnClick;
        owner.exitMenu.SelectEvent += OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;

        // Show keybinds
        keybindPromptUI.Show();
        keybindPromptUI.SetText(0, "Select");
        keybindPromptUI.SetText(1, "Cancel");
        keybindPromptUI.HidePrompt(2);
    }

    public override void Exit()
    {
        owner.exitMenu.Hide();
        owner.exitMenu.ClickEvent -= OnClick;
        owner.exitMenu.SelectEvent -= OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;
    }
    #endregion
}
