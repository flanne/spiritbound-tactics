﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class ItemStackProperties : IUIProperties
    {
        public ItemStack itemStack;
        public bool isSellPrice;

        public ItemStackProperties(ItemStack i, bool s = false)
        {
            itemStack = i;
            isSellPrice = s;
        }
    }

    public class ItemStackWidget : Widget<ItemStackProperties>
    {
        [SerializeField] Image icon;
        [SerializeField] TMP_Text nameTMP;
        [SerializeField] TMP_Text inUseTMP;
        [SerializeField] TMP_Text totalTMP;
        [SerializeField] TMP_Text priceTMP;

        public override void SetProperties(ItemStackProperties properties)
        {
            ItemStack itemStack = properties.itemStack;

            icon.sprite = itemStack.item.icon;

            nameTMP.text = MasterSingleton.main.GameStrings.Get(itemStack.item.nameStringID);

            inUseTMP.text = itemStack.inUse.ToString();
            if (itemStack.inUse < itemStack.total || itemStack.inUse == 0)
                inUseTMP.color = Color.white;
            else
                inUseTMP.color = new Color32(255, 100, 100, 255);

            totalTMP.text = itemStack.total.ToString();

            if (priceTMP != null)
            {
                if (properties.isSellPrice)
                    priceTMP.text = (itemStack.item.cost / 2).ToString();
                else
                    priceTMP.text = itemStack.item.cost.ToString();
            }
        }

        public void FormatUnEquippable()
        {
            nameTMP.color = new Color32(150, 150, 150, 255);
        }
    }
}
