﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emote : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer baseSprite;

    [SerializeField]
    SpriteRenderer emoteSprite;

    public void SetEmoteSprite(Sprite sprite)
    {
        emoteSprite.sprite = sprite;
    }

    void Start()
    {
        Color bc = baseSprite.color;
        bc.a = 0;
        baseSprite.color = bc;
        LeanTween.alpha(baseSprite.gameObject, 1f, 0.2f);

        LeanTween.moveLocalY(baseSprite.gameObject, emoteSprite.transform.localPosition.y + 0.02f, 1f);

        Color ec = emoteSprite.color;
        ec.a = 0;
        emoteSprite.color = ec;
        LeanTween.alpha(emoteSprite.gameObject, 1f, 0.2f);
        
        LeanTween.moveLocalY(emoteSprite.gameObject, emoteSprite.transform.localPosition.y + 0.07f, 1f).setOnComplete(this.FadeOut);
    }

    void FadeOut()
    {
        LeanTween.alpha(baseSprite.gameObject, 0f, 0.1f);
        LeanTween.alpha(emoteSprite.gameObject, 0f, 0.1f).setOnComplete(this.RemoveMe);
    }

    void RemoveMe()
    {
        Destroy(this.gameObject);
    }
}
