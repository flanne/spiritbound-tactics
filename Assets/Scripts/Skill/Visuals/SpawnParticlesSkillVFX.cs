﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnParticlesSkillVFX : SpawnGameObjectSkillVFX
{
    protected override bool DespawnCondition(GameObject obj)
    {
        ParticleSystem particles = obj.GetComponent<ParticleSystem>();

        if (particles.isPlaying)
            return false;

        return true;
    }

    protected override void Initialize(GameObject obj)
    {
        ParticleSystem particles = obj.GetComponent<ParticleSystem>();
        particles.Play();
    }
}
