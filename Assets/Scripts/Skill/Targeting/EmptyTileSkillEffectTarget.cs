﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyTileSkillEffectTarget : SkillEffectTarget
{
    public override bool IsTarget(Unit actor, BoardTile target)
    {
        if (target.content == null)
            return true;

        return false;
    }
}
