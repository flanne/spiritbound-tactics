﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowGameObjectSkillVFX : SpawnGameObjectSkillVFX
{
    enum ThrowType
    {
        DefaultThrow,
        ThrowIfRanged
    }

    [SerializeField]
    ThrowType throwType;

    [SerializeField]
    float throwDelay = 0.5f;
    [SerializeField]
    float throwDuration = 0.3f;

    [SerializeField]
    float throwTargetYOffset = 0.75f;

    GameObject currentObj;

    Vector3 currentDestination;

    bool destroyUnthrown = false; // Destroy without throwing for close ranged

    public override TargetType targetType { get { return TargetType.Special; } }

    public override void SpecialPlay(Unit actor, BoardTile target)
    {
        // Calls SpawnGameObjectsSkillVFX's animate
        StartCoroutine(SpawnObjAfterDelay(actor, target));
    }

    protected override bool DespawnCondition(GameObject obj)
    {
        if (destroyUnthrown)
        {
            destroyUnthrown = false; // reset bool
            return true;
        }
        else
            return false;
    }

    protected override void Initialize(GameObject obj) 
    {
        currentObj = obj;
    }

    IEnumerator SpawnObjAfterDelay(Unit actor, BoardTile target)
    {
        yield return new WaitForSeconds(delay);
        StartCoroutine(base.Animate(actor.tile));

        if (throwType == ThrowType.DefaultThrow)
        {
            StartCoroutine(ThrowAfterDelay(currentObj, actor, target));
        }
        else if (throwType == ThrowType.ThrowIfRanged) // Only throw if more than 1 square away
        {
            int distance = Mathf.Abs(actor.tile.xy.x - target.xy.x) + Mathf.Abs(actor.tile.xy.y - target.xy.y);

            if (distance > 1)
                StartCoroutine(ThrowAfterDelay(currentObj, actor, target));
        }

        StartCoroutine(StartDestroyTimer());
    }

    IEnumerator StartDestroyTimer()
    {
        yield return new WaitForSeconds(throwDelay + throwDuration);
        destroyUnthrown = true;
    }

    IEnumerator ThrowAfterDelay(GameObject obj, Unit actor, BoardTile target)
    {
        if (throwDelay > 0.6f)
        {
            yield return new WaitForSeconds(throwDelay - 0.7f); // Throw animation
            actor.SetAnimationState(UnitAnimState.Throw);
            yield return new WaitForSeconds(0.7f);
        }
        else
            yield return new WaitForSeconds(throwDelay);

        Vector3 toPosition = target.GetWorldPosition();
        toPosition.y += throwTargetYOffset;
        StartCoroutine(MoveTowardsVec2(obj.transform, toPosition, throwDuration));

        yield return new WaitForSeconds(0.5f);

        // Reset throw animation
        if (actor.GetAnimationState() == UnitAnimState.Throw)
            actor.SetAnimationState(UnitAnimState.Idle);
    }

    IEnumerator MoveTowardsVec2(Transform objectToMove, Vector3 toPosition, float duration)
    {
        float counter = 0;

        currentDestination = toPosition;

        while (counter < duration && objectToMove != null)
        {
            counter += Time.deltaTime;
            Vector2 currentPosVec2 = new Vector2(objectToMove.position.x, objectToMove.position.y);
            Vector2 toPositionVec2 = new Vector2(toPosition.x, toPosition.y);

            float time = Vector2.Distance(currentPosVec2, toPositionVec2) / (duration - counter) * Time.deltaTime;

            Vector2 newPos = Vector2.MoveTowards(currentPosVec2, toPosition, time);
            objectToMove.position = new Vector3(newPos.x, newPos.y, objectToMove.position.z);

            yield return null;
        }
    }
}
