﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Language : ScriptableObject
{
    public string[] keys;
    public string[] strings;
}
