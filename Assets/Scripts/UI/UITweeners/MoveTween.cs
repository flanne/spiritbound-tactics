﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTween : UITweener
{
    [SerializeField]
    LeanTweenType easeType = LeanTweenType.linear;

    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }

    public Direction entranceFrom;
    public float moveAmount;

    Vector3 moveVector;
    Vector3 startPosition;

    void Awake()
    {
        switch (entranceFrom)
        {
            case Direction.Left:
                moveVector = new Vector3(-1 * moveAmount, 0, 0);
                break;
            case Direction.Right:
                moveVector = new Vector3(moveAmount, 0, 0);
                break;
            case Direction.Up:
                moveVector = new Vector3(0, moveAmount, 0);
                break;
            case Direction.Down:
                moveVector = new Vector3(0, -1 * moveAmount, 0);
                break;
        }

        startPosition = this.transform.localPosition;
    }

    public override void Show()
    {
        LeanTween.moveLocal(this.gameObject, startPosition, duration).setEase(easeType);
    }

    public override void Hide()
    {
        LeanTween.moveLocal(this.gameObject, startPosition + moveVector, duration);
    }

    public override void SetOff()
    {
        LeanTween.moveLocal(this.gameObject, startPosition + moveVector, 0);
    }
}
