﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.InputSystem;

public class TileSelectIndicator : MonoBehaviour
{
    #region Fields
    public event EventHandler<InfoEventArgs<BoardTile>> SelectEvent;
    public event EventHandler<InfoEventArgs<BoardTile>> ClickEvent;

    [SerializeField] PlayerInput playerInput;

    public BoardTile currentTile { get; private set; }
    private Board board;

    AudioClip moveSelectionSound { get { return MasterSingleton.main.SFXLibrary.Get(4); } }
    #endregion

    #region Eventhandlers
    void OnMove(InputAction.CallbackContext obj)
    {
        Vector2Int vec = new Vector2Int((int)obj.ReadValue<Vector2>().x, (int)obj.ReadValue<Vector2>().y);
        SelectTile(vec + currentTile.xy);
    }

    void OnPoint(InputAction.CallbackContext obj)
    {
        Vector2 vec = obj.ReadValue<Vector2>();
        SelectTileAtScreenPosition(vec);
    }

    void OnClick(InputAction.CallbackContext obj)
    {
        if (ClickEvent != null)
            ClickEvent(this, new InfoEventArgs<BoardTile>(currentTile));
    }
    #endregion

    #region LifeCycle
    void Start()
    {
        LeanTween.alpha(gameObject, 0f, 0f);
    }
    #endregion

    #region Public
    public void Show()
    {
        LeanTween.alpha(gameObject, 1f, 0.1f);

        playerInput.currentActionMap.FindAction("Navigate").performed += OnMove;
        playerInput.currentActionMap.FindAction("Point").performed += OnPoint;
        playerInput.currentActionMap.FindAction("Submit").performed += OnClick;
        playerInput.currentActionMap.FindAction("Click").performed += OnClick;
    }

    public void Hide()
    {
        LeanTween.alpha(gameObject, 0f, 0.1f);

        playerInput.currentActionMap.FindAction("Navigate").performed -= OnMove;
        playerInput.currentActionMap.FindAction("Point").performed -= OnPoint;
        playerInput.currentActionMap.FindAction("Submit").performed -= OnClick;
        playerInput.currentActionMap.FindAction("Click").performed -= OnClick;
    }

    public void SetBoard(Board board)
    {
        this.board = board;
        SelectTile(Vector2Int.zero);
    }

    public void SelectTile(Vector2Int p)
    {
        if (currentTile != null && (board == null || currentTile.xy == p || !board.HasTile(p)))
            return;

        currentTile = board.GetTile(p);

        Vector3 worldPosition = currentTile.GetWorldPosition();
        worldPosition.z += 1f;
        this.transform.position = worldPosition;

        // Event
        if (SelectEvent != null)
            SelectEvent(this, new InfoEventArgs<BoardTile>(currentTile));
    }
    #endregion

    #region Private
    void SelectTileAtScreenPosition(Vector2 screenPos)
    {
        if (board == null)
            return;

        var worldPos = Camera.main.ScreenToWorldPoint(screenPos);

        for (worldPos.z = board.max.z;  worldPos.z >= 0; worldPos.z--)
        {
            Vector3Int checkPos = board.WorldToCell(worldPos);
            checkPos.x -= 1;
            checkPos.y -= 1;

            if (board.HasTile(checkPos))
            {
                // Don't reselect tile if already current
                if (currentTile == board.GetTile(new Vector2Int(checkPos.x, checkPos.y)))
                    return;

                Vector3 indicatorPos = board.CellToWorld(checkPos);
                indicatorPos.z += 2.2f;
                indicatorPos.y += .5f;

                currentTile = board.GetTile(checkPos.x, checkPos.y);
                this.transform.position = indicatorPos;

                // Play Audio cue
                MasterSingleton.main.AudioPlayer.Play(moveSelectionSound);

                // Event
                if (SelectEvent != null)
                    SelectEvent(this, new InfoEventArgs<BoardTile>(currentTile));

                return;
            }
        }
    }
    #endregion
}
