﻿using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

public class MissionEditor : OdinMenuEditorWindow
{
    [MenuItem("Tools/Mission Data")]
    private static void OpenWindow()
    {
        GetWindow<MissionEditor>().Show();
    }

    private CreateNewMissionData createNewMissionData;

    protected override void OnDestroy()
    {
        base.OnDestroy();

        if (createNewMissionData != null)
            DestroyImmediate(createNewMissionData.mission);
    }

    protected override OdinMenuTree BuildMenuTree()
    {
        var tree = new OdinMenuTree();

        createNewMissionData = new CreateNewMissionData();
        tree.Add("Create New Mission", createNewMissionData);
        tree.AddAllAssetsAtPath("Missions", "Assets/missions/Resources", typeof(MissionData));
        tree.SortMenuItemsByName(true);

        return tree;
    }

    protected override void OnBeginDrawEditors()
    {
        //gets reference to currently selected item
        OdinMenuTreeSelection selected = this.MenuTree.Selection;

        SirenixEditorGUI.BeginHorizontalToolbar();
        {
            GUILayout.FlexibleSpace();

            if (SirenixEditorGUI.ToolbarButton("Delete Current"))
            {
                MissionData asset = selected.SelectedValue as MissionData;
                string path = AssetDatabase.GetAssetPath(asset);
                AssetDatabase.DeleteAsset(path);
                AssetDatabase.SaveAssets();
            }
        }
        SirenixEditorGUI.EndHorizontalToolbar();
    }

    public class CreateNewMissionData
    {
        public CreateNewMissionData()
        {
            mission = ScriptableObject.CreateInstance<MissionData>();
        }

        [LabelWidth(100)]
        [GUIColor(1f, 1f, 0f)]
        public string assetName;
        [InlineEditor(ObjectFieldMode = InlineEditorObjectFieldModes.Hidden)]
        public MissionData mission;

        [Button("Add New Mission SO")]
        private void CreateNewData()
        {
            AssetDatabase.CreateAsset(mission, "Assets/missions/Resources/" + assetName + ".asset");
            AssetDatabase.SaveAssets();

            mission = ScriptableObject.CreateInstance<MissionData>();
        }
    }
}
