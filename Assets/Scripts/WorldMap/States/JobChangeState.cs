﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class JobChangeState : WorldMapState
{
    Unit unit { get { return owner.menuUnit; } }

    #region Eventhandler
    void OnClick(object sender, InfoEventArgs<Job> e)
    {
        Job selectedJob = e.info;
        if (!selectedJob.PrereqsMet(unit))
        {
            audioPlayer.Play(owner.cancelSFX);
            return;
        }

        if (owner.isPrimarySlot)
        {
            // Swap secondary and primary jobs if selected job is already your secondary
            if (unit.jobs.secondary == selectedJob)
                unit.jobs.SetSecondary(unit.jobs.primary);

            unit.jobs.SetPrimary(selectedJob);

            // Remove equipment incompatible with new job
            unit.equipment.UnEquipIncompatibleItems();

            owner.ChangeState<JobChangeEquipState>();
        }
        else
        {

            if (unit.jobs.primary == selectedJob)
            {
                // Cannot select your current primary job as secondary if no secondary job is set
                // Swap secondary and primary jobs if selected job is already your primary
                if (unit.jobs.secondary != null)
                {
                    unit.jobs.SetPrimary(unit.jobs.secondary);

                    // Remove equipment incompatible with new job
                    unit.equipment.UnEquipIncompatibleItems();

                    owner.ChangeState<JobChangeEquipState>();
                }
            }
            else
            {
                owner.ChangeState<JobManageState>();
            }

            unit.jobs.SetSecondary(selectedJob);
            jobChangeController.Hide();
        }

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnSelect(object sender, InfoEventArgs<DescribableObject> e)
    {
        descriptionBox.SetToDescribable(e.info);
        descriptionBox.Show();

        audioPlayer.Play(owner.selectSFX);
    }


    void OnCancel(InputAction.CallbackContext obj)
    {
        jobChangeController.Hide();
        owner.ChangeState<JobManageState>();

        audioPlayer.Play(owner.cancelSFX);
    }
    #endregion

    #region LifeCycle
    public override void Enter()
    {
        jobChangeController.SetToUnit(owner.menuUnit);
        jobChangeController.Show();

        if (owner.isPrimarySlot)
            jobChangeController.ShowEquippables();
        else
            jobChangeController.HideEquippables();

        jobChangeController.JobClickEvent += OnClick;
        jobChangeController.DescribableSelectEvent += OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;

        // Show keybinds
        keybindPromptUI.Show();
        keybindPromptUI.SetText(0, "Select");
        keybindPromptUI.SetText(1, "Cancel");
        keybindPromptUI.HidePrompt(2);
    }

    public override void Exit()
    {
        jobChangeController.JobClickEvent -= OnClick;
        jobChangeController.DescribableSelectEvent -= OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;
    }
    #endregion
}
