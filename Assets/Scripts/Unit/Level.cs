﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    #region Notifications
    public const string XPGainedNotification = "Level.XPGainedNotification";
    public const string LevelUpNotification = "Level.LevelUpNotification";
    #endregion

    #region Properties
    [SerializeField]
    Unit unit;

    [System.NonSerialized]
    public int rank;

    [System.NonSerialized]
    public int xp;

    int toGainXP = 0;
    #endregion

    #region Monobehaviour
    void OnEnable()
    {
        this.AddObserver(OnSkillHit, BaseSkillEffect.HitNotification);
    }

    void OnDisable()
    {
        this.RemoveObserver(OnSkillHit, BaseSkillEffect.HitNotification);
    }
    #endregion

    #region Eventhandlers
    void OnSkillHit(object sender, object args)
    {
        var info = args as Info<Unit, Unit, int, Skill>;
        Unit actor = info.arg0;
        Unit target = info.arg1;

        if (unit != actor || target == null)
            return;

        int rankDiff = target.level.rank - rank;
        int xpGain = Mathf.Max(0, 10 + 2 * rankDiff);
        QueueXPGain(xpGain);
    }
    #endregion

    #region Public
    public void GainQueuedXP()
    {
        xp += toGainXP;

        if (xp < 100)
            unit.PostNotification(XPGainedNotification, toGainXP);
        else
            unit.PostNotification(LevelUpNotification, rank);

        toGainXP = 0;

        while (xp > 100)
        {
            xp -= 100;
            rank += 1;
        }
    }

    public void ClearQueuedXP()
    {
        toGainXP = 0;
    }
    #endregion

    #region Private
    void QueueXPGain(int amount)
    {
        toGainXP = Mathf.Max(toGainXP, amount);
    }
    #endregion
}
