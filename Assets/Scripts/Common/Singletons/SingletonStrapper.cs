﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonStrapper : MonoBehaviour
{
    [SerializeField]
    GameObject singletonPrefab;

    void Awake()
    {
        if (MasterSingleton.main != null)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            Instantiate(singletonPrefab);
        }
    }
}
