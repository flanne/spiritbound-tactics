﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundFXCutsceneAction : CutsceneAction
{
    [SerializeField]
    AudioClip soundFXClip;

    public override void Play()
    {
        MasterSingleton.main.AudioPlayer.Play(soundFXClip);
    }
}
