﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;

namespace flanne.UI
{
    public class UnitBasicInfoWidget : UnitWidget
    {
        [SerializeField] Image portrait;
        [SerializeField] Image spriteImage;
        [SerializeField] TMP_Text unitNameTMP;
        [SerializeField] TMP_Text jobNameTMP;
        [SerializeField] TMP_Text levelTMP;
        [SerializeField] TMP_Text xpTMP;
        [SerializeField] HealthBar healthBar;

        [FoldoutGroup("Stats")]
        [SerializeField] TMP_Text patkTMP, matkTMP, pdefTMP, mdefTMP, movTMP, evdTMP;

        public override void SetProperties(UnitProperties properties)
        {
            Unit unit = properties.unit;

            if (spriteImage != null)
                spriteImage.sprite = unit.unitSpriteSheet.GetSprite("f_still");

            if (portrait != null)
            {
                if (unit.uniqueSprite != null)
                    this.portrait.sprite = unit.uniqueSprite.portrait;
                else
                    this.portrait.sprite = unit.jobs.primary.portrait;
            }

            if (unitNameTMP != null)
                unitNameTMP.text = unit.unitName;

            if (jobNameTMP != null)
            {
                jobNameTMP.text = MasterSingleton.main.GameStrings.Get(unit.jobs.primary.nameStringID);

                if (unit.jobs.secondary)
                    jobNameTMP.text += "/" + MasterSingleton.main.GameStrings.Get(unit.jobs.secondary.nameStringID);
            }

            if (levelTMP != null)
                levelTMP.text = unit.level.rank.ToString();

            if (xpTMP != null)
                xpTMP.text = unit.level.xp.ToString();

            if (healthBar != null)
            {
                healthBar.SetMaxHealth(unit.health.MHP);
                healthBar.SetHealth(unit.health.HP, unit.health.SHP);
            }

            SetStats(unit);
        }

        void SetStats(Unit unit)
        {
            Stats stats = unit.stats;

            SetStat(patkTMP, StatTypes.ATK, stats);
            SetStat(matkTMP, StatTypes.MAT, stats);
            SetStat(pdefTMP, StatTypes.DEF, stats);
            SetStat(mdefTMP, StatTypes.MDF, stats);
            SetStat(movTMP,  StatTypes.MOV, stats);
            SetStat(evdTMP,  StatTypes.EVD, stats);
        }

        void SetStat(TMP_Text tmp, StatTypes statType, Stats stats)
        {
            if (tmp != null && stats != null)
                tmp.text = stats[statType].ToString();
        }
    }
}