﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace flanne.UI
{
    public class SelectorImage : MonoBehaviour, ISelectHandler, IDeselectHandler
    {
        [SerializeField] Image img; 

        public void OnSelect(BaseEventData eventData)
        {
            img.enabled = true;
        }

        public void OnDeselect(BaseEventData eventData)
        {
            img.enabled = false;
        }
    }
}