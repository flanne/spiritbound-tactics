﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[HideLabel]
[System.Serializable]
public class AIOptionCondition
{
    [HideLabel]
    [SerializeField] public ConditionType type;

    [Range(0, 1)]
    [HideLabel]
    [ShowIf("@this.type == ConditionType.AllyUnderHPPercent || this.type == ConditionType.SelfUnderHPPercent")]
    public float amount;

    [ShowIf("@this.type == ConditionType.AllyUnderHPPercent")]
    public int range;

    [ShowIf("@this.type == ConditionType.EveryCertainTurns")]
    public int turns;
}

public enum ConditionType
{
    IfAvailable,
    AllyUnderHPPercent,
    SelfUnderHPPercent,
    NoMight,
    EveryCertainTurns
}
