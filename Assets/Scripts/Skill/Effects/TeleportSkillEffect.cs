﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportSkillEffect : BaseSkillEffect
{
	public override int Predict(Unit actor, BoardTile target, int power, Skill skill)
	{
		return 0;
	}

	protected override int OnApply(Unit actor, BoardTile target, int power, Skill skill)
	{
		actor.Place(target);
		actor.MatchToTile();
		return 0;
	}
}
