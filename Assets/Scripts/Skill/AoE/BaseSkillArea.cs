﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class BaseSkillArea
{
    public virtual bool fireLocationOriented { get { return true; } }

    public abstract List<BoardTile> GetTilesInArea(Board board, Vector2Int pos, Unit unit, SkillRangeData skillRange, int horizontal, int vertical, Skill skill);
}