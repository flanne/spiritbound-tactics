﻿using UnityEngine;

[CreateAssetMenu(menuName = "SpiritlinkTactics/Registry/Location Registry")]
public class LocationRegistry : Registry<WorldMapLocation>
{

}