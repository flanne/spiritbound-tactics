﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using flanne.UI;

public class TitleScreenController : StateMachine
{
    [SerializeField] AudioClip _titleScreenMusic;
    public AudioClip confirmSFX;
    public AudioClip selectSFX;
    public AudioClip cancelSFX;

    public flanne.UI.Menu mainMenu;
    public flanne.UI.Menu newGameWarningUI;

    public OptionsController optionsMenu;

    public Panel surveyButton;

    public Panel steamButton;

    void Start()
    {
        // Confine cursor to window
        Cursor.lockState = CursorLockMode.Confined;

        InitPrefs();

        MasterSingleton.main.AudioPlayer.PlayMusic(_titleScreenMusic);

        MasterSingleton.main.Party.DeactivateAll();
        MasterSingleton.main.GameLevel.ClearBoard();

        ChangeState<InitTitleScreenState>();
    }

    void InitPrefs()
    {
        Screen.fullScreen = (PlayerPrefs.GetInt("isFullscreen", 1) == 1);

        MasterSingleton.main.AudioPlayer.MusicVolume = PlayerPrefs.GetFloat("bgmVolume", 0.5f);
        MasterSingleton.main.AudioPlayer.EffectsVolume = PlayerPrefs.GetFloat("sfxVolume", 0.5f);


        int isFullscreen;
        if (Screen.fullScreen)
            isFullscreen = 1;
        else
            isFullscreen = 0;

        PlayerPrefs.SetInt("isFullscreen", isFullscreen);

        PlayerPrefs.SetFloat("bgmVolume", MasterSingleton.main.AudioPlayer.MusicVolume);
        PlayerPrefs.SetFloat("sfxVolume", MasterSingleton.main.AudioPlayer.EffectsVolume);

        optionsMenu.SetToPrefs();
    }
}
