﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetTileException : BaseException
{
    public BoardTile targetTile;

    public TargetTileException(BoardTile targetTile) : base(true)
    {
        this.targetTile = targetTile;
    }
}
