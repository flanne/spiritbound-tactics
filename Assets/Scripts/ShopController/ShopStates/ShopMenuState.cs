﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class ShopMenuState : ShopState
{
    int _index = 0;

    #region Eventhandler
    void OnClick(object sender, InfoEventArgs<int> e)
    {
        switch (e.info)
        {
            case 0:
                buySellMenu.isSelling = false;
                owner.ChangeState<BuySellMenuState>();
                break;
            case 1:
                buySellMenu.isSelling = true;
                owner.ChangeState<BuySellMenuState>();
                break;
            case 2:
                owner.ChangeState<SelectUnitTryMenuState>();
                break;
            case 3:
                HideUI();
                ExitShop();
                break;
        }

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnSelect(object sender, InfoEventArgs<int> e)
    {
        _index = e.info;

        audioPlayer.Play(owner.selectSFX);
    }

    void OnCancel(InputAction.CallbackContext obj)
    {
        HideUI();
        ExitShop();

        audioPlayer.Play(owner.cancelSFX);
    }
    #endregion

    #region LifeCycle
    public override void Enter()
    {
        fundsText.text = MasterSingleton.main.Inventory.money.ToString();
        fundsDisplay.Show();
        mainMenu.Show();
        mainMenu.Select(_index);

        dialogueBox.SetDialogueBox(welcomeDialogueStringID);
        dialogueBox.Show();

        mainMenu.ClickEvent += OnClick;
        mainMenu.SelectEvent += OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;
    }

    public override void Exit()
    {
        HideUI();
    }
    #endregion

    #region Private
    void HideUI()
    {
        mainMenu.Hide();

        dialogueBox.Hide();

        mainMenu.ClickEvent -= OnClick;
        mainMenu.SelectEvent -= OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;
    }

    void ExitShop()
    {
        MasterSingleton.main.AudioPlayer.FadeOutMusic(1f);
        MasterSingleton.main.ScreenFilter.FadeOut(LoadWorldMap);
    }

    void LoadWorldMap()
    {
        SceneManager.LoadScene("WorldMap");
        MasterSingleton.main.ScreenFilter.FadeIn();
    }
    #endregion
}
