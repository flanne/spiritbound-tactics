﻿using UnityEngine;
using System.Collections;

public abstract class HitRate
{
    #region Notifications
    /// <summary>
    /// Includes a toggleable MatchException argument which defaults to false.
    /// </summary>
    public const string AutomaticHitCheckNotification = "HitRate.AutomaticHitCheckNotification";

    /// <summary>
    /// Includes a toggleable MatchException argument which defaults to false.
    /// </summary>
    public const string AutomaticMissCheckNotification = "HitRate.AutomaticMissCheckNotification";

    /// <summary>
    /// Includes an Info argument with three parameters: Attacker (Unit), Defender (Unit), 
    /// and Defender's calculated Evade / Resistance (int).  Status effects which modify Hit Rate
    /// should modify the arg2 parameter.
    /// </summary>
    public const string StatusCheckNotification = "HitRate.StatusCheckNotification";
    #endregion

    #region Properties
    // Skill base chance to hit
    public int baseHitRate = 100;
    public virtual bool IsAngleBased { get { return true; } }

    protected Unit attacker;
    #endregion

    #region Public
    /// <summary>
    /// Returns a value in the range of 0 t0 100 as a percent chance of
    /// an ability succeeding to hit
    /// </summary>
    public int Calculate(Unit actor, BoardTile target, Skill skill)
    {
        if (target.content != null)
            return Calculate(actor, target.content.GetComponent<Unit>(), skill);

        return -1;
    }

    public abstract int Calculate(Unit actor, Unit targetUnit, Skill skill);

    public virtual bool RollForHit(Unit actor, Unit target, Skill skill)
    {
        int roll = UnityEngine.Random.Range(0, 101);
        int chance = Calculate(actor, target, skill);
        return roll <= chance;
    }
    #endregion

    #region Protected
    protected virtual bool AutomaticHit(Unit actor, Unit targetUnit, Skill skill)
    {
        MatchException exc = new MatchException(actor, targetUnit);
        var info = new Info<MatchException, Skill>(exc, skill);
        this.PostNotification(AutomaticHitCheckNotification, info);
        return exc.toggle;
    }

    protected virtual bool AutomaticMiss(Unit actor, Unit targetUnit, Skill skill)
    {
        MatchException exc = new MatchException(actor, targetUnit);
        var info = new Info<MatchException, Skill>(exc, skill);
        this.PostNotification(AutomaticMissCheckNotification, info);
        return exc.toggle;
    }

    protected virtual int AdjustForStatusEffects(Unit actor, Unit targetUnit, int rate)
    {
        Info<Unit, Unit, int> args = new Info<Unit, Unit, int>(actor, targetUnit, rate);
        this.PostNotification(StatusCheckNotification, args);
        return args.arg2;
    }

    protected virtual int Final(int evade)
    {
        return baseHitRate - evade;
    }
    #endregion
}