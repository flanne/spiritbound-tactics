﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicalDamageSkillEffect : DamageSkillEffect
{
    public override int GetAttack(Unit actor, Unit targetUnit)
    {
        return actor.stats[StatTypes.ATK];
    }

    public override int GetDefence(Unit actor, Unit targetUnit)
    {
        return targetUnit.stats[StatTypes.DEF];
    }
}
