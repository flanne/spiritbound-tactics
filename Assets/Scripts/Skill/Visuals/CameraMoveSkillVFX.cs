﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoveSkillVFX : BaseSkillVisual
{
    public TargetType serializedTargetType;
    public override TargetType targetType { get { return serializedTargetType; } }

    private const float speed = 8.0f;
    private const float targetIntensity = 0.5f;

    private CameraRig cameraRig { get { return MasterSingleton.main.MainCameraRig; } }
    private GameObject followObj;

    public override void Exit()
    {
        Destroy(followObj);
    }

    protected override IEnumerator Animate(BoardTile target)
    {
        followObj = new GameObject("Camera follow point");
        followObj.transform.position = target.GetWorldPosition();
        cameraRig.follow = followObj.transform;

        yield return null;

        isPlaying = false;
    }
}
