﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class NameChangeUI : Panel
    {
        [SerializeField] Party party;
        [SerializeField] TMP_Text nameTMP;
        [SerializeField] MenuEntry initialEntry;

        public bool isNameSet { get; private set; }

        [SerializeField] int characterLimit;
        string _nameString;

        public override void Show()
        {
            isNameSet = false;
            base.Show();

            _nameString = "";

            if (initialEntry != null)
                initialEntry.Select();

            Refresh();
        }

        public void OnConfirmPressed()
        {
            if (_nameString != "")
            {
                party.GetByID(IDEnum.MC).unitName = _nameString;

                isNameSet = true;

                Hide();
            }
        }

        public void AddLetter(string s)
        {
            if (_nameString.Length < characterLimit)
            {
                _nameString += s;
                Refresh();
            }
        }

        public void DeleteLetter()
        {
            if (_nameString != null && _nameString.Length != 0)
            {
                _nameString = _nameString.Substring(0, _nameString.Length - 1);
                Refresh();
            }
        }

        void Refresh()
        {
            string displayText = _nameString;
            for (int i = _nameString.Length; i < characterLimit; i++)
                displayText += "_";

            nameTMP.text = displayText;
        }
    }
}
