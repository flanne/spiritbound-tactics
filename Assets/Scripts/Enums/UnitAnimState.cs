﻿using System.Collections;
using System.Collections.Generic;

public enum UnitAnimState
{
    Idle     = 0,
    Walk     = 1,
    Attack   = 2,
    Cast     = 3,
    Bow      = 4,
    Spear    = 5,
    Hurt     = 6,
    Weak     = 7, // defunct
    Still    = 8,
    Jump     = 9,
    Throw    = 10,
    Special1 = 11,
    Special2 = 12,
    Special3 = 13,
    Special4 = 14
}
