﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StatusOnBackstabPassive<T> : StatusOnHitPassive<T> where T : StatusEffect
{
    // Check if is a backstab
    protected override bool ConditionsMet(object sender, Info<Unit, Unit, int, Skill> info)
    {
        Unit actor = info.arg0;
        Unit target = info.arg1;
        Skill skill = info.arg3;

        // Not backstab if no target unit
        if (target == null)
            return false;

        // Backstab are weapon attacks from back side
        return actor.GetFacing(target) == Facings.Back 
            && sender is PhysicalDamageSkillEffect 
            && skill.ContainsTag(SkillTag.Weapon);
    }
}
