﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spiritlink.Battle;

public class ReaperPassive : Passive
{
    protected override void AddListeners()
    {
        this.AddObserver(OnKill, PerformSkillState.KillNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnKill, PerformSkillState.KillNotification);
    }

    void OnKill(object sender, object args)
    {
        var info = args as Info<Unit>;
        Unit unit = info.arg0;

        unit.stats[StatTypes.CTR] = 0;

        TurnStatusCondition condition = unit.status.Add<TimelessStatusEffect, TurnStatusCondition>();
        condition.turns = 1;
    }
}
