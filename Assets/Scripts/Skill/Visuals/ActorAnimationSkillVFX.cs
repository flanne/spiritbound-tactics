﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorAnimationSkillVFX : BaseSkillVisual
{
    public override TargetType targetType { get { return TargetType.Actor; } }

    public enum ActionType
    {
        WeaponAttack,
        SpellCast,
        Still
    }

    public ActionType actionType;
    private Unit unit;

    public override void Exit()
    {
        unit.SetAnimationState(UnitAnimState.Idle);
    }

    protected override IEnumerator Animate(BoardTile target)
    {
        if (target.content == null || target.content.GetComponent<Unit>() == null)
        {
            Debug.Log("No unit found at target tile.");
            yield break;
        }

        unit = target.content.GetComponent<Unit>();
        UnitAnimState anim;

        if (actionType == ActionType.SpellCast)
        {
            anim = UnitAnimState.Cast;
        }
        else if (actionType == ActionType.Still)
        {
            anim = UnitAnimState.Still;
        }
        else 
        {
            Weapon weapon = null;
            if (unit.equipment.GetItem(EquipSlots.Primary) != null)
                weapon = unit.equipment.GetItem(EquipSlots.Primary) as Weapon;
            else if (unit.equipment.GetItem(EquipSlots.Secondary) != null)
                weapon = unit.equipment.GetItem(EquipSlots.Secondary) as Weapon;


            if (weapon == null)
            {
                anim = UnitAnimState.Attack;
            }
            else
            {
                if (weapon.weaponType == WeaponType.Bow)
                    anim = UnitAnimState.Bow;
                else if (weapon.weaponType == WeaponType.Spear)
                    anim = UnitAnimState.Spear;
                else
                    anim = UnitAnimState.Attack;
            }
        }

        unit.SetAnimationState(anim);

        while (unit.AnimatorIsPlaying() && actionType != ActionType.Still)
            yield return null;

        isPlaying = false;
    }
}
