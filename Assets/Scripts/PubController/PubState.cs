﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

namespace Spiritlink.Pub
{
    public abstract class PubState : State
    {
        protected PubController owner;

        protected PlayerInput playerInput { get { return owner.playerInput; } }

        protected DescribableMenu missionSelectMenu { get { return owner.missionSelectMenu; } }
        protected DescribableFullWidget questDescription { get { return owner.questDescription; } }
        protected flanne.UI.Menu confirmMenu { get { return owner.confirmMenu; } }

        public AudioPlayer audioPlayer { get { return MasterSingleton.main.AudioPlayer; } }

        void Awake()
        {
            owner = GetComponent<PubController>();
        }
    }
}
