﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spiritlink.Battle;

public class BurnStatusEffect : StatusEffect
{
    protected override string nameID { get { return "KEYWORD_BURN"; } }
    public override bool isBuff { get { return false; } }
    public override string spriteFileName { get { return "icon_sts_burn"; } }
    public override Keyword keyword { get { return Resources.Load<Keyword>("Burn"); } }

    Skill burnSkill { get { return Resources.Load<Skill>("sk_burn"); } }

    float multiplier = 0.5f;

    protected override void OnApply()
    {
        this.AddObserver(OnNewTurn, BeforeTurnState.StartOfTurnEffectsNotification, owner);
        this.AddObserver(OnHeal, BaseSkillEffect.TweakHealNotification);
    }

    protected override void OnRemove()
    {
        this.RemoveObserver(OnNewTurn, BeforeTurnState.StartOfTurnEffectsNotification, owner);
        this.RemoveObserver(OnHeal, BaseSkillEffect.TweakHealNotification);
    }

    void OnNewTurn(object sender, object args)
    {
        var effects = args as List<SkillDirections>;
        effects.Add(new SkillDirections(burnSkill, owner, owner.tile));
    }

    void OnHeal(object sender, object args)
    {
        var info = args as Info<List<ValueModifier>, Info<Unit, Unit, Skill>>;
        var targetUnit = info.arg1.arg1;
        var mods = info.arg0;
        
        if (targetUnit == owner)
            mods.Add(new MultValueModifier(3, multiplier));
    }
}
