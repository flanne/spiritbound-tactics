﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddBleedOnHitFeature : AddPassiveFeature<BleedOnHitPassive>
{
    [SerializeField]
    [Range(0,1)]
    float chanceToHit;

    [SerializeField]
    int turnDuration;

    protected override void SetParameters(BleedOnHitPassive passive)
    {
        passive.chanceToHit = this.chanceToHit;
        passive.turnDuration = this.turnDuration;
    }
}
