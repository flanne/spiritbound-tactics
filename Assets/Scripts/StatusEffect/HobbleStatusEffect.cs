﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HobbleStatusEffect : StatusEffect
{
    protected override string nameID { get { return "KEYWORD_HOBBLE"; } }
    public override bool isBuff { get { return false; } }
    public override string spriteFileName { get { return "icon_sts_hobble"; } }
    public override Keyword keyword { get { return Resources.Load<Keyword>("Hobble"); } }

    protected override void OnApply()
    {
        owner.stats[StatTypes.MOV] -= 2;
    }

    protected override void OnRemove()
    {
        owner.stats[StatTypes.MOV] += 2;
    }
}
