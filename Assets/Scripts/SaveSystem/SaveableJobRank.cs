﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveableJobRank
{
    public string jobGuid;
    public int rank;
    public int xp;
    public AbilityTree abilityTree;
}
