﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class DialogueUI : Panel
    {
        public event EventHandler finishedEvent;

        [SerializeField] PlayerInput playerInput;
        [SerializeField] Image portrait;
        [SerializeField] TMP_Text unitName;
        [SerializeField] TMP_Text dialogueText;

        const float timeBetweenShowChar = 0.01f;
        bool isPlaying = true;
        string dialogueString;
        IEnumerator showDialogueCoroutine;

        // Dialogue box needs to be set before Show()
        public void SetDialogueBox(Unit unit, string dialogueStringID)
        {
            if (unit.GetComponent<UniqueSprite>() != null)
                this.portrait.sprite = unit.uniqueSprite.portrait;
            else
                this.portrait.sprite = unit.jobs.primary.portrait;

            unitName.text = unit.unitName;
            dialogueString = MasterSingleton.main.GameStrings.Get(dialogueStringID);
        }

        // Dialogue box needs to be set before Show()
        public void SetDialogueBox(string dialogueStringID)
        {
            dialogueString = MasterSingleton.main.GameStrings.Get(dialogueStringID);
        }

        public override void Show()
        {
            base.Show();
            playerInput.currentActionMap.FindAction("AnyButton").canceled += OnAnyKey;

            showDialogueCoroutine = ShowDialogueText();
            StartCoroutine(showDialogueCoroutine);
        }

        public override void Hide()
        {
            base.Hide();
            playerInput.currentActionMap.FindAction("AnyButton").canceled -= OnAnyKey;
        }

        void OnAnyKey(InputAction.CallbackContext obj)
        {
            if (isPlaying)
            {
                isPlaying = false;
                StopCoroutine(showDialogueCoroutine);
                dialogueText.text = dialogueString;
                MasterSingleton.main.AudioPlayer.StopDialogue();
            }
            else if (finishedEvent != null)
                finishedEvent(this, EventArgs.Empty);
        }

        IEnumerator ShowDialogueText()
        {
            MasterSingleton.main.AudioPlayer.PlayDialogue();
            isPlaying = true;

            for (int i = 0; i <= dialogueString.Length; i++)
            {
                // add next character
                dialogueText.text = dialogueString.Substring(0, i);

                yield return new WaitForSeconds(timeBetweenShowChar);
            }

            MasterSingleton.main.AudioPlayer.StopDialogue();
            isPlaying = false;
        }
    }
}
