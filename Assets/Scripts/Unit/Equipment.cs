﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class Equipment : MonoBehaviour
{
	#region Notifications
	public const string EquippedNotification = "Equipment.EquippedNotification";
	public const string UnEquippedNotification = "Equipment.UnEquippedNotification";
	#endregion

	#region Fields / Properties

	public Unit unit { get { return _unit; } }
	[SerializeField] Unit _unit;

	[SerializeField] Sprite[] defaultWeaponSpriteSheet;

	Dictionary<EquipSlots, Equippable> items = new Dictionary<EquipSlots, Equippable>();
	Dictionary<EquipSlots, EquipSlots> occupied = new Dictionary<EquipSlots, EquipSlots>();

	public bool extraConsumable = false;
	#endregion

	#region Public
	public bool Equip(Equippable item, EquipSlots slots, bool notify = true)
	{
		if (!CanEquip(item))
			return false;

		EquipSlots equipSlots = EquipSlots.None;

		// Check if equiped to primary or secondary slots
		if ((item.defaultSlots & slots) == slots)
		{
			equipSlots = item.defaultSlots;
		}
		else
		{
			foreach(var s in item.secondarySlots)
				if ((s & slots) == slots)
					equipSlots = s;
		}

		if (equipSlots != EquipSlots.None)
		{
			UnEquip(slots, notify);
			items.Add(slots, item);

			// Occupy secondary slots
			foreach (var es in equipSlots.GetFlags())
			{
				if ((EquipSlots)es != slots)
                {
					UnEquip((EquipSlots)es, notify);
					occupied.Add((EquipSlots)es, slots);
				}
			}
			
			item.OnEquip(unit);

			if (notify)
				this.PostNotification(EquippedNotification, item);
		}

		return true;
	}

	public void UnEquip(EquipSlots slots, bool notify = true)
	{
		if (items.ContainsKey(slots))
        {
			Equippable item = items[slots];
			items.Remove(slots);

			item.OnUnEquip(unit);

			if (notify)
				this.PostNotification(UnEquippedNotification, item);

			// Remove from occupying other slots
			List<EquipSlots> slotsToUnoccupy = new List<EquipSlots>();
			foreach (var e in occupied)
            {
				if (e.Value == slots)
					slotsToUnoccupy.Add(e.Key);
			}

			foreach (var s in slotsToUnoccupy)
				occupied.Remove(s);
		}
		else if (occupied.ContainsKey(slots))
        {
			UnEquip(occupied[slots]);
        }

		EmptyHandedCHeck();
	}

	public void UnEquipIncompatibleItems()
    {
		List<EquipSlots> slotsToRemove = new List<EquipSlots>();
		foreach(var i in items)
        {
			if (!CanEquip(i.Value))
				slotsToRemove.Add(i.Key);
        }

		foreach(var slot in slotsToRemove)
			UnEquip(slot);
    }

	public Equippable GetItem(EquipSlots slots)
	{
		if (!items.ContainsKey(slots))
			return null;

		return items[slots];
	}

	public Equippable GetOccupyingItem(EquipSlots slots)
	{
		if (!occupied.ContainsKey(slots))
			return null;

		return items[occupied[slots]];
	}

	public Equippable[] GetAllItems()
    {
		List<Equippable> equippables = new List<Equippable>();
		foreach (var e in items)
			equippables.Add(e.Value);

		return equippables.ToArray();
	}

	public bool CanEquip(Equippable equippable)
    {
		if (equippable is Weapon)
        {
			Weapon weapon = equippable as Weapon;
            foreach(var usabkeWeaponType in unit.jobs.primary.usableWeapons)
            {
				if (weapon.weaponType == usabkeWeaponType)
					return true;
			}

			return false;
		}
		else if (equippable is Armor)
        {
			Armor armor = equippable as Armor;
			foreach (var usableArmorType in unit.jobs.primary.usableArmors)
			{
				if (armor.armorType == usableArmorType)
					return true;
			}

			return false;
        }
		else if (equippable is Accessory)
        {
			return unit.jobs.primary.useAccessories;
		}
		else if (equippable is Consumable)
		{
			return unit.jobs.primary.useConsumables;
		}
		else if (equippable is OffHand)
		{
			return unit.jobs.primary.useOffhands;
		}

		return false;
    }

	void EmptyHandedCHeck()
    {
		if (GetItem(EquipSlots.Primary) == null && GetItem(EquipSlots.Secondary) == null )
			unit.weaponSpriteSheet?.LoadSpriteSheet(defaultWeaponSpriteSheet);
	}
	#endregion
}