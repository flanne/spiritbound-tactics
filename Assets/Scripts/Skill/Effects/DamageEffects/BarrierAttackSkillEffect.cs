﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierAttackSkillEffect : DamageSkillEffect
{
    public override int GetAttack(Unit actor, Unit targetUnit)
    {
        return actor.health.SHP;
    }

    public override int GetDefence(Unit actor, Unit targetUnit)
    {
        return 0;
    }

	protected override int OnApply(Unit actor, BoardTile target, int power, Skill skill)
	{
        int value = base.OnApply(actor, target, power, skill);
        actor.health.HP -= actor.health.SHP;
		return value;
	}
}
