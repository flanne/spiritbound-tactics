﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ToBattleAnimState : WorldMapState
{
    public override void Enter()
    {
        // Hide keybinds
        keybindPromptUI.Hide();


        StartCoroutine(PlayToBattleAnimation());
    }

    IEnumerator PlayToBattleAnimation()
    {
        MasterSingleton.main.AudioPlayer.FadeOutMusic(1f);

        MasterSingleton.main.AudioPlayer.Play(owner.toBattleSound);

        toBattleAnimator.transform.parent.gameObject.SetActive(true);

        while (toBattleAnimator.GetCurrentAnimatorStateInfo(0).length > toBattleAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime)
            yield return null;

        toBattleTextGraphic.Show();

        yield return new WaitForSeconds(0.5f);

        MasterSingleton.main.SceneTransition.LoadScene("Battle");
    }
}
