﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemStack 
{
    public Equippable item;
    public uint total;
    public uint inUse;

    public ItemStack(Equippable item, uint amount = 1)
    {
        this.item = item;
        this.total = amount;
    }

    public bool Equals(ItemStack other)
    {
        if (other == null) return false;
        return (this.item.Equals(other.item));
    }
}
