﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class InitNewGameState : TitleScreenState
{
    public override void Enter()
    {
        InitNewGame();
    }

    void InitNewGame()
    {
        MasterSingleton.main.MissionLog.InitNew();
        MasterSingleton.main.Inventory.InitNew();
        MasterSingleton.main.Party.InitNewParty();
        MasterSingleton.main.WorldMapData.InitNew();
        MasterSingleton.main.TutorialShown.InitNew();
        MasterSingleton.main.SceneTransition.LoadScene("Battle");
    }
}
