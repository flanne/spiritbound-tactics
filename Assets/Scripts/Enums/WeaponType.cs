﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponType
{
	Dagger,
	Sword,
	Axe,
	Spear,
	Bow,
	Staff,
	Monster
}
