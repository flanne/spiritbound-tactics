﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddManaModFeature : AddPassiveFeature<ManaCostModPassive>
{
    [SerializeField]
    int amount;

    protected override void SetParameters(ManaCostModPassive passive)
    {
        passive.amount = this.amount;
    }
}
