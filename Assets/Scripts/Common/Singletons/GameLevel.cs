﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLevel : MonoBehaviour
{
    public Board board { get; private set; }

    public void Load(Board boardPrefab)
    {
        if (board != null)
            ClearBoard();

        board = Instantiate(boardPrefab.gameObject).GetComponent<Board>();

        board.transform.SetParent(this.transform);
    }

    public void ClearBoard()
    {
        if (board)
            Destroy(board?.gameObject);
    }
}
