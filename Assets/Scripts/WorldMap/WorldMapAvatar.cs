﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldMapAvatar : MonoBehaviour
{
    [SerializeField]
    Animator animator;

    [SerializeField]
    float MoveSpeed = 1f;

    [System.NonSerialized]
    public WorldMapNode node;

    public IEnumerator MoveTo(WorldMapNode to)
    {
        // Determine which way the avatar should face
        var moveVector = to.transform.position - this.transform.position;
        if (moveVector.x > 0)
            this.transform.localScale = new Vector2(-1, 1);
        else
            this.transform.localScale = new Vector2(1, 1);

        if (moveVector.y > 0)
            animator.SetBool("FacingFront", false);
        else
            animator.SetBool("FacingFront", true);

        // Move avatar
        var destination = to.transform.position;
        destination.y += 0.15f;
        int id = LeanTween.move(this.gameObject, destination, 1/MoveSpeed).id;

        while (LeanTween.isTweening(id))
            yield return null;

        node = to;
        MasterSingleton.main.WorldMapData.currentLocation = node.level;
    }
}
