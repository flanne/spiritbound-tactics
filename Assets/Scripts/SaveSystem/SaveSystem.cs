﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveSystem : MonoBehaviour
{
    #region Fields
    [SerializeField] JobRegistry jobRegistry;
    [SerializeField] ModelRegistry modelRegistry;
    [SerializeField] EquipRegistry equipRegistry;
    [SerializeField] MissionRegistry missionRegistry;
    [SerializeField] LocationRegistry locationRegistry;

    [SerializeField] Party party;
    [SerializeField] Inventory inventory;
    [SerializeField] MissionLog missionLog;
    [SerializeField] WorldMapData worldMapData;
    [SerializeField] TutorialFlags tutorialFlags;

    int saveSlot;
    #endregion

    #region Public
    public void Save()
    {
        SaveData data = GetData();
        string json = JsonUtility.ToJson(data);
        WriteToFile(GetSaveFileName(saveSlot), json);
    }

    public void Load(int slot)
    {
        SaveData data = new SaveData();
        saveSlot = slot;
        string json = ReadFromFile(GetSaveFileName(saveSlot));
        JsonUtility.FromJsonOverwrite(json, data);

        // Load from data
        party.LoadParty(data.GetParty(jobRegistry, modelRegistry, equipRegistry));

        inventory.SetInventory(data.GetInventoryItems(equipRegistry), data.money);
        worldMapData.SetWorldMap(data.GetUnlockedLocations(locationRegistry), data.GetCurrentLocation(locationRegistry));

        missionLog.SetMissions(data.GetCompletedMission(missionRegistry), data.GetActiveMission(missionRegistry));
        tutorialFlags.LoadFromSave(data);
    }

    public bool SaveSlotExists(int slot) 
    {
        return System.IO.File.Exists(GetFilePath(GetSaveFileName(saveSlot)));
    }
    #endregion

    #region Private
    void WriteToFile(string fileName, string json)
    {
        string filePath = GetFilePath(fileName);
        FileStream fileStream = new FileStream(filePath, FileMode.Create);

        using (StreamWriter writer = new StreamWriter(fileStream))
        {
            writer.Write(json);
        }
    }

    string ReadFromFile(string fileName)
    {
        string filePath = GetFilePath(fileName);
        if (File.Exists(filePath))
        {
            using (StreamReader reader = new StreamReader(filePath))
            {
                string json = reader.ReadToEnd();
                return json;
            }
        }
        else
            Debug.LogWarning("File, " + fileName + ", not found!");

        return "";
    }

    string GetFilePath(string fileName)
    {
        return Application.persistentDataPath + "/" + fileName;
    }

    SaveData GetData()
    {
        SaveData data = new SaveData();

        data.SetParty(party.myParty);
        data.SetInventory(inventory);
        data.SetWorldMap(worldMapData);
        data.SetMissions(missionLog);
        data.SetTutorialFlags(tutorialFlags);

        return data;
    }

    string GetSaveFileName(int slot)
    {
        return "v3_save_" + slot.ToString() + ".txt";
    }
    #endregion
}
