﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class LanguageParser
{
	[MenuItem("Pre Production/Parse Localization Files")]
	public static void Parse()
	{
		CreateDirectories();
		ParseLanguageFile();
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
	}

	static void CreateDirectories()
	{
		if (!AssetDatabase.IsValidFolder("Assets"))
			AssetDatabase.CreateFolder("Assets", "Localization/Resources");
	}

	static void ParseLanguageFile()
	{
		string readPath = string.Format("{0}/Settings/LocalizedStrings.tsv", Application.dataPath);
		string[] readText = File.ReadAllLines(readPath);

		string[] languageNames = readText[1].Split('\t');
		for (int i = 2; i < languageNames.Length; i++)
			ParseStrings(readText, i, languageNames[i]);
	}

	static void ParseStrings(string[] readText, int index, string languageName)
	{
		Language languageAsset = ScriptableObject.CreateInstance<Language>();

		languageAsset.keys = new string[readText.Length - 2];
		languageAsset.strings = new string[readText.Length - 2];

		for (int i = 2; i < readText.Length; i++)
        {
			string[] elements = readText[i].Split('\t');
			languageAsset.keys[i-2] = elements[0];
			languageAsset.strings[i-2] = elements[index];
		}

		AssetDatabase.CreateAsset(languageAsset, "Assets/Localization/Resources/" + languageName + ".asset");
	}
}
