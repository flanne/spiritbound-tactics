﻿using UnityEngine;
using System.Collections;

public abstract class StatusEffect : MonoBehaviour
{
    public Unit inflictor;

    public string statusName {
        get {
            if (nameID == "")
                return "";
            else
                return MasterSingleton.main.GameStrings.Get(nameID); 
        } 
    }
    protected abstract string nameID { get; }

    public abstract bool isBuff { get; }

    public abstract string spriteFileName { get; }

    public abstract Keyword keyword { get; }

    protected Unit owner;

    void OnEnable()
    {
        owner = GetComponentInParent<Unit>();
        OnApply();
    }

    void OnDisable()
    {
        OnRemove();
    }

    protected virtual void OnApply()
    {

    }

    protected virtual void OnRemove()
    {

    }
}