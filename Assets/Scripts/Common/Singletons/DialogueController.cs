﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using flanne.UI;

public class DialogueController : MonoBehaviour
{
    [SerializeField] DialogueUI leftDialogue;
    [SerializeField] DialogueUI rightDialogue;

    public bool isPlaying { get; private set; }

    void OnEnable()
    {
        leftDialogue.finishedEvent += OnDialogueFinished;
        rightDialogue.finishedEvent += OnDialogueFinished;
    }

    void OnDisable()
    {
        leftDialogue.finishedEvent -= OnDialogueFinished;
        rightDialogue.finishedEvent -= OnDialogueFinished;
    }

    public void Hide()
    {
        leftDialogue.Hide();
        rightDialogue.Hide();
    }

    public void PlayDialogueLeft(Unit unit, string stringID)
    {
        PlayDialogue(leftDialogue, unit, stringID);
    }

    public void PlayDialogueRight(Unit unit, string stringID)
    {
        PlayDialogue(rightDialogue, unit, stringID);
    }

    void PlayDialogue(DialogueUI dialogueUI, Unit unit, string stringID)
    {
        isPlaying = true;

        dialogueUI.SetDialogueBox(unit, stringID);
        dialogueUI.Show();
    }

    void OnDialogueFinished(object sender, System.EventArgs e)
    {
        isPlaying = false;
    }
}
