﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace flanne.UI
{
    public class ObjectiveProperties : IUIProperties
    {
        public string victoryText;
        public string defeatText;

        public ObjectiveProperties(string v, string d)
        {
            victoryText = v;
            defeatText = d;
        }
    }

    public class ObjectiveUIController : Widget<ObjectiveProperties>
    {
        [SerializeField] TMP_Text victoryCondition;
        [SerializeField] TMP_Text defeatCondition;

        public override void SetProperties(ObjectiveProperties properties)
        {
            victoryCondition.text = properties.victoryText;
            defeatCondition.text = properties.defeatText;
        }
    }
}
