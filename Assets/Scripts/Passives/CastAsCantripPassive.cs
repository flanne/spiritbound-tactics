﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastAsCantripPassive : Passive
{
    public SkillTag skillTag;

    protected override void AddListeners()
    {
        this.AddObserver(OnCantripCheck, Skill.CanBeCantripCheck);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnCantripCheck, Skill.CanBeCantripCheck);
    }
    
    void OnCantripCheck(object sender, object args)
    {
        var info = args as Info<Unit, BaseException>;
        Unit actor = info.arg0;

        if (actor != owner)
            return;

        BaseException exc = info.arg1;
        Skill skill = sender as Skill;

        if (skill.ContainsTag(skillTag))
            exc.FlipToggle();
    }
}
