﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using flanne.UI;

public abstract class ShopState : State
{
    #region Fields
    protected ShopController owner;

    protected PlayerInput playerInput { get { return owner.playerInput; } }

    protected DialogueUI dialogueBox { get { return owner.dialogueBox; } }
    protected flanne.UI.Menu mainMenu { get { return owner.mainMenu; } }
    protected BuySellController buySellMenu { get { return owner.buySellMenu; } }
    protected DescriptionBox descriptionBox { get { return owner.descriptionBox; } }
    protected UnitListMenu unitListMenu { get { return owner.unitListMenu; } }
    protected TryEquipController tryEquipController { get { return owner.tryEquipController; } }
    protected ShopMenu tryBuyMenu { get { return owner.tryBuyMenu; } }
    protected EquipmentCompareUI equipCompareUI { get { return owner.equipCompareUI; } }

    protected Panel fundsDisplay { get { return owner.fundsDisplay; } }
    protected TMP_Text fundsText { get { return owner.fundsText; } }
    protected ShopConfirmUI confirmUI { get { return owner.confirmUI; } }
    protected string welcomeDialogueStringID { get { return owner.welcomeDialogueStringID; } }
    
    protected Equippable[] shopItems { get { return owner.shopItems; } }

    public AudioPlayer audioPlayer { get { return MasterSingleton.main.AudioPlayer; } }
    #endregion

    #region Monobehaviour
    protected virtual void Awake()
    {
        owner = GetComponent<ShopController>();
    }
    #endregion
}
