﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddEndOfTurnSkillFeature : AddPassiveFeature<EndOfTurnSkillPassive>
{
    [SerializeField]
    Skill skill;

    protected override void SetParameters(EndOfTurnSkillPassive passive)
    {
        passive.skill = this.skill;
    }
}
