﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Status : MonoBehaviour
{
    [SerializeField]
    Unit unit;

    public const string AddedNotification = "Status.AddedNotification";
    public const string RemovedNotification = "Status.RemovedNotification";
    public const string CanCleanseCheck = "Status.CanCleanseCheck";

    public U Add<T, U>(Unit inflictor = null) where T : StatusEffect where U : StatusCondition
    {
        T effect = GetComponentInChildren<T>();

        if (effect == null)
        {
            effect = gameObject.AddChildComponent<T>();
            effect.inflictor = inflictor;
        }

        unit.PostNotification(AddedNotification, effect);

        return effect.gameObject.AddChildComponent<U>();
    }

    // Remove when conditions expire
    public void Remove(StatusCondition target)
    {
        StatusEffect effect = target.GetComponentInParent<StatusEffect>();

        target.transform.SetParent(null);
        Destroy(target.gameObject);

        StatusCondition condition = effect.GetComponentInChildren<StatusCondition>();
        if (condition == null)
        {
            effect.transform.SetParent(null);
            Destroy(effect.gameObject);
            unit.PostNotification(RemovedNotification, effect);
        }
    }

    // Remove by skill or ability effects
    public void Remove(StatusEffect effect)
    {
        // Post exception to see if any system prevents status removal
        BaseException exc = new BaseException(true);
        this.PostNotification(CanCleanseCheck, new Info<StatusEffect, BaseException>(effect, exc));

        if (exc.toggle == true)
        {
            effect.transform.SetParent(null);
            Destroy(effect.gameObject);
            unit.PostNotification(RemovedNotification, effect);
        }
    }

    // Clear all status
    public void ClearAll()
    {
        var statusEffects = GetComponentsInChildren<StatusEffect>();

        foreach (var effect in statusEffects)
        {
            effect.transform.SetParent(null);
            Destroy(effect.gameObject);
            unit.PostNotification(RemovedNotification, effect);
        }
    }
}