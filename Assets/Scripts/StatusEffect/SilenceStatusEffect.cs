﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SilenceStatusEffect : StatusEffect
{
    protected override string nameID { get { return "KEYWORD_SILENCE"; } }
    public override bool isBuff { get { return false; } }
    public override string spriteFileName { get { return "icon_sts_silence"; } }
    public override Keyword keyword { get { return Resources.Load<Keyword>("Silence"); } }

    protected override void OnApply()
    {
        this.AddObserver(OnCanPerformCheck, Skill.CanPerformCheck);
    }

    protected override void OnRemove()
    {
        this.RemoveObserver(OnCanPerformCheck, Skill.CanPerformCheck);
    }

    void OnCanPerformCheck(object sender, object args)
    {
        var info = args as Info<Unit, BaseException>; // Actor, Exception for whether the unit can cast

        Unit actor = info.arg0;
        if (actor != owner)
            return;

        BaseException exc = info.arg1;
        Skill skill = sender as Skill;

        if (skill.ContainsTag(SkillTag.Spell))
            exc.FlipToggle();
    }
}
