﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spiritlink.Battle;

public class WaitSkillPassive : Passive
{
    public Skill skill;
    bool triggered;

    protected override void AddListeners()
    {
        this.AddObserver(OnWait, EndTurnState.WaitNotification, owner);
        this.AddObserver(OnEndTurn, AfterTurnState.EndOfTurnEffectsNotification, owner);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnWait, EndTurnState.WaitNotification, owner);
        this.RemoveObserver(OnEndTurn, AfterTurnState.EndOfTurnEffectsNotification, owner);
    }

    void OnWait(object sender, object args)
    {
        triggered = true;
    }

    void OnEndTurn(object sender, object args)
    {
        if (triggered)
        {
            var effects = args as List<SkillDirections>;
            effects.Add(new SkillDirections(skill, owner, owner.tile));

            triggered = false;
        }
    }
}
