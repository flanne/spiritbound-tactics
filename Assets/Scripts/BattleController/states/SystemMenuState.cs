﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Spiritlink.Battle
{
    public class SystemMenuState : BattleState
    {
        void OnClick(object sender, InfoEventArgs<int> e)
        {
            if (e.info == 0)
                owner.ChangeState<OptionsMenuState>();
            else if (e.info == 1)
                owner.ChangeState<ExitWarningState>();
            else if (e.info == 2)
                owner.ChangeState<ExploreState>();

            audioPlayer.Play(owner.confirmSFX);
        }

        void OnSelect(object sender, InfoEventArgs<int> e)
        {
            audioPlayer.Play(owner.selectSFX);
        }

        void OnCancel(InputAction.CallbackContext obj)
        {
            owner.ChangeState<ExploreState>();

            audioPlayer.Play(owner.cancelSFX);
        }

        public override void Enter()
        {
            owner.systemMenu.Show();
            owner.systemMenu.Select(0);
            owner.systemMenu.ClickEvent += OnClick;
            owner.systemMenu.SelectEvent += OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed += OnCancel;
        }

        public override void Exit()
        {
            owner.systemMenu.Hide();
            owner.systemMenu.ClickEvent -= OnClick;
            owner.systemMenu.SelectEvent -= OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed -= OnCancel;
        }
    }
}
