﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

public class JobManageState : WorldMapState
{
    #region EventHandlers
    void OnJobClick(object sender, InfoEventArgs<int> e)
    {
        if (e.info == 0)
        {
            owner.isPrimarySlot = true;
            owner.ChangeState<JobChangeState>();
        }
        else if (e.info == 1)
        {
            owner.isPrimarySlot = false;
            owner.ChangeState<JobChangeState>();
        }

        classMenu.interactable = false;

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnAbilityClick(object sender, InfoEventArgs<Info<int, int, AbilityTreeSelection>> e)
    {
        int whichTree = e.info.arg0;
        int rank = e.info.arg1;
        AbilityTreeSelection treeSelection = e.info.arg2;

        JobRank jobRank;
        if (whichTree == 0) // primary tree
            jobRank = owner.menuUnit.jobRanks.Get(owner.menuUnit.jobs.primary);
        else // secondary tree
            jobRank = owner.menuUnit.jobRanks.Get(owner.menuUnit.jobs.secondary);

        owner.menuUnit.jobs.DeactivateFeatures();
        jobRank.SetAbilityTreeSelection(rank, treeSelection);
        owner.menuUnit.jobs.ReactivateFeatures();

        // Refresh
        classMenu.SetToUnit(owner.menuUnit);

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnDescribableSelect(object sender, InfoEventArgs<DescribableObject> e)
    {
        DescribableObject desObj = e.info;

        if (desObj != null)
        {
            descriptionBox.Show();
            descriptionBox.SetToDescribable(desObj, DescriptionBox.Side.Right);
        }
        else
        {
            descriptionBox.Hide();
        }

        audioPlayer.Play(owner.selectSFX);
    }

    void OnCancel(InputAction.CallbackContext obj)
    {
        owner.classMenu.Hide();
        owner.ChangeState<UnitListMenuState>();

        audioPlayer.Play(owner.cancelSFX);
    }
    #endregion

    #region LifeCycle
    public override void Enter()
    {
        classMenu.SetToUnit(owner.menuUnit);

        classMenu.JobClickEvent += OnJobClick;
        classMenu.AbilityClickEvent += OnAbilityClick;
        classMenu.DescribableSelectEvent += OnDescribableSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;

        classMenu.Show();
        classMenu.SelectDefault();

        // Check for tutorial
        if (MasterSingleton.main.TutorialShown.job == false)
        {
            MasterSingleton.main.TutorialShown.job = true;
            StartCoroutine(ShowJobTutorial());
        }
        else if (MasterSingleton.main.TutorialShown.ability == false)
        {
            JobRank primary = owner.menuUnit.jobRanks.Get(owner.menuUnit.jobs.primary);

            JobRank secondary = null;
            if (owner.menuUnit.jobs.secondary != null)
                secondary = owner.menuUnit.jobRanks.Get(owner.menuUnit.jobs.secondary);

            if (primary.rank > 1 || (secondary != null && secondary.rank > 1))
            {
                MasterSingleton.main.TutorialShown.ability = true;
                StartCoroutine(ShowAbilityTutorial());
            }
        }

        // Show keybinds
        keybindPromptUI.Show();
        keybindPromptUI.SetText(0, "Select");
        keybindPromptUI.SetText(1, "Save & Back");
        keybindPromptUI.HidePrompt(2);
    }

    public override void Exit()
    {
        classMenu.JobClickEvent -= OnJobClick;
        classMenu.AbilityClickEvent -= OnAbilityClick;
        classMenu.DescribableSelectEvent -= OnDescribableSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;

        descriptionBox.Hide();
    }
    #endregion

    #region Private
    IEnumerator ShowJobTutorial()
    {
        classMenu.interactable = false;
        yield return null;

        owner.ChangeState<JobTutorialState>();
    }

    IEnumerator ShowAbilityTutorial()
    {
        classMenu.interactable = false;
        yield return null;

        owner.ChangeState<AbilityTutorialState>();
    }
    #endregion
}