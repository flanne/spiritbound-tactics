﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class UnitPlacementState : BattleState
    {
        #region Fields
        Unit selectedUnit;
        List<BoardTile> tiles;
        #endregion

        #region EventHandler
        void OnClick(object sender, InfoEventArgs<BoardTile> e)
        {
            // check if tile is in the unit placement tiles
            if (tiles.Contains(currentTile))
            {
                if (currentTile.content != null)
                {
                    Unit unit = currentTile.content.GetComponent<Unit>();

                    if (unit != null)
                    {
                        audioPlayer.Play(owner.confirmSFX);

                        // Swap units if a unit is already selected
                        if (selectedUnit != null && unit.alliance.type == AllianceType.Hero)
                        {
                            BoardTile tempTile = unit.tile;
                            unit.Place(selectedUnit.tile);

                            selectedUnit.Place(tempTile);

                            unit.MatchToTile();
                            selectedUnit.MatchToTile();

                            Unselect();
                        }
                        // Select unit if no unit already selected
                        else if (unit.alliance.type == AllianceType.Hero)
                        {
                            selectedUnit = unit;
                            selectedUnit.spriteMaterialSwapper.SetToNew();
                        }
                    }
                }
                // place unit on empty tile if selected unit is not null
                else if (selectedUnit != null)
                {
                    audioPlayer.Play(owner.confirmSFX);

                    selectedUnit.Place(currentTile);
                    selectedUnit.MatchToTile();

                    Unselect();
                }
            }
        }

        void OnSelect(object sender, InfoEventArgs<BoardTile> e)
        {
            if (e.info.content != null)
            {
                tileHighlighter.Clear();

                // Rehighlight placement tiles
                tileHighlighter.Highlight(tiles, new Color(1f, 0.3f, 0.15f, 1f));

                Unit unit = e.info.content.GetComponent<Unit>();

                if (unit != null)
                {
                    primaryHUD.SetProperties(new UnitProperties(unit));
                    primaryHUD.Show();
                    if (unit.alliance.type != AllianceType.Hero)
                    {
                        tileHighlighter.Highlight(unit.movement.GetTilesInRange(board), new Color(0.2f, 1f, 0.6f, 1f));
                    }

                    return;
                }
            }

            primaryHUD.Hide();
            tileHighlighter.Clear();

            // Rehighlight placement tiles
            tileHighlighter.Highlight(tiles, new Color(1f, 0.3f, 0.15f, 1f));
        }

        void OnCancel(InputAction.CallbackContext obj)
        {
            // Unselected current unit
            if (selectedUnit != null)
            {
                audioPlayer.Play(owner.cancelSFX);

                Unselect();
            }
            // if no unit selected, show start battle prompt
            else
            {
                owner.ChangeState<PlacementConfirmState>();

                audioPlayer.Play(owner.confirmSFX);
            }
        }
        #endregion

        #region LifeCycle
        public override void Enter()
        {
            // AddListener
            tileSelectIndicator.ClickEvent += OnClick;
            tileSelectIndicator.SelectEvent += OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed += OnCancel;

            selectedUnit = null;

            tiles = new List<BoardTile>();
            foreach (var xy in MasterSingleton.main.MissionLog.current.unitPlacementTiles)
                tiles.Add(board.GetTile(xy));

            tileHighlighter.Highlight(tiles, new Color(1f, 0.3f, 0.15f, 1f));

            tileSelectIndicator.Show();
            tileSelectIndicator.SelectTile(MasterSingleton.main.MissionLog.current.unitPlacementTiles[0]);

            // Show keybinds
            keybindPromptUI.Show();
            keybindPromptUI.SetText(0, "Place Unit");
            keybindPromptUI.SetText(1, "Start Battle");
        }

        public override void Exit()
        {
            // Remove Listeners
            tileSelectIndicator.ClickEvent -= OnClick;
            tileSelectIndicator.SelectEvent -= OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed -= OnCancel;

            primaryHUD.Hide();

            tileSelectIndicator.Hide();
            tileHighlighter.Clear();

            if (!MasterSingleton.main.MissionLog.current.disableBattleSpeed)
                battleSpeedButton.Show();

            // Hide keybinds
            keybindPromptUI.Hide();
        }
        #endregion

        #region Private
        void Unselect()
        {
            if (selectedUnit != null)
            {
                selectedUnit.spriteMaterialSwapper.SetToDefault();
                selectedUnit = null;
            }
        }
        #endregion
    }
}
