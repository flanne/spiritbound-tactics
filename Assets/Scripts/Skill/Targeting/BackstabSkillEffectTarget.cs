﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackstabSkillEffectTarget : SkillEffectTarget
{
    public override bool IsTarget(Unit actor, BoardTile target)
    {
        Stats s = target?.content?.GetComponent<Unit>()?.stats;
        Unit t = target.content?.GetComponent<Unit>();

        return s != null && s[StatTypes.HP] > 0 && t != null && actor.GetFacing(t) == Facings.Back;
    }
}