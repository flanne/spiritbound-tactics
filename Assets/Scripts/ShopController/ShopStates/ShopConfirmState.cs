﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ShopConfirmState : ShopState
{
    void OnSubmit(InputAction.CallbackContext obj)
    {
        Equippable item = confirmUI.selectedItem;

        if (!confirmUI.isSelling)
        {
            if (MasterSingleton.main.Inventory.Buy(item, confirmUI.amount))
            {
                StartCoroutine(WaitToExit());

                // Play sound effect
                AudioClip coinSound = MasterSingleton.main.SFXLibrary.CoinSound;
                MasterSingleton.main.AudioPlayer.Play(coinSound);
            }
            else
            {
                audioPlayer.Play(owner.cancelSFX);
            }
        }
        else
        {
            if (MasterSingleton.main.Inventory.Sell(item, confirmUI.amount))
            {
                StartCoroutine(WaitToExit());

                // Play sound effect
                AudioClip coinSound = MasterSingleton.main.SFXLibrary.CoinSound;
                MasterSingleton.main.AudioPlayer.Play(coinSound);
            }
            else
            {
                audioPlayer.Play(owner.cancelSFX);
            }
        }
    }
    
    void OnNavigate(InputAction.CallbackContext obj)
    {
        if (obj.ReadValue<Vector2>() != Vector2.zero)
            audioPlayer.Play(owner.selectSFX);
    }

    void OnCancel(InputAction.CallbackContext obj)
    {
        StartCoroutine(WaitToExit());

        audioPlayer.Play(owner.cancelSFX);
    }

    public override void Enter()
    {
        confirmUI.Show();
        playerInput.currentActionMap.FindAction("Navigate").performed += OnNavigate;
        playerInput.currentActionMap.FindAction("Submit").performed += OnSubmit;
        playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;
    }

    public override void Exit()
    {
        confirmUI.Hide();

        // Refresh funds display
        fundsText.text = MasterSingleton.main.Inventory.money.ToString();
    }

    IEnumerator WaitToExit()
    {
        playerInput.currentActionMap.FindAction("Navigate").performed -= OnNavigate;
        playerInput.currentActionMap.FindAction("Submit").performed -= OnSubmit;
        playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;

        yield return null;
        owner.ChangeState<BuySellMenuState>();
    }
}
