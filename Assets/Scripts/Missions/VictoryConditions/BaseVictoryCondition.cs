﻿using UnityEngine;
using System.Collections;

namespace Spiritlink.Battle
{
    public abstract class BaseVictoryCondition : MonoBehaviour
    {
        #region Properties
        public bool isVictor { get { return victoryAchieved; } }
        public AllianceType alliance = AllianceType.None;
        protected AllianceType enemyAlliance
        {
            get
            {
                if (alliance == AllianceType.Hero)
                    return AllianceType.Villain;
                else if (alliance == AllianceType.Villain)
                    return AllianceType.Hero;
                else
                    return AllianceType.None;
            }
        }
        protected bool victoryAchieved = false;
        public BattleController bc;
        #endregion

        protected virtual void OnEnable()
        {
            this.AddObserver(OnHPDidChangeNotification, Stats.DidChangeNotification(StatTypes.HP));
        }

        protected virtual void OnDisable()
        {
            this.RemoveObserver(OnHPDidChangeNotification, Stats.DidChangeNotification(StatTypes.HP));
        }

        protected virtual void OnHPDidChangeNotification(object sender, object args)
        {
            CheckForVictory();
        }

        protected virtual bool IsDefeated(Unit unit)
        {
            return unit.IsKnockedOut();
        }

        protected virtual bool PartyDefeated(AllianceType type)
        {
            for (int i = 0; i < bc.units.Count; ++i)
            {
                Alliance a = bc.units[i].alliance;
                if (a == null)
                    continue;
                if (a.type == type && !IsDefeated(bc.units[i]))
                    return false;
            }
            return true;
        }

        protected abstract void CheckForVictory();
    }
}