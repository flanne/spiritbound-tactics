﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spiritlink.Battle
{
    public class PrebattleCutsceneTrigger : CutsceneTrigger
    {
        void Start()
        {
            this.AddObserver(OnBattleBegan, InitBattleState.BattleBeganNotification);
        }

        void OnDestroy()
        {
            this.RemoveObserver(OnBattleBegan, InitBattleState.BattleBeganNotification);
        }

        void OnBattleBegan(object sender, object args)
        {
            Trigger();
            this.RemoveObserver(OnBattleBegan, InitBattleState.BattleBeganNotification);
        }
    }
}
