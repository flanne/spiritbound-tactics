﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoomOnDebuffsPassive : Passive
{
    public int threshold;

    protected override void AddListeners()
    {
        this.AddObserver(OnDebuff, Status.AddedNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnDebuff, Status.AddedNotification);
    }

    void OnDebuff(object sender, object args)
    {
        var effect = args as StatusEffect;
        var defender = sender as Unit;

        if (effect.isBuff || effect is KnockedOutStatusEffect)
            return;

        var statusEffects = defender.status.GetComponentsInChildren<StatusEffect>();
        int debuffCounter = 0;
        foreach (var s in statusEffects)
            if (!s.isBuff)
                debuffCounter++;

        if (debuffCounter >= threshold && 
            defender.alliance.type != owner.alliance.type &&
            defender.status.GetComponentInChildren<DoomStatusEffect>() == null)
            defender.status.Add<DoomStatusEffect, StatusCondition>(owner);
    }
}