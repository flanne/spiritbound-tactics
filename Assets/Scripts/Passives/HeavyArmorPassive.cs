﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeavyArmorPassive : Passive
{
    float reductionMultiplier = 0.80f;

    protected override void AddListeners()
    {
        this.AddObserver(OnDealingDamage, DamageSkillEffect.TweakDamageNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnDealingDamage, DamageSkillEffect.TweakDamageNotification);
    }

    void OnDealingDamage(object sender, object args)
    {
        var info = args as Info<List<ValueModifier>, Info<Unit, Unit, Skill>>;
        Unit targetUnit = info.arg1.arg1;

        if (targetUnit != owner)
            return;

        if (owner.health.SHP > 0)
            info.arg0.Add(new MultValueModifier(3, reductionMultiplier));
    }
}

