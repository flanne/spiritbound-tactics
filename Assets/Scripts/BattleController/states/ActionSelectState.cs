﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class ActionSelectState : BattleState
    {
        #region Eventhandlers
        // Change states depending what was clicked
        void OnClick(object sender, InfoEventArgs<int> e)
        {
            switch (e.info)
            {
                case 0:
                    owner.ChangeState<MoveTargetState>();
                    break;
                case 1:
                    turn.skill = turn.actor.basicAttack;
                    owner.ChangeState<SkillTargetState>();
                    break;
                case 2:
                    owner.ChangeState<SkillSelectState>();
                    break;
                case 3:
                    // check for tutorial
                    if (MasterSingleton.main.TutorialShown.item == false)
                    {
                        owner.ChangeState<ItemTutorialState>();
                        MasterSingleton.main.TutorialShown.item = true;
                    }
                    else
                        owner.ChangeState<ConsumablesMenuState>();
                    break;
                case 4:
                    owner.ChangeState<EndTurnState>();
                    break;
            }

            audioPlayer.Play(owner.confirmSFX);
        }

        // Set the label on which action is selected
        void OnSelect(object sender, InfoEventArgs<int> e)
        {
            string labelString = "";
            switch (e.info)
            {
                case 0:
                    labelString = MasterSingleton.main.GameStrings.Get("GAME_ACTION_MOVE");
                    break;
                case 1:
                    labelString = MasterSingleton.main.GameStrings.Get("GAME_ACTION_ATTACK");
                    break;
                case 2:
                    labelString = MasterSingleton.main.GameStrings.Get("GAME_ACTION_SKILL");
                    break;
                case 3:
                    labelString = MasterSingleton.main.GameStrings.Get("GAME_ACTION_ITEM");
                    break;
                case 4:
                    labelString = MasterSingleton.main.GameStrings.Get("GAME_ACTION_WAIT");
                    break;
            }

            actionMenuLabel.text = labelString;

            audioPlayer.Play(owner.selectSFX);
        }

        // Undos move if moved, else to go explore state
        void OnCancel(InputAction.CallbackContext obj)
        {
            if (!turn.lockMove && turn.hasUnitMoved)
            {
                turn.UndoMove();
                actionMenu.UnLock(0);
                actionMenu.SelectFirstAvailable();

                keybindPromptUI.SetText(1, "Explore");
            }
            else
                owner.ChangeState<ExploreState>();

            audioPlayer.Play(owner.cancelSFX);
        }
        #endregion

        #region LifeCycle
        public override void Enter()
        {
            base.Enter();
            primaryHUD.SetProperties(new UnitProperties(turn.actor));
            primaryHUD.Show();
            manaBar.Show();

            if (driver.Current == DriverType.Computer)
                StartCoroutine(ComputerTurn());
        }

        public override void Exit()
        {
            base.Exit();
            primaryHUD.Hide();
        }

        protected override void OnEnterUI()
        {
            // Add listeners
            actionMenu.ClickEvent += OnClick;
            actionMenu.SelectEvent += OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed += OnCancel;

            // Select curent tile
            tileSelectIndicator.SelectTile(turn.actor.tile.xy);

            // Setup action menu
            actionMenu.UnlockAll();

            // Check if unit has moved
            if (turn.hasUnitMoved)
                actionMenu.Lock(0);

            // Check if unit has acted
            if (turn.hasUnitActed)
            {
                actionMenu.Lock(1);
                actionMenu.Lock(2);
                actionMenu.Lock(3);
            }

            // Select
            actionMenu.SelectFirstAvailable();

            // Check if unit has a consumable equipped
            if (turn.actor.equipment.GetItem(EquipSlots.Consumable1) == null && turn.actor.equipment.GetItem(EquipSlots.Consumable2) == null)
                actionMenu.Lock(3);

            actionMenu.Show();

            // Show keybinds
            keybindPromptUI.Show();
            keybindPromptUI.SetText(0, "Select");
            if (!turn.lockMove && turn.hasUnitMoved)
                keybindPromptUI.SetText(1, "Undo Move");
            else
                keybindPromptUI.SetText(1, "Explore");
        }

        protected override void OnExitUI()
        {
            // Remove listeners
            actionMenu.ClickEvent -= OnClick;
            actionMenu.SelectEvent -= OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed -= OnCancel;

            actionMenu.Hide();
        }
        #endregion

        #region AI
        IEnumerator ComputerTurn()
        {
            if (turn.plan == null)
            {
                turn.plan = owner.cpu.Evaluate();
                turn.skill = turn.plan.skill;
            }

            yield return new WaitForSeconds(0.5f);

            if (turn.hasUnitMoved == false && turn.plan.moveLocation != turn.actor.tile.xy)
                owner.ChangeState<MoveTargetState>();
            else if (turn.hasUnitActed == false && turn.plan.skill != null)
            {
                owner.ChangeState<SkillTargetState>();
            }
            else
                owner.ChangeState<EndTurnState>();
        }
        #endregion
    }
}
