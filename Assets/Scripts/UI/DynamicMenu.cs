﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace flanne.UI
{
    public class DynamicMenu<T> : Menu where T : IUIProperties
    {
        #region Fields
        enum NavType
        {
            Horizontal,
            Vertical,
            Automatic
        }
        [SerializeField] NavType navType;

        [SerializeField] LayoutGroup layout;
        [SerializeField] GameObject entryPrefab;
        #endregion

        #region Public
        public MenuEntry AddEntry(T properties)
        {
            return AddEntry(properties, layout);
        }

        public MenuEntry AddEntry(T properties, LayoutGroup lg)
        {
            GameObject o = Instantiate(entryPrefab, lg.transform);

            // Set Properties
            var widget = o.GetComponent<Widget<T>>();
            widget.SetProperties(properties);

            // Add listeners
            var entry = o.GetComponent<MenuEntry>();
            int closureIndex = entries.Count;
            entry?.onClick.AddListener(() => OnEntryClicked(closureIndex));
            entry?.onSelect.AddListener(() => OnEntrySelected(closureIndex));

            // Set navigation
            if (entry != null)
            {
                if (navType == NavType.Automatic)
                {
                    Navigation nav = entry.navigation;
                    nav.mode = Navigation.Mode.Automatic;
                }
                else
                {
                    Navigation currentNav = entry.navigation;
                    currentNav.mode = Navigation.Mode.Explicit;

                    if (entries.Count > 0)
                    {
                        MenuEntry lastEntry = entries[entries.Count - 1];
                        Navigation lastNav = lastEntry.navigation;

                        if (navType == NavType.Horizontal)
                        {
                            lastNav.selectOnRight = entry;
                            currentNav.selectOnLeft = lastEntry;
                        }
                        else
                        {
                            lastNav.selectOnDown = entry;
                            currentNav.selectOnUp = lastEntry;
                        }
                        lastEntry.navigation = lastNav;
                    }

                    entry.navigation = currentNav;
                }
            }

            // Add entry
            if (entry != null)
                entries.Add(entry);

            return entry;
        }

        public void Clear()
        {
            for (int i = 0; i < entries.Count; i++)
            {
                int closureIndex = i;
                entries[closureIndex].onClick.RemoveListener(() => OnEntryClicked(closureIndex));
                entries[closureIndex].onSelect.RemoveListener(() => OnEntrySelected(closureIndex));

                Destroy(entries[i].gameObject);
            }

            entries.Clear();
        }
        #endregion
    }
}
