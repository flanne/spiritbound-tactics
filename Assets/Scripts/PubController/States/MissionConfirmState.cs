﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Spiritlink.Pub
{
    public class MissionConfirmState : PubState
    {
        #region Eventhandlers
        void OnClick(object sender, InfoEventArgs<int> e)
        {
            if (e.info == 0)
            {
                confirmMenu.Hide();
                MasterSingleton.main.MissionLog.AcceptMission(owner.selectedMission);
                owner.ChangeState<MissionSelectState>();
            }
            else if (e.info == 1)
            {
                owner.ChangeState<MissionSelectState>();
            }

            audioPlayer.Play(owner.confirmSFX);
        }

        void OnSelect(object sender, InfoEventArgs<int> e)
        {
            audioPlayer.Play(owner.selectSFX);
        }

        void OnCancel(InputAction.CallbackContext obj)
        {
            owner.ChangeState<MissionSelectState>();

            audioPlayer.Play(owner.cancelSFX);
        }
        #endregion

        #region LifeCycle
        public override void Enter()
        {
            confirmMenu.Show();
            confirmMenu.Select(0);
            confirmMenu.ClickEvent += OnClick;
            confirmMenu.SelectEvent += OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;
        }

        public override void Exit()
        {
            confirmMenu.Hide();
            confirmMenu.ClickEvent -= OnClick;
            confirmMenu.SelectEvent -= OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;
        }
        #endregion
    }
}
