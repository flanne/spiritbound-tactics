﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddDamageMultiplierFeature : AddPassiveFeature<DamageMultiplierPassive>
{
    [SerializeField]
    DamageTypes damageType;

    [SerializeField]
    float multiplier = 1;

    protected override void SetParameters(DamageMultiplierPassive passive)
    {
        passive.damageType = this.damageType;
        passive.multiplier = this.multiplier;
    }
}
