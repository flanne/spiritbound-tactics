﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spiritlink.Battle;

public class RegenerationStatusEffect : StatusEffect
{
    protected override string nameID { get { return "KEYWORD_SHORT_REGEN"; } }
    public override bool isBuff { get { return true; } }
    public override string spriteFileName { get { return "icon_sts_regen"; } }
    public override Keyword keyword { get { return Resources.Load<Keyword>("Regeneration"); } }

    protected override void OnApply()
    {
        this.AddObserver(OnNewTurn, TurnOrderController.TurnBeganNotification, owner);
    }

    protected override void OnRemove()
    {
        this.RemoveObserver(OnNewTurn, TurnOrderController.TurnBeganNotification, owner);
    }

    void OnNewTurn(object sender, object args)
    {
        Stats stats = owner.stats;
        int maxHP = stats[StatTypes.MHP];
        stats[StatTypes.HP] += Mathf.FloorToInt(maxHP * 0.15f);
    }
}