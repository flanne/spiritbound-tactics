﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alliance : MonoBehaviour
{
    public AllianceType type;

    public bool IsMatch(Alliance other, AITargeting target)
    {
        bool isMatch = false;
        switch (target)
        {
            case AITargeting.Self:
                isMatch = other == this;
                break;
            case AITargeting.Ally:
                isMatch = type == other.type || (type == AllianceType.Ally && other.type == AllianceType.Hero) || (type == AllianceType.Hero && other.type == AllianceType.Ally);
                break;
            case AITargeting.Foe:
                isMatch = type != other.type && !(type == AllianceType.Ally && other.type == AllianceType.Hero) && !(type == AllianceType.Hero && other.type == AllianceType.Ally) && other.type != AllianceType.None;
                break;
            case AITargeting.SelfAndFoe:
                isMatch = other == this || (type != other.type && !(type == AllianceType.Ally && other.type == AllianceType.Hero) && !(type == AllianceType.Hero && other.type == AllianceType.Ally) && other.type != AllianceType.None);
                break;
        }

        return isMatch;
    }
}
