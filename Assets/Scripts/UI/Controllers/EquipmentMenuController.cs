﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace flanne.UI
{
    public class EquipmentMenuController : MonoBehaviour
    {
        #region Fields/Properties
        public event EventHandler<InfoEventArgs<EquipSlots>> SlotSelectEvent;
        public event EventHandler<InfoEventArgs<EquipSlots>> SlotClickEvent;

        [SerializeField] protected Menu equipmentMenu;
        [SerializeField] protected Menu consumableMenu;
        [SerializeField] UnitBasicInfoWidget basicInfoWidget;
        [SerializeField] Panel panel;

        public bool interactable
        {
            get
            {
                return equipmentMenu.interactable;
            }
            set
            {
                equipmentMenu.interactable = value;
                consumableMenu.interactable = value;

                if (panel != null)
                    panel.interactable = value;
            }
        }

        public EquipSlots currentSlot { get; private set; }
        #endregion

        #region EventHandlers
        void OnEquipSelect(object sender, InfoEventArgs<int> e)
        {
            if (SlotSelectEvent != null)
            {
                currentSlot = GetEquipSlot(e.info);
                SlotSelectEvent(this, new InfoEventArgs<EquipSlots>(currentSlot));
            }
        }

        void OnEquipClick(object sender, InfoEventArgs<int> e)
        {
            if (SlotClickEvent != null)
                SlotClickEvent(this, new InfoEventArgs<EquipSlots>(GetEquipSlot(e.info)));
        }

        void OnConsumeSelect(object sender, InfoEventArgs<int> e)
        {
            if (SlotSelectEvent != null)
            {
                currentSlot = GetConsumeSlot(e.info);
                SlotSelectEvent(this, new InfoEventArgs<EquipSlots>(currentSlot));
            }
        }

        void OnConsumeClick(object sender, InfoEventArgs<int> e)
        {
            if (SlotClickEvent != null)
                SlotClickEvent(this, new InfoEventArgs<EquipSlots>(GetConsumeSlot(e.info)));
        }
        #endregion

        #region LifeCycle
        void Awake()
        {
            // Add Listeners
            equipmentMenu.SelectEvent += OnEquipSelect;
            equipmentMenu.ClickEvent += OnEquipClick;
            consumableMenu.SelectEvent += OnConsumeSelect;
            consumableMenu.ClickEvent += OnConsumeClick;
        }

        void OnDestroy()
        {
            // Remove Listeners
            equipmentMenu.SelectEvent -= OnEquipSelect;
            equipmentMenu.ClickEvent -= OnEquipClick;
            consumableMenu.SelectEvent -= OnConsumeSelect;
            consumableMenu.ClickEvent -= OnConsumeClick;
        }
        #endregion

        #region Public
        public void SetToUnit(Unit unit)
        {
            if (basicInfoWidget != null)
                basicInfoWidget.SetProperties(new UnitProperties(unit));

            SetToEquipment(unit.equipment);
        }

        public void SetToEquipment(Equipment equipment)
        {
            // Set all equipment entries
            equipmentMenu.SetProperties<DescribableProperties>(0, new DescribableProperties(equipment.GetItem(EquipSlots.Primary)));
            equipmentMenu.SetProperties<DescribableProperties>(1, new DescribableProperties(equipment.GetItem(EquipSlots.Secondary)));
            equipmentMenu.SetProperties<DescribableProperties>(2, new DescribableProperties(equipment.GetItem(EquipSlots.Body)));
            equipmentMenu.SetProperties<DescribableProperties>(3, new DescribableProperties(equipment.GetItem(EquipSlots.Accessory)));

            consumableMenu.SetProperties<DescribableProperties>(0, new DescribableProperties(equipment.GetItem(EquipSlots.Consumable1)));
            consumableMenu.SetProperties<DescribableProperties>(1, new DescribableProperties(equipment.GetItem(EquipSlots.Consumable2)));

            // See if unit is allowed extra consumable or not
            if (equipment.extraConsumable)
            {
                consumableMenu.SetEntryActive(2, true);
                consumableMenu.SetProperties<DescribableProperties>(2, new DescribableProperties(equipment.GetItem(EquipSlots.Consumable3)));
            }
            else
            {
                consumableMenu.SetEntryActive(2, false);
            }

            // Set occupying slots
            if (equipment.GetOccupyingItem(EquipSlots.Primary) != null)
            {
                equipmentMenu.SetProperties<DescribableProperties>(0, new DescribableProperties(equipment.GetOccupyingItem(EquipSlots.Primary)));
                var widget = equipmentMenu.GetEntry(0).GetComponent<DescribableNamedWidget>();
                if (widget != null)
                    widget.FormatOccupy();
            }

            if (equipment.GetOccupyingItem(EquipSlots.Secondary) != null)
            {
                equipmentMenu.SetProperties<DescribableProperties>(1, new DescribableProperties(equipment.GetOccupyingItem(EquipSlots.Secondary)));
                var widget = equipmentMenu.GetEntry(1).GetComponent<DescribableNamedWidget>();
                if (widget != null)
                    widget.FormatOccupy();
            }
        }

        public void SelectDefault()
        {
            equipmentMenu.Select(0);
            currentSlot = EquipSlots.Primary;
        }

        public void Select(EquipSlots slot)
        {
            GetEntry(slot).Select();
        }

        public void Show()
        {
            equipmentMenu.Show();
            consumableMenu.Show();

            if (panel != null)
                panel.Show();
        }

        public void Hide()
        {
            equipmentMenu.Hide();
            consumableMenu.Hide();

            if (panel != null)
                panel.Hide();
        }
        #endregion

        #region Protected
        protected MenuEntry GetEntry(EquipSlots slot)
        {
            if (slot == EquipSlots.Primary)
                return equipmentMenu.GetEntry(0);
            else if (slot == EquipSlots.Secondary)
                return equipmentMenu.GetEntry(1);
            else if (slot == EquipSlots.Body)
                return equipmentMenu.GetEntry(2);
            else if (slot == EquipSlots.Accessory)
                return equipmentMenu.GetEntry(3);
            else if (slot == EquipSlots.Consumable1)
                return consumableMenu.GetEntry(0);
            else if (slot == EquipSlots.Consumable2)
                return consumableMenu.GetEntry(1);
            else if (slot == EquipSlots.Consumable3)
                return consumableMenu.GetEntry(2);

            return null;
        }
        #endregion

        #region Private
        EquipSlots GetEquipSlot(int index)
        {
            if (index == 0)
                return EquipSlots.Primary;
            else if (index == 1)
                return EquipSlots.Secondary;
            else if(index == 2)
                return EquipSlots.Body;
            else
                return EquipSlots.Accessory;
        }

        EquipSlots GetConsumeSlot(int index)
        {
            if (index == 0)
                return EquipSlots.Consumable1;
            else if (index == 1)
                return EquipSlots.Consumable2;
            else
                return EquipSlots.Consumable3;
        }
        #endregion
    }
}
