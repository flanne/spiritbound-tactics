﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class CombatTutorialState : TutorialState<BeforeTurnState>
    {
        protected override Panel tutorialPanel { get { return owner.combatTutorial; } }
    }
}