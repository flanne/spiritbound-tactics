﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class CameraRig : MonoBehaviour
{
    public float speed = 1.0F;
    [System.NonSerialized]
    public Transform follow;
    Transform _transform;

    [SerializeField]
    PixelPerfectCamera pixelPerfectCamera;
    public Camera cam { get { return GetComponentInChildren<Camera>(); } }

    Rect boundRect = new Rect(-1 * int.MaxValue, -1 * int.MaxValue, int.MaxValue, int.MaxValue);

    public Vector3 realPos
    {
        get 
        {
            return _realPos;
        }
        set
        {
            _transform.position = value;
            _realPos = value;
        }
    }

    Vector3 _realPos;

    void Awake()
    {
        _transform = transform;
    }

    void Update()
    {
        if (follow)
        {
            Vector2 xyPos = new Vector2(realPos.x, realPos.y);
            Vector2 xyFollowPos = new Vector2(follow.position.x, follow.position.y);

            Vector2 newPos =  Vector2.MoveTowards(xyPos, xyFollowPos, speed * Time.deltaTime);
            _realPos = new Vector3(newPos.x, newPos.y, _transform.position.z);

            _transform.position = pixelPerfectCamera.RoundToPixel(_realPos);
        }
    }

    public bool ReachedFollowPosition()
    {
        Vector2 xyPos = new Vector2(_transform.position.x, _transform.position.y);
        Vector2 xyFollowPos = new Vector2(follow.position.x, follow.position.y);
        return pixelPerfectCamera.RoundToPixel(xyPos) == pixelPerfectCamera.RoundToPixel(xyFollowPos);
    }
}
