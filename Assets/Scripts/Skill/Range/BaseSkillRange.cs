﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class BaseSkillRange
{
    public virtual bool positionOriented { get { return true; } }
    public virtual bool directionOriented { get { return false; } }

    public abstract List<BoardTile> GetTilesInRange(Board board, Unit unit, int horizontal, int vertical);
}