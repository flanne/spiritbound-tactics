﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitTitleScreenState : TitleScreenState
{
    public override void Enter()
    {
        MasterSingleton.main.ScreenFilter.FadeIn(OnDoneFade);
    }

    void OnDoneFade()
    {
        owner.ChangeState<TitleMenuState>();
    }
}
