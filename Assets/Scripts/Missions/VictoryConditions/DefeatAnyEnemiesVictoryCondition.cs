﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spiritlink.Battle
{
    public class DefeatAnyEnemiesVictoryCondition : BaseVictoryCondition
    {
        protected override void CheckForVictory()
        {
            if (bc == null)
                return;

            foreach (var unit in bc.units)
                if (unit.alliance.type == enemyAlliance && IsDefeated(unit))
                    victoryAchieved = true;
        }
    }
}
