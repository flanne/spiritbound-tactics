﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public abstract class UITweener : MonoBehaviour
{
    public float duration = 0.1f;

    public abstract void Show();
    public abstract void Hide();

    public abstract void SetOff();
}
