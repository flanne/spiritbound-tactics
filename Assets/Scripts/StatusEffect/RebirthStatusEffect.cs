﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RebirthStatusEffect : StatusEffect
{
    protected override string nameID { get { return ""; } } // Deprecated Effect
    public override bool isBuff { get { return true; } }
    public override string spriteFileName { get { return ""; } }
    public override Keyword keyword { get { return null; } }

    protected override void OnApply()
    {
       this.AddObserver(OnStatus, Status.AddedNotification, owner);
    }

    protected override void OnRemove()
    {
       this.RemoveObserver(OnStatus, Status.AddedNotification, owner);
    }

    void OnStatus(object sender, object args)
    {
        // Check if unit got knocked out
        if (args is KnockedOutStatusEffect)
        {
            
        }
    }
}
