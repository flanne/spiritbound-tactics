﻿using UnityEngine;
using System.Collections;

public enum AllianceType
{
	None = 0,
	Ally = 1 << 0,
	Hero = 1 << 1,
	Villain = 1 << 2
}