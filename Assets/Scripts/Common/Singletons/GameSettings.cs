﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour
{
    public float battleSpeed
    {
        get
        {
            return PlayerPrefs.GetFloat("battleSpeed", 1f);
        }
        set
        {
            PlayerPrefs.SetFloat("battleSpeed", value);
        }
    }
}
