﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapMaterialSkillVFX : BaseSkillVisual
{ 
    [SerializeField]
    Material material;

    [SerializeField]
    float duration;

    [SerializeField]
    TargetType _targetType;
    public override TargetType targetType { get { return _targetType; } }

    List<Unit> swappedUnits = new List<Unit>();

    public override void Exit()
    {
        foreach (Unit u in swappedUnits)
            u.spriteMaterialSwapper.SetToDefault();
    }

    protected override IEnumerator Animate(BoardTile target)
    {
        if (target.content == null || target.content.GetComponent<Unit>() == null)
        {
            Debug.Log("No unit found at target tile.");
            isPlaying = false;
            yield break;
        }

        Unit unit = target.content.GetComponent<Unit>();

        unit.spriteMaterialSwapper.Set(material);
        yield return new WaitForSeconds(duration);
        unit.spriteMaterialSwapper.SetToDefault();

        isPlaying = false;
    }
}
