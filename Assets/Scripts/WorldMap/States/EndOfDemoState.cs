﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndOfDemoState : WorldMapState
{
    public override void Enter()
    {
        owner.wishListPrompt.Show();
        var canvasGroup = owner.wishListPrompt.GetComponent<CanvasGroup>();
        canvasGroup.interactable = true;
    }
}
