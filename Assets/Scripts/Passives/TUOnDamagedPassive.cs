﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TUOnDamagedPassive : Passive
{
    int tuGain = 20;

    protected override void AddListeners()
    {
        this.AddObserver(OnHealthChange, Health.HealthChangeNotification, owner.health);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnHealthChange, Health.HealthChangeNotification, owner.health);
    }

    void OnHealthChange(object sender, object args)
    {
        var info = args as Info<int, int, int, int>;
        int hpChange = info.arg1 - info.arg0;
        int shieldChange = info.arg3 - info.arg2;
        int totalChange = hpChange + shieldChange;

        if (totalChange < 0)
            owner.stats[StatTypes.CTR] -= tuGain;
    }
}
