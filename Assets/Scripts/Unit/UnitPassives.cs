﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitPassives : MonoBehaviour
{
    [SerializeField]
    Unit unit;

    public T Add<T>() where T : Passive
    {
        return gameObject.AddChildComponent<T>();
    }

    public void Remove<T>() where T : Passive
    {
        T passive = GetComponentInChildren<T>();

        if (passive != null)
        {
            passive.transform.SetParent(null);
            Destroy(passive.gameObject);
        }
    }
}
