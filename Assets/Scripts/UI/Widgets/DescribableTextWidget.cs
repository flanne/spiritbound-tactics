﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class DescribableTextWidget : DescribableWidget
    {
        [SerializeField] TMP_Text nameTMP;
        [SerializeField] TMP_Text descriptionTMP;

        public override void SetProperties(DescribableProperties properties)
        {
            if (properties.des == null)
            {
                nameTMP.text = "None";
                descriptionTMP.text = "";
            }
            else
            {
                nameTMP.text = MasterSingleton.main.GameStrings.Get(properties.des.nameStringID);
                descriptionTMP.text = properties.des.GetDescription();
            }
        }
    }
}
