﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveableItemStack
{
    public string itemGuid;
    public uint total;
    public uint inUse;
}
