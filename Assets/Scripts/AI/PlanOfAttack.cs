﻿using UnityEngine;
using System.Collections;

public class PlanOfAttack
{
    public Skill cantrip;
    public Skill skill;
    public AITargeting targeting;
    public Vector2Int moveLocation;
    public Vector2Int fireLocation;
    public Direction attackDirection;
}
