﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public class JobRank
{
    #region Serializables
    [HorizontalGroup("BasicInfo", 170)]
    [HideLabel]
    public Job job;

    [HorizontalGroup("BasicInfo")]
    [LabelWidth(30)]
    [Range(1, 5)]
    public int rank;

    [Title("Abilities")]
    [HideLabel]
    [SerializeField]
    public AbilityTree abilityTree;
    #endregion

    // Maximum rank
    public const int MaxRank = 4;

    // Amount of xp needed to level up at each rank
    public static readonly int[] XPToLevel = {100, 200, 400};

    public int XP;

    // IF no JobRank for a job is serialized, this is the default
    public JobRank(Job job)
    {
        this.job = job;
        rank = 1;
        XP = 0;
    }

    /// <summary>
    /// Add XP towards this job. Levels up if XP threshold is hit
    /// </summary>
    /// <param name="amount">Amount of xp to gain</param>
    public void GainXP(int amount)
    {
        // Cannot gain xp after max rank
        if (rank == MaxRank)
            return;

        XP += amount;

        if (XP >= XPToLevel[rank - 1])
        {
            XP -= XPToLevel[rank - 1];
            rank += 1;
        }
    }

    public bool SetAbilityTreeSelection(int rank, AbilityTreeSelection selection)
    {
        if (rank > this.rank || rank < 2)
            return false;

        if (rank == 2)
        {
            abilityTree.SetSelection(rank, selection);
            return true;
        }
        else
        {
            if (abilityTree.GetSelection(rank - 1) == AbilityTreeSelection.None)
            {
                return false;
            }
            else
            {
                abilityTree.SetSelection(rank, selection);
                return true;
            }
        }
    }
}
