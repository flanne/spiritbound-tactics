﻿using UnityEngine;
using System.Collections;

namespace Spiritlink.Battle
{
    public class DefeatTargetVictoryCondition : BaseVictoryCondition
    {
        public Unit target;

        protected override void CheckForVictory()
        {
            if (IsDefeated(target))
                victoryAchieved = true;
        }
    }
}