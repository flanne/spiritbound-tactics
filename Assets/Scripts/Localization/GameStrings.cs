﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStrings : MonoBehaviour
{
    Language languageAsset;
    Dictionary<string, string> strings;

    void Awake()
    {
        languageAsset = Resources.Load<Language>("English");
        strings = new Dictionary<string, string>();

        for (int i = 0; i < languageAsset.keys.Length; i++)
        {
            if (strings.ContainsKey(languageAsset.keys[i]))
                Debug.Log("Duplicate string key: " + languageAsset.keys[i]);
            else
                strings.Add(languageAsset.keys[i], languageAsset.strings[i]);
        }
    }

    public string Get(string stringID)
    {
        string s = "";
        try
        {
            s = strings[stringID];
        }
        catch (KeyNotFoundException)
        {
            Debug.Log("Cannot find string with the string ID: " + stringID);
        }

        return s;
    }
}
