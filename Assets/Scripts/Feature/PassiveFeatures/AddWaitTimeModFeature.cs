﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddWaitTimeModFeature : AddPassiveFeature<WaitTimeModPassive>
{
    [SerializeField]
    int amount;

    protected override void SetParameters(WaitTimeModPassive passive)
    {
        passive.amount = this.amount;
    }
}