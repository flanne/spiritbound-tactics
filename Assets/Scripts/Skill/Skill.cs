﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Skill : DescribableObject
{
    #region Serialized Fields
    [BoxGroup("Basic Info")]
    [SerializeField]
    SkillTag[] tags;

    [HorizontalGroup("Basic Info/Cost"), LabelWidth(50), LabelText("Mana")]
    public int manaCost = 0;
    [HorizontalGroup("Basic Info/Cost"), LabelWidth(50), LabelText("TU")]
    public int timeCost = 100;

    [BoxGroup("Basic Info")]
    [ToggleLeft]
    public bool isCantrip = false;

    [BoxGroup("Basic Info")]
    [ToggleLeft]
    public bool isBasic = false;

    [BoxGroup("Basic Info")]
    [ToggleLeft]
    public bool isWeaponSkill = false;

    [BoxGroup("Visuals"), LabelWidth(75)]
    [SerializeField]
    private SkillVFXPlayer skillVFX;
    [BoxGroup("Visuals"), LabelWidth(75)]
    [SerializeField]
    private float impactTime;
    [BoxGroup("Visuals")]
    [SerializeField]
    private SkillSoundFXData[] soundFX;

    [BoxGroup("Parameters")]

    [BoxGroup("Parameters/Range", false), HideLabel]
    [SerializeField]
    private SkillRangeData skillRange;

    [BoxGroup("Parameters/Area", false), HideLabel]
    [SerializeField]
    private SkillAreaData skillArea;

    [TableList]
    [BoxGroup("Parameters/Effects")]
    [GUIColor(1f, 0.75f, 0.5f)]
    [SerializeField]
    SkillEffectData[] primaryEffects;

    [TableList]
    [BoxGroup("Parameters/Effects")]
    [GUIColor(1f, 1f, 0.5f)]
    [SerializeField]
    private SkillEffectData[] secondaryEffects; // The first skill effect is used for predictions
    #endregion

    #region NonSerialized Fields
    public bool directionOriented { get { return skillRange.directionOriented; } }
    public bool positionOriented { get { return skillRange.positionOriented; } }
    public bool fireLocationOriented { get { return skillArea.fireLocationOriented; } }

    private GameObject vfxObj;
    #endregion

    #region Notifications
    public const string CanBeCantripCheck = "Skill.CanBeCantripCheck";
    public const string CanTargetCheck = "Skill.CanTargetCheck";
    public const string CanPerformCheck = "Skill.CanPerformCheck";
    public const string FailedNotification = "Skill.FailedNotification";
    public const string DidPerformNotification = "Skill.DidPerformNotification";
    public const string HitNotification = "Skill.HitNotification";
    public const string GetManaCost = "Skill.GetManaCost";
    public const string GetTimeCostNotification = "Skill.GetTimeCost";
    #endregion

    #region DecribableObject
    public override string GetDescription()
    {
        string description = "";
        if (manaCost > 0)
            description += manaCost + " <color=#5fcde4>Mana</color>, ";
        description += timeCost + " <color=#eec39a>TU</color> \n";
        description += MasterSingleton.main.GameStrings.Get(descriptionStringID);

        return description;
    }

    public string GetDescription(Unit unit)
    {
        string description = "";

        int finalManaCost = manaCost.NotifyModifiers(GetManaCost, unit);

        if (finalManaCost > 0)
        {
            if (finalManaCost == manaCost)
                description += finalManaCost + " <color=#5fcde4>Mana</color>, ";
            else
                description += " <color=#fbf236>" + finalManaCost + "</color>" + " <color=#5fcde4>Mana</color>, ";
        }

        int finalTimeCost = timeCost.NotifyModifiers<Skill>(GetTimeCostNotification, unit, this);

        description += timeCost + " <color=#eec39a>TU</color> \n";
        description += MasterSingleton.main.GameStrings.Get(descriptionStringID);

        return description;
    }
    #endregion

    #region Range/Area
    public List<BoardTile> GetTilesInRange(Board board, Unit unit)
    {
        return skillRange.GetTilesInRange(board, unit, this);
    }

    public List<BoardTile> GetTilesInArea(Board board, Vector2Int pos, Unit unit)
    {
        return skillArea.GetTilesInArea(board, pos, unit, skillRange, this);
    }
    #endregion

    #region Skill Effects
    public int GetHitRate(Unit actor, BoardTile target)
    {
        for (int i = 0; i < primaryEffects.Length; i++)
            if (primaryEffects[i].IsTarget(actor, target))
                return primaryEffects[i].GetHitRate(actor, target, this);

        return 0;
    }

    public int GetPrediction(Unit actor, BoardTile target)
    {
        for (int i = 0; i < primaryEffects.Length; i++)
            if (primaryEffects[i].IsTarget(actor, target))
                return primaryEffects[i].GetPrediction(actor, target, this);

        return 0;
    }

    public int GetPredictionClamped(Unit actor, BoardTile target)
    {
        for (int i = 0; i < primaryEffects.Length; i++)
            if (primaryEffects[i].IsTarget(actor, target))
                return primaryEffects[i].GetPredictionClamped(actor, target, this);

        return 0;
    }

    public bool IsAngleBased()
    {
        for (int i = 0; i < primaryEffects.Length; i++)
            if (primaryEffects[i].IsAngleBased())
                return true;

        return false;
    }

    public bool IsTarget(Unit actor, BoardTile target, bool checkTargeting = true)
    {
        for (int i = 0; i < primaryEffects.Length; i++)
        {
            if (primaryEffects[i].IsTarget(actor, target))
            {
                // Post exception to see if anything prevents this unit from targeting
                BaseException exc = new BaseException(true);
                if (checkTargeting)
                    this.PostNotification(CanTargetCheck, new Info<Unit, BoardTile, BaseException>(actor, target, exc));
                return exc.toggle;
            }
        }

        return false;
    }

    public bool AreAnyTargetsValid(Unit actor, List<BoardTile> targets)
    {
        foreach (var t in targets)
            if (IsTarget(actor, t))
                return true;

        return false;
    }

    public bool CanPerform(Unit actor)
    {
        // Post exception to see if any system prevents this unit from casting
        BaseException exc = new BaseException(true);
        this.PostNotification(CanPerformCheck, new Info<Unit, BaseException>(actor, exc));
        return exc.toggle;
    }

    public void Perform(Unit actor, List<BoardTile> targets, bool costTime = true)
    {
        // At TU cost to counter
        if (costTime)
        {
            int finalTimeCost = timeCost.NotifyModifiers<Skill>(GetTimeCostNotification, actor, this);
            actor.stats[StatTypes.CTR] += finalTimeCost;
        }

        bool anyEffectsHit = false;
        bool anyEffectsMiss = false;
        for (int i = 0; i < targets.Count; ++i)
        {
            // Make sure target tile has unit
            if (!IsTarget(actor, targets[i]))
                continue;

            for (int j = 0; j < primaryEffects.Length; j++)
            {
                bool isHit = primaryEffects[j].RollForHit(actor, targets[i], this);
                MasterSingleton.main.StartCoroutine(ApplyEffects(actor, targets[i], j, isHit));

                if (isHit)
                    anyEffectsHit = true;
                else
                    anyEffectsMiss = true;
            }
        }

        PlayFX(actor, targets, anyEffectsHit, anyEffectsMiss);

        this.PostNotification(DidPerformNotification, actor);
    }
    
    IEnumerator ApplyEffects(Unit actor, BoardTile target, int effectIndex, bool isHit)
    {
        // Wait until the impact time to apply skill effects which will notify the unit reaction system
        yield return new WaitForSeconds(impactTime);

        if (primaryEffects[effectIndex].Apply(actor, target, isHit, this))
        {
            var info = new Info<Unit, BoardTile>(actor, target);
            this.PostNotification(HitNotification, info);

            // Apply secondary effects if hit
            for (int j = 0; j < secondaryEffects.Length; j++)
                secondaryEffects[j].Apply(actor, target, this);
        }
    }
    #endregion 

    #region Visuals
    public void PlayFX(Unit actor, List<BoardTile> targets, bool anyEffectsHit, bool anyEffectsMiss)
    {
        // Play visual effects
        if (vfxObj == null)
            vfxObj = Instantiate(skillVFX.gameObject);

        vfxObj.GetComponent<SkillVFXPlayer>().Play(actor, targets, anyEffectsHit, this);

        // Play sound
        foreach (var s in soundFX)
        {
            if (anyEffectsHit || !s.onHitOnly)
                MasterSingleton.main.AudioPlayer.Play(s.audioClip, s.delay);
        }

        // Play miss sounds if any effects missed
        if (anyEffectsMiss)
        {
            AudioClip skillMissSFX = MasterSingleton.main.SFXLibrary.SkillMissSFX;
            MasterSingleton.main.AudioPlayer.Play(skillMissSFX, impactTime);
        }
    }

    public bool IsPlayingVFX()
    {
        return vfxObj.GetComponent<SkillVFXPlayer>().IsPlaying();
    }

    public void ExitVFX()
    {
        vfxObj.GetComponent<SkillVFXPlayer>().ExitAnimations();
    }
    #endregion

    #region Public
    public int GetTimeCost(Unit actor)
    {
        return timeCost.NotifyModifiers<Skill>(GetTimeCostNotification, actor, this);
    }

    public bool CanBeCantrip(Unit actor)
    {
        // Post exception to see if any system changes wheter or not this skill is a cantrip
        BaseException exc = new BaseException(isCantrip);
        this.PostNotification(CanBeCantripCheck, new Info<Unit, BaseException>(actor, exc));
        return exc.toggle;
    }

    public bool ContainsTag(SkillTag tag)
    {
        foreach(var t in tags)
        {
            if (t == tag)
                return true;
        }

        return false;
    }
    #endregion
}
