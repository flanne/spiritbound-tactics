﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenFlashSkillVFX : BaseSkillVisual
{
    private ScreenFilter screenFilter { get { return MasterSingleton.main.ScreenFilter; } }

    public override TargetType targetType { get { return TargetType.None; } }

    public override void Exit()
    {
    }

    protected override IEnumerator Animate(BoardTile target)
    {
        screenFilter.Flash();
        yield return null;
        isPlaying = false;
    }
}
