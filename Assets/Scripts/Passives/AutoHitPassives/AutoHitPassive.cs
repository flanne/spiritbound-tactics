﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoHitPassive : Passive
{
    public SkillTag skillTag;

    protected override void AddListeners()
    {
        this.AddObserver(OnCheckAutoHit, HitRate.AutomaticHitCheckNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnCheckAutoHit, HitRate.AutomaticHitCheckNotification);
    }

    void OnCheckAutoHit(object sender, object args)
    {
        var info = args as Info<MatchException, Skill>;
        var exc = info.arg0;
        var skill = info.arg1;

        if (owner == exc.attacker && skill.ContainsTag(skillTag))
            exc.FlipToggle();
    }
}
