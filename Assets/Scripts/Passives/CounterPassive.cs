﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CounterPassive : Passive, IEquatable<CounterPassive>, IComparable<CounterPassive>
{
    public abstract int priority { get; }
    public abstract Skill counterSkill { get; }

    protected override void AddListeners()
    {
        this.AddObserver(OnHit, BaseSkillEffect.HitNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnHit, BaseSkillEffect.HitNotification);
    }

    public abstract bool CanCounter(Unit actor, Skill skill);

    protected virtual void OnHit(object sender, object args)
    {
        var info = args as Info<Unit, Unit, int, Skill>;
        Unit actor = info.arg0;
        Unit target = info.arg1;
        Skill skill = info.arg3;

        // Do not react if target is not owner or if actor is ownr
        if (target != owner || actor == owner)
            return;

        if (CanCounter(actor, skill))
            owner.reaction.Queue(counterSkill, actor.tile, priority);
    }

    #region Sorting
    public int CompareTo(CounterPassive compareCounter)
    {
        // A null value means that this object is greater.
        if (compareCounter == null)
            return 1;

        else
            return this.priority.CompareTo(compareCounter.priority);
    }

    public bool Equals(CounterPassive other)
    {
        if (other == null) return false;
        return (this.priority.Equals(other.priority));
    }
    #endregion
}
