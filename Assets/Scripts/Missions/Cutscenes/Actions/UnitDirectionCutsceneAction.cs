﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitDirectionCutsceneAction : UnitCutsceneAction
{
    [SerializeField]
    Direction direction;

    public override void Play()
    {
        unit.direction = direction;
        isPlaying = false;
    }
}
