﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

public abstract class WorldMapState : State
{
    #region Properties
    protected WorldMapController owner;

    protected PlayerInput playerInput { get { return owner.playerInput; } }

    // Controller
    public EquipmentCompareUI equipCompareUI { get { return owner.equipCompareUI; } }
    public JobChangeController jobChangeController { get { return owner.jobChangeController; } }
    public ControllerCursor controllerCursor { get { return owner.controllerCursor; } }
    public DialogueUI dialogueBox { get { return owner.dialogueBox; } }

    // Menus
    protected UnitListMenu unitListMenu { get { return owner.unitListMenu; } }
    protected flanne.UI.Menu unitOptionsMenu { get { return owner.unitOptionsMenu; } }
    protected EquipmentMenuController equipmentMenu { get { return owner.equipmentMenu; } }
    protected ItemStackMenu equipChangeMenu { get { return owner.equipChangeMenu; } }
    protected JobOverviewController classMenu { get { return owner.classMenu; } }
    protected Menu exitMenu { get { return owner.exitMenu; } }
    protected Menu mapActionMenu { get { return owner.mapActionMenu; } }

    // Widgets
    protected DescriptionBox descriptionBox { get { return owner.descriptionBox; } }
    protected Panel jobChangeEquipPrompt { get { return owner.jobChangeEquipPrompt; } }

    protected WorldMapAvatar avatar { get { return owner.avatar; } }
    protected WorldMap map { get { return owner.map; } }
    protected Animator toBattleAnimator { get { return owner.toBattleAnimator; } }
    protected Panel toBattleTextGraphic { get { return owner.toBattleTextGraphic; } }
    protected KeybindPromptUI keybindPromptUI { get { return owner.keybindPromptUI; } }

    CameraRig cameraRig { get { return MasterSingleton.main.MainCameraRig; } }
    public AudioPlayer audioPlayer { get { return MasterSingleton.main.AudioPlayer; } }
    #endregion

    #region Monobehaviour
    protected virtual void Awake()
    {
        owner = GetComponent<WorldMapController>();
    }
    #endregion

    #region Protected
    protected bool CheckForActiveMission()
    {
        MissionLog missionLog = MasterSingleton.main.MissionLog;
        foreach (var mission in missionLog.activeMissions)
        {
            if (avatar.node.level == mission.level)
            {
                return true;
            }
        }

        return false;
    }
    #endregion
}
