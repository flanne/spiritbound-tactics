﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayableJobList : MonoBehaviour
{
    [SerializeField]
    Job[] playableJobs;

    public Job[] Get()
    {
        return playableJobs;
    }
}
