﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddPoisonOnBackstabFeature : AddPassiveFeature<PoisonOnBackstabPassive>
{
    [SerializeField]
    [Range(0, 1)]
    float chanceToHit;

    [SerializeField]
    int turnDuration;

    protected override void SetParameters(PoisonOnBackstabPassive passive)
    {
        passive.chanceToHit = this.chanceToHit;
        passive.turnDuration = this.turnDuration;
    }
}
