﻿using UnityEngine;
using System.Collections;

public static class FacingsExtension
{
	public static Facings GetFacing(this Unit attacker, Unit target)
	{
		Vector2 targetDirection = target.direction.GetNormal();
		Vector2 approachDirection = ((Vector2)(target.tile.xy - attacker.tile.xy)).normalized;
		float dot = Vector2.Dot(approachDirection, targetDirection);
		if (dot >= 0.45f)
			return Facings.Back;
		if (dot <= -0.45f)
			return Facings.Front;
		return Facings.Side;
	}
}
