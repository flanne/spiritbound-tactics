﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace flanne.UI
{
    public class DamagePreviewProperties : IUIProperties
    {
        public string hitChance;
        public int amount;

        public DamagePreviewProperties(string hitChance, int amount)
        {
            this.hitChance = hitChance;
            this.amount = amount;
        }
    }

    public class DamagePreview : Widget<DamagePreviewProperties>
    {
        [SerializeField] TMP_Text hitChanceTMP;
        [SerializeField] TMP_Text amountTMP;

        public override void SetProperties(DamagePreviewProperties properties)
        {
            string hitChance = properties.hitChance;
            int amount = properties.amount;

            hitChanceTMP.text = hitChance;
            if (hitChance != "No Counter")
                hitChanceTMP.text += "%";

            if (amount < 0)
            {
                amountTMP.text = amount + " HP";
                amountTMP.color = new Color32(205, 85, 125, 255);
            }
            else if (amount > 0)
            {
                amountTMP.text = "+" + amount + " HP";
                amountTMP.color = new Color32(90, 215, 125, 255);
            }
            else
                amountTMP.text = "";
        }
    }
}
