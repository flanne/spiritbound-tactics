﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldMapOptionsState : WorldMapState
{
    public override void Enter()
    {
        owner.optionsMenu.Show();
        owner.optionsMenu.CancelEvent += OnCancel;

        // Show keybinds
        keybindPromptUI.Hide();
    }

    public override void Exit()
    {
        owner.optionsMenu.Hide();
        owner.optionsMenu.CancelEvent -= OnCancel;

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnCancel(object sender, EventArgs e)
    {
        owner.ChangeState<TraverseWorldMapState>();
    }
}