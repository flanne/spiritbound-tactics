﻿using UnityEngine;
using System.Collections;

public enum AITargeting
{
    None,
    Self,
    Ally,
    Foe,
    Tile,
    SelfAndFoe
}