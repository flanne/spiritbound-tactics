﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using System.Collections;
using TMPro;

namespace Spiritlink.Battle
{
    public class EndRewardLootState : BattleState
    {
        #region Notifications
        public const string PostBattleNotifaction = "EndRewardLootState.PostBattleNotifaction";
        #endregion

        #region Properties
        MissionData currentMission { get { return MasterSingleton.main.MissionLog.current; } }
        Inventory inventory { get { return MasterSingleton.main.Inventory; } }

        AudioClip victoryMusic { get { return MasterSingleton.main.SFXLibrary.Get(0); } }
        AudioClip defeatMusic { get { return MasterSingleton.main.SFXLibrary.Get(1); } }
        #endregion

        #region EventHandler
        void OnAnyKey(InputAction.CallbackContext obj)
        {
            if (DidPlayerWin())
            {
                this.PostNotification(PostBattleNotifaction);
                owner.ChangeState<EndRewardJobXpState>();
            }
            else
            {
                MasterSingleton.main.SceneTransition.LoadScene("MainMenu");
                owner.ChangeState<WaitToTransitionState>();
            }

            audioPlayer.Play(owner.confirmSFX);
        }
        #endregion

        #region LifeCycle
        public override void Enter()
        {
            // Hide UI
            manaBar.Hide();
            battleSpeedButton.Hide();
            turnOrderUI.Hide();
            turnOrderUI.HidePreviewArrow();
            keybindPromptUI.Hide();

            foreach (var u in units)
                u.statusIndicator.Hide();

            // Fade music out
            audioPlayer.FadeOutMusic(1);

            StartCoroutine(TransitionAnimation());
        }

        public override void Exit()
        {
            // Remove listeners
            playerInput.currentActionMap.FindAction("AnyButton").performed -= OnAnyKey;

            lootUI.Hide();
        }
        #endregion

        #region Private
        IEnumerator TransitionAnimation()
        {
            yield return new WaitForSeconds(0.5f);
            foreach (var u in units)
                u.SetAnimationState(UnitAnimState.Still);

            yield return new WaitForSeconds(2);
            endScreenBanner.SetDidWin(DidPlayerWin());
            endScreenBanner.Show();

            if (DidPlayerWin())
            {
                audioPlayer.PlayMusic(victoryMusic);

                // Move Banner Up
                StartCoroutine(endScreenBanner.MoveBannerUp());

                // Set loot display
                yield return new WaitForSeconds(1.5f);
                lootUI.SetGold(currentMission.goldReward);
                lootUI.SetLoot(currentMission.loot);
                lootUI.Show();

                // Give player loot
                inventory.AcquireMoney(currentMission.goldReward);
                foreach (var item in currentMission.loot)
                    inventory.Add(item);
            }
            else
            {
                audioPlayer.PlayMusic(defeatMusic);
            }

            yield return new WaitForSeconds(0.5f);

            // Add listeners
            playerInput.currentActionMap.FindAction("AnyButton").performed += OnAnyKey;
        }
        #endregion
    }
}