﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Spiritlink.Battle
{
	public class PerformSkillState : BattleState
	{
		#region Notification
		public const string KillNotification = "PerformSkillState.KillNotification";
		#endregion

		#region BattleState
		public override void Enter()
		{
			base.Enter();

			if (!turn.skill.isBasic)
			{
				uiLabel.GetComponentInChildren<TMP_Text>().text = MasterSingleton.main.GameStrings.Get(turn.skill.nameStringID);
				uiLabel.Show();
			}

			turnOrderUI.Hide();
			turnOrderUI.HidePreviewArrow();

			// Set animation speed
			if (!MasterSingleton.main.MissionLog.current.disableBattleSpeed)
				Time.timeScale = MasterSingleton.main.GameSettings.battleSpeed;

			StartCoroutine(Animate());

			// Hide keybinds
			keybindPromptUI.Hide();
		}

		public override void Exit()
		{
			base.Exit();

			uiLabel.Hide();
			turnOrderUI.Show();
			turnOrderUI.ShowPreviewArrow();
			turnOrderUI.SetToUnits(turn.actor, units);

			// Reset animation speed
			Time.timeScale = 1;

			MasterSingleton.main.MainCameraRig.follow = turn.actor.transform; // reset Camera
		}
		#endregion

		#region Private
		IEnumerator Animate()
		{
			// Check if skill is to be casted as cantrip
			bool castAsCantrip;
			if (turn.skill.CanBeCantrip(turn.actor) && !turn.hasUnitCantriped)
			{
				turn.hasUnitCantriped = true;
				castAsCantrip = true;
			}
			else
			{
				turn.hasUnitActed = true;
				castAsCantrip = false;
			}

			if (turn.hasUnitMoved)
				turn.lockMove = true;

			// Clears and queued XP Gain to get ready for XP gain after performing this skill
			turn.actor.level.ClearQueuedXP();

			// Slight wait before casting skill
			yield return new WaitForSeconds(0.5f);

			// Actor performs skill
			bool shouldCostTU = !castAsCantrip || turn.skill.isCantrip;
			yield return PlaySkill(turn.skill, turn.actor, turn.targets, shouldCostTU);

			// Get all units that will react
			List<Unit> reactingUnits = new List<Unit>();
			foreach (var u in units)
			{
				if (u.reaction.skill != null && !u.IsKnockedOut())
					reactingUnits.Add(u);
			}

			// Play reactions
			foreach (var unit in reactingUnits)
			{
				// Small wait time
				yield return new WaitForSeconds(0.3f);

				List<BoardTile> targets = unit.reaction.skill.GetTilesInArea(board, unit.reaction.target.xy, unit);
				yield return PlaySkill(unit.reaction.skill, unit, targets, false);
			}

			// Clear all leftover reactions
			foreach (var u in units)
				u.reaction.Clear();

			// Small wait time
			yield return new WaitForSeconds(0.2f);

			// Gain XP for the skill
			if (!turn.actor.IsKnockedOut()) // Don't gain XP if knocked out
				if (turn.hasUnitActed) // Don't gain XP for cantrips
					if (turn.actor.alliance.type == AllianceType.Hero) // Only heroes gain xp
						turn.actor.level.GainQueuedXP();

			// Small wait time for smoother transition back into player controlled states
			yield return new WaitForSeconds(0.5f);

			// Post OnKill notifications
			foreach (var t in turn.targets)
			{
				Unit targetUnit = t?.content?.GetComponent<Unit>();
				if (targetUnit != null && targetUnit.IsKnockedOut())
					this.PostNotification(KillNotification, new Info<Unit>(turn.actor));
			}

			// Set new original tile if movement skill was used
			if (turn.skill.ContainsTag(SkillTag.Movement))
				turn.NewStartTile();

			if (IsBattleOver())
				owner.ChangeState<EndRewardLootState>();
			else if (turn.actor.IsKnockedOut())
				owner.ChangeState<SelectUnitState>();
			else if (turn.hasUnitMoved && turn.hasUnitActed)
				owner.ChangeState<EndTurnState>();
			else
				owner.ChangeState<ActionSelectState>();
		}
		#endregion
	}
}
