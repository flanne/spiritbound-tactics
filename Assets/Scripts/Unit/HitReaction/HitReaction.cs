﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitReaction : MonoBehaviour
{
    #region Properties
    public Unit unit { get { return _unit; } }
    [SerializeField]
    Unit _unit;

    [SerializeField]
    SpriteMaterialSwapper spriteMaterialSwapper;
    [SerializeField]
    Material flashMaterial;

    [SerializeField]
    float flashDuration = 0.125f;

    [SerializeField]
    float shakeSpeed = 0.2f;
    [SerializeField]
    float shakeIntensity = 0.008f;
    [SerializeField]
    float duration = 0.4f;

    float timer = 0.0f;
    bool isPlaying = false;
    #endregion

    #region Monobehaviour
    void OnEnable()
    {
        this.AddObserver(OnHealthChange, Health.HealthChangeNotification, unit.health);
        this.AddObserver(OnMiss, BaseSkillEffect.MissedNotification);

        this.AddObserver(OnLevelUp, Level.LevelUpNotification, unit);
    }

    void OnDisable()
    {
        this.RemoveObserver(OnHealthChange, Health.HealthChangeNotification, unit.health);
        this.RemoveObserver(OnMiss, BaseSkillEffect.MissedNotification);

        this.RemoveObserver(OnLevelUp, Level.LevelUpNotification, unit);
    }
    #endregion

    #region Eventhandlers
    void OnHealthChange(object sender, object args)
    {
        var info = args as Info<int, int, int, int>;
        int hpChange = info.arg1 - info.arg0;
        int shieldChange = info.arg3 - info.arg2;
        int totalChange = hpChange + shieldChange;

        if (!isPlaying && totalChange < 0)
        {
            isPlaying = true;
            StartCoroutine(HurtAnimation());
            StartCoroutine(SpriteFlash());
        }
        else if (!isPlaying && totalChange > 0)
        {
            isPlaying = true;
            StartCoroutine(JumpAnimation());
            StartCoroutine(SpriteFlash());
        }
    }

    void OnMiss(object sender, object args)
    {
        var info = args as Info<Unit, Unit>;
        Unit target = info.arg0;
        Unit actor = info.arg1;
        if (target != unit)
            return;

        Vector3 dodgeVector = new Vector3(0.2f, -0.1f, 0);
        switch(actor.direction)
        {
            case Direction.Left:
                dodgeVector = new Vector3(0.2f, -0.1f, 0);
                break;
            case Direction.Up:
                dodgeVector = new Vector3(0.2f, 0.1f, 0);
                break;
            case Direction.Right:
                dodgeVector = new Vector3(-0.2f, 0.1f, 0);
                break;
            default:
                dodgeVector = new Vector3(-0.2f, -0.1f, 0);
                break;
        }

        LeanTween.move(unit.gameObject, unit.jumper.transform.position + dodgeVector, 0.15f).setLoopPingPong(1);
    }

    void OnLevelUp(object sender, object args)
    {
        StartCoroutine(JumpAnimation());
        MasterSingleton.main.AudioPlayer.Play(MasterSingleton.main.SFXLibrary.LevelUpSound);
    }
    #endregion

    #region Private
    IEnumerator SpriteFlash()
    {
        spriteMaterialSwapper.Set(flashMaterial);

        yield return new WaitForSeconds(flashDuration);

        spriteMaterialSwapper.SetToDefault();
    }

    IEnumerator JumpAnimation()
    {
        if (unit.GetAnimationState() == UnitAnimState.Idle)
        {
            unit.SetAnimationState(UnitAnimState.Jump);
            yield return null;

            while (unit.AnimatorIsPlaying())
                yield return null;

            if (unit.GetAnimationState() == UnitAnimState.Jump) // Make sure your not reseting animation to idle if another animation took over
                unit.SetAnimationState(UnitAnimState.Idle);
        }

        isPlaying = false;

        yield return null;
    }

    IEnumerator HurtAnimation()
    {
        if (unit.GetAnimationState() == UnitAnimState.Idle)
        {
            unit.SetAnimationState(UnitAnimState.Hurt);

            Vector3 originalPos = unit.jumper.transform.position;

            timer = 0.0f;
            while (timer < duration)
            {
                Vector3 unitPos = unit.jumper.transform.position;
                unitPos.y += Mathf.PingPong(shakeSpeed * Time.time, shakeIntensity) - (shakeIntensity / 2.0f);
                unit.jumper.transform.position = unitPos;

                timer += Time.deltaTime;
                yield return null;
            }

            unit.jumper.transform.position = originalPos;
        }

        isPlaying = false;
        yield return null;

        // Check if KO'd
        if (unit.IsKnockedOut())
        {
            unit.SetAnimationState(UnitAnimState.Still);
            LeanTween.color(unit.gameObject, Color.black, 0.1f);
            LeanTween.alpha(unit.gameObject, 0, 0.3f);
            unit.magicCircle.Deactivate();
        }
        else if (unit.GetAnimationState() == UnitAnimState.Hurt) // Make sure your not reseting animation to idle if another animation took over
            unit.SetAnimationState(UnitAnimState.Idle);

        yield return null;
    }
    #endregion
}
