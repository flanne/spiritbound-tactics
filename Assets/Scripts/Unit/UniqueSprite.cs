﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniqueSprite : MonoBehaviour
{
    public Sprite portrait;
    public Sprite[] spriteSheet;

    void Awake()
    {
        GetComponentInChildren<SpriteSheetSetter>().LoadSpriteSheet(spriteSheet);
    }
}
