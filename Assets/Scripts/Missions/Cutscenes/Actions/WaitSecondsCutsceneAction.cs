﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitSecondsCutsceneAction : CutsceneAction
{
    [SerializeField]
    float seconds;

    public override void Play()
    {
        StartCoroutine(Wait());
    }

    IEnumerator Wait()
    {
        isPlaying = true;

        yield return new WaitForSeconds(seconds);

        isPlaying = false;
    }
}