﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

namespace Spiritlink.Pub
{
    public class PubController : StateMachine
    {
        #region Fields
        [SerializeField] AudioClip hubMusic;
        public AudioClip confirmSFX;
        public AudioClip selectSFX;
        public AudioClip cancelSFX;

        public PlayerInput playerInput;

        public DescribableMenu missionSelectMenu;
        public DescribableFullWidget questDescription;
        public flanne.UI.Menu confirmMenu;

        [System.NonSerialized] public MissionData selectedMission;
        #endregion

        #region LifeCycle
        void Start()
        {
            MasterSingleton.main.AudioPlayer.PlayMusic(hubMusic);
            ChangeState<MissionSelectState>();
        }

        void OnDestroy()
        {
        }
        #endregion
    }
}
