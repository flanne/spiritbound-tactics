﻿using UnityEngine;
using System.Collections;

public abstract class SkillEffectTarget 
{
    public abstract bool IsTarget(Unit actor, BoardTile target);
}