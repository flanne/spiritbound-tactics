﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class DescribableNamedWidget : DescribableWidget
    {
        [SerializeField] Image icon;
        [SerializeField] protected TMP_Text nameTMP;
        [SerializeField] Color noneTextColor;
        [SerializeField] string noneString = "None";
        [SerializeField] Color defaultColor = Color.white;

        public override void SetProperties(DescribableProperties properties)
        {
            if (properties.des == null)
            {
                if (icon != null)
                    icon.enabled = false;

                nameTMP.text = noneString;
                nameTMP.color = noneTextColor;
            }
            else
            {
                if (icon != null)
                {
                    icon.enabled = true;
                    icon.sprite = properties.des.icon;
                }

                nameTMP.text = MasterSingleton.main.GameStrings.Get(properties.des.nameStringID);
                nameTMP.color = defaultColor;
            }
        }

        public void FormatOccupy()
        {
            if (icon != null)
                icon.enabled = false;

            if (nameTMP != null)
                nameTMP.color = noneTextColor;
        }

        public void FormatTry()
        {
            if (icon != null)
                icon.enabled = true; 

            if (nameTMP != null)
                nameTMP.color = new Color32(255, 150, 150, 255);
        }
    }
}
