﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Passive : MonoBehaviour
{
    protected Unit owner;

    protected abstract void AddListeners();
    protected abstract void RemoveListeners();

    void OnEnable()
    {
        owner = GetComponentInParent<Unit>();
        AddListeners();
    }

    void OnDisable()
    {
        RemoveListeners();
    }
}
