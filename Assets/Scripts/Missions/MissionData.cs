﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Spiritlink.Battle;

public class MissionData : DescribableObject
{
    #region Serializables
    [FoldoutGroup("Mission Info")]
    [LabelWidth(100)]
    [SerializeField]
    string giverStringID;

    public bool disableBattleSpeed { get { return _disableBattleSpeed; } }
    [FoldoutGroup("Mission Info")]
    [LabelWidth(140)]
    [SerializeField] bool _disableBattleSpeed;

    public bool noLevelScaling { get { return _noLevelScaling; } }
    [FoldoutGroup("Mission Info")]
    [LabelWidth(140)]
    [SerializeField] bool _noLevelScaling;

    public bool isPatrolBattle { get { return _isPatrolBattle; } }
    [FoldoutGroup("Mission Info")]
    [LabelWidth(140)]
    [SerializeField] bool _isPatrolBattle;

    public AudioClip battleMusic { get { return _battleMusic; } }
    [FoldoutGroup("Mission Info")]
    [LabelWidth(100)]
    [SerializeField]
    AudioClip _battleMusic;

    public WorldMapLocation level { get { return _level; } }
    [FoldoutGroup("Mission Info")]
    [LabelWidth(100)]
    [SerializeField]
    WorldMapLocation _level;

    public MissionData[] preReq { get { return _preReq; } }
    [FoldoutGroup("Mission Info")]
    [SerializeField]
    MissionData[] _preReq;

    public Vector3 startCameraPos { get { return _startCameraPos; } }
    [FoldoutGroup("Mission Info")]
    [SerializeField]
    Vector3 _startCameraPos;

    public VictoryConditionData heroVictoryCondition { get { return _heroVictoryCondition; } }
    [FoldoutGroup("Mission Info")]
    [SerializeField]
    VictoryConditionData _heroVictoryCondition;

    public VictoryConditionData villainVictoryCondition { get { return _villainVictoryCondition; } }
    [FoldoutGroup("Mission Info")]
    [SerializeField]
    VictoryConditionData _villainVictoryCondition;

    public GameObject[] cutsceneSequences { get { return _cutsceneSequences; } }
    [FoldoutGroup("Mission Info")]
    [SerializeField]
    GameObject[] _cutsceneSequences;

    public GameObject fullPostCutscene { get { return _fullPostCutscene; } }
    [FoldoutGroup("Mission Info")]
    [SerializeField]
    GameObject _fullPostCutscene;

    public RewardJobXP jobXpReward { get { return _jobXpReward; } }
    [FoldoutGroup("Mission Info/Rewards")]
    [LabelWidth(100)]
    [SerializeField]
    RewardJobXP _jobXpReward;

    public uint goldReward { get { return _goldReward; } }
    [FoldoutGroup("Mission Info/Rewards")]
    [LabelWidth(100)]
    [SerializeField]
    uint _goldReward;

    public Equippable[] loot { get { return _loot; } }
    [FoldoutGroup("Mission Info/Rewards")]
    [SerializeField]
    Equippable[] _loot;

    public bool noUnitPlacement { get { return _noUnitPlacement; } }
    [FoldoutGroup("Party Placement")]
    [SerializeField] bool _noUnitPlacement = false;

    public Vector2Int[] unitPlacementTiles { get { return _unitPlacementTiles; } }
    [FoldoutGroup("Party Placement")]
    [HideIf("_noUnitPlacement")]
    [SerializeField] Vector2Int[] _unitPlacementTiles;

    public PartyUnitSpawn[] partySpawns { get { return _partySpawns; } }
    [TableList]
    [SerializeField]
    PartyUnitSpawn[] _partySpawns;

    [TableList]
    public UnitRecipe[] unitRecipes;
    #endregion

    public override string GetDescription()
    {
        string description = "";
        description += MasterSingleton.main.GameStrings.Get(descriptionStringID);
        description += "\n \n -" + MasterSingleton.main.GameStrings.Get(giverStringID);

        return description;
    }
}
