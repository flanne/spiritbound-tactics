﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class UnitInfoController : Panel
    {
        #region Fields
        public event EventHandler<InfoEventArgs<DescribableObject>> DescribableSelectEvent;

        [SerializeField] UnitBasicInfoWidget basicInfo;
        [SerializeField] EquipmentMenuController equipmentMenu;
        [SerializeField] JobOverviewController jobOverview;

        Unit _unit;
        #endregion

        #region Eventhandlers
        void OnEquipSlotSelect(object sender, InfoEventArgs<EquipSlots> e)
        {
            if (_unit != null)
            {
                if (DescribableSelectEvent != null)
                    DescribableSelectEvent(this, new InfoEventArgs<DescribableObject>(_unit.equipment.GetItem(e.info)));
            }
        }

        void OnJobOverviewSelect(object sender, InfoEventArgs<DescribableObject> e)
        {
            if (DescribableSelectEvent != null)
                DescribableSelectEvent(this, e);
        }
        #endregion

        #region LifeCycle
        void Awake()
        {
            // Add Listeners
            equipmentMenu.SlotSelectEvent += OnEquipSlotSelect;
            jobOverview.DescribableSelectEvent += OnJobOverviewSelect;
        }
        
        void OnDestroy()
        {
            // Remove Listeners
            equipmentMenu.SlotSelectEvent -= OnEquipSlotSelect;
            jobOverview.DescribableSelectEvent -= OnJobOverviewSelect;
        }
        #endregion

        #region Public
        public void SetToUnit(Unit unit)
        {
            _unit = unit;

            basicInfo.SetProperties(new UnitProperties(unit));
            equipmentMenu.SetToEquipment(unit.equipment);
            jobOverview.SetToUnit(unit);
        }

        public override void Show()
        {
            base.Show();
            equipmentMenu.Show();
            jobOverview.Show();

            // Select first equipslot
            equipmentMenu.SelectDefault();
        }

        public override void Hide()
        {
            base.Hide();
            equipmentMenu.Hide();
            jobOverview.Hide();
        }
        #endregion
    }
}