﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverhealPassive : Passive
{
    protected override void AddListeners()
    {
        this.AddObserver(OnHeal, BaseSkillEffect.WillHealNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnHeal, BaseSkillEffect.WillHealNotification);
    }

    void OnHeal(object sender, object args)
    {
        var info = args as Info<Unit, Unit, int>;
        Unit actor = info.arg0;
        if (owner != actor)
            return;

        Unit target = info.arg1;
        int amount = info.arg2;

        int toHP = target.stats[StatTypes.HP] + amount;
        int mhp = target.stats[StatTypes.MHP];

        if (toHP > mhp)
            target.health.SHP += Mathf.FloorToInt((toHP - mhp) / 2);
    }
}