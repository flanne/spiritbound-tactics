﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class ItemTutorialState : TutorialState<ConsumablesMenuState>
    {
        protected override Panel tutorialPanel { get { return owner.itemTutorial; } }
    }
}