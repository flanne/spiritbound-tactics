﻿using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.InputSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Experimental.Rendering.Universal;
using TMPro;
using Sirenix.OdinInspector;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class BattleController : StateMachine
    {
        #region Fields
        public PlayerInput playerInput;
        public BattleCameraPanner cameraPanner;

        [Header("View Model Components")]
        public CutsceneController cutsceneController;
        public ComputerPlayer cpu;

        [Header("SFX")]
        public AudioClip cancelSFX;
        public AudioClip confirmSFX;
        public AudioClip selectSFX;

        public CameraRig cameraRig { get { return MasterSingleton.main.MainCameraRig; } }
        public Board board { get { return MasterSingleton.main.GameLevel.board; } }

        [System.NonSerialized]
        public List<Unit> units = new List<Unit>();
        public Turn turn = new Turn();
        public IEnumerator round;
        #endregion

        #region UI Objects
        [BoxGroup("UI")]

        [FoldoutGroup("UI/Menus")] public flanne.UI.Menu actionMenu;
        [FoldoutGroup("UI/Menus")] public TMP_Text actionMenuLabel;
        [FoldoutGroup("UI/Menus")] public DescribableMenu skillMenu;
        [FoldoutGroup("UI/Menus")] public ConsumableSkillMenu consumablesMenu;
        [FoldoutGroup("UI/Menus")] public flanne.UI.Menu systemMenu;
        [FoldoutGroup("UI/Menus")] public flanne.UI.Menu startConfirmMenu;
        [FoldoutGroup("UI/Menus")] public flanne.UI.Menu exitConfirmMenu;

        [FoldoutGroup("UI/UIControllers")] public TileSelectIndicator tileSelectIndicator;
        [FoldoutGroup("UI/UIControllers")] public TileHighlightController tileHighlighter;
        [FoldoutGroup("UI/UIControllers")] public Panel battleSpeedButton;
        [FoldoutGroup("UI/UIControllers")] public DirectionArrowController directionArrowUI;
        [FoldoutGroup("UI/UIControllers")] public Panel directionPrompt;
        [FoldoutGroup("UI/UIControllers")] public OptionsController optionsController;
        [FoldoutGroup("UI/UIControllers")] public TurnOrderUI turnOrderUI;
        [FoldoutGroup("UI/UIControllers")] public EndScreenBanner endScreenBanner;
        [FoldoutGroup("UI/UIControllers")] public LootUIController lootUI;
        [FoldoutGroup("UI/UIControllers")] public JobXPRewardController jobXPRewardController;
        [FoldoutGroup("UI/UIControllers")] public ObjectiveUIController objectiveUI;
        [FoldoutGroup("UI/UIControllers")] public UnitInfoController unitInfoController;
        [FoldoutGroup("UI/UIControllers")] public DescriptionBox descriptionBox;
        [FoldoutGroup("UI/UIControllers")] public KeybindPromptUI keybindPromptUI;
        [FoldoutGroup("UI/UIControllers")] public Panel letterBox;
        [FoldoutGroup("UI/UIControllers")] public Panel uiLabel;

        [FoldoutGroup("UI/Widgets")] public UnitHUD primaryHUD;
        [FoldoutGroup("UI/Widgets")] public UnitHUD secondaryHUD;
        [FoldoutGroup("UI/Widgets")] public ManaBar manaBar;
        [FoldoutGroup("UI/Widgets/DamagePreview")] public Panel damagePreviewPanel;
        [FoldoutGroup("UI/Widgets/DamagePreview")] public DamagePreview damagePreviewLeft;
        [FoldoutGroup("UI/Widgets/DamagePreview")] public DamagePreview damagePreviewRight;
        [FoldoutGroup("UI/Widgets/Tutorials")] public Panel combatTutorial;
        [FoldoutGroup("UI/Widgets/Tutorials")] public Panel actionTutorial;
        [FoldoutGroup("UI/Widgets/Tutorials")] public Panel manaTutorial;
        [FoldoutGroup("UI/Widgets/Tutorials")] public Panel itemTutorial;
        [FoldoutGroup("UI/Widgets/Tutorials")] public Panel partyPlacementTutorial;
        #endregion

        #region Monobehaviour
        void Start()
        {
            // Set options menu sliders
            optionsController.SetToPrefs();

            Cursor.lockState = CursorLockMode.Confined;

            ChangeState<InitBattleState>();
        }
        #endregion
    }
}