﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace flanne.UI
{
    [RequireComponent(typeof(Image))]
    public class FlashingImage : MonoBehaviour
    {
        int _id;

        void Awake()
        {
            Image uiImage = GetComponent<Image>();
            _id = LeanTween.alpha(uiImage.rectTransform, 0.5f, 0.5f).setLoopPingPong().id;
        }

        void OnDestory()
        { 
            LeanTween.cancel(_id); 
        }
    }
}
