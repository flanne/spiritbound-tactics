﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spiritlink.Battle
{
    public class CutscenePreBattleState : CutsceneState
    {
        /*protected override void OnFire(object sender, InfoEventArgs<int> e)
        {
            if (e.info == 1 && !controlLockedOut)
            {
                //dialogueBox.Hide();
                controlLockedOut = true;

                MasterSingleton.main.ScreenFilter.FadeOut(0.5f, StopScene);
            }
        }*/

        void StopScene()
        {
            // Stop cutscene
            cutsceneController.Stop();

            MasterSingleton.main.ScreenFilter.FadeIn(1f);

            GoToNextState();
        }

        protected override void GoToNextState()
        {
            // Reset camera
            MasterSingleton.main.MainCameraRig.realPos = MasterSingleton.main.MissionLog.current.startCameraPos;

            owner.ChangeState<ShowObjectiveState>();
        }
    }
}