﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace flanne.UI
{
    public class TurnOrderUI : DynamicMenu<UnitProperties>
    {
        #region Fields
        [SerializeField] LayoutGroup currentTurnLayout;
        [SerializeField] GameObject currentTurnPrefab;
        [SerializeField] TurnOrderPreviewArrow previewArrow;

        List<Unit> _unitList;
        Unit _currentUnit;
        #endregion

        #region Public
        public void SetToUnits(Unit currentUnit, List<Unit> units)
        {
            Clear();

            _unitList = new List<Unit>(units);

            // Sort units by turn counter
            _unitList.Sort((a, b) => GetCounter(a).CompareTo(GetCounter(b)));

            // Remove current unit
            _unitList.Remove(currentUnit);

            // Remove KO'ed units
            List<Unit> toRemove = new List<Unit>();
            for (int i = _unitList.Count - 1; i >= 0; i--)
                if (_unitList[i].IsKnockedOut())
                    toRemove.Add(_unitList[i]);

            foreach (var u in toRemove)
                _unitList.Remove(u);

            // Add current turn order UI
            AddCurrentUnit(currentUnit);

            // Add rest
            for (int i = 0; i < _unitList.Count; i++)
                AddEntry(new UnitProperties(_unitList[i]));
        }

        public void SetHighlight(Unit unit, bool on)
        {
            UnHighlightAll();

            if (_currentUnit == unit)
            {
                entries[0].GetComponent<UnitTurnWidget>()?.SetHighlight(on);
            }
            else
            {
                for (int i = 0; i < _unitList.Count; i++)
                {
                    if (unit == _unitList[i])
                    {
                        entries[i+1].GetComponent<UnitTurnWidget>()?.SetHighlight(on);
                        return;
                    }
                }
            }
        }

        public void UnHighlightAll()
        {
            foreach (var entry in entries)
                entry.GetComponent<UnitTurnWidget>()?.SetHighlight(false);
        }

        public void ShowPreviewArrow(int tuAmount)
        {
            previewArrow.Show();
            previewArrow.SetTUText(tuAmount);

            for (int i = 0; i < _unitList.Count; i++)
            {
                if (GetCounter(_unitList[i]) > tuAmount)
                {
                    previewArrow.SetArrowToAboveRect(entries[i+1].GetComponent<RectTransform>());
                    return;
                }
            }

            // if tu is later than all units set to below bottom
            previewArrow.SetArrowToUnderRect(entries[entries.Count-1].transform);
        }

        public void ShowPreviewArrow()
        {
            previewArrow.Show();
        }

        public void HidePreviewArrow()
        {
            previewArrow.Hide();
        }
        #endregion

        #region Private
        int GetCounter(Unit target)
        {
            return target.stats[StatTypes.CTR];
        }

        void AddCurrentUnit(Unit currentUnit)
        {
            _currentUnit = currentUnit;

            GameObject o = Instantiate(currentTurnPrefab, currentTurnLayout.transform);
            var widget = o.GetComponent<UnitTurnWidget>();
            widget.SetProperties(new UnitProperties(_currentUnit));

            var entry = o.GetComponent<MenuEntry>();
            int closureIndex = entries.Count;
            entry.onClick.AddListener(() => OnEntryClicked(0));
            entry.onSelect.AddListener(() => OnEntrySelected(0));

            entries.Insert(0, entry);
        }
        #endregion
    }
}
