﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

namespace Spiritlink.Battle
{
    public abstract class TutorialState<T> : BattleState where T : BattleState
    {
        protected abstract Panel tutorialPanel { get; }

        public override void Enter()
        {
            tutorialPanel.Show();

            StartCoroutine(WaitLockout());
        }

        public override void Exit()
        {
            tutorialPanel.Hide();
        }

        void OnAnyKey(InputAction.CallbackContext obj)
        {
            // Remove listeners
            playerInput.currentActionMap.FindAction("AnyButton").canceled -= OnAnyKey;

            owner.ChangeState<T>();
        }

        IEnumerator WaitLockout()
        {
            yield return new WaitForSeconds(1f);

            // Remove listeners
            playerInput.currentActionMap.FindAction("AnyButton").canceled += OnAnyKey;
        }
    }
}