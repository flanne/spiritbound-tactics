﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaNode : MonoBehaviour
{
    [SerializeField]
    ParticleSystem useUpManaParticleEffect;

    public Image image { get { return imageNode; } }
    [SerializeField]
    Image imageNode;

    void Start()
    {
        LeanTween.alpha(this.gameObject, 0.6f, 1f).setLoopPingPong().setEase(LeanTweenType.easeInOutQuad);
    }

    public bool UseUp()
    {
        if (imageNode.enabled != false)
        {
            imageNode.enabled = false;
            useUpManaParticleEffect.Play();
            return true;
        }

        return false;
    }

    public bool FillUp()
    {
        if (imageNode.enabled != true)
        {
            imageNode.enabled = true;
            return true;
        }

        return false;
    }
}
