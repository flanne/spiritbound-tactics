﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumableCatalog : MonoBehaviour
{
	public const string GetMaxChargeNotification = "ConsumableCatalog.GetMaxChargeNotification";

	public int maxChargeMod
    {
        get
        {
			return _maxChargeMod;
		}
        set 
		{
			_maxChargeMod = value;
			foreach (var c in myConsumables)
				c.ModifyMaxCharge(value);
		}
    }
	int _maxChargeMod;

	[SerializeField]
	Unit owner;

	List<MyConsumable> myConsumables = new List<MyConsumable>();

	public List<MyConsumable> GetConsumables()
    {
		return myConsumables;
	}

	public Skill GetSkill(int consumableIndex)
    {
		if (consumableIndex < myConsumables.Count)
			return myConsumables[consumableIndex].consumableSkill;
		else
			return null;
	}

	public void AddConsumable(Consumable consumable, int maxCharge)
	{
		int finalMaxCharge = maxCharge + maxChargeMod;
		myConsumables.Add(new MyConsumable(consumable, finalMaxCharge));
	}

	public void RemoveConsumable(Consumable consumable)
	{
		List<MyConsumable> toRemove = new List<MyConsumable>();
		foreach (var c in myConsumables)
			if (c.consumableSkill == consumable.skill)
				toRemove.Add(c);

		foreach (var r in toRemove)
			myConsumables.Remove(r);
	}

	public void Use(int consumableIndex)
    {
		myConsumables[consumableIndex].Use();
	}

	public void Reset()
    {
		foreach (var c in myConsumables)
			c.Fill();
    }
}
