﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spiritlink.Battle;

public class DoomStatusEffect : StatusEffect
{
    protected override string nameID { get { return "KEYWORD_DOOM"; } }
    public override bool isBuff { get { return false; } }
    public override string spriteFileName { get { return "icon_sts_doom"; } }
    public override Keyword keyword { get { return Resources.Load<Keyword>("Doom"); } }

    Skill doomCount2 { get { return Resources.Load<Skill>("sk_doom_count_2"); } }
    Skill doomCount1 { get { return Resources.Load<Skill>("sk_doom_count_1"); } }
    Skill doomTrigger { get { return Resources.Load<Skill>("sk_doom_trigger"); } }

    int turnCounter = 0;

    protected override void OnApply()
    {
        this.AddObserver(OnNewTurn, BeforeTurnState.StartOfTurnEffectsNotification, owner);
    }

    protected override void OnRemove()
    {
        this.RemoveObserver(OnNewTurn, BeforeTurnState.StartOfTurnEffectsNotification, owner);
    }

    void OnNewTurn(object sender, object args)
    {
        Skill skill;
        if (turnCounter == 0)
            skill = doomCount2;
        else if (turnCounter == 1)
            skill = doomCount1;
        else
            skill = doomTrigger;

        var effects = args as List<SkillDirections>;
        effects.Add(new SkillDirections(skill, owner, owner.tile));

        if (turnCounter >= 2)
            owner.status.Remove(this);

        turnCounter++;
    }
}