﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace flanne.UI
{
    public class URLButton : Panel
    {
        [SerializeField] string url;

        public void OpenURL()
        {
            Application.OpenURL(url);
        }
    }
}
