﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Spiritlink.Battle
{
    public class PlacementConfirmState : BattleState
    {
        #region Eventhandlers
        void OnClick(object sender, InfoEventArgs<int> e)
        {
            if (e.info == 0)
                owner.ChangeState<SelectUnitState>();
            else
                owner.ChangeState<UnitPlacementState>();

            audioPlayer.Play(owner.confirmSFX);
        }

        void OnSelect(object sender, InfoEventArgs<int> e)
        {
            audioPlayer.Play(owner.selectSFX);
        }

        void OnCancel(InputAction.CallbackContext obj)
        {
            owner.ChangeState<UnitPlacementState>();

            audioPlayer.Play(owner.cancelSFX);
        }
        #endregion

        #region LifeCycle
        public override void Enter()
        {
            startConfirmMenu.Show();
            startConfirmMenu.Select(1);

            // AddListener
            startConfirmMenu.ClickEvent += OnClick;
            startConfirmMenu.SelectEvent += OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed += OnCancel;
        }

        public override void Exit()
        {
            startConfirmMenu.Hide();

            // Remove Listeners
            startConfirmMenu.ClickEvent -= OnClick;
            startConfirmMenu.SelectEvent -= OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed -= OnCancel;
        }
        #endregion
    }
}
