﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

public class BuySellMenuState : ShopState
{
    #region Eventhandlers
    void OnClick(object sender, InfoEventArgs<Equippable> e)
    {
        confirmUI.selectedItem = e.info;

        if (buySellMenu.isSelling && !MasterSingleton.main.Inventory.IsAvailable(confirmUI.selectedItem))
            return;

        confirmUI.isSelling = buySellMenu.isSelling;
        owner.ChangeState<ShopConfirmState>();

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnSelect(object sender, InfoEventArgs<Equippable> e)
    {
        if (e.info != null)
        {
            descriptionBox.SetToDescribable(e.info, DescriptionBox.Side.Right);
            descriptionBox.Show();
        }
        else
        {
            descriptionBox.Hide();
        }

        audioPlayer.Play(owner.selectSFX);
    }

    void OnCancel(InputAction.CallbackContext obj)
    {
        buySellMenu.Hide();
        descriptionBox.Hide();
        owner.ChangeState<ShopMenuState>();

        audioPlayer.Play(owner.cancelSFX);
    }
    #endregion

    #region LifeCycle
    public override void Enter()
    {
        buySellMenu.Show();
        buySellMenu.SelectFirstAvailable();

        buySellMenu.ClickEvent += OnClick;
        buySellMenu.SelectEvent += OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled += OnCancel;

        if (buySellMenu.isSelling)
        {
            List<Equippable> allItemsList = new List<Equippable>();
            foreach (var stack in MasterSingleton.main.Inventory.itemList)
                allItemsList.Add(stack.item);

            buySellMenu.SetToItems(allItemsList.ToArray());
        }
        else
        {
            buySellMenu.SetToItems(shopItems);
        }
    }

    public override void Exit()
    {
        buySellMenu.Hide();

        buySellMenu.ClickEvent -= OnClick;
        buySellMenu.SelectEvent -= OnSelect;
        playerInput.currentActionMap.FindAction("Cancel").canceled -= OnCancel;
    }
    #endregion
}
