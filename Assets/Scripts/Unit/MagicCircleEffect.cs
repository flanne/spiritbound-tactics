﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class MagicCircleEffect : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer magicCircleSprite;
    [SerializeField]
    SpriteRenderer shadow;

    public void Activate()
    {
        StartCoroutine(FadeIn());
    }

    public void Deactivate()
    {
        StartCoroutine(FadeOut());
    }

    IEnumerator FadeIn()
    {
        magicCircleSprite.enabled = true;
        shadow.enabled = false;

        Color c = magicCircleSprite.color;
        float fadeTime = 1f;
        float timer = 0;
        while (timer < fadeTime)
        {
            c.a = (timer / fadeTime) * 1f;
            magicCircleSprite.color = c;

            timer += Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator FadeOut()
    {
        Color c = magicCircleSprite.color;
        float fadeTime = 0.5f;
        float timer = 0;
        while (timer < fadeTime)
        {
            c.a = 1f - ((timer / fadeTime) * 1f);
            magicCircleSprite.color = c;

            timer += Time.deltaTime;
            yield return null;
        }

        magicCircleSprite.enabled = false;
        shadow.enabled = true;
    }
}
