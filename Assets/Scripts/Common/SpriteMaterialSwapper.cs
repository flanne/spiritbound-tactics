﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteMaterialSwapper : MonoBehaviour
{
    [SerializeField]
    Material newMaterial;

    Material defaultMaterial;
    SpriteRenderer spriteRenderer;
    
    void OnEnable()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        defaultMaterial = spriteRenderer.material;
    }

    public void Set(Material material)
    {
        spriteRenderer.material = material;
    }

    public void SetToNew()
    {
        spriteRenderer.material = newMaterial;
    }

    public void SetToDefault()
    {
        spriteRenderer.material = defaultMaterial;
    }
}
