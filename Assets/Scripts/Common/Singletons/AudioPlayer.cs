﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
	// Audio players components.
	public AudioSource effectsSource;
	public AudioSource dialogueSource;
	public AudioSource musicSource;

	public AudioClip currentMusic { get; private set; }

	public float EffectsVolume
    {
		get { return _effectsVolume; }
		set
        {
			_effectsVolume = value;
			effectsSource.volume = _effectsVolume;
			dialogueSource.volume = _effectsVolume;
		}
    }
	float _effectsVolume;

	public float MusicVolume
    {
		get { return _musicVolume; }
		set
		{
			_musicVolume = value;
			musicSource.volume = _musicVolume;
		}
	}
	float _musicVolume;

	IEnumerator fadeInCoroutine;
	IEnumerator fadeOutCoroutine;

	public void Awake()
    {
		_musicVolume = musicSource.volume;
		_effectsVolume = effectsSource.volume;
	}

	// Play a single clip through the sound effects source.
	public void Play(AudioClip clip)
	{
		effectsSource.PlayOneShot(clip);
	}

	public void Play(AudioClip clip, float delay)
    {
		StartCoroutine(PlayAfterDelay(clip, delay));
	}

	public void PlayDialogue()
    {
		dialogueSource.Play();
	}

	public void StopDialogue()
	{
		dialogueSource.Stop();
	}

	IEnumerator PlayAfterDelay(AudioClip clip, float delay)
    {
		yield return new WaitForSeconds(delay);
		Play(clip);
    }

	// Play a single clip through the music source.
	public void PlayMusic(AudioClip clip, float fadeTime = 0.5f)
	{
		currentMusic = clip;
		fadeInCoroutine = FadeIn(fadeTime);
		StartCoroutine(fadeInCoroutine);
		musicSource.loop = true;
		musicSource.clip = clip;
		musicSource.Play();
	}

	public void FadeOutMusic(float fadeTime)
    {
		fadeOutCoroutine = FadeOut(fadeTime);
		StartCoroutine(fadeOutCoroutine);
		currentMusic = null;

	}

	IEnumerator FadeOut(float fadeTime)
	{
		if (fadeInCoroutine != null)
			StopCoroutine(fadeInCoroutine);

		while (musicSource.volume > 0)
		{
			musicSource.volume -= _musicVolume * Time.deltaTime / fadeTime;

			yield return null;
		}

		musicSource.Stop();
		musicSource.volume = _musicVolume;
	}

	IEnumerator FadeIn(float fadeTime)
	{
		if (fadeOutCoroutine != null)
			StopCoroutine(fadeOutCoroutine);

		musicSource.volume = 0;

		while (musicSource.volume < _musicVolume)
		{
			musicSource.volume += _musicVolume * Time.deltaTime / fadeTime;

			yield return null;
		}
	}
}