﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spiritlink.Battle
{
    public class SelectUnitState : BattleState
    {
        public const string TurnWillStartNotification = "SelectUnitState.TurnWillStartNotification";

        public override void Enter()
        {
            base.Enter();
            StartCoroutine("ChangeCurrentUnit");
        }

        public override void Exit()
        {
            base.Exit();
        }

        IEnumerator ChangeCurrentUnit()
        {
            owner.round.MoveNext();
            turnOrderUI.Show();
            turnOrderUI.SetToUnits(turn.actor, units);
            turnOrderUI.HidePreviewArrow();

            turn.actor.PostNotification(TurnWillStartNotification);

            yield return null;

            // Check for Cutscenes
            if (cutsceneController.CutsceneQueued())
            {
                owner.ChangeState<CutsceneMidBattleState>();
            }
            // Check for tutorials
            else if (MasterSingleton.main.TutorialShown.combat == false && turn.actor.alliance.type == AllianceType.Hero)
            {
                MasterSingleton.main.MainCameraRig.follow = turn.actor.transform;
                owner.ChangeState<CombatTutorialState>();
                MasterSingleton.main.TutorialShown.combat = true;
            }
            else // if no tutorial start turn
            {
                owner.ChangeState<BeforeTurnState>();
            }
        }
    }
}
