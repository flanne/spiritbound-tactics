﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowCutsceneAction : UnitCutsceneAction
{
    public override void Play()
    {
        StartCoroutine(CameraFollow());
    }

    IEnumerator CameraFollow()
    {
        isPlaying = true;

        CameraRig cameraRig = GameObject.FindWithTag("CameraRig").GetComponent<CameraRig>();
        cameraRig.follow = unit.transform;

        while (!cameraRig.ReachedFollowPosition())
            yield return null;

        isPlaying = false;
    }
}
