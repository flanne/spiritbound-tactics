﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddDoomOnDebuffFeature : AddPassiveFeature<DoomOnDebuffsPassive>
{
    [SerializeField]
    int threshold;

    protected override void SetParameters(DoomOnDebuffsPassive passive)
    {
        passive.threshold = this.threshold;
    }
}
