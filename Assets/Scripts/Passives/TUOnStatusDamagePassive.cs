﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TUOnStatusDamagePassive : Passive
{
    int tuGain = 10;

    protected override void AddListeners()
    {
        this.AddObserver(OnDealingDamage, DamageSkillEffect.TweakDamageNotification);
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnDealingDamage, DamageSkillEffect.TweakDamageNotification);
    }

    void OnDealingDamage(object sender, object args)
    {
        var info = args as Info<List<ValueModifier>, Info<Unit, Unit, Skill>>;
        Unit defender = info.arg1.arg1;
        Skill skill = info.arg1.arg2;

        if (defender.alliance.type != owner.alliance.type && skill.ContainsTag(SkillTag.Status))
            owner.stats[StatTypes.CTR] -= tuGain;
    }
}
