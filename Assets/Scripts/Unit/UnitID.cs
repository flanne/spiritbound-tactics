﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ID for characters used in cutscenes
public class UnitID : MonoBehaviour
{
    public IDEnum id;

    public static Unit GetUnitWithID(IDEnum id)
    {
        if (id == IDEnum.None)
        {
            Debug.Log("Unit id in cutsceene action is set to 'None' ");
            return null;
        }

        UnitID[] uids = GameObject.FindObjectsOfType<UnitID>();
        foreach (var u in uids)
        {
            if (u.id == id)
            {
                return u.GetComponent<Unit>();
            }
        }

        Debug.Log("Could not find unit with ID: " + id);
        return null;
    }
}
