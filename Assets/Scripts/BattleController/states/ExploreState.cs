﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class ExploreState : BattleState
    {
        #region Eventhandlers
        void OnClick(object sender, InfoEventArgs<BoardTile> e)
        {
            if (e.info.content != null)
            {
                Unit unit = currentTile.content.GetComponent<Unit>();

                if (unit != null)
                {
                    owner.ChangeState<UnitMenuState>();
                    keybindPromptUI.Hide();
                }

                audioPlayer.Play(owner.confirmSFX);
            }
        }

        void OnSelect(object sender, InfoEventArgs<BoardTile> e)
        {
            if (e.info.content != null)
            {
                tileHighlighter.Clear();
                Unit unit = e.info.content.GetComponent<Unit>();

                if (unit != null)
                {
                    primaryHUD.SetProperties(new UnitProperties(unit));
                    primaryHUD.Show();
                    tileHighlighter.Highlight(unit.movement.GetTilesInRange(board), new Color(0.2f, 1f, 0.6f, 1f));
                    return;
                }
            }

            primaryHUD.Hide();
            tileHighlighter.Clear();
        }

        void OnSecondary(InputAction.CallbackContext obj)
        {
            owner.ChangeState<SystemMenuState>();

            keybindPromptUI.Hide();

            audioPlayer.Play(owner.confirmSFX);
        }

        void OnCancel(InputAction.CallbackContext obj)
        {
            owner.ChangeState<ActionSelectState>();
            MasterSingleton.main.MainCameraRig.follow = turn.actor.transform;

            audioPlayer.Play(owner.cancelSFX);
        }
        #endregion

        #region LifeCycle
        protected override void OnEnterUI()
        {
            MasterSingleton.main.MainCameraRig.follow = null;
            cameraPanner.active = true;

            tileSelectIndicator.ClickEvent += OnClick;
            tileSelectIndicator.SelectEvent += OnSelect;
            playerInput.currentActionMap.FindAction("Secondary").performed += OnSecondary;
            playerInput.currentActionMap.FindAction("Cancel").performed += OnCancel;

            tileSelectIndicator.Show();

            // Show keybinds
            keybindPromptUI.Show();
            keybindPromptUI.SetText(0, "Unit Info");
            keybindPromptUI.SetText(1, "Go Back");
            keybindPromptUI.SetText(2, "Menu");
        }

        protected override void OnExitUI()
        {
            cameraPanner.active = false;

            // Remove Listeners
            tileSelectIndicator.ClickEvent -= OnClick;
            tileSelectIndicator.SelectEvent -= OnSelect;
            playerInput.currentActionMap.FindAction("Secondary").performed -= OnSecondary;
            playerInput.currentActionMap.FindAction("Cancel").performed -= OnCancel;

            tileSelectIndicator.Hide();
            primaryHUD.Hide();

            tileHighlighter.Clear();

            keybindPromptUI.HidePrompt(2);
        }
        #endregion
    }
}