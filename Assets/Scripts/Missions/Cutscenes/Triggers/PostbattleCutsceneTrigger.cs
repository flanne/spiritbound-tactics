﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spiritlink.Battle
{
    public class PostbattleCutsceneTrigger : CutsceneTrigger
    {
        void Start()
        {
            this.AddObserver(OnPostBattle, EndRewardLootState.PostBattleNotifaction);
        }

        void OnDestroy()
        {
            this.RemoveObserver(OnPostBattle, EndRewardLootState.PostBattleNotifaction);
        }

        void OnPostBattle(object sender, object args)
        {
            Trigger();
            this.RemoveObserver(OnPostBattle, EndRewardLootState.PostBattleNotifaction);
        }
    }
}
