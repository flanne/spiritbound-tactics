﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneTrigger : MonoBehaviour
{
    protected virtual void Trigger()
    {
        CutsceneController controller = GetComponentInParent<CutsceneController>();
        CutsceneSequence cutscene = GetComponent<CutsceneSequence>();
        if (controller)
            controller.TriggerCutscene(cutscene);
    }
}
