﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using flanne.UI;

public abstract class TitleScreenState : State
{
    #region Fields
    protected TitleScreenController owner;
    protected flanne.UI.Menu mainMenu { get { return owner.mainMenu; } }
    protected OptionsController optionsMenu { get { return owner.optionsMenu; } }
    protected flanne.UI.Menu newGameWarningUI { get { return owner.newGameWarningUI; } }

    public AudioPlayer audioPlayer { get { return MasterSingleton.main.AudioPlayer; } }
    #endregion

    #region Monobehaviour
    protected virtual void Awake()
    {
        owner = GetComponent<TitleScreenController>();
    }
    #endregion
}
