﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffHand : Equippable
{
    public override EquipSlots defaultSlots { get { return EquipSlots.Secondary; } }
}
