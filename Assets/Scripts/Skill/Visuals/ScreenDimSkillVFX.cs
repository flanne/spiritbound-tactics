﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class ScreenDimSkillVFX : BaseSkillVisual
{
    private const float speed = 8.0f;
    private const float targetIntensity = 0.5f;

    Light2D globalLight { get { return GameObject.FindWithTag("GlobalLight").GetComponent<Light2D>(); } }
    float originalIntensity;

    public override TargetType targetType { get { return TargetType.None; } }

    public override void Exit()
    {
        StartCoroutine("Undim");
    }

    protected override IEnumerator Animate(BoardTile target)
    {
        originalIntensity = globalLight.intensity;

        while ( globalLight.intensity > targetIntensity )
        {
            globalLight.intensity = Mathf.Lerp(globalLight.intensity, targetIntensity - 0.01f, speed * Time.deltaTime);
            yield return null;
        }

        isPlaying = false;
    }

    IEnumerator Undim()
    {
        while ( globalLight.intensity < originalIntensity && !isPlaying)
        {
            globalLight.intensity = Mathf.Lerp(globalLight.intensity, originalIntensity + 0.01f, speed * Time.deltaTime);
            yield return null;
        }
    }
}
