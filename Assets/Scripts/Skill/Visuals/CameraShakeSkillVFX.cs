﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakeSkillVFX : BaseSkillVisual
{
    public float duration;
    public float magnitude;

    public override TargetType targetType { get { return TargetType.None; } }

    public override void Exit()
    {
    }

    protected override IEnumerator Animate(BoardTile target)
    {
        Vector3 originalPos = Camera.main.transform.localPosition;

        float elapsed = 0f;

        while (elapsed < duration)
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            Camera.main.transform.localPosition = new Vector3(originalPos.x + x, originalPos.y+y, originalPos.z);

            elapsed += Time.deltaTime;

            yield return null;
        }

        Camera.main.transform.localPosition = originalPos;
        isPlaying = false;
    }
}
