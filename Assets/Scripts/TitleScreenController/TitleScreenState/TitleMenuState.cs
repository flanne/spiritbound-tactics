﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleMenuState : TitleScreenState
{
    void OnClick(object sender, InfoEventArgs<int> e)
    {
        if (e.info == 0) // Continue
        {
            owner.ChangeState<LoadGameState>();
        }
        else if (e.info == 1) // New game
        {
            if (MasterSingleton.main.SaveSystem.SaveSlotExists(0))
                owner.ChangeState<NewGameWarningState>();
            else
                owner.ChangeState<InitNewGameState>();
        }
        else if (e.info == 2) // Options
        {
            owner.ChangeState<OptionMenuTitleState>();
        }
        else if (e.info == 3) // Exit game
        {
            Application.Quit();
        }

        mainMenu.Hide();

        audioPlayer.Play(owner.confirmSFX);
    }

    void OnSelect(object sender, InfoEventArgs<int> e)
    {
        audioPlayer.Play(owner.selectSFX);
    }

    public override void Enter()
    {
        if (!MasterSingleton.main.SaveSystem.SaveSlotExists(0))
            mainMenu.Lock(0);

        mainMenu.Show();
        mainMenu.SelectFirstAvailable();

        owner.surveyButton.Show();
        owner.steamButton.Show();

        mainMenu.ClickEvent += OnClick;
        mainMenu.SelectEvent += OnSelect;
    }

    public override void Exit()
    {
        mainMenu.ClickEvent -= OnClick;
        mainMenu.SelectEvent -= OnSelect;
    }
}
