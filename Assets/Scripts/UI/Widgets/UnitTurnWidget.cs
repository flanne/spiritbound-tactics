﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace flanne.UI
{
    public class UnitTurnWidget : UnitWidget
    {
        [SerializeField] Image portrait;
        [SerializeField] Image bg;
        [SerializeField] Image highlight;
        [SerializeField] TMP_Text timeDisplay;

        public override void SetProperties(UnitProperties properties)
        {
            Unit unit = properties.unit;
            if (unit.uniqueSprite != null)
                portrait.sprite = unit.uniqueSprite.portrait;
            else
                portrait.sprite = unit.jobs.primary.portrait;

            timeDisplay.text = unit.stats[StatTypes.CTR].ToString();

            if (unit.alliance.type == AllianceType.Hero)
                bg.color = new Color32(95, 205, 228, 100);
            else
                bg.color = new Color32(172, 50, 50, 100);

            highlight.enabled = false;
        }

        public void SetHighlight(bool on)
        {
            highlight.enabled = on;
        }
    }
}
