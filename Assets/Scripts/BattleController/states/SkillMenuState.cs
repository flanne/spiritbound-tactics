﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using flanne.UI;

namespace Spiritlink.Battle
{
    public class SkillSelectState : BattleState
    {
        #region Properties
        List<Skill> skills;
        #endregion

        #region Eventhandlers
        void OnClick(object sender, InfoEventArgs<int> e)
        {
            turn.skill = skills[e.info];
            owner.ChangeState<SkillTargetState>();

            audioPlayer.Play(owner.confirmSFX);
        }

        void OnSelect(object sender, InfoEventArgs<int> e)
        {
            manaBar.Preview(skills[e.info].manaCost);
            descriptionBox.SetToDescribable(skills[e.info]);
            descriptionBox.SetDescriptionOverride(skills[e.info].GetDescription(turn.actor)); // Manually set description to see mana/tu mods

            // Set mana preview
            manaBar.Preview(skills[e.info].manaCost);

            audioPlayer.Play(owner.selectSFX);
        }

        void OnCancel(InputAction.CallbackContext obj)
        {
            owner.ChangeState<ActionSelectState>();

            audioPlayer.Play(owner.cancelSFX);
        }
        #endregion

        #region LifeCycle
        public override void Enter()
        {
            skills = turn.actor.skillCatalog.GetSkills();
            base.Enter();
        }

        protected override void OnEnterUI()
        {
            // Add listeners
            skillMenu.ClickEvent += OnClick;
            skillMenu.SelectEvent += OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed += OnCancel;

            // Set SkillMenu
            skillMenu.Clear();

            for (int i = 0; i < skills.Count; i++)
            {
                skillMenu.AddEntry(new DescribableProperties(skills[i]));

                if (turn.hasUnitCantriped && skills[i].isCantrip)
                    skillMenu.Lock(i);

                if (!skills[i].CanPerform(turn.actor))
                    skillMenu.Lock(i);
            }

            // Select
            skillMenu.SelectFirstAvailable();
            skillMenu.Show();
            descriptionBox.Show();

            // Show keybinds
            keybindPromptUI.Show();
            keybindPromptUI.SetText(0, "Select");
            keybindPromptUI.SetText(1, "Cancel");
        }

        protected override void OnExitUI()
        {
            // Remove listeners
            skillMenu.ClickEvent -= OnClick;
            skillMenu.SelectEvent -= OnSelect;
            playerInput.currentActionMap.FindAction("Cancel").performed -= OnCancel;

            skillMenu.Clear();
            skillMenu.Hide();

            descriptionBox.Hide();

            manaBar.ClearPreview();
        }
        #endregion
    }
}