﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Spiritlink.Battle
{
    public class ExitWarningState : BattleState
    {
        void OnClick(object sender, InfoEventArgs<int> e)
        {

            if (e.info == 0)
            {
                HideAllUI();

                MasterSingleton.main.AudioPlayer.FadeOutMusic(1f);
                MasterSingleton.main.SceneTransition.LoadScene("MainMenu");

                audioPlayer.Play(owner.confirmSFX);
            }
            else
            {
                owner.ChangeState<ExploreState>();

                audioPlayer.Play(owner.cancelSFX);
            }
        }

        void OnCancel(InputAction.CallbackContext obj)
        {
            owner.ChangeState<ExploreState>();

            audioPlayer.Play(owner.cancelSFX);
        }

        public override void Enter()
        {
            exitConfirmMenu.Show();
            exitConfirmMenu.Select(1);

            // Add Listeners
            exitConfirmMenu.ClickEvent += OnClick;
            playerInput.currentActionMap.FindAction("Cancel").performed += OnCancel;
        }

        public override void Exit()
        {
            exitConfirmMenu.Hide();

            // Remove Listeners
            exitConfirmMenu.ClickEvent -= OnClick;
            playerInput.currentActionMap.FindAction("Cancel").performed -= OnCancel;
        }

        void HideAllUI()
        {
            exitConfirmMenu.Hide();

            battleSpeedButton.Hide();
            turnOrderUI.Hide();
            turnOrderUI.HidePreviewArrow();
            manaBar.Hide();

            // Remove Listeners
            exitConfirmMenu.ClickEvent -= OnClick;
            playerInput.currentActionMap.FindAction("Cancel").performed -= OnCancel;
        }
    }
}
